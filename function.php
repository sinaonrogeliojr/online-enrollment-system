<?php 
include_once("config.php");
include_once("classes.php");
$action = mysqli_real_escape_string($con,$_POST['action']);

switch ($action) {

	case 'check_email':
		$email = mysqli_real_escape_string($con,$_POST['email']);
		check_email($con,$email);
	break;

	case 'login':
		$uname = mysqli_real_escape_string($con,$_POST['uname']);
		$pwd = mysqli_real_escape_string($con,$_POST['pwd']);
		login($con,$uname,$pwd);
	break;
	
	case 'load_school_fees':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id']);
		load_school_fees($con,$grade_id);
	break;

	case 'load_terms_of_payment':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id']);
		$term_id = mysqli_real_escape_string($con,$_POST['term_id']);
		load_terms_of_payment($con,$term_id,$grade_id);
	break;
	
	case 'auto_login':
		# code...
		$email = mysqli_real_escape_string($con,$_POST['email']);
		$p_pass = mysqli_real_escape_string($con,$_POST['p_pass']);
		auto_login($con,$email,$p_pass);
		
	break;

	case 'save_registration':

		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id']);
		$lname = mysqli_real_escape_string($con,$_POST['lname']);
		$fname = mysqli_real_escape_string($con,$_POST['fname']);
		$mname = mysqli_real_escape_string($con,$_POST['mname']);
		$esc_qvr_number = mysqli_real_escape_string($con,$_POST['esc_qvr_number']);
		
		$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
		$place_of_birth = mysqli_real_escape_string($con,$_POST['place_of_birth']);
		$gender = mysqli_real_escape_string($con,$_POST['gender']);
		$religion = mysqli_real_escape_string($con,$_POST['religion']);
		$citizenship = mysqli_real_escape_string($con,$_POST['citizenship']);
		$contact = mysqli_real_escape_string($con,$_POST['contact']);
		$address = mysqli_real_escape_string($con,$_POST['address']);

		$lname_f = mysqli_real_escape_string($con,$_POST['lname_f']);
		$fname_f = mysqli_real_escape_string($con,$_POST['fname_f']);
		$mname_f = mysqli_real_escape_string($con,$_POST['mname_f']);
		$contact_f = mysqli_real_escape_string($con,$_POST['contact_f']);
		$occupation_f = mysqli_real_escape_string($con,$_POST['occupation_f']);

		$lname_m = mysqli_real_escape_string($con,$_POST['lname_m']);
		$fname_m = mysqli_real_escape_string($con,$_POST['fname_m']);
		$mname_m = mysqli_real_escape_string($con,$_POST['mname_m']);
		$contact_m = mysqli_real_escape_string($con,$_POST['contact_m']);
		$occupation_m = mysqli_real_escape_string($con,$_POST['occupation_m']);

		$lname_g = mysqli_real_escape_string($con,$_POST['lname_g']);
		$fname_g = mysqli_real_escape_string($con,$_POST['fname_g']);
		$mname_g = mysqli_real_escape_string($con,$_POST['mname_g']);
		$contact_g = mysqli_real_escape_string($con,$_POST['contact_g']);
		$occupation_g = mysqli_real_escape_string($con,$_POST['occupation_g']);

		$siblings = mysqli_real_escape_string($con,$_POST['siblings']);
        
		$si_kindergarten = mysqli_real_escape_string($con,$_POST['si_kindergarten']);
		$si_gradeschool = mysqli_real_escape_string($con,$_POST['si_gradeschool']);
		$si_junior_hs = mysqli_real_escape_string($con,$_POST['si_junior_hs']);
		$si_senior_hs = mysqli_real_escape_string($con,$_POST['si_senior_hs']);
		$with_medical_condition = mysqli_real_escape_string($con,$_POST['with_medical_condition']);
		$medical_condition_affects = mysqli_real_escape_string($con,$_POST['medical_condition_affects']);
		$medical_condition = mysqli_real_escape_string($con,$_POST['medical_condition']);

		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		$term_id = mysqli_real_escape_string($con,$_POST['term_id']);
		$suffix = mysqli_real_escape_string($con,$_POST['suffix']);
		$category = mysqli_real_escape_string($con,$_POST['category']);

		$email = mysqli_real_escape_string($con,$_POST['email']);
		$fb_account = mysqli_real_escape_string($con,$_POST['fb_account']);
		$messenger_account = mysqli_real_escape_string($con,$_POST['messenger_account']);
		$mother_tongue = mysqli_real_escape_string($con,$_POST['mother_tongue']);
		$other_mother_tongue = mysqli_real_escape_string($con,$_POST['other_mother_tongue']);
		$ethnicity_val = mysqli_real_escape_string($con,$_POST['ethnicity_val']);
		$other_ethnicity = mysqli_real_escape_string($con,$_POST['other_ethnicity']);

		$gadgets_val = mysqli_real_escape_string($con,$_POST['gadgets_val']);
		$other_gadget = mysqli_real_escape_string($con,$_POST['other_gadget']);

		$internet = mysqli_real_escape_string($con,$_POST['internet_val']);
		$other_i = mysqli_real_escape_string($con,$_POST['other_i']);
		$internet_rate = mysqli_real_escape_string($con,$_POST['internet_rate']);
		$lrn = mysqli_real_escape_string($con,$_POST['lrn']);
		$relation_g = mysqli_real_escape_string($con,$_POST['relation_g']);

		$learning_dmode = mysqli_real_escape_string($con,$_POST['learning_dmode']);
		$other_p = mysqli_real_escape_string($con,$_POST['other_p']);
		$preferred_days_val = mysqli_real_escape_string($con,$_POST['preferred_days_val']);
		$other_gp = mysqli_real_escape_string($con,$_POST['other_gp']);
		$financial_capability_val = mysqli_real_escape_string($con,$_POST['financial_capability_val']);
		$other_fc = mysqli_real_escape_string($con,$_POST['other_fc']);
		$teacher_home_val = mysqli_real_escape_string($con,$_POST['teacher_home_val']);
		$other_th = mysqli_real_escape_string($con,$_POST['other_th']);

		$p_userid = mysqli_real_escape_string($con,$_POST['p_userid']);
		$p_pass = mysqli_real_escape_string($con,$_POST['p_pass']);

		$go_to_school_val =  mysqli_real_escape_string($con,$_POST['go_to_school_val']);
		$connected_to_internet_val =  mysqli_real_escape_string($con,$_POST['connected_to_internet_val']);

		$affects_DE_val = mysqli_real_escape_string($con,$_POST['affects_DE_val']);
		$other_affects = mysqli_real_escape_string($con,$_POST['other_affects']);

		$acronym = mysqli_real_escape_string($con,$_POST['acronym']);

		$blended_pmc_val = mysqli_real_escape_string($con,$_POST['blended_pmc_val']);
		$other_pmc = mysqli_real_escape_string($con,$_POST['other_pmc']);

		$suggestions = mysqli_real_escape_string($con, $_POST['suggestions']);

		$school_year = mysqli_real_escape_string($con, $_POST['school_year']);

        save_registration($con,$grade_id,$lname,$fname,$mname,$esc_qvr_number,$bdate,$place_of_birth,$gender,$religion,$citizenship,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g,$contact_g,$occupation_g,$siblings,$si_kindergarten,$si_gradeschool,$si_junior_hs,$si_senior_hs,$with_medical_condition,$medical_condition_affects,$medical_condition,$student_id,$term_id,$suffix,$category,$email,$fb_account,$messenger_account,$mother_tongue,$other_mother_tongue,$ethnicity_val,$other_ethnicity,$gadgets_val,$other_gadget,$internet,$other_i,$internet_rate,$lrn,$relation_g,$learning_dmode,$other_p,$preferred_days_val,$other_gp,$financial_capability_val,$other_fc,$teacher_home_val,$other_th,$p_userid,$p_pass,$go_to_school_val,$connected_to_internet_val,$affects_DE_val,$other_affects,$acronym,$blended_pmc_val,$other_pmc,$suggestions,$school_year);

	break;

	case 'gen_id':
		gen_id($con);
	break;

	case 'return_age':
		$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
		return_age($con,$bdate);
	break;

	case 'remove_file':
		$file_name = mysqli_real_escape_string($con,$_POST['file_name']);
		remove_file($con,$file_name);
	break;

	case 'load_supporting_files':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		load_supporting_files($con,$student_id);
	break;

	case 'delete_doc':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		$path = mysqli_real_escape_string($con,$_POST['path']);
		delete_doc($con,$id,$path);
	break;

	case 'auto_login':
		auto_login($con);
	break;

	case 'load_household_members':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		load_household_members($con,$student_id,$grade_id);
	break;

	case 'save_member':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		$member = mysqli_real_escape_string($con,$_POST['member']);
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		save_member($con,$grade_id,$member,$student_id);
	break;

	case 'remove_member':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		remove_member($con,$id);
	break;

	//Manage Scholastic
	case 'save_scholastic':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_s']);
		$scholastic_value = mysqli_real_escape_string($con,$_POST['scholastic_value']);
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		$school_year_val = mysqli_real_escape_string($con,$_POST['school_year_val']);
		save_scholastic($con,$grade_id,$scholastic_value,$student_id,$school_year_val);
	break;

	case 'load_scholastics':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_s']);
		load_scholastics($con,$student_id,$grade_id);
	break;

	case 'remove_scholastic':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		remove_scholastic($con,$id);
	break;

}
?>