	function show_all(){
		var start,limit,src_user;
		src_user = document.getElementById('src_user');
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var mydata = 'start=' + start.value + '&limit=' + limit.value  + '&src_user=' + src_user.value;
		// alert(mydata);
		$.ajax({
			type:"POST",
			url:"show_all_person.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#all_person").html('<center><img src="../img/flat.gif" width="50" class="img-responsive"></center>');
			},
			success:function(data){
				$("#all_person").html(data);
			}
		});
	}

	function show_all_more(){
		var start,limit,src_user;
		src_user = document.getElementById('src_user');
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var mydata = 'start=' + start.value + '&limit=' + limit.value  + '&src_user=' + src_user.value;
		// alert(mydata);
		$.ajax({
			type:"POST",
			url:"show_all_person.php",
			data:mydata,
			cache:false,
			success:function(data){
				$("#all_person").html(data);
				$("#load_more").text('Load more');
			}
		});
	}

	function key_off_person(myval){
		if (myval == "") 
		{
			$("#src_rem").hide('fast');
			$("#src_label").show('fast');
			$("#rem_btn").removeClass('btn btn-danger');
		}
		else
		{
			$("#src_rem").show('fast');
			$("#src_label").hide('fast');
			$("#rem_btn").addClass('btn btn-danger');
		}
	}

	function clear_btn(){
		$("#src_rem").hide('fast');
		$("#src_label").show('fast');
		$("#rem_btn").removeClass('btn btn-danger');
		$("#src_all").val('');
		$("#all_person").show('fast');
		$("#all_person_src").hide('fast');

	}

	function sort_filter(){
		
		var start,limit,src_user,src_all;
		src_all = document.getElementById('src_all');
		src_user = document.getElementById('src_user');
		start = document.getElementById('start');
		limit = document.getElementById('limit');

		if (src_all.value == "") 
		{
			src_all.focus();
		}
		else
		{
			$("#all_person").hide('fast');
		var mydata = 'start=' + start.value + '&limit=' + limit.value  + '&src_user=' + src_user.value + '&src_all=' + src_all.value;
		// alert(mydata);
		$.ajax({
			type:"POST",
			url:"show_all_person.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#all_person_src").html('<center><img src="../img/flat.gif" width="50" class="img-responsive"></center>');
			},
			success:function(data){
				$("#all_person").hide('fast');
				$("#all_person_src").show('fast');
				$("#all_person_src").html(data);
			}
		});
		}
		
	}