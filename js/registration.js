var url = 'function.php';

function load_school_fees(){

	var grade_id,mydata;
	  grade_id = $("#grade_id").val();
	  mydata = 'action=load_school_fees' + '&grade_id=' + grade_id;
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    beforeSend:function(){
				$("#tbl_school_fees").html('<center><h4 class="animated pulse infinite">loading fees...</h4></center>');
			},
	    success:function(data){
	      setTimeout(function(){$("#tbl_school_fees").html(data);},500);
	    }
	  });

}

function load_terms_of_payment(){

	var grade_id,mydata;
	  var term_id = $("#term_id").val();
	  grade_id = $('#grade_id').val();
	  mydata = 'action=load_terms_of_payment' + '&term_id=' + term_id + '&grade_id=' + grade_id;
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    beforeSend:function(){
				$("#tbl_terms_of_payment").html('<center><h4 class="animated pulse infinite">loading...</h4></center>');
			},
	    success:function(data){
	      setTimeout(function(){$("#tbl_terms_of_payment").html(data);},500);
	    }
	  });

}

function form1_validation(){
	var lname,fname,email,category,grade_id;
	lname = $('#lname').val();
	fname = $('#fname').val();
	category = $('#category').val();
	grade_id = $('#grade_id').val();
	email = $('#email').val();
	var school_year = $('#school_year').val();
	//next1();
	if(category == ''){
		$('#category').focus();
	}else if(school_year == ''){
		$('#school_year').focus();
	}else if(grade_id == ''){
		$('#grade_id').focus();
	}else if(lname == ''){
		$('#lname').focus();
	}else if(fname == ''){
		$('#fname').focus();
	}else if(email == ''){
		$('#email').focus();
	}else{
		next1();
	}
}

function next1(){

	$('#form').removeClass('active');
	$('#pi').addClass('active');
	$('#reg').css('display','none');
	$('#bdate').focus();
	$('#form1').removeClass('active');
	$('#form2').addClass('active');
}

function back1(){

	$('#pi').removeClass('active');
	$('#form').addClass('active');
	$('#reg').css('display','block');
	$('#category').focus();
	$('#form2').removeClass('active');
	$('#form1').addClass('active');
}

function form2_validation(){
	var bdate,place_of_birth,gender,religion,citizenship,contact,address;
	bdate = $('#bdate').val();
	place_of_birth = $('#place_of_birth').val();
	gender = $('#gender').val();
	religion = $('#religion').val();
	citizenship = $('#citizenship').val();
	contact = $('#contact').val();
	address = $('#address').val();
	//next2();
	if(bdate == ''){
		$('#bdate').focus();
	}else if(place_of_birth == ''){
		$('#place_of_birth').focus();
	}
	else if(gender == ''){
		$('#gender').focus();
	}
	else if(place_of_birth == ''){
		$('#place_of_birth').focus();
	}
	else if(religion == ''){
		$('#religion').focus();
	}
	else if(citizenship == ''){
		$('#citizenship').focus();
	}
	else if(contact == ''){
		$('#contact').focus();
	}
	else if(address == ''){
		$('#address').focus();
	}else{
		next2();
	}
}

function next2(){

	$('#pi').removeClass('active');
	$('#family').addClass('active');
	$('#lname_f').focus();
	$('#form2').removeClass('active');
	$('#form3').addClass('active');
}

function next3(){

	$('#family').removeClass('active');
	$('#other_info').addClass('active');
	$('#si_kindergarten').focus();
	$('#form3').removeClass('active');
	$('#form4').addClass('active');
}
function next4(){

	$('#other_info').removeClass('active');
	$('#other_info2').addClass('active');
	///$('#si_kindergarten').focus();
	$('#form4').removeClass('active');
	$('#form5').addClass('active');

}


function back2(){

	$('#family').removeClass('active');
	$('#pi').addClass('active');
	$('#category').focus();
	$('#form3').removeClass('active');
	$('#form2').addClass('active');
}

function back3(){

	$('#other_info').removeClass('active');
	$('#family').addClass('active');
	$('#lname_f').focus();
	$('#form4').removeClass('active');
	$('#form3').addClass('active');
}

function back4(){

	$('#other_info2').removeClass('active');
	$('#other_info').addClass('active');
	//$('#lname_f').focus();
	$('#form5').removeClass('active');
	$('#form4').addClass('active');

}

function save_registration(){

	var grade_id,lname,fname,mname,esc_qvr_number,bdate,place_of_birth,gender,religion,citizenship,contact,address,lname_f,fname_f,mname_f,contact_f,occupation_f,lname_m,fname_m,mname_m,contact_m,occupation_m,lname_g,fname_g,mname_g,contact_g,occupation_g,siblings,si_kindergarten,si_gradeschool,si_junior_hs,si_senior_hs,with_medical_condition,medical_condition_affects,medical_condition,student_id,term_id,suffix,category,email,fb_account,messenger_account,mother_tongue,other_mother_tongue,ethnicity_val,other_ethnicity,gadgets_val,other_gadget,internet_val,other_i,internet_rate,lrn,relation_g,acronym,school_year;

		grade_id = document.getElementById('grade_id');
		lname = $('#lname').val();
		fname = $('#fname').val();
		mname = $('#mname').val();
		esc_qvr_number = $('#esc_qvr_number').val();
		bdate = $('#bdate').val();
		place_of_birth = $('#place_of_birth').val();
		gender = $('#gender').val();
		religion = $('#religion').val();
		citizenship = $('#citizenship').val();
		contact = $('#contact').val();
		address = $('#address').val();
		lname_f = $('#lname_f').val();
		fname_f = $('#fname_f').val();
		mname_f = $('#mname_f').val();
		contact_f = $('#contact_f').val();
		occupation_f = $('#occupation_f').val();
		lname_m = $('#lname_m').val();
		fname_m = $('#fname_m').val();
		mname_m = $('#mname_m').val();
		contact_m = $('#contact_m').val();
		occupation_m = $('#occupation_m').val();
		lname_g = $('#lname_g').val();
		fname_g = $('#fname_g').val();
		mname_g = $('#mname_g').val();
		contact_g = $('#contact_g').val();
		occupation_g = $('#occupation_g').val();
		siblings = $('#siblings').val();

		si_kindergarten = $('#si_kindergarten').val();
		si_gradeschool = $('#si_gradeschool').val();
		si_junior_hs = $('#si_junior_hs').val();
		si_senior_hs = $('#si_senior_hs').val();
		with_medical_condition = $('#with_medical_condition').val();
		medical_condition_affects = $('#medical_condition_affects').val();
		medical_condition = $('#medical_condition').val();

		student_id = $('#student_id').val();
		term_id = $('#term_id').val();
		suffix = $('#suffix').val();
		category = $('#category').val();

		email = $('#email').val();
		fb_account = $('#fb_account').val();
		messenger_account = $('#messenger_account').val();
		mother_tongue = $('#mother_tongue').val();
		other_mother_tongue = $('#other_mother_tongue').val();
		ethnicity_val = $('#ethnicity_val').val();
		other_ethnicity = $('#other_ethnicity').val();
		gadgets_val = $('#gadgets_val').val();
		other_gadget = $('#other_gadget').val();
		internet_val = $('#internet_val').val();
		other_i = $('#other_i').val();
		internet_rate = $('#internet_rate').val();
		lrn = $('#lrn').val();
		relation_g = $('#relation_g').val();

		var learning_dmode = $('#learning_dmode').val();
		var other_p = $('#other_p').val();
		var preferred_days_val = $('#preferred_days_val').val();
		var other_gp = $('#other_gp').val();
		var financial_capability_val = $('#financial_capability_val').val();
		var other_fc = $('#other_fc').val();
		var teacher_home_val = $('#teacher_home_val').val();
		var other_th = $('#other_th').val(); 

		var p_userid = $('#p_userid').val();
		var p_pass = $('#p_pass').val();

		var go_to_school_val = $('#go_to_school_val').val();
		var connected_to_internet_val = $('#connected_to_internet_val').val();

		var affects_DE_val = $('#affects_DE_val').val();
		var other_affects = $('#other_affects').val();
		acronym = $('#acronym').val();

		var blended_pmc_val = $('#blended_pmc_val').val();
		var other_pmc = $('#other_pmc').val();

		var suggestions = $('#suggestions').val();

		var school_year = $('#school_year').val();

		var mydata = 'action=save_registration' + '&grade_id=' + grade_id.value + '&lname=' + lname + '&fname=' + fname + '&mname=' + mname + '&esc_qvr_number=' + esc_qvr_number + '&bdate=' + bdate + '&place_of_birth=' + place_of_birth + '&gender=' + gender + '&religion=' + religion + '&citizenship=' + citizenship + '&contact=' + contact + '&address=' + address + '&lname_f=' + lname_f + '&fname_f=' + fname_f + '&mname_f=' + mname_f + '&contact_f=' + contact_f + '&occupation_f=' + occupation_f + '&lname_m=' + lname_m + '&fname_m=' + fname_m + '&mname_m=' + mname_m + '&contact_m=' + contact_m + '&occupation_m=' + occupation_m + '&lname_g=' + lname_g + '&fname_g=' + fname_g + '&mname_g=' + mname_g + '&contact_g=' + contact_g + '&occupation_g=' + occupation_g + '&siblings=' + siblings + '&si_kindergarten=' + si_kindergarten + '&si_gradeschool=' + si_gradeschool + '&si_junior_hs=' + si_junior_hs + '&si_senior_hs=' + si_senior_hs + '&with_medical_condition=' + with_medical_condition + '&medical_condition_affects=' + medical_condition_affects + '&medical_condition=' + medical_condition + '&student_id=' + student_id + '&term_id=' + term_id + '&suffix=' + suffix + '&category=' + category + '&email=' + email + '&fb_account=' + fb_account + '&messenger_account=' + messenger_account + '&mother_tongue=' + mother_tongue + '&other_mother_tongue=' + other_mother_tongue + '&ethnicity_val=' + ethnicity_val + '&other_ethnicity=' + other_ethnicity + '&gadgets_val=' + gadgets_val + '&other_gadget=' + other_gadget + '&internet_val=' + internet_val + '&other_i=' + other_i + '&internet_rate=' + internet_rate + '&lrn=' + lrn + '&relation_g=' + relation_g + '&learning_dmode=' + learning_dmode + '&other_p=' + other_p + '&preferred_days_val=' + preferred_days_val + '&other_gp=' + other_gp + '&financial_capability_val=' + financial_capability_val + '&other_fc=' + other_fc + '&teacher_home_val=' + teacher_home_val + '&other_th=' + other_th + '&p_userid=' + p_userid + '&p_pass=' + p_pass + '&go_to_school_val=' + go_to_school_val + '&connected_to_internet_val=' + connected_to_internet_val + '&affects_DE_val=' + affects_DE_val + '&other_affects=' + other_affects + '&acronym=' + acronym + '&blended_pmc_val=' + blended_pmc_val + '&other_pmc=' + other_pmc + '&suggestions=' + suggestions +'&school_year=' + school_year;
		
		$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      beforeSend:function(){
	      	document.getElementById('btn_save').disabled = true
			$("#btn_save").text('Submitting registration...');
			},
	      success:function(data){
	        console.log(data);
	        if (data == 1) 
	        {
	        	$("#btn_save").text('Submit');
	         	document.getElementById('btn_save').disabled = true
				$("#btn_save").text('Saving registration...');
		        setTimeout(function(){auto_login();},1000);
	          //setTimeout(function(){window.location='login.php';},3000);
	        }
	        else if (data == 2){

	        	$("#btn_save").text('Submit');
	         	document.getElementById('btn_save').disabled = true
				$("#btn_save").text('Saving registration...');
				setTimeout(function(){window.location='login.php';},1000);
	         	swal("Success","Successfully Registered! Please login your account","success");

	        }else if (data == 'duplicate'){

	         	swal("warning","Account already exists. Please check your school year.","warning");

	        }else{
	        	 swal("Info","Error on registration","info");
	        }
	      }

	    });

}

function auto_login(){

	var email = $('#email').val();
	var p_pass = $('#p_pass').val();
	var mydata = 'action=auto_login' + '&email=' + email + '&p_pass=' + p_pass;

	$.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        //console.log(data);
        if (data == 1) 
        {
            swal("Success","Successfully Registered! Please wait redirecting to you account...","success"); 
            setTimeout(function(){
          	window.location='client/index.php';
          },1000);
        }
        else if (data == 2){
          swal("Info","Account already exists!","info");
        }
      }
    });

}

function gen_id(){

	var mydata = 'action=gen_id';
	$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
				//alert(data);
				$("#student_id").val(data);
			}
	});

}

function return_age(){
	var bdate = $('#bdate').val();
	var mydata = 'action=return_age' + '&bdate=' + bdate;
	$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
				//alert(data);
				$("#age").val(data);
			}
	});

}

function del_temp_file(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this post ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == "") 
                {
              	 showToast.show("File has been removed!",1000);
              	  $('#fupForm')[0].reset();
              	  $('#preview_file').html("");
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

function login(){
	var uname,pwd;
	uname = $('#uname').val();
	pwd = $('#pwd').val();

	if (uname == "") 
	{
		uname.focus();
	}
	else if (pwd == "")
	{
		pwd.focus();
	}
	else 
	{
	var mydata = 'action=login' + '&uname=' + uname + '&pwd=' + pwd;
	$.ajax({
		type:'POST',
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
		$("#btn_log").text('Loading...');
		},
		success:function(data){
		$("#btn_log").text('Login');
		 //alert(data);
			if (data == 1) 
			{
				window.location='client/';
			}
			else if (data == 2) 
			{
				window.location='administrator/';
			}
			else
			{
			swal("Oops!","Invalid account !","error");
			}
		}
	});
	}
}

function load_supporting_files(){
	var student_id = $('#student_id').val();
	var mydata = 'action=load_supporting_files' + '&student_id=' + student_id;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#load_files").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#load_files").html(data);
		}
	});
}

function delete_doc(id,path){

	var mydata = 'action=delete_doc' + '&id=' + id + '&path=' + path;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this file?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("file has been removed!",3000);
              	 load_supporting_files();
                }
                else
                {
                    swal("Error","Error on deleting file.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
	}

function say_yes() {
	  var checkBox = document.getElementById("switch1");
  if (checkBox.checked == true){
  	document.getElementById('btn_save').disabled = false
  } else {
    document.getElementById('btn_save').disabled = true
  }
}

function iba_pa() {
	  var checkBox = document.getElementById("iba_pa");
  if (checkBox.checked == true){
  	document.getElementById('other_mother_tongue').disabled = false
  } else {
    document.getElementById('other_mother_tongue').disabled = true
  }
}

function get_style_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#m_tongue input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#mother_tongue").val(selected);
}

function get_ethnicity(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#ethnicity input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#ethnicity_val").val(selected);
}

function iba_pa_e() {
	  var checkBox = document.getElementById("iba_pa_e");
  if (checkBox.checked == true){
  	document.getElementById('other_ethnicity').disabled = false
  } else {
    document.getElementById('other_ethnicity').disabled = true
  }
}

function get_gadgets(){

  var chkArray = [];
  
  $("#gadgets input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#gadgets_val").val(selected);

}

function iba_pa_g() {
	  var checkBox = document.getElementById("iba_pa_g");
  if (checkBox.checked == true){
  	document.getElementById('other_gadget').disabled = false
  } else {
    document.getElementById('other_gadget').disabled = true
  }
}

function get_ic(){

  var chkArray = [];
  
  $("#internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#internet_val").val(selected);

}

function get_delivery_mode(){

  var chkArray = [];
  
  $("#delivery_mode input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#learning_dmode").val(selected);

}

function get_preferred_days(){

  var chkArray = [];
  
  $("#preferred_days input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#preferred_days_val").val(selected);
  $('#other_gp').val('');

}

function iba_pa_gp() {
	  var checkBox = document.getElementById("iba_pa_gp");
  if (checkBox.checked == true){
  	$("#preferred_days_val").val('');
  	document.getElementById('other_gp').disabled = false
  } else {
    document.getElementById('other_gp').disabled = true
  }
}

function iba_pa_i() {
	  var checkBox = document.getElementById("iba_pa_i");
  if (checkBox.checked == true){
  	document.getElementById('other_i').disabled = false
  } else {
    document.getElementById('other_i').disabled = true
  }
}

function iba_pa_p() {
	  var checkBox = document.getElementById("iba_pa_p");
  if (checkBox.checked == true){
  	document.getElementById('other_p').disabled = false
  } else {
    document.getElementById('other_p').disabled = true
  }
}

function get_financial_capability(){

  var chkArray = [];
  
  $("#financial_capability input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#financial_capability_val").val(selected);
  $('#other_fc').val('');

}

function iba_pa_fc() {
	  var checkBox = document.getElementById("iba_pa_fc");
  if (checkBox.checked == true){
  	$("#financial_capability_val").val('');
  	document.getElementById('other_fc').disabled = false
  } else {
    document.getElementById('other_fc').disabled = true
  }
}

function get_teacher_home(){

  var chkArray = [];
  
  $("#teacher_home input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#teacher_home_val").val(selected);

}

function iba_pa_th() {
	  var checkBox = document.getElementById("iba_pa_th");
  if (checkBox.checked == true){
  	document.getElementById('other_th').disabled = false
  } else {
    document.getElementById('other_th').disabled = true
  }
}

function get_gotoschool(){

  var chkArray = [];
  
  $("#go_to_school input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#go_to_school_val").val(selected);

}

function get_pmc(){

  var chkArray = [];
  
  $("#blended_pmc input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#blended_pmc_val").val(selected);

}

function iba_pa_pmc() {
	  var checkBox = document.getElementById("iba_pa_pmc");
  if (checkBox.checked == true){
  	document.getElementById('other_pmc').disabled = false
  } else {
    document.getElementById('other_pmc').disabled = true
  }
}


function load_household_members(){

	var student_id = $('#student_id').val();
	var grade_id = $('#grade_id_h').val();
	var mydata = 'action=load_household_members' + '&student_id=' + student_id + '&grade_id_h=' + grade_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_household_members").html(data);
		}
	});
}

function save_household(){

	var mydata,grade_id,member,student_id;

	grade_id = $('#grade_id_h').val();
	member = $('#member_value').val();
	student_id = $('#student_id').val();
	mydata = 'action=save_member' + '&grade_id_h=' + grade_id + '&member=' + member + '&student_id=' + student_id;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 1) 
	        {
	         	load_household_members();
	         	$('#member_value').val('');

	        }else if(data == 2){
	        	swal("Info","Grade already exists. Please select another grade level.","info");
	        	$('#member_value').val('');
	        }
	        else{
	          swal("Error","Error on adding data!","error");
	        }
	      }
	    });

}

function remove_member(id){

	var mydata = 'action=remove_member' + '&id=' + id;

	$.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 	showToast.show("Data has been removed!",1000);
	              	load_household_members();
                }
                else
                {
                    //alert(data);
                }
            }
        });

}

function get_is_connected(){

  var chkArray = [];
  
  $("#connected_to_internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#connected_to_internet_val").val(selected);

}

function get_affects(){

  var chkArray = [];
  
  $("#affects_DE input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#affects_DE_val").val(selected);

}

function iba_pa_affects() {
	  var checkBox = document.getElementById("iba_pa_affects");
  if (checkBox.checked == true){
  	document.getElementById('other_affects').disabled = false
  } else {
    document.getElementById('other_affects').disabled = true
  }
}

  function load_mobile_index(){
      if($(window).width() <=575){
        $('#desktop_btn_navbar').css('display','none');
        $('#navbar-header').css('display','');
        $('#navbar-brand-logo').css('display','');
        $('#header_image').css('display','none');
      }else{
        $('#desktop_btn_navbar').css('display','');
         $('#navbar-header').css('display','none');
         $('#navbar-brand-logo').css('display','none');
         $('#header_image').css('display','');
      }
  }
  $(window).resize(function() {
    if($(window).width() <=575){ 
      $('#desktop_btn_navbar').css('display','none');
      $('#navbar-header').css('display','');
       $('#navbar-brand-logo').css('display','');
        $('#header_image').css('display','none');
    }else{
      $('#desktop_btn_navbar').css('display','');
      $('#navbar-header').css('display','none');
      $('#navbar-brand-logo').css('display','none');
       $('#header_image').css('display','');
    }
  });

//Manage Scholastic
function save_scholastic(){

	var mydata,grade_id,scholastic_value,student_id,school_year_val;

	grade_id = $('#grade_id_s').val();
	scholastic_value = $('#scholastic_value').val();
	student_id = $('#student_id').val();
	school_year_val = $('#school_year_val').val();
	mydata = 'action=save_scholastic' + '&grade_id_s=' + grade_id + '&scholastic_value=' + scholastic_value + '&student_id=' + student_id + '&school_year_val=' + school_year_val;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 1) 
	        {
	         	load_scholastics();
	         	$('#scholastic_value').val('');
	         	$('#school_year_val').val('');
	         	$('#grade_id_s').val('');

	        }else{
	          swal("Error","Error on adding data!","error");
	        }
	      }
	    });

}

function load_scholastics(){

	var student_id = $('#student_id').val();
	var grade_id = $('#grade_id_s').val();
	var mydata = 'action=load_scholastics' + '&student_id=' + student_id + '&grade_id_s=' + grade_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_scholastics").html(data);
		}
	});
}

function remove_scholastic(id){

	var mydata = 'action=remove_scholastic' + '&id=' + id;

	$.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 	showToast.show("Data has been removed!",1000);
	              	load_scholastics();
                }
                else
                {
                    //alert(data);
                }
            }
        });

}