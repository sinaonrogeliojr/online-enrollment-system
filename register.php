<?php
 session_start();
 if (isset($_SESSION['id'])) {
 	@header('location:client/');
 }
 include_once('config.php');

function with_highschool($con){
	$sql = mysqli_query($con,"SELECT * from tbl_company where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
         $row = mysqli_fetch_assoc($sql);
         return $row['with_highschool'];
    }
}

function get_terms($con){
    $sql = mysqli_query($con,"SELECT * from tbl_terms where is_active = 1 order by id asc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['terms'].'</option>';
        }
    }
}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        echo $row['school_year'];
    }
}

function get_school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by is_active desc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		 $default_color = 'w3-light-blue';
		$school_name =$row['company_name1'];	
		$school_name2 = ''.$row['company_name2'].'';
		$acronym = ''.$row['acronym'].'';
		$with_hs = ''.$row['with_highschool'].'';
		$with_gs = ''.$row['with_gradeschool'].'';
		$with_payment = ''.$row['with_payment'].'';

		$p_userid = "P-".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
		$p_pass = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

		$company_name = ''.$row['company_name1'].'';
		$header = '<img width="100px;" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
		$header_logo = '<img style=" max-width: 100%;" src="data:image/jpeg;base64,'.base64_encode($row['header_logo']).'" class="rounded img-fluid w-100"/>';
   		$navbar_bg = ''.$row['header_bg'].'';


	}

function get_grade_level($con,$with_hs,$with_gs){

    $sql = mysqli_query($con,"SELECT * from tbl_grade where is_highschool = '$with_hs' or is_gradeschool = '$with_gs' order by orderno asc");

    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['ID'].'">'.$row['grade'].'</option>';
        }
    }

}

function get_sy($con){

    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by is_active desc");

    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }

}


?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo 	$school_name ;  ?></title>
	<?php include_once('links.php'); ?>
</head>

<style type="text/css">
	body {

		background-color: #f2f2f2;
	 
	  /*background-color: #cccccc;*/
	}

	.display-none{
		display: none;
	}
</style>

<body onload="gen_id(); load_school_fees(); load_terms_of_payment(); load_household_members();load_mobile_index()">
 <nav class="navbar navbar-expand-md w3-gray text-white" id="desktop_btn_navbar" style="display: none">
 
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            
            
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link page-scroll" href="login.php"><b><span class="fa fa-sign-in-alt"></span> LOGIN</b></a>
            </li>
        </ul>
    </div>
</nav>

 <br>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">

			<div class="card" id="reg">
			  <div class="card-header w3-gray text-white"></div>
			  <div class="card-body">
			  		<input type="hidden" id="with_hs" value="<?php echo $with_hs ?>">
			  		<input type="hidden" id="with_gs" value="<?php echo $with_gs ?>">
			  		<h4>
			  			<b>ONLINE STUDENT REGISTRATION FORM FOR S.Y. <?php echo get_school_year_active($con);  ?> ENROLMENT</b>
			  		</h4>
			  		<hr>
					<p class="text-danger">Note: Please read the instruction below for proper guidance.</p>
					<p class="w3-large">This form intends to gather actual enrollment data of all <?php echo $school_name; ?> Learners.</p>
					<p>
						Please answer this registration form with utmost honesty.  <br><br>

						The information you will be providing us will be treated Confidential following policy guidelines and implementing rules and regulations of the Data Privacy Act of 2012.<br><br>

						Thank you.<br>
						<span class="badge badge-danger">* REQUIRED FIELDS.</span>
						<input type="hidden" id="acronym" value="<?php echo $acronym; ?>">
						
				</p>

			  </div>
			</div>
			<br>
			
			<div class="card">

			  <div class="card-header w3-gray text-white">
			  	<input type="hidden"  id="p_userid" value="<?php echo $p_userid; ?>">
			  	<input type="hidden"  id="p_pass" value="<?php echo $p_pass; ?>">
			  </div>

			  <div class="card-body">
		
			  	<ul class="nav nav-pills" role="tablist" style="display: none;">
				    <li class="nav-item">
				      <a class="nav-link active" id="form1" data-toggle="tab" href="#form">Home</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" id="form2" data-toggle="tab" href="#pi">Menu 1</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" id="form3" data-toggle="tab" href="#family">Menu 2</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" id="form4" data-toggle="tab" href="#other_info">Menu 3</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" id="form5" data-toggle="tab" href="#other_info2">Menu 4</a>
				    </li>
				 </ul>

				 <div class="tab-content">
				 	<!-- FORM 1 -->
				  	<div id="form" class="tab-pane active">
				  		<br>
				  		<ul class="list-group">
						  <li class="list-group-item list-group-item-info w3-blue"><b><span class="fa fa-info-circle"></span> Learner's Information</b></li>
						  <li class="list-group-item list-group-item-action" style="size: 12px;">Check the spelling of the recorded information. It should be based on PSA / Municipal Birth Certificate.</li>
						</ul>

				  		<br>
				  		
				  		<div class="form-group">
						  <label for="category"><b>Choose Category:</b> <span class="text-danger">*</span></label>
						  <select class="form-control" id="category" >
						   	<option value=""></option>
						  	<option value="Old Student">OLD STUDENT</option>
						  	<option value="New Student">NEW STUDENT</option>
						  	<option value="Transferee">TRANSFEREE</option>
						  </select>
						</div>

						<div class="form-group">
						  <label for="school_year"><b>School Year:</b> <span class="text-danger">*</span></label>
						  <select class="form-control" id="school_year">
						  	<option selected="" value="">-- Select School Year --</option>
						    <?php echo get_sy($con,$with_hs,$with_gs); ?>
						  </select>
						</div>

						<div class="form-group">
						  <label for="grade_id"><b>Grade Level Applied For:</b> <span class="text-danger">*</span></label>
						  <select class="form-control" id="grade_id" oninput="load_school_fees(); load_terms_of_payment();" onchange="load_school_fees(); load_terms_of_payment();">
						    <?php echo get_grade_level($con,$with_hs,$with_gs); ?>
						  </select>
						</div>
					
						<div class="form-group">
						  <label for="lname"><b>Lastname:</b> <span class="text-danger">*</span></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="lname" class="form-control text-capitalize" id="lname" >
						</div>

						<div class="form-group">
						  <label for="fname"><b>Firstname:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="fname" class="form-control text-capitalize" id="fname">
						</div>
						<div class="form-group">
						  <label for="mname"><b>Middlename:</b></label>
						  <input type="text" id="mname" class="form-control text-capitalize" id="mname" >
						</div>

						<div class="form-group">
						  <label for="suffix"><b>Suffix(Jr., Sr., I, II, III, IV):</b></label>
						  <input type="text" id="suffix" class="form-control text-capitalize" id="suffix" >
						</div>

						<div class="form-group">
						  <label for="email"><b>Email: <span class="text-danger">*</span></b>
						  <span class="text-success" id="validEmail"></span>
						</label>
						  <input type="email" class="form-control" id="email" >
						</div>

						<div class="form-group">
						  <label for="fb_account"><b>Faceboook Account:</b></label>
						  <input type="text" class="form-control" id="fb_account" >
						</div>
						<?php if($acronym <> 'CFS'){ ?>
							<div class="form-group">
							  <label for="messenger_account"><b>Messenger Account:</b></label>
							  <input type="text" class="form-control" id="messenger_account" >
							</div>
						<?php } ?>
						
						<?php if(with_highschool($con) == 'YES'){ ?>

							<div class="form-group">
							  <label for="esc_qvr_number"><b>ESC Number(JHS)/ QVR Number(SHS) if subsidy recipient:</b></label>
							  <input type="text" id="esc_qvr_number" class="form-control" >
							</div>

						<?php } ?>

						<div class="form-group">
						  <label for="lrn"><b>Learner Reference Number(LRN):</b> </label>
						  <input type="text" id="lrn" class="form-control text-capitalize" >
						</div>
						
						<button id="next1" class="btn <?php echo $default_color; ?>" onclick="form1_validation();"><span class="fa fa-arrow-right"></span> Next</button>

				  	</div>
				  	<!-- FORM 1 -->
				  	<!-- FORM 2 PERSONAL INFORMATION -->
					  <div id="pi" class="tab-pane">
					  	<br>
					  	<h4><b><span class="fa fa-user"></span> PERSONAL INFORMATION</b></h4>
						<div class="form-group">
						  <label for="bdate"><b>Birth Date:</b> <span class="text-danger">*</span></label>
						  <input type="Date" id="bdate" oninput="return_age();" onchange="return_age();" class="form-control">
						</div>

						<div class="form-group">
						  <label for="age"><b>Age:</b> <span class="text-danger">*</span></label>
						  <input type="number" id="age" class="form-control">
						</div>

						<div class="form-group">
						  <label for="place_of_birth"><b>Place of Birth:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="place_of_birth" class="form-control text-capitalize">
						</div>

						<div class="form-group">

						  <label for="gender"><b>Gender:</b> <span class="text-danger">*</span></label>

						  <select class="form-control" id="gender">
						  	<option selected="" value="FEMALE">FEMALE</option>
						    <option  value="MALE">MALE</option>
						  </select>

						</div>

						<div class="form-group">

						  <label for="religion"><b>Religion:</b> <span class="text-danger">*</span></label>

						  <select class="form-control" id="religion">

						  	<option value=""></option>
						  	<option value="ROMAN - CATHOLIC">Roman Catholic</option>
						  	<option value="NON CATHOLIC">Non Catholic</option>
						  	<option value="ISLAM">Islam</option>
						  	<option value="BUDDHIST">Buddhist</option>
						  	<option value="OTHER">Other</option>

						  </select>

						</div>

						<div class="form-group">
						  <label for="citizenship"><b>Citizenship:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="citizenship" class="form-control">
						</div>

						<div class="form-group">
						  <label for="messenger_account"><b>Mother Tongue(Dialect Use at Home):</b></label>
						  
							<input type="hidden" id="mother_tongue">

							<div id="m_tongue">

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_style_value();" value="Tagalog" class="custom-control-input" id="Tagalog" name="example1">
								    <label class="custom-control-label" for="Tagalog">Tagalog</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_style_value();" value="English" class="custom-control-input" id="English" name="example1">
								    <label class="custom-control-label" for="English">English</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_style_value();" value="Ilocano" class="custom-control-input" id="Ilocano" name="example1">
								    <label class="custom-control-label" for="Ilocano">Ilocano</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_style_value();" value="Ybanag" class="custom-control-input" id="Ybanag" name="example1">
								    <label class="custom-control-label" for="Ybanag">Ybanag</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_style_value();" value="Yogad" class="custom-control-input" id="Yogad" name="example1">
								    <label class="custom-control-label" for="Yogad">Yogad</label>
								</div>

								<div class="input-group mb-3">
									<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="iba_pa();" value="Iba pa" class="custom-control-input" id="iba_pa" name="example1">
								    <label class="custom-control-label" for="iba_pa">Iba pa:</label>
								      <input type="text" id="other_mother_tongue" placeholder="Other...">
								    </div>
								</div>

							</div>

						</div>

						<div class="form-group">
						  <label for="messenger_account"><b>Ethnicity:</b></label>
						  
							<input type="hidden" id="ethnicity_val">

							<div id="ethnicity">

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_ethnicity();" value="Gaddang" class="custom-control-input" id="Gaddang" name="example1">
								    <label class="custom-control-label" for="Gaddang">Gaddang</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_ethnicity();" value="Ifugao" class="custom-control-input" id="Ifugao" name="example1">
								    <label class="custom-control-label" for="Ifugao">Ifugao</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_ethnicity();" value="Ilocano" class="custom-control-input" id="Ilocano_e" name="example1">
								    <label class="custom-control-label" for="Ilocano_e">Ilocano</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_ethnicity();" value="Ybanag" class="custom-control-input" id="Ybanag_e" name="example1">
								    <label class="custom-control-label" for="Ybanag_e">Ybanag</label>
								</div>

								<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="get_ethnicity();" value="Yogad" class="custom-control-input" id="Yogad_e" name="example1">
								    <label class="custom-control-label" for="Yogad_e">Yogad</label>
								</div>

								<div class="input-group mb-3">
									<div class="custom-control custom-checkbox">
								    <input type="checkbox" onclick="iba_pa_e();" value="Iba pa" class="custom-control-input" id="iba_pa_e" name="example1">
								    <label class="custom-control-label" for="iba_pa_e">Iba pa:</label>
								      <input type="text" id="other_ethnicity" placeholder="Other...">
								    </div>
								</div>

							</div>

						</div>

						<div class="form-group">
						  <label for="contact"><b>Cellphone Number:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="contact" class="form-control text-capitalize" id="contact" >
						</div>

						<div class="form-group">
						  <label for="address"><b>Complete Address (House No., Street, Barangay, City/Municipality:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="address" class="form-control text-capitalize" id="address" >
						</div>

						<button class="btn <?php echo $default_color; ?>" onclick="back1();"><span class="fa fa-arrow-left"></span> Back</button>
						<button class="btn <?php echo $default_color; ?>" onclick="form2_validation();"><span class="fa fa-arrow-right"></span> Next</button>
						
						
					  </div>
					  <!-- FORM 2 PERSONAL INFORMATION -->

					  <!-- FORM 3 FAMILY INFORMATION -->
					  <div id="family" class="tab-pane">
					  	<br>
					  	<h4><b><span class="fa fa-users"></span>FAMILY INFORMATION</b></h4>

					  	<div class="card">

					  		<div class="card-header text-white w3-blue">
					  			<b>FATHER INFORMATION </b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">
								  <label for="lname"><b>Lastname:</b></label>
								  <input type="text" id="lname_f" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="fname"><b>Firstname:</b></label>
								  <input type="text" id="fname_f" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Middlename:</b></label>
								  <input type="text" id="mname_f" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Contact:</b></label>
								  <input type="text" id="contact_f" class="form-control">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Occupation:</b></label>
								  <input type="text" id="occupation_f" class="form-control text-capitalize">
								</div>

					  		</div>

					  	</div>

					  	<div class="card" style="margin-top: 10px;">

					  		<div class="card-header w3-bluetext-white">
					  			<b>MOTHER'S INFORMATION</b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">
								  <label for="lname"><b>Lastname:</b></label>
								  <input type="text" id="lname_m" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="fname"><b>Firstname:</b></label>
								  <input type="text" id="fname_m" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Middlename:</b></label>
								  <input type="text" id="mname_m" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Contact:</b></label>
								  <input type="text" id="contact_m" class="form-control">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Occupation:</b></label>
								  <input type="text" id="occupation_m" class="form-control text-capitalize">
								</div>

					  		</div>

					  	</div>

					  	<div class="card" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white">
					  			<b>GUARDIAN'S INFORMATION</b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">
								  <label for="lname"><b>Lastname:</b></label>
								  <input type="text" id="lname_g" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="fname"><b>Firstname:</b></label>
								  <input type="text" id="fname_g" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Middlename:</b></label>
								  <input type="text" id="mname_g" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Contact:</b></label>
								  <input type="text" id="contact_g" class="form-control">
								</div>

								<div class="form-group">
								  <label for="mname"><b>Occupation:</b></label>
								  <input type="text" id="occupation_g" class="form-control text-capitalize">
								</div>

								<div class="form-group">
								  <label for="relation_g"><b>Relation to the Learner:</b></label>
								  <input type="text" id="relation_g" class="form-control text-capitalize">
								</div>

					  		</div>

					  	</div>

					  	<div class="form-group" style="margin-top: 10px;">
						  <label for="siblings"><b>How many siblings do you have?</b></label>
							  <select class="form-control" id="siblings">
							  	<option value=""></option>
							  	<option value="None (Only Child)">None (Only Child)</option>
							  	<option value="1">1</option>
							  	<option value="2">2</option>
							  	<option value="3">3</option>
							  	<option value="4">4</option>
							  	<option value="5">5</option>
							  	<option value="More than 5">More than 5</option>
							  </select>
						</div>

						<button class="btn <?php echo $default_color; ?>" onclick="back2();"><span class="fa fa-arrow-left"></span> Back</button>
						<button class="btn <?php echo $default_color; ?>" onclick="next3();"><span class="fa fa-arrow-right"></span> Next</button>
						
					  </div>
					  <!-- FORM 3 FAMILY INFORMATION -->

					  <!-- FORM 4 OTHER INFO -->
					  <div id="other_info" class="tab-pane">
					  	<br>
					  	<h4><b><span class="fa fa-info"></span> OTHER INFORMATION</b></h4>

					  	<div class="card">

					  		<div class="card-header w3-blue text-white">
					  			<b>A. SCHOLASTIC INFORMATION </b> <br>
								Please encode your previous school based on the given levels below.
			  				</div>

					  		<div class="card-body">
					  			<div class="form-check-inline">
					  				 
					  				<select class="form-control" id="grade_id_s">
					  					<option selected="" value="">-- Select gradelevel --</option>
										    <?php echo get_grade_level($con,$with_hs,$with_gs); ?>
									</select>
								</div>
								<div class="form-check-inline">
					  				<input type="text" id="scholastic_value" class="form-control" placeholder="Enter School...">
								</div>
								<div class="form-check-inline">
					  				 <select class="form-control" id="school_year_val">
									    <option selected="" value="">-- Select School Year --</option>
									    <?php echo get_school_year($con); ?>
									  </select>
								</div>
								<div class="form-check-inline">
					  				 <button class="btn btn-primary" id="btn_save_scholastic" type="button" onclick="save_scholastic();">Save</button>
								</div>
								<hr>
								<div class="form-group">
									<ul class="list-group" id="load_scholastics"></ul>
								</div>	
								
					  		</div>

					  	</div>

					  	<div class="card" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white">
					  			<b>B. HEALTH RECORDS</b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">
								  <label for="with_medical_condition"><b>Do you have impairment, disability, or long term medical condition?</b></label>
								  <select class="form-control" id="with_medical_condition">
								  	<option value=""></option>
								  	<option value="Yes">Yes</option>
								  	<option value="No">No</option>
								  	<option value="Rather not to say">Rather not to say</option>
								  </select>
								</div>

								<div class="form-group">
								  <label for="medical_condition_affects"><b>If Yes, does your impairment, disability, or long-term medical condition affect your study?</b></label>
								  <select class="form-control" id="medical_condition_affects">
								  	<option value=""></option>
								  	<option value="Yes">Yes</option>
								  	<option value="No">No</option>
								  	<option value="Rather not to say">Rather not to say</option>
								  </select>
								</div>

								<div class="form-group">
								  <label for="medical_condition"><b>What is your impairment, disability, or long-term medical condition?
								Please leave this part if you opt not to declare your impairment, disability, or long-term medical condition.</b></label>
								  <input type="text" id="medical_condition" class="form-control text-capitalize" placeholder="Your answer.">
								</div>

					  		</div>

					  	</div>

					  	<div class="card with_payment" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white">
					  			<b>C. SCHOOL FEES</b>
			  				</div>

					  		<div class="card-body">

					  			<div class="table-responsive">   
									<table class="table table-bordered table-hover" id="tbl_school_fees"></table>
		    					</div>

					  		</div>

					  	</div>

					  	<div class="card with_payment" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white">
					  			<b>D. TERMS OF PAYMENT</b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">
								  <label for="term_id"><b>Select terms of payment:</b> <span class="text-danger">*</span></label>
								  <select class="form-control" id="term_id" oninput="load_terms_of_payment();" onchange="load_terms_of_payment();">
								    <?php echo get_terms($con); ?>
								  </select>
								</div>

					  			<div class="table-responsive">   
									<table class="table table-bordered table-hover" id="tbl_terms_of_payment"></table>
		    					</div>

					  		</div>

					  	</div>

					  	<div class="card" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white" id="support_doc">
					  			<b>E. SUPPORTING DOCUMENTS AND CREDENTIALS</b>
			  				</div>

					  		<div class="card-body">

					  			<input type="hidden" name="el" id="el">
								
					  			<form enctype="multipart/form-data" id="fupForm" >

					  				<input type="hidden" id="student_id" name="student_id">
					  				<input type="hidden" name="doc_id" id="doc_id">
					  				<input type="hidden" name="doc_type" id="doc_type">
								    <input type="hidden" id="f_name" name="f_name">

					  				<label for="with_medical_condition"><b>For Transferees: Please upload scanned copy the following supporting documents: </b> 

					  					<?php if(with_highschool($con) == 'YES'){ ?>

					  						<br>1. Form 138(Report Card)
						  					<br>2. ESC/QVR Certificate(if Recipient)
						  					<br>3. Birth Certificate
						  					<br>4. Certificate of GMRC . If you don't have a scanner, you may present these documents to the Registrar's Office upon payment of enrolment fees at the <?php echo $acronym; ?> Accounting Office.

										<?php }else{ ?>

											<br>1. Form 138(Report Card)
						  					<br>2. Birth Certificate
						  					<br>3. Certificate of GMRC . If you don't have a scanner, you may present these documents to the Registrar's Office upon payment of enrolment fees at the <?php echo $acronym; ?> Accounting Office.

										<?php } ?>

					  				</label>

					  				<div class="form-group">
									  <label for="txt_doc_name"><b>Supporting file </b></label>
										  <select class="form-control" id="txt_doc_name" name="txt_doc_name">
										  	<option value=""></option>

										  	<?php if(with_highschool($con) == 'YES'){ ?>
										  	<option value="ESC/QVR Certificate(if Recipient)">ESC/QVR Certificate(if Recipient)</option>
										    <?php } ?>

										  	<option value="Form 138(Report Card">Form 138(Report Card)</option>
										  	<option value="Birth Certificate">Birth Certificate</option>
										  	<option value="Certificate of GMRC">Certificate of GMRC</option>
										  </select>
									</div>

									<div class="form-group">
								        <input type="file" class="form-control" id="file" name="file" style="display: none;"/>
								    </div>
								    <div class="form-group">
								    	<div class="btn-group">
									    	<button type="button" class="btn <?php echo $default_color; ?> btn-small" id="btn-select" onclick="$('#file').click();"><span class="fa fa-upload"></span> Document/Picture</button>
									    </div>
								    </div>
								    
								    <div class="text-center" id="preview_file"></div>
								    <div class="form-group">
								    	<input type="submit" name="submit" id="btn_ann" class="btn <?php echo $default_color; ?>  btn-block submitBtn" value="Attach"/>
								    </div>
				    				
				    				<div id="load_pend"></div>

					  			</form>

					  			<div id="load_files">
					  				
					  			</div>

					  		</div>

					  	</div>
					  	
					  	<br>
					  	
					  		<button class="btn <?php echo $default_color; ?>" onclick="back3();"><span class="fa fa-arrow-left"></span> Back</button>
					  	<button class="btn <?php echo $default_color; ?>" onclick="next4();"><span class="fa fa-arrow-right"></span> Next</button>
					  
						
					  </div>
					   <!-- FORM 4 OTHER INFO -->
					   <div id="other_info2" class="tab-pane">

					   	<div class="card" style="margin-top: 10px;">

					  		<div class="card-header w3-blue text-white" id="other_information">
					  			<b>F. KTMS Beyond the Classroom : Survey on Learning Modality Preference for SY <?php echo get_school_year_active($con); ?></b>
			  				</div>

					  		<div class="card-body">

					  			<div class="form-group">

							  <label><b>1. How does your child go to school? Choose all that applies.:</b></label>
							  
								<input type="hidden" id="go_to_school_val">

								<div id="go_to_school">

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_gotoschool();" value="Walking" class="custom-control-input" id="Walking" name="gotoschool">
									    <label class="custom-control-label" for="Walking">Walking</label>
									</div>

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_gotoschool();" value="public commute (land/water)" class="custom-control-input" id="public commute (land/water)" name="gotoschool">
									    <label class="custom-control-label" for="public commute (land/water)">public commute (land/water)</label>
									</div>

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_gotoschool();" value="family-owned vehicle" class="custom-control-input" id="family-owned vehicle" name="gotoschool">
									    <label class="custom-control-label" for="family-owned vehicle">family-owned vehicle</label>
									</div>

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_gotoschool();" value="school service" class="custom-control-input" id="school" name="gotoschool">
									    <label class="custom-control-label" for="school">school service</label>
									</div>

								</div>

						</div>

						<div class="form-group">

							<label><b>2. How many of your household members (including the enrollee) are studying in School Year 2020-2021? Please specify each.:</b></label>
							<div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <select class="form-control" id="grade_id_h">
							    <?php echo get_grade_level($con,$with_hs,$with_gs); ?>
							  </select>
							  </div>
							  <input type="number" id="member_value" class="form-control" placeholder="Enter Value...">
							  <div class="input-group-append">
							    <button class="btn btn-primary" id="btn_save_member" type="button" onclick="save_household();">Save</button>
							  </div>

							</div>
								
						</div>
						<div class="form-group">
							<ul class="list-group" id="load_household_members"></ul>
						</div>

						<div class="form-group">
							 <!-- <label><b>Teacher-partner at home *:</b></label> -->
							  <label><b>3. Who among the household members can provide instructional support to  the child's distance learning? Choose all that applies.:</b></label>

								<input type="hidden" id="teacher_home_val">

								<div id="teacher_home">

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_teacher_home();" value="Parents/Guardians" class="custom-control-input" id="Parents/Guardians" name="tp">
									    <label class="custom-control-label" for="Parents/Guardians">Parents/Guardians</label>
									</div>

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_teacher_home();" value="Elder siblings" class="custom-control-input" id="Elder siblings" name="tp">
									    <label class="custom-control-label" for="Elder siblings">Elder siblings</label>
									</div>


									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_teacher_home();" value="Grandparents" class="custom-control-input" id="Grandparents" name="tp">
									    <label class="custom-control-label" for="Grandparents">Grandparents</label>
									</div>

									<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="get_teacher_home();" value="Extended members of the family" class="custom-control-input" id="extended members of the family" name="tp">
									    <label class="custom-control-label" for="extended members of the family">Extended members of the family</label>
									</div>

									<div class="input-group mb-3">

										<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="iba_pa_th();" value="" class="custom-control-input" id="iba_pa_th" name="tp">
									    <label class="custom-control-label" for="iba_pa_th">Other:</label>
									      <input type="text" id="other_th" placeholder="Other...">
									    </div>

									</div>

								</div>

						</div>

						<div class="form-group">

								  <label><b>4. What devices are available at home that the learner can use for learning? Check all that applies.</b></label>
								  
									<input type="hidden" id="gadgets_val">

									<div id="gadgets">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Desktop" class="custom-control-input" id="g1" name="example1">
										    <label class="custom-control-label" for="g1">Desktop</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Laptop" class="custom-control-input" id="g2" name="example1">
										    <label class="custom-control-label" for="g2">Laptop</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Tablet" class="custom-control-input" id="g3" name="example1">
										    <label class="custom-control-label" for="g3">Tablet</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Smart phone" class="custom-control-input" id="g4" name="example1">
										    <label class="custom-control-label" for="g4">Smart phone</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="None of the above" class="custom-control-input" id="g5" name="example1">
										    <label class="custom-control-label" for="g5">None of the above</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_g();" value="Iba pa" class="custom-control-input" id="iba_pa_g" name="example1">
										    <label class="custom-control-label" for="iba_pa_g">Others:</label>
										      <input type="text" id="other_gadget" placeholder="Other...">
										    </div>
										</div>

									</div>

								</div>

								<div class="form-group">
										
									  <label><b> 5. Do you have a way to connect to the internet?:</b></label>
									  
										<input type="hidden" id="connected_to_internet_val">

										<div id="connected_to_internet">

											<div class="custom-control custom-radio">
											    <input type="radio" onclick="get_is_connected();" value="YES" class="custom-control-input" id="YES" name="internet_conn">
											    <label class="custom-control-label" for="YES">YES</label>
											</div>

											<div class="custom-control custom-radio">
											    <input type="radio" onclick="get_is_connected();" value="NO" class="custom-control-input" id="NO" name="internet_conn">
											    <label class="custom-control-label" for="NO">NO (if no please proceed to 8.)</label>
											</div>

										</div>

								</div>

								<div class="form-group">
									
								  <label><b>6. What type of internet connection do you have at home?:</b></label>
								  
									<input type="hidden" id="internet_val">

									<div id="internet">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="No internet connection" class="custom-control-input" id="i1" name="example1">
										    <label class="custom-control-label" for="i1">No internet connection</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile data (prepaid)" class="custom-control-input" id="i2" name="example1">
										    <label class="custom-control-label" for="i2">Mobile data (prepaid)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile data (post paid or plan)" class="custom-control-input" id="i3" name="example1">
										    <label class="custom-control-label" for="i3">Mobile data (post paid or plan)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile broadband" class="custom-control-input" id="i4" name="example1">
										    <label class="custom-control-label" for="i4">Mobile broadband</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Cable internet" class="custom-control-input" id="i5" name="example1">
										    <label class="custom-control-label" for="i5">Cable internet</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Fiber internet" class="custom-control-input" id="i6" name="example1">
										    <label class="custom-control-label" for="i6">Fiber internet</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_i();" value="Iba pa" class="custom-control-input" id="iba_pa_i" name="example1">
										    <label class="custom-control-label" for="iba_pa_i">Others:</label>
										      <input type="text" id="other_i" placeholder="Other...">
										    </div>
										</div>

									</div>

								</div>

								<div class="form-group">

								  <label for="internet_rate"><b>7. Rate your internet connectivity from 1 to 5 (5 as the strongest 1 as the weakest):</b></label>

								  <select class="form-control" id="internet_rate">

								  	<option value=""></option>
								  	<option value="5">5</option>
								  	<option value="4">4</option>
								  	<option value="3">3</option>
								  	<option value="2">2</option>
								  	<option value="1">1</option>

								  </select>

								</div>

								<div class="form-group">
									
								  <label><b> 8. What setup do you prefer for your children for this coming school <?php echo get_school_year_active($con); ?>?</b></label>
								  
									<input type="hidden" id="learning_dmode">

									<div id="delivery_mode">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)" class="custom-control-input" id="8_1" name="learning_delivery">
										    <label class="custom-control-label" for="8_1">Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Purely online classes (all classes will be done online)" class="custom-control-input" id="8_2" name="learning_delivery">
										    <label class="custom-control-label" for="8_2">Purely online classes (all classes will be done online)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)" class="custom-control-input" id="8_3" name="learning_delivery">
										    <label class="custom-control-label" for="8_3">Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_p();" value="" class="custom-control-input" id="iba_pa_p" name="learning_delivery">
										    <label class="custom-control-label" for="iba_pa_p">Others:</label>
										      <input type="text" id="other_p" placeholder="Other...">
										    </div>
										</div>

									</div>

								</div>

								<div class="form-group">
									
								  <label><b> 9. Are you interested to enroll in blended PMC classes? (for Elementary learners only)</b></label>
								  
									<input type="hidden" id="blended_pmc_val">

									<div id="blended_pmc">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="Yes" class="custom-control-input" id="pmc1" name="is_pmc">
										    <label class="custom-control-label" for="pmc1">Yes</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="No" class="custom-control-input" id="pmc2" name="is_pmc">
										    <label class="custom-control-label" for="pmc2">No</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="Maybe" class="custom-control-input" id="pmc3" name="is_pmc">
										    <label class="custom-control-label" for="pmc3">Maybe</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_pmc();" value="" class="custom-control-input" id="iba_pa_pmc" name="is_pmc">
										    <label class="custom-control-label" for="iba_pa_pmc">Others:</label>
										      <input type="text" id="other_pmc" placeholder="Other...">
										    </div>
										</div>

									</div>

								</div>

								<div class="form-group" style="display:none;">
									
								  <label><b> 9. Financial capability *:</b></label>
								  
									<input type="hidden" id="financial_capability_val">

									<div id="financial_capability">

										<div class="custom-control custom-radio">
										    <input type="radio" onclick="get_financial_capability();" value="Can settle previous account" class="custom-control-input" id="Can settle previous account" name="fc">
										    <label class="custom-control-label" for="Can settle previous account">Can settle previous account</label>
										</div>

										<div class="custom-control custom-radio">
										    <input type="radio" onclick="get_financial_capability();" value="Can pay the current registration fee" class="custom-control-input" id="Can pay the current registration fee" name="fc">
										    <label class="custom-control-label" for="Can pay the current registration fee">Can pay the current registration fee</label>
										</div>

										<div class="custom-control custom-radio">
										    <input type="radio" onclick="get_financial_capability();" value="Can pay less than the current registration fee" class="custom-control-input" id="Can pay less than the current registration fee" name="fc">
										    <label class="custom-control-label" for="Can pay less than the current registration fee">Can pay less than the current registration fee</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-radio">
										    <input type="radio" onclick="iba_pa_fc();" value="Iba pa" class="custom-control-input" id="iba_pa_fc" name="fc">
										    <label class="custom-control-label" for="iba_pa_fc">Others:</label>
										      <input type="text" id="other_fc" placeholder="Others...">
										    </div>
										</div>

									</div>

								</div>
						
								<div class="form-group">
									
								  <label><b>9. What are the challenges that may affect your child's learning process through distance education? Choose all that applies:</b></label>
								  
									<input type="hidden" id="affects_DE_val">

									<div id="affects_DE">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Lack of available gadgets/ equipment" class="custom-control-input" id="log" name="affects">
										    <label class="custom-control-label" for="log">Lack of available gadgets/ equipment</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Insufficient load/data allowance" class="custom-control-input" id="i" name="affects">
										    <label class="custom-control-label" for="i">Insufficient load/data allowance</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Unstable mobile/internet connection" class="custom-control-input" id="u" name="affects">
										    <label class="custom-control-label" for="u">Unstable mobile/internet connection</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Existing health condition's" class="custom-control-input" id="ex" name="affects">
										    <label class="custom-control-label" for="ex">Existing health condition's</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Difficulty in independent learning" class="custom-control-input" id="di" name="affects">
										    <label class="custom-control-label" for="di">Difficulty in independent learning</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Conflict with other activities(e.g house chores)" class="custom-control-input" id="cw" name="affects">
										    <label class="custom-control-label" for="cw">Conflict with other activities(e.g house chores)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="High electrical consumptions" class="custom-control-input" id="he" name="affects">
										    <label class="custom-control-label" for="he">High electrical consumptions</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Distractions(i.e.| social media | noise from community/neighbor)" class="custom-control-input" id="de" name="affects">
										    <label class="custom-control-label" for="de">Distractions(i.e.,social media, noise from community/neighbor)</label>
										</div>

										<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_affects();" value="Iba pa" class="custom-control-input" id="iba_pa_affects" name="example1">
										    <label class="custom-control-label" for="iba_pa_affects">Others:</label>
										      <input type="text" id="other_affects" placeholder="Other...">
										    </div>
										</div>

									</div>

								</div>

								<div class="form-group">
									<label><b>10. Are there other suggestions and concerns that you may have?</b></label>
									<textarea class="form-control" rows="3" id="suggestions"></textarea>
								</div>
								
					  		</div>

					  	</div>

					  	<ul class="list-group" style="margin-top: 10px;">
						  <li class="list-group-item list-group-item-success"><b> I hereby certify that the above information given are true and correct to the best of my knowledge and I allow the <?php echo $school_name; ?> to use my child's details to create and/or update his/her learner profile in the Online Registration System. The information herein shall be treated Confidential in compliance with the Data Privacy Act of 2012. <span class="text-danger">*</span></b></li>
						  <li class="list-group-item list-group-item-action" style="size: 12px;">
						  	<div class="custom-control custom-switch">
						    <input type="checkbox" class="custom-control-input" id="switch1" onclick="say_yes();">
						    <label class="custom-control-label" for="switch1">Yes</label>
							</div>
							</li>
						</ul>

					   	<br>
					  	
					  	<button class="btn <?php echo $default_color; ?>" onclick="back4();"><span class="fa fa-arrow-left"></span> Back</button>
						<button class="btn <?php echo $default_color; ?>" id="btn_save" onclick="save_registration();"><span class="fa fa-save"></span> Submit</button>

					   </div>

					  <br>
				</form>
				</div>

			  </div>

			</div>

		  </div>

		<div class="col-sm-1"></div>

	</div>

</div>

</body>

<?php include_once('scripts.php'); ?>


<script>

$(document).ready(function() {

<?php if($with_payment == 'NO'){  ?>
	
	$('.with_payment').css('display','none');
	$('#support_doc').html('<b>C. SUPPORTING DOCUMENTS AND CREDENTIALS</b>');
	$('#other_information').html('<b>D. HOUSEHOLD CAPACITY AND ACCESS TO DISTANCE LEARNING</b>');
	
<?php } ?>

    $("#email").keyup(function(){

        var email = $("#email").val();

        if(email != 0)
        {
            if(isValidEmailAddress(email))
            {
            	
               	document.getElementById('next1').disabled = false;
            	$("#validEmail").removeClass('text-danger');
                $('#validEmail').addClass('text-success');
                $("#validEmail").text('Valid email');

            } else {
                document.getElementById('next1').disabled = true;
                $('#validEmail').removeClass('text-success');
                $("#validEmail").addClass('text-danger');
                $("#validEmail").text('Invalid email.');
              
               //showToast.show("Invalid Email",1000);
            }
        } else {
        	document.getElementById('next1').disabled = false;
             $("#validEmail").text('');        
        }

    });

});
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

$(document).ready(function(e){

		document.getElementById('btn_save').disabled = true
		document.getElementById('other_mother_tongue').disabled = true
		document.getElementById('other_ethnicity').disabled = true
		document.getElementById('other_gadget').disabled = true
		document.getElementById('other_i').disabled = true
		document.getElementById('other_p').disabled = true
		document.getElementById('other_fc').disabled = true
		document.getElementById('other_th').disabled = true
		document.getElementById('other_affects').disabled = true

	    $("#fupForm").on('submit', function(e){
	        e.preventDefault();
	        var a = $("#el").val();

	        var txt_doc_name = $('#txt_doc_name').val();

	        if(txt_doc_name == ''){

	        	$('#txt_doc_name').focus();

	        }else{

	        	$.ajax({
	            type: 'POST',
	            url: 'upload_file.php',
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
	            beforeSend: function(){
	                $('.submitBtn').attr("disabled","disabled");
	                if (doc_id.value == "")
					{
						$("#load_pend").prepend('<div class="well w3-blue">Loading...</div>');
					}
					else
					{
						$("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
					}
	            },
	            success: function(msg){
	                $('.statusMsg').html('');
	                if(msg == 1){
	                    
	                	$('#doc_id').val('');
						$('#doc_type').val('');
						$('#f_name').val('');
						$('#txt_doc_name').val('');

	                    $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('New File Succefully Uploaded!',3000);
					    },2000);
					    load_supporting_files();
	                    //display_documents();
						$("#load_pend").html('');
						$("#preview_file").html('');
	                }else if(msg == 2){
	                	
	                	$('#doc_id').val('');
						$('#doc_type').val('');
						$('#f_name').val('');
						$('#txt_doc_name').val('');

	                    $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('File Succefully Updated!',3000);
					    },2000);
					    load_supporting_files();
	                    //display_documents();
						$("#load_pend").html('');
						$("#preview_file").html('');
						$('#btn_cupdate').css('display','none');
	                }
	                else{
	                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
	                }
	                $('#fupForm').css("opacity","");
	                $(".submitBtn").removeAttr("disabled");
	            }
	        });

	        }

	        
	    });
	    
	    //file type validation
	    $("#file").change(function() {
	        var file = this.files[0];
	        var imagefile = file.type;
	        var form_data = new FormData();
	        //alert(imagefile);
	        $('#doc_type').val(imagefile.trim(""));
	        var match= ["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","image/jpeg","image/png","image/jpg", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.ms-powerpoint", "text/plain"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6]) || (imagefile==match[7]) || (imagefile==match[8]) || (imagefile==match[9]) || (imagefile==match[10]))){
	            alert('INVALID FILE!');
	            $("#file").val('');
	            return false;
	        }else{

				form_data.append("file", document.getElementById('file').files[0]);

	        	$.ajax({
			      url:"upload_file_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			        $('#preview_file').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data)
			      {
			       if(data == 404){

					swal("Warning", "Song Already Exists.", "warning");
					$('#preview_file').html("<label class='text-danger col-sm-12'>File Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");  

			       }else{
			       	  // $('#btn_remove_file').addClass('display-none');
			       	  $('#file_update').html('');
			          $('#preview_file').html(data);
			          var file_name = $('#file_name').val();
			          $('#f_name').val(file_name);

			       }	
			      
			      }
			      });


	        }
	    });
	});

</script>

</html>