<html>

    <?php
    include_once('../config.php');
    $sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

    if (isset($_REQUEST['sid'])) {
        $student_id = $_REQUEST['sid'];
    }
    else
    {   
        echo "No Student ID Selected !!!";
    }

    if (mysqli_num_rows($sql)>0) {

        $row = mysqli_fetch_assoc($sql);

        $default_color = ' w3-'.$row['default_color'].' ';
        $school_name = ''.$row['company_name1'].'';
        $acronym = ''.$row['acronym'].'';
        $with_payment = ''.$row['with_payment'].'';
        $logo = $row['logo'];
        $cid = $row['companyid'];

        $company_name = '<b class="img-responsive" style="font-size:25px;"> '.$row['company_name1'].' </b>';
        $address = '<small style="font-size:30px;"> '.$row['address'].' </small>';
        $default_color = ' w3-'.$row['default_color'].' ';

        $header = '<img width="100px;" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
        $header_bg = ''.$row['header_bg'].'';
        $navbar_bg = $row['navbar_bg'];
        $school_name2 = ''.$row['company_name2'].'';

    }?>

    <?php
        $sql=mysqli_query($con,"SELECT * FROM tbl_school_year WHERE is_active=1");
        $data=mysqli_fetch_assoc($sql);
        $school_yearId=$data['id'];
    ?>

    <head>
        <title><?php echo $school_name; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link href="../css/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
        <link rel="stylesheet" type="text/css" href="../css/w3.css">
        <link rel="stylesheet" type="text/css" href="../css/animate.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.css">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="../css/showToast.css">
    </head>
   
    <body>
        
        <div class="">
            <img src="../img/print_header.png" class="img-fluid">
        </div>
        <div class="container-fluid">
            <h3 class="text-center font-weight-bold">STUDENT INFORMATION</h3>
            <div class="text-center"><?php echo date('F d, Y H:i:s')?></div>

            <?php

               $sql = mysqli_query($con,"SELECT t1.*,t2.`grade` AS grade_name, t3.`category` FROM tbl_students t1 LEFT JOIN tbl_grade t2 ON t1.`grade` = t2.`ID` LEFT JOIN tbl_enrolled_students t3 ON t1.`student_id` = t3.`student_id` where t1.`student_id` = '$student_id'");

               if (mysqli_num_rows($sql)>0) {
                  $row = mysqli_fetch_assoc($sql);

                  $name = $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
               } 
            ?>

            <ul class="list-group mt-5">
               <li class="list-group-item d-flex justify-content-between list-group-item-success">
                  <b>PERSONAL INFORMATION</b>
               </li>
              
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  Category:
                  <b><?php echo strtoupper($row['category']); ?></b>
                </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Name:
                  <b><?php echo strtoupper($name); ?></b>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  Grade & Section:
                  <b><?php echo strtoupper($row['grade_name']) .' - '. strtoupper($row['section']); ?></b>
               </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  Gender:
                  <b><?php echo strtoupper($row['gender']); ?></b>
               </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  Place of birth:
                  <b><?php echo strtoupper($row['place_of_birth']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Birth date:
                  <b><?php echo strtoupper($row['birthday']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Age:
                  <b><?php echo strtoupper($row['age']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Address:
                  <b><?php echo strtoupper($row['address']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Contact:
                  <b><?php echo strtoupper($row['contact']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Religion:
                  <b><?php echo strtoupper($row['religion']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Citizenship:
                  <b><?php echo strtoupper($row['citizenship']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Mother Tongue(Dialect Use at Home):
                  <b><?php echo strtoupper($row['mother_tongue']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Ethnicity:
                  <b><?php echo strtoupper($row['ethnicity']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Email:
                  <b><?php echo strtoupper($row['email']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Facebook:
                  <b><?php echo strtoupper($row['fb_account']); ?></b>
               </li>
            </ul>


            <ul class="list-group mt-5">
               <li class="list-group-item d-flex justify-content-between list-group-item-success">
                  <b>FAMILY INFORMATION</b>
               </li>

            <?php  

               $father= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Father' and student_id = '$student_id'");

               if (mysqli_num_rows($father)>0) {
                    $f = mysqli_fetch_assoc($father);

                    $fname = $f['lastname'].', '.$f['firstname'].' '.$f['mi'];
            ?>

               <li class="list-group-item d-flex justify-content-between list-group-item-info">
                  <b>Father Information</b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Name:
                  <b><?php echo strtoupper($fname); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Address:
                  <b><?php echo strtoupper($f['address']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Contact:
                  <b><?php echo strtoupper($f['contact']); ?></b>
               </li>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                  Occupation:
                  <b><?php echo strtoupper($f['occupation']); ?></b>
               </li>

         <?php }else{

            echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
                     <b>Father Information</b>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                     <span class="badge badge-warning">No data.</span>
                  </li>';
            } 

         ?>


         <?php  

            $mother= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Mother' and student_id = '$student_id'");

            if (mysqli_num_rows($mother)>0) {
                $m = mysqli_fetch_assoc($mother);

                $mname = $m['lastname'].', '.$m['firstname'].' '.$m['mi'];
         ?>

            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>Mother Information</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Name:
               <b><?php echo strtoupper($mname); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Address:
               <b><?php echo strtoupper($m['address']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Contact:
               <b><?php echo strtoupper($m['contact']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Occupation:
               <b><?php echo strtoupper($m['occupation']); ?></b>
            </li>

         <?php }else{
            echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
                     <b>Mother Information</b>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                     <span class="badge badge-warning">No data.</span>
                  </li>';
            } 
         ?>

         <?php  

            $guardian= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Guardian' and student_id = '$student_id'");

            if (mysqli_num_rows($guardian)>0) {
                $g = mysqli_fetch_assoc($guardian);

                $gname = $g['lastname'].', '.$g['firstname'].' '.$g['mi'];
         ?>>
            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>Guardian Information</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Name:
               <b><?php echo strtoupper($gname); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Address:
               <b><?php echo strtoupper($g['address']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Contact:
               <b><?php echo strtoupper($g['contact']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Occupation:
               <b><?php echo strtoupper($g['occupation']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Relationship:
               <b><?php echo strtoupper($g['relationship']); ?></b>
            </li>

         <?php }else{
            echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
                     <b>Guardian Information</b>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                     <span class="badge badge-warning">No data.</span>
                  </li>';
            } 
         ?>

            <li class="list-group-item d-flex justify-content-between align-items-center">
               Siblings:
               <b><?php echo strtoupper($row['siblings']); ?></b>
            </li>

         </ul>

         <ul class="list-group mt-5">
            <li class="list-group-item d-flex justify-content-between list-group-item-success">
               <b>OTHER INFORMATION</b>
            </li>

            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>A. Scholastic Information</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Kindergarten:
               <b><?php echo strtoupper($row['si_kindergarten']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Grade School:
               <b><?php echo strtoupper($row['si_gradeschool']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Junior High School:
               <b><?php echo strtoupper($row['si_junior_hs']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Senior High School:
               <b><?php echo strtoupper($row['si_junior_hs']); ?></b>
            </li>

            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>B. Health Records</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Do you have impairment, disability, or long term medical condition?
               <b><?php echo strtoupper($row['with_medical_condition']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Does your impairment, disability, or long-term medical condition affect your study?
               <b><?php echo strtoupper($row['medical_condition_affects']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               What is your impairment, disability, or long-term medical condition?
               <b><?php echo strtoupper($row['medical_condition']); ?></b>
            </li>

         <?php

            $docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$student_id'");

            if (mysqli_num_rows($docs)>0) {
               $num = 0;
               while ($d = mysqli_fetch_assoc($docs))  
               { 
                  $num++; 
         ?>
               
            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>C. Supporting Documents and Credentials</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               <b><a href="<?php echo '../'.$d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
               </b>
               <b><a download="" href="<?php echo '../'.$d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> </a></b>
            </li>


         <?php }

            }

         ?>

         </ul>

         <ul class="list-group mt-5">
            <li class="list-group-item d-flex justify-content-between list-group-item-success">
               <b>Survey Information</b>
               <b><button type="button" class="close" data-dismiss="modal">&times;</button></b>
            </li>

         <?php

            $sql = mysqli_query($con,"SELECT * FROM tbl_enrolled_students WHERE student_id = '$student_id'");

             if (mysqli_num_rows($sql)>0) {
                 $row = mysqli_fetch_assoc($sql);

         ?>

            <li class="list-group-item d-flex justify-content-between list-group-item-info">
               <b>HOUSEHOLD CAPACITY AND ACCESS TO DISTANCE LEARNING</b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               1. How does your child go to school? Choose all that applies.:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo $row['go_to_school']; ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               2. How many of your household members (including the enrollee) are studying in School Year 2020-2021? Please specify each.:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php //echo $row['']; ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               3. Who among the household members can provide instructional support to the child's distance learning? Choose all that applies.:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['teacher_partner_home']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               4. What devices are available at home that the learner can use for learning? Check all that applies.
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['available_gadgets']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               5. Do you have a way yo connect to the internet?:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['connected_to_internet']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               6. What type of internet connection do you have at home?:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['type_of_internet']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               7. Rate your internet connectivity from 1 to 5 (5 as the strongest 1 as the weakest):
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['internet_rate']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               8. What learning delivery/modality do you prefer for your child?
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['learning_delivery_mode']); ?></b>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               9. What are the challenges that may affect your child's learning process through distance education? Choose all that applies:
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               Answer: <b><?php echo strtoupper($row['affects_DE']); ?></b>
            </li>

         <?php 
            }
         ?>

         </ul>
      </div> 
    </body>
</html>