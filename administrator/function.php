<?php 
include_once("../config.php");
include_once("classes.php");
$action = mysqli_real_escape_string($con,$_POST['action']);
$admin_id=$_SESSION['admin_pane'];
$date_time = date('Y-m-d h:i:s');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");


switch ($action) {

	//Term
	case 'load_terms':
		load_terms($con);
	break;

	case 'update_term':
		$id = mysqli_real_escape_string($con,$_POST['t_id']);
		$term = mysqli_real_escape_string($con,$_POST['term_name']);
		$t_status = mysqli_real_escape_string($con,$_POST['t_status']);
		update_term($con,$id,$term,$t_status);
	break;
	//Term
	
	case 'update_password':
		$old_pass = mysqli_real_escape_string($con,$_POST['old_pass']);
		$retype_pass = mysqli_real_escape_string($con,$_POST['retype_pass']);
		update_password($con,$old_pass,$retype_pass);
	break;
	
	//edit stduent
	

	case 'load_fam':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		load_fam($con,$student_id);
	break;
	
	case 'load_supporting_files':
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		load_supporting_files($con,$student_id);
	break;

	case 'delete_doc':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		$path = mysqli_real_escape_string($con,$_POST['path']);
		delete_doc($con,$id,$path);
	break;

	case 'save_member':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		$member = mysqli_real_escape_string($con,$_POST['member']);
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		save_member($con,$grade_id,$member,$student_id);
	break;

	case 'remove_member':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		remove_member($con,$id);
	break;

	case 'load_household_members':
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		load_household_members($con,$student_id,$grade_id);
	break;

	case 'update_student_info':

		$s_id = mysqli_real_escape_string($con,$_POST['s_id']);
		$category = mysqli_real_escape_string($con,$_POST['category']);
		$lname = mysqli_real_escape_string($con,$_POST['lname']);
		$fname = mysqli_real_escape_string($con,$_POST['fname']);
		$mname = mysqli_real_escape_string($con,$_POST['mname']);
		$suffix = mysqli_real_escape_string($con,$_POST['suffix']);
		$email = mysqli_real_escape_string($con,$_POST['email']);
		$fb_account = mysqli_real_escape_string($con,$_POST['fb_account']);
		$lrn = mysqli_real_escape_string($con,$_POST['lrn']);
		$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
		$age = mysqli_real_escape_string($con,$_POST['age']);
		$place_of_birth = mysqli_real_escape_string($con,$_POST['place_of_birth']);
		$gender = mysqli_real_escape_string($con,$_POST['gender']);
		$religion = mysqli_real_escape_string($con,$_POST['religion']);
		$citizenship = mysqli_real_escape_string($con,$_POST['citizenship']);
		$mother_tongue = mysqli_real_escape_string($con,$_POST['mother_tongue']);
		$ethnicity_val = mysqli_real_escape_string($con,$_POST['ethnicity_val']);
		$contact = mysqli_real_escape_string($con,$_POST['contact']);
		$address = mysqli_real_escape_string($con,$_POST['address']);

		$lname_f = mysqli_real_escape_string($con,$_POST['lname_f']);
		$fname_f = mysqli_real_escape_string($con,$_POST['fname_f']);
		$mname_f = mysqli_real_escape_string($con,$_POST['mname_f']);
		$contact_f = mysqli_real_escape_string($con,$_POST['contact_f']);
		$occupation_f = mysqli_real_escape_string($con,$_POST['occupation_f']);
		$lname_m = mysqli_real_escape_string($con,$_POST['lname_m']);
		$fname_m = mysqli_real_escape_string($con,$_POST['fname_m']);
		$mname_m = mysqli_real_escape_string($con,$_POST['mname_m']);
		$contact_m = mysqli_real_escape_string($con,$_POST['contact_m']);
		$occupation_m = mysqli_real_escape_string($con,$_POST['occupation_m']);
		$lname_g = mysqli_real_escape_string($con,$_POST['lname_g']);
		$fname_g = mysqli_real_escape_string($con,$_POST['fname_g']);
		$mname_g = mysqli_real_escape_string($con,$_POST['mname_g']);
		$contact_g = mysqli_real_escape_string($con,$_POST['contact_g']);
		$occupation_g = mysqli_real_escape_string($con,$_POST['occupation_g']);
		$relation_g = mysqli_real_escape_string($con,$_POST['relation_g']);
		$siblings = mysqli_real_escape_string($con,$_POST['siblings']);

		$with_medical_condition = mysqli_real_escape_string($con, $_POST['with_medical_condition']);
		$medical_condition_affects = mysqli_real_escape_string($con, $_POST['medical_condition_affects']);
		$medical_condition = mysqli_real_escape_string($con, $_POST['medical_condition']);

		$other_mother_tongue = mysqli_real_escape_string($con, $_POST['other_mother_tongue']);
		$other_ethnicity = mysqli_real_escape_string($con, $_POST['other_ethnicity']);

		$go_to_school_val = mysqli_real_escape_string($con, $_POST['go_to_school_val']);
		$teacher_home_val = mysqli_real_escape_string($con, $_POST['teacher_home_val']);
		$other_th = mysqli_real_escape_string($con, $_POST['other_th']);
		$gadgets_val = mysqli_real_escape_string($con, $_POST['gadgets_val']);
		$other_gadget = mysqli_real_escape_string($con, $_POST['other_gadget']);
		$connected_to_internet_val = mysqli_real_escape_string($con, $_POST['connected_to_internet_val']);
		$internet_val = mysqli_real_escape_string($con, $_POST['internet_val']);
		$other_i = mysqli_real_escape_string($con, $_POST['other_i']);
		$internet_rate = mysqli_real_escape_string($con, $_POST['internet_rate']);
		$learning_dmode = mysqli_real_escape_string($con, $_POST['learning_dmode']);
		$other_p = mysqli_real_escape_string($con, $_POST['other_p']);
		$affects_DE_val = mysqli_real_escape_string($con, $_POST['affects_DE_val']);
		$other_affects = mysqli_real_escape_string($con, $_POST['other_affects']);

		$blended_pmc_val = mysqli_real_escape_string($con, $_POST['blended_pmc_val']);
		$other_pmc = mysqli_real_escape_string($con, $_POST['other_pmc']);
		$suggestions = mysqli_real_escape_string($con, $_POST['suggestions']);

	update_student_info($con,$s_id,$category,$lname,$fname,$mname,$suffix,$email,$fb_account,$lrn,$bdate,$age,$place_of_birth,$gender,$religion,$citizenship,$mother_tongue,$ethnicity_val,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g ,$contact_g,$occupation_g,$relation_g,$siblings,$with_medical_condition,$medical_condition_affects,$medical_condition,$other_mother_tongue,$other_ethnicity,$go_to_school_val,$teacher_home_val,$other_th,$gadgets_val,$other_gadget,$connected_to_internet_val,$internet_val,$other_i,$internet_rate,$learning_dmode,$other_p,$affects_DE_val,$other_affects,$blended_pmc_val,$other_pmc,$suggestions);

	break;

	//edit student

	case 'load_pass':
		load_pass($con);
	break;
	
	case 'load_pic':

		$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

		if (mysqli_num_rows($sql)>0) {

			$row = mysqli_fetch_assoc($sql);

			$pic = $row['logo'];

		}
		
		load_pic($con,$pic);
	break;

	case 'gen_id':
		gen_id($con);
	break;

	case 'return_age':
		$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
		return_age($con,$bdate);
	break;

	case 'remove_file':
		$file_name = mysqli_real_escape_string($con,$_POST['file_name']);
		remove_file($con,$file_name);
	break;

	case 'load_enrollees':
		$search = mysqli_real_escape_string($con,$_POST['search']);
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id']);
		$school_year = mysqli_real_escape_string($con,$_POST['school_year']);
		load_enrollees($con,$search,$grade_id,$school_year);
	break;

	case 'load_student_info':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		load_student_info($con,$student_id);
	break;
	
	case 'load_payment_info':
		$student_id_2 = mysqli_real_escape_string($con,$_POST['student_id_2']);
		load_payment_info($con,$student_id_2);
	break;

	case 'enroll_student':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id_3']);
		$sec_grade = mysqli_real_escape_string($con,$_POST['sec_grade']);
		$school_year = mysqli_real_escape_string($con,$_POST['school_year']);
		enroll_student($con,$student_id,$sec_grade,$school_year);
	break;

	case 'unenroll_student':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id_4']);
		$school_year = mysqli_real_escape_string($con,$_POST['school_year']);
		unenroll_student($con,$student_id,$school_year);
	break;

	case 'delete_student':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id_5']);
		delete_student($con,$student_id);
	break;

	// Manage Fee
	case 'load_fees':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_fee']);
		load_fees($con,$grade_id);
	break;
	
	case 'save_update_fee':
		$fee_id = mysqli_real_escape_string($con,$_POST['fee_id']);
		$fee_name = mysqli_real_escape_string($con,$_POST['fee_name']);
		$fee_amount = mysqli_real_escape_string($con,$_POST['fee_amount']);
		$fee_grade_id = mysqli_real_escape_string($con,$_POST['fee_grade_id']);
		save_update_fee($con,$fee_id,$fee_name,$fee_amount,$fee_grade_id);
	break;

	case 'delete_fee':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		delete_fee($con,$id);	
	break;
	//Manage Fee

	//Manage Terms Payment

	case 'load_terms_fields':
		$id = mysqli_real_escape_string($con,$_POST['t_id']);
		load_terms_fields($con,$id);
	break;

	case 'load_terms_of_payment':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_terms']);
		load_terms_of_payment($con,$grade_id);
	break;
	
	case 'save_update_term_fee':
		$t_id = mysqli_real_escape_string($con,$_POST['t_id']);
		$term_name = mysqli_real_escape_string($con,$_POST['term_name']);
		$term_amount = mysqli_real_escape_string($con,$_POST['term_amount']);
		$term_grade_id = mysqli_real_escape_string($con,$_POST['term_grade_id']);
		save_update_term_fee($con,$t_id,$term_name,$term_amount,$term_grade_id);
	break;

	case 'delete_term_fee':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		delete_term_fee($con,$id);	
	break;

	//Mange Terms Payment

	//Manage Company Details

	case 'remove_file':
		$file_name = mysqli_real_escape_string($con,$_POST['file_name']);
		remove_file($con,$file_name);
	break;

	case 'load_company_details':
		load_company_details($con);
	break;

	case 'load_emails':
		load_emails($con);
	break;

	case 'save_update_email':
		$email_id = mysqli_real_escape_string($con,$_POST['email_id']);
		$designation = mysqli_real_escape_string($con,$_POST['designation']);
		$company_email = mysqli_real_escape_string($con,$_POST['company_email']);
		save_update_email($con,$email_id,$designation,$company_email);
	break;
	
	case 'delete_email':
		$id = mysqli_real_escape_string($con,$_POST['id']);	
		delete_email($con,$id);
	break;
	//Emails
	//BAnks
	case 'load_banks':
		load_banks($con);
	break;

	case 'save_update_bank':
		$bank_id = mysqli_real_escape_string($con,$_POST['bank_id']);
		$bank_name = mysqli_real_escape_string($con,$_POST['bank_name']);
		$account_name = mysqli_real_escape_string($con,$_POST['account_name']);
		$account_number = mysqli_real_escape_string($con,$_POST['account_number']);
		save_update_bank($con,$bank_id,$bank_name,$account_name,$account_number);
	break;

	case 'delete_bank':
		$id = mysqli_real_escape_string($con,$_POST['id']);	
		delete_bank($con,$id);
	break;
	//Banks
	
	//Sections
	case 'load_grade_sec':
		$grade_id_section = mysqli_real_escape_string($con,$_POST['grade_id_section']);	
		load_grade_sec($con,$grade_id_section);
	break;

	case 'load_sections':
		$grade_id_sec = mysqli_real_escape_string($con,$_POST['grade_id_sec']);	
		load_sections($con,$grade_id_sec);
	break;

	case 'save_update_section':
		$sec_id = mysqli_real_escape_string($con,$_POST['sec_id']);
		$sec_grade = mysqli_real_escape_string($con,$_POST['sec_grade']);
		$sec_name = mysqli_real_escape_string($con,$_POST['sec_name']);
		save_update_section($con,$sec_id,$sec_grade,$sec_name);
	break;

	case 'delete_section':
		$id = mysqli_real_escape_string($con,$_POST['id']);	
		delete_section($con,$id);
	break;

	case 'load_logo':
		load_logo($con);
	break;
	
	case 'save_update_company':

		$company_name = mysqli_real_escape_string($con,$_POST['company_name']);
		$company_name2 = mysqli_real_escape_string($con,$_POST['company_name2']);
		$address = mysqli_real_escape_string($con,$_POST['address']);
		$telephone = mysqli_real_escape_string($con,$_POST['telephone']);
		$cellphone = mysqli_real_escape_string($con,$_POST['cellphone']);
		$acronym = mysqli_real_escape_string($con,$_POST['acronym']);
		save_update_company($con,$company_name,$company_name2,$address,$telephone,$cellphone,$acronym);

	break;

	//Manage Company Details

	//check the last comment <<<----
	//nag lagay ako ng variable sa taas yung $admin_id at yung $date_time
	case 'load_payments':
		load_payments($con);
	break;
	case 'accept_payment':
		$id= mysqli_real_escape_string($con,$_POST['id']);
		$amount= mysqli_real_escape_string($con,$_POST['amount']);
		$studentId= mysqli_real_escape_string($con,$_POST['studentId']);
		$amount_due= mysqli_real_escape_string($con,$_POST['amount_due']);
		accept_payment($con,$id,$amount,$studentId,$admin_id,$date_time,$amount_due);
	break;
	case 'delete_payment':
		$id= mysqli_real_escape_string($con,$_POST['id']);
		delete_payment($con,$id);
	break;
	case 'public_select':
		$query=mysqli_real_escape_string($con,$_POST['query']);
		select_count($con,$query);
	break;
	case 'count_studentgrade':
		$id= mysqli_real_escape_string($con,$_POST['id']);
		$limit= mysqli_real_escape_string($con,$_POST['limit']);
		$offset= mysqli_real_escape_string($con,$_POST['offset']);
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		count_studentgrade($con,$id,$limit,$offset,$sy_id);
	break;
	case 'device_survey':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		count_device_survey($con,$sy_id);
	break;
	case 'connection_survey':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);	
		count_connection_survey($con,$sy_id);
	break;
	case 'tab_button':
		$id= mysqli_real_escape_string($con,$_POST['id']);
		$displayId= mysqli_real_escape_string($con,$_POST['displayId']);
		tab_button($con,$id,$displayId);
	break;
	case 'load_speed':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_speed($con,$sy_id);
	break;
	case 'load_delivery':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_delivery($con,$sy_id);
	break;
	case 'load_learning_day':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_learning_day($con,$sy_id);
	break;
	case 'load_financial_cap':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_financial_cap($con,$sy_id);
	break;
	case 'load_teacher_partner':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_teacher_partner($con,$sy_id);
	break;
	case 'load_transpo':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_transpo($con,$sy_id);
	break;
	case 'load_is_connected':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_is_connected($con,$sy_id);
	break;
	case 'load_interfere':
		$sy_id= mysqli_real_escape_string($con,$_POST['sy_id']);
		load_interfere($con,$sy_id);
	break;

	
	//>>>>Portal setting<<<<

	//portal banner
	case 'load_portal_banner';
		$id = mysqli_real_escape_string($con,$_POST['company_id']);

		display_portal_banner($con,$id);
	break;

	case 'set_status_active_pb':
		$id=mysqli_real_escape_string($con,$_POST['id']);
		$status='1';
		change_status_pb($con,$id,$status);
	break;

	case 'set_status_inactive_pb':
		$id=mysqli_real_escape_string($con,$_POST['id']);
		$status='0';
		change_status_pb($con,$id,$status);
	break;

	case 'delete_portal_banner';
		$id = mysqli_real_escape_string($con,$_POST['id']);
		
		delete_portal_banner($con,$id);
	break;

	//portal content
	case 'load_portal_content';
		$id= mysqli_real_escape_string($con,$_POST['company_id']);

		display_portal_content($con,$id);
	break;

	case 'save_portal_content';
		$id= mysqli_real_escape_string($con,$_POST['id']);
		$comp_id= mysqli_real_escape_string($con,$_POST['company_id']);
		$categ = mysqli_real_escape_string($con,$_POST['categ']);
		$item = mysqli_real_escape_string($con,$_POST['items']);
		$option = mysqli_real_escape_string($con,$_POST['opt']);

		save_portal_content($con,$id,$comp_id,$categ,$item,$option);
	break;

	case 'delete_portal_content';
		$id = mysqli_real_escape_string($con,$_POST['id']);
		
		delete_portal_content($con,$id);
	break;

	case 'set_status_active_pc':
		$id=mysqli_real_escape_string($con,$_POST['id']);
		$status='1';
		change_status($con,$id,$status);
	break;

	case 'set_status_inactive_pc':
		$id=mysqli_real_escape_string($con,$_POST['id']);
		$status='0';
		change_status($con,$id,$status);
	break;
	//---->>>>
}
?>