<?php
session_start();
if (!isset($_SESSION['admin_pane'])) {
  @header('location:../');  
}

include_once('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

  if (mysqli_num_rows($sql)>0) {

    $row = mysqli_fetch_assoc($sql);

    $company_id = $row['companyid'];
    $default_color = 'w3-light-blue';
    $school_name = ''.$row['company_name1'].'';
    $acronym = $row['acronym'];
    $with_payment = ''.$row['with_payment'].'';
    $cid = $row['companyid'];
    $navbar_bg = $row['navbar_bg'];
    $logo = '<img width="60" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
     $company_name = $row['company_name1'];

  }

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        echo $row['school_year'];
    }
}

function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade order by orderno asc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['ID'].'">'.$row['grade'].'</option>';
        }
    }
}

function get_school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by is_active desc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $acronym; ?></title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/w3.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="../css/showToast.css">

  <style type="text/css">
    /*css new file*/
  .header-image{
    border-bottom: none;
      padding: 15px 15px 0 15px;
      background-image: url(../img/modal-header.jpg);
      height: 100px;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center
  }
  .cards-image{
    border-bottom: none;
      padding: 15px 15px 0 15px;
      background-image: url(../img/card.jpg);
      height: 100px;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center
  }
  .img50{
    width: 50px;
    height: 50px;
  }
  .tab-btn.active{
      background-color: blue;
      color: white;
  }
  </style>
</head>

<body id="page-top" onload="load_enrollees(); load_pass();">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <!-- <i class="fas fa-laugh-wink"></i> -->
          <?php echo $logo; ?>
        </div>
        <div class="sidebar-brand-text mx-1"><?php echo $acronym; ?> Admin <!-- <sup>2</sup> --></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
        <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-edit"></i>
          <span style="font-size: 11px;">Enrollement <?php echo get_school_year_active($con); ?></span></a>
      </li>
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Administrator</span>
                <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
                <span class="fa fa-user-secret fa-2x"></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#manage_account">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logout_me">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><span class="fa fa-users"></span> Manage Enrollees</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>

          <!-- Content Row -->
          <div class="row">

            <div class="col-xl-12 col-md-12 mb-4">

              <div class="card shadow ">

            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List of enrollees</h6>
            </div>
            
            <div class="card-body">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <select class="form-control" id="school_year" oninput="load_enrollees();" onchange="load_enrollees();">
                  <?php echo get_school_year($con); ?>
                  </select>
                </div>
                <div class="input-group-prepend">
                  <select class="form-control" id="grade_id" oninput="load_enrollees();" onchange="load_enrollees();">
                    <option selected="" value="">--Select Grade level--</option>
                  <?php echo get_grade_level($con); ?>
                  </select>
                </div>
                <input type="text" id="search" class="form-control" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn btn-success" onclick="load_enrollees();">Go</button>
                </div>
              </div>

              <div class="table-responsive">   
                <table class="table table-bordered table-sm table-hover" id="tbl_enrollees"></table>
              </div>
            </div>
            
            </div>

            </div>

            

          </div>

          <!-- Content Row -->

          
          <!-- Content Row -->
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
  <?php include_once("modals.php");?>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="../js/showToast.js"></script>
  <script src="../js/w3.js"></script>
  <script src="function.js"></script>

   <script type="text/javascript">
    
    $(document).ready(function(e){

      $("#fupForm").on('submit', function(e){
          e.preventDefault();
          var a = $("#el").val();

          var txt_doc_name = $('#txt_doc_name').val();

          if(txt_doc_name == ''){

            $('#txt_doc_name').focus();

          }else{

            $.ajax({
              type: 'POST',
              url: 'upload_file_doc.php',
              data: new FormData(this),
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('.submitBtn').attr("disabled","disabled");
                  if (doc_id.value == "")
                  {
                    $("#load_pend_doc").prepend('<div class="well w3-blue">Loading...</div>');
                  }
                  else
                  {
                    $("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
                  }
              },
              success: function(msg){
                  $('.statusMsg').html('');
                  if(msg == 1){
                      
                    $('#doc_id').val('');
                    $('#doc_type').val('');
                    $('#f_name_doc').val('');
                    $('#txt_doc_name').val('');

                      $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

                      setTimeout(function(){   
                  $('.statusMsg').html('');
                  showToast.show('New File Succefully Uploaded!',3000);
              },2000);
              load_supporting_files();
                      //display_documents();
            $("#load_pend_doc").html('');
            $("#preview_doc").html('');
                  }else if(msg == 2){
                    
                    $('#doc_id').val('');
            $('#doc_type').val('');
            $('#f_name_doc').val('');
            $('#txt_doc_name').val('');

                      $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

                      setTimeout(function(){   
                  $('.statusMsg').html('');
                  showToast.show('File Succefully Updated!',3000);
              },2000);
              load_supporting_files();
                      //display_documents();
            $("#load_pend_doc").html('');
            $("#preview_doc").html('');
            $('#btn_cupdate').css('display','none');
                  }
                  else{
                      $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
                  }
                  $('#fupForm').css("opacity","");
                  $(".submitBtn").removeAttr("disabled");
              }
          });

          }

          
      });
      
      //file type validation
      $("#file").change(function() {
          var file = this.files[0];
          var imagefile = file.type;
          var form_data = new FormData();
          //alert(imagefile);
          $('#doc_type').val(imagefile.trim(""));
          var match= ["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","image/jpeg","image/png","image/jpg", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.ms-powerpoint", "text/plain"];
          if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6]) || (imagefile==match[7]) || (imagefile==match[8]) || (imagefile==match[9]) || (imagefile==match[10]))){
              alert('INVALID FILE!');
              $("#file").val('');
              return false;
          }else{


        form_data.append("file", document.getElementById('file').files[0]);

            $.ajax({
            url:"upload_file_temp_doc.php",
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(data){
              $('#preview_doc').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
            },   
            success:function(data)
            {
             if(data == 404){

          swal("Warning", "File already exists", "warning");
          $('#preview_doc').html("<label class='text-danger col-sm-12'>File Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");  

             }else{
                // $('#btn_remove_file').addClass('display-none');
                $('#file_update').html('');
                $('#preview_doc').html(data);
                var file_name = $('#file_name').val();
                $('#f_name_doc').val(file_name);

             }  
            
            }
            });


          }
      });
  });

  </script>
  
</body>

</html>
