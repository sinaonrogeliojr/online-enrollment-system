<?php 

include('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$company_id = $row['companyid'];
		$default_color = 'w3-light-blue';
		$school_name = ''.$row['company_name1'].'';
		$acronym = ''.$row['acronym'].'';
	}

?>

<div class="modal fade" role="dialog" id="edit_term">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"><span class="fa fa-edit"></span> Edit Term <input type="hidden" id="t_id">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<div class="form-group">
				  <label for="term_name"><b>Term:</b></label>
				  <input type="text" id="term_name" class="form-control text-capitalize">
				</div>
				<div class="form-group">
					<label for="religion"><b>Status:</b></label>
					<select class="form-control" id="t_status">
			    		<option value="1">Active</option>
			    		<option value="0">Inactive</option>
				  	</select>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" onclick="update_term();" id="btn_term" class="btn btn-block btn-info">Save</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block <?php echo $default_color; ?>">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">No</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="student_info">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">Student Information<input type="hidden" id="student_id">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body" id="stud_info">
				
			</div>
			<div class="modal-footer">
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="payments">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">Payment Transactions<input type="hidden" id="student_id_2">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<div class="card mb-2">
					<div class="card-header">Payment Center & Details</div>
					<div class="card-body">
						Bank: <strong id="bank_detail"></strong>
						<div class="dropdown-divider"></div>
						Reference #: <strong id="ref_no_details"></strong>
					</div>
				</div>
				<div class="table table-responsive">
					<table class="table table-bordered table-hover" id="payment_info"></table>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="enroll">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"><span class="fa fa-check"></span> Mark as Enroll <input type="hidden" id="student_id_3">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<input type="hidden" id="grade_id_section">
				<h3>Are you sure you want to enroll <b id="s_name"></b>?</h3>

				<div class="form-group" id="load_section">
					 
				</div>

			</div>
			<div class="modal-footer">
				<a href="#" onclick="enroll_student();" id="btn_enroll" class="btn btn-block btn-info">Enroll</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="unenroll">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"><span class="fa fa-user-lock"></span> Mark as Unenroll <input type="hidden" id="student_id_4">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<h3>Are you sure you want to unenroll <b id="s_name2"></b>?</h3>
			</div>
			<div class="modal-footer">
				<a href="#" onclick="unenroll_student();" id="btn_unenroll" class="btn btn-block btn-info">Unenroll</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="delete_student">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header"><span class="fa fa-trash"></span> Delete Student <input type="hidden" id="student_id_5">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<h3>Are you sure you want to remove <b id="s_name4"></b>?</h3>
			</div>
			<div class="modal-footer">
				<a href="#" onclick="delete_student();" id="btn_delete" class="btn btn-block btn-danger">Delete</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div>
		</div>
	</div>
</div>

<!-- term Fees modal -->
<div class="modal fade" role="dialog" id="addedit_term_fee">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form onsubmit="event.preventDefault(); update_term_fee();">
			<div class="modal-header" >
				<h3 id="header_term"><span class="fa fa-edit"></span> Update Fee</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<input type="hidden" id="t_id">
				<input type="hidden" id="term_id">
				<input type="hidden" id="term_grade_id">
				
				<span id="load_term_field">
					
				</span>

			</div>
			<div class="modal-footer">
				<a href="#" type="submit" onclick="update_term_fee();" id="btn_update_term" class="btn btn-block btn-info">Update</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Update Company logo -->

<!-- Add Edit Bank Details Modal -->
<div class="modal fade" role="dialog" id="addedit_bank">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form onsubmit="event.preventDefault(); save_update_bank();">
			<div class="modal-header" >
				<h3 id="header_bank"><span class="fa fa-edit"></span> Add Bank</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<input type="hidden" id="bank_id">
				
				<div class="form-group">
					<label for="bank_name"><b>Bank Name:</b></label>
					<input type="bank_name" class="form-control" id="bank_name" required="">
				</div>
				<div class="form-group">
					<label for="account_name"><b>Account Name:</b></label>
					<input type="account_name" class="form-control" id="account_name" required="">
				</div>
				<div class="form-group">
					<label for="account_number"><b>Account Number:</b></label>
					<input type="account_number" class="form-control" id="account_number" required="">
				</div>

			</div>
			<div class="modal-footer">
				<a href="#" type="submit" onclick="save_update_bank();" id="btn_update_bank" class="btn btn-block btn-info">Save</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Add Edit Email Modal -->
<div class="modal fade" role="dialog" id="addedit_email">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frm" onsubmit="event.preventDefault(); save_update_email();">
			<div class="modal-header" >
				<h3 id="header_email"><span class="fa fa-edit"></span> Add Email</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<input type="hidden" id="email_id">
				
				<div class="form-group">
					<label for="designation"><b>Designation:</b></label>
					<input type="designation" class="form-control" id="designation" required="">
				</div>
				<div class="form-group">
					<label for="company_email"><b>Email:</b></label>
					<input type="company_email" class="form-control" id="company_email" required="">
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" type="submit" onclick="save_update_email();" id="btn_update_email" class="btn btn-block btn-info">Save</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Add Edit Section Modal -->
<div class="modal fade" role="dialog" id="addedit_section">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frm2" onsubmit="event.preventDefault(); save_update_section();">
			<div class="modal-header" >
				<h3 id="header_section"><span class="fa fa-edit"></span> Add Section</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<input type="hidden" id="sec_id">
				
				<div class="form-group">
					  <select class="form-control" id="sec_grade">
				    	<option selected="" value="">--Select Grade level--</option>
				    	<?php echo get_grade_level($con); ?>
				  	  </select>
				</div>
				<div class="form-group">
					<label for="sec_name"><b>Section Name:</b></label>
					<input type="sec_name" class="form-control" id="sec_name" required="">
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" type="submit" onclick="save_update_section();" id="btn_update_section" class="btn btn-block btn-info">Save</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>
			</form>
		</div>
	</div>
</div>

<!--- etong huling modal  nag lagay ako ng picture sa img folder sa main directory yung modal-header.jpg--->
<div class="modal fade" role="dialog" id="viewPayment_modal">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="header-image">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<h3><label id="viewPayment_name"></label> - School Payments<h3>
				

				<div class="form-row">
					<div class="col-xl-4 col-sm-12">
						<div class="text-center">
							<div id="uploadFile"></div>
						</div>
					</div>
					<div class="col-xl-8 col-sm-12">
						<div class="card">
							<div class="card-body">
								
								<div><small>Bank: <strong id="view_bank" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
								<div><small>Acct No: <strong id="view_acctno" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
								<div><small>Acct Name: <strong id="view_acctname" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
								<div><small>Cp. #: <strong id="view_cp" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
								<div><small>Address: <strong id="view_address" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
								<div><small>Ref #: <strong id="view_ref" class="w3-right"></strong></small></div>
								<div class="dropdown-divider"></div>
							</div>
						</div>
					</div>

				</div>
				<input type="hidden" id="viewPayment_text">
				<input type="hidden" id="viewId_text">
				<input type="hidden" id="viewStudent_Id">
				<input type="hidden" id="viewPayment_amount_due" >
				<div class="w3-right mt-2">
					<button type="button" class="btn btn-danger" title="Cancel" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
					<button class="btn btn-primary" onclick="accept_payment()" title="Accept Payment"><i class="fa fa-coins"></i> <span id="viewPayment_amount"></span></button>
				</div>
			</div>
		</div>
	</div>

</div>


<!-- School Fees modal -->
<div class="modal fade" role="dialog" id="addedit_fee">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form onsubmit="event.preventDefault(); update_fee();">
			<div class="modal-header" >
				<h3 id="header_fee"><span class="fa fa-edit"></span> Update Fee</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="fee_id">
				<input type="hidden" id="fee_grade_id">
				<div class="form-group">
					<label for="fee_name">Name:</label>
					<textarea class="form-control" rows="3" id="fee_name"></textarea>
				</div>

				<div class="form-group">
				  <label for="fee_amount"><b>Amount:</b></label>
				  <input type="number" id="fee_amount" class="form-control text-capitalize">
				</div>

			</div>
			<div class="modal-footer">
				<a href="#" type="submit" onclick="update_fee();" id="btn_update_fee" class="btn btn-block btn-info">Update</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Add Banner modal -->
<div class="modal fade" role="dialog" id="addedit_banner">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="header_fee"><span class="fa fa-edit"></span>Upload Banner</h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="card text-white bg-primary mb-3">
								<div class="card-header">Image Sizing</div>
								<div class="card-body">
							    	<p class="card-text"><span class="fa fa-check-circle"></span> Images resolution must be <span class="badge badge-dark">780 x 370</span> pixels or <span class="badge badge-dark">1920 x 930</span> pixels with low DPI.</p>
							    	<p class="card-text"><span class="fa fa-check-circle"></span> Images size must be <span class="badge badge-dark">100kb</span> up to <span class="badge badge-dark">300kb</span> ONLY.</p>
							  	</div>
							</div>
						</div>
					</div>

					<div class="row">

						<div class="col-md-2"></div>

						<div class="col-md-8">

							<div class="form-group">
								<form enctype="multipart/form-data" id="upload_banner" >
									<div class="card-body text-center hide_logo" id="tbl_logo"></div>
									
							   	<input type="hidden" id="comp_id" name="comp_id" value="<?php echo $company_id; ?>">
									<input type="hidden" id="f_name" name="f_name">

									<div class="form-group">
								        <input type="file" class="form-control" id="banner_pic" name="banner_pic" style="display: none;"/>
								    </div>
									    
									<div class="text-center" id="preview_banner"></div>
									    
					    			<div id="load_pend"></div>

								    <button type="button" class="btn btn-secondary btn-block" id="btn_browse" onclick="$('#banner_pic').click();"><span class="fa fa-upload"></span> Browse</button>
					    			
					    			<input type="submit" name="submit" id="btn_save_banner" class="btn btn-primary btn-block" value="Save" disabled/>
								</form>
							</div>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Add Content modal -->
<div class="modal fade" role="dialog" id="addedit_content">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="header_fee"><span class="fa fa-edit"></span>Portal Content</h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<input type="hidden" id="pc_id">
							<div class="form-group">
							  <label for="address"><b>Category: </b></label>
							  <select class="form-control" id="categ">
						    		<option value="0">- Select Category -</option>
						    		<option value="1">Mission</option>
						    		<option value="2">Vision</option>
						    		<option value="3">Philosophy</option>
							  	</select>
							</div>

							<div class="form-group">
							  <label for="address"><b>Content Area: </b></label>
							  <textarea class="form-control" rows="10" id="item"></textarea>
							</div>
							
							<button type="button" class="btn <?php echo $default_color; ?> btn-block" id="btn_save_pcontent" onclick="save_portal_content('<?php echo $company_id; ?>')" value="Save"> Save
						   </button>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MANAGE USER ACCOUNT -->

<div class="modal fade" role="dialog" id="manage_account">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" >
				<h3 id="header_bank"><span class="fa fa-edit"></span> Manage Account</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<div class="form-group">
						<label for="userid"><b>Username:</b></label>
						<input type="text" class="form-control" id="userid" value="<?php echo $_SESSION['user_id'] ?>" disabled="">
					</div>

					<label for="old_pass"><b>Old Password:<span id="old_pass_validator"></span></b></label>
					<div class="input-group mb-3" id="show_pass">
					  
					</div>

					<div class="form-group">
						<label for="new_pass"><b>New Password</b></label>
						<input type="password" class="form-control" id="new_pass" required="">
					</div>
					<div class="form-group">
						<label for="retype_pass"><b>Retype Password:</b> <span id="retype_pass_validator"></span></label>
						<input type="password" class="form-control" id="retype_pass" required="">
					</div>

					<button class="btn btn-block btn-info" id="btn_up_account" onclick="update_password();">
						Save
					</button>

			</div>
			<div class="modal-footer">
				
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="manage_account_edit">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" >
				<h3><span class="fa fa-edit"></span> Edit <b id="header_stud">Student<b></h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<input type="hidden" id="s_id">
			</div>
			<div class="modal-body">

				<ul class="nav nav-pills nav-justified" style="font-weight: bolder;">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="pill" href="#info" id="info_tab"><span class="fa fa-registered"></span> Personal Information</a>
				  </li>
				  <li class="nav-item	">
				    <a class="nav-link" data-toggle="pill" href="#fam" id="fam_tab"><span class="fa fa-home"></span> Family Information</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="pill" href="#other_info" id="other_info_tab"><span class="fa fa-money-bill-wave"></span> Other Information</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="pill" href="#survey_info"  id="survey_info_tab"><span class="fa fa-poll"></span> Survey Information</a>
				  </li>
				</ul>

				<div class="tab-content">

	  				<div class="tab-pane container active mt-3" id="info">

	  					<div class="row">
	  						<div class="col-md-6">
	  							<div class="form-group">
								  <label for="category"><b>Choose Category:</b> <span class="text-danger">*</span></label>
								  <select class="form-control" id="category" >
								   	<option value=""></option>
								  	<option value="Old Student">OLD STUDENT</option>
								  	<option value="New Student">NEW STUDENT</option>
								  	<option value="Transferee">TRANSFEREE</option>
								  </select>
								</div>
	  						</div>
	  						<div class="col-md-6">
	  							 <div class="form-group">
								  <label for="lrn"><b>Learner Reference Number(LRN):</b> </label>
								  <input type="text" id="lrn" class="form-control text-capitalize" >
								</div>
	  						</div>
	  					</div>
	  					
						<div class="row">
							
							<div class="col-md-4">
								<div class="form-group">
								  <label for="lname"><b>Lastname:</b> <span class="text-danger">*</span></label> 
								  <input type="text" id="lname" class="form-control text-capitalize" id="lname" >
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
								  <label for="fname"><b>Firstname:</b> <span class="text-danger">*</span></label>
								  <input type="text" id="fname" class="form-control text-capitalize" id="fname">
								</div>
							</div>

							<div class="col-md-3">
								
								<div class="form-group">
								  <label for="mname"><b>Middlename:</b></label>
								  <input type="text" id="mname" class="form-control text-capitalize" id="mname" >
								</div>

							</div>

							<div class="col-md-2">
								<div class="form-group">
								  <label for="suffix"><b>Suffix:</b></label>
								  <input type="text" id="suffix" class="form-control text-capitalize" id="suffix" >
								</div>
							</div>

						</div>
						
						<div class="row">
							
							<div class="col-md-6">
								<div class="form-group">
								  <label for="email"><b>Email: <span class="text-danger">*</span></b>
								  <span class="text-success" id="validEmail"></span>
								  </label>
								  <input type="email" class="form-control" id="email" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="fb_account"><b>Faceboook Account:</b></label>
								  <input type="text" class="form-control" id="fb_account" >
								</div>
							</div>
							
						</div>

						<div class="row">
						
							<div class="col-md-4">
								<div class="form-group">
								  <label for="bdate"><b>Birth Date:</b> <span class="text-danger">*</span></label>
								  <input type="Date" id="bdate" oninput="return_age();" onchange="return_age();" class="form-control">
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
								  <label for="age"><b>Age:</b> <span class="text-danger">*</span></label>
								  <input type="number" id="age" class="form-control">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
								  <label for="place_of_birth"><b>Place of Birth:</b> <span class="text-danger">*</span></label>
								  <input type="text" id="place_of_birth" class="form-control text-capitalize">
								</div>
							</div>

						</div>

						<div class="form-group">

						  <label for="gender"><b>Gender:</b> <span class="text-danger">*</span></label>

						  <select class="form-control" id="gender">
						  	<option selected="" value="FEMALE">FEMALE</option>
						    <option  value="MALE">MALE</option>
						  </select>

						</div>

						<div class="form-group">

						  <label for="religion"><b>Religion:</b> <span class="text-danger">*</span></label>

						  <select class="form-control" id="religion">

						  	<option value=""></option>
						  	<option value="ROMAN - CATHOLIC">Roman Catholic</option>
						  	<option value="NON CATHOLIC">Non Catholic</option>
						  	<option value="ISLAM">Islam</option>
						  	<option value="BUDDHIST">Buddhist</option>
						  	<option value="OTHER">Other</option>

						  </select>

						</div>

						<div class="form-group">
						  <label for="citizenship"><b>Citizenship:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="citizenship" class="form-control">
						</div>

						<div class="row">

							<div class="col-md-6">
								
								<div class="form-group">
								  <label for="messenger_account"><b>Mother Tongue(Dialect Use at Home):</b></label>
								  
									<input type="hidden" id="mother_tongue">

									<div id="m_tongue">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_style_value();" value="Tagalog" class="custom-control-input" id="Tagalog" name="example1">
										    <label class="custom-control-label" for="Tagalog">Tagalog</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_style_value();" value="English" class="custom-control-input" id="English" name="example1">
										    <label class="custom-control-label" for="English">English</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_style_value();" value="Ilocano" class="custom-control-input" id="Ilocano" name="example1">
										    <label class="custom-control-label" for="Ilocano">Ilocano</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_style_value();" value="Ybanag" class="custom-control-input" id="Ybanag" name="example1">
										    <label class="custom-control-label" for="Ybanag">Ybanag</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_style_value();" value="Yogad" class="custom-control-input" id="Yogad" name="example1">
										    <label class="custom-control-label" for="Yogad">Yogad</label>
										</div>

									</div>

									<div class="input-group mb-3">
										<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="iba_pa();" value="Iba pa" class="custom-control-input" id="iba_pa" name="example1">
									    <label class="custom-control-label" for="iba_pa">Iba pa:</label>
									      <input type="text" id="other_mother_tongue" placeholder="Other...">
									    </div>
									</div>

								</div>

							</div>

							<div class="col-md-6">
								
								<div class="form-group">
								  <label for="messenger_account"><b>Ethnicity:</b></label>
								  	
									<input type="hidden" id="ethnicity_val">

									<div id="ethnicity">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ethnicity();" value="Gaddang" class="custom-control-input" id="Gaddang" name="example1">
										    <label class="custom-control-label" for="Gaddang">Gaddang</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ethnicity();" value="Ifugao" class="custom-control-input" id="Ifugao" name="example1">
										    <label class="custom-control-label" for="Ifugao">Ifugao</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ethnicity();" value="Ilocano" class="custom-control-input" id="Ilocano_e" name="example1">
										    <label class="custom-control-label" for="Ilocano_e">Ilocano</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ethnicity();" value="Ybanag" class="custom-control-input" id="Ybanag_e" name="example1">
										    <label class="custom-control-label" for="Ybanag_e">Ybanag</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ethnicity();" value="Yogad" class="custom-control-input" id="Yogad_e" name="example1">
										    <label class="custom-control-label" for="Yogad_e">Yogad</label>
										</div>

									</div>
									<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_e();" value="Iba pa" class="custom-control-input" id="iba_pa_e" name="example1">
										    <label class="custom-control-label" for="iba_pa_e">Iba pa:</label>
										      <input type="text" id="other_ethnicity" placeholder="Other...">
										    </div>
										</div>

								</div>

							</div>
						</div>

						<div class="form-group">
						  <label for="contact"><b>Cellphone Number:</b> <span class="text-danger">*</span></label>
						  <input type="text" id="contact" class="form-control text-capitalize" id="contact" >
						</div>

						<div class="form-group">
						  <label for="address"><b>Complete Address (House No., Street, Barangay, City/Municipality):</b> <span class="text-danger">*</span></label>
						  <input type="text" id="address" class="form-control text-capitalize" id="address" >
						</div>

	  				</div>

	  				<div class="tab-pane container mt-3" id="fam">
	  					
	  					<div id="load_fam">
	  					
		  				</div>

		  				<div class="form-group mt-3">
						  <label for="siblings"><b>How many siblings do you have?</b></label>
							  <select class="form-control" id="siblings">
							  	<option value=""></option>
							  	<option value="None (Only Child)">None (Only Child)</option>
							  	<option value="1">1</option>
							  	<option value="2">2</option>
							  	<option value="3">3</option>
							  	<option value="4">4</option>
							  	<option value="5">5</option>
							  	<option value="More than 5">More than 5</option>
							  </select>
						</div>

	  				</div>

	  				<div class="tab-pane container mt-3" id="other_info">

	  					<div class="card">

	  						<div class="card-header w3-blue text-white">
	  							<b> HEALTH RECORDS </b>
	  						</div>

	  						<div class="card-body">

	  							<div class="form-group">
								  <label for="with_medical_condition"><b>Do you have impairment, disability, or long term medical condition?</b></label>
								  <select class="form-control" id="with_medical_condition">
								  	<option value=""></option>
								  	<option value="Yes">Yes</option>
								  	<option value="No">No</option>
								  	<option value="Rather not to say">Rather not to say</option>
								  </select>
								</div>

								<div class="form-group">
								  <label for="medical_condition_affects"><b>If Yes, does your impairment, disability, or long-term medical condition affect your study?</b></label>
								  <select class="form-control" id="medical_condition_affects">
								  	<option value=""></option>
								  	<option value="Yes">Yes</option>
								  	<option value="No">No</option>
								  	<option value="Rather not to say">Rather not to say</option>
								  </select>
								</div>

								<div class="form-group">
								  <label for="medical_condition"><b>What is your impairment, disability, or long-term medical condition?
								Please leave this part if you opt not to declare your impairment, disability, or long-term medical condition.</b></label>
								  <input type="text" id="medical_condition" class="form-control text-capitalize" placeholder="Your answer.">
								</div>

	  						</div>

	  					</div>

					  	<div class="card mt-3">

					  		<div class="card-header w3-blue text-white" id="support_doc">
					  			<b>SUPPORTING DOCUMENTS AND CREDENTIALS</b>
			  				</div>

					  		<div class="card-body">

					  			<input type="hidden" name="el" id="el">
								
					  			<form enctype="multipart/form-data" id="fupForm" >

					  				<input type="hidden" id="s_id_doc" name="s_id_doc">
					  				<input type="hidden" name="doc_id" id="doc_id">
					  				<input type="hidden" name="doc_type" id="doc_type">
								    <input type="hidden" id="f_name_doc" name="f_name_doc">

					  				<label for="with_medical_condition"><b>For Transferees: Please upload scanned copy the following supporting documents: </b> 

					  						<br>1. Form 138(Report Card)
						  					<br>2. ESC/QVR Certificate(if Recipient)
						  					<br>3. Birth Certificate
						  					<br>4. Certificate of GMRC . If you don't have a scanner, you may present these documents to the Registrar's Office upon payment of enrolment fees at the CFS Accounting Office.

					  				</label>

					  				<div class="form-group">
									  <label for="txt_doc_name"><b>Supporting file </b></label>
										  <select class="form-control" id="txt_doc_name" name="txt_doc_name">
										  	<option value=""></option>

										  	<option value="ESC/QVR Certificate(if Recipient)">ESC/QVR Certificate(if Recipient)</option>

										  	<option value="Form 138(Report Card">Form 138(Report Card)</option>
										  	<option value="Birth Certificate">Birth Certificate</option>
										  	<option value="Certificate of GMRC">Certificate of GMRC</option>
										  </select>
									</div>

									<div class="form-group">
								        <input type="file" class="form-control" id="file" name="file" style="display: none;"/>
								    </div>
								    <div class="form-group">
								    	<div class="btn-group">
									    	<button type="button" class="btn <?php echo $default_color; ?> btn-small" id="btn-select" onclick="$('#file').click();"><span class="fa fa-upload"></span> Document/Picture</button>
									    </div>
								    </div>
								    
								    <div class="text-center" id="preview_doc"></div>
								    <div class="form-group">
								    	<input type="submit" name="submit" id="btn_ann" class="btn <?php echo $default_color; ?>  btn-block submitBtn" value="Attach"/>
								    </div>
				    				
				    				<div id="load_pend_doc"></div>

					  			</form>

					  			<div id="load_files">
					  				
					  			</div>

					  		</div>

					  	</div>

	  				</div>

	  				<div class="tab-pane container mt-3" id="survey_info">

	  					<div class="card">

	  						<div class="card-header w3-blue text-white">
	  							<b> HOUSEHOLD CAPACITY AND ACCESS TO DISTANCE LEARNING </b>
	  						</div>

	  						<div class="card-body">

	  							<div class="form-group">

									  <label><b> How does your child go to school? Choose all that applies:</b></label>
									  
										<input type="hidden" id="go_to_school_val">

										<div id="go_to_school">

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" value="Walking" class="custom-control-input" id="g1" onclick="get_gotoschool();" name="gotoschool">
											    <label class="custom-control-label" for="g1">Walking</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" value="public commute (land/water)" onclick="get_gotoschool();" class="custom-control-input" id="g2" name="gotoschool">
											    <label class="custom-control-label" for="g2">public commute (land/water)</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" value="family-owned vehicle" onclick="get_gotoschool();" class="custom-control-input" id="g3" name="gotoschool">
											    <label class="custom-control-label" for="g3">family-owned vehicle</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="get_gotoschool();" value="school service" class="custom-control-input" id="g4" name="gotoschool">
											    <label class="custom-control-label" for="g4">school service</label>
											</div>

										</div>

								</div>

								<div class="form-group">

									<label>
										<b>How many of your household members (including the enrollee) are studying in School Year 2020-2021? Please specify each:</b>
									</label>
									<div class="input-group mb-3">

									  <div class="input-group-prepend">

									    <select class="form-control" id="grade_id_h">
									    	<?php echo get_grade_level($con,$with_hs,$with_gs); ?>
									  	</select>
									  </div>

									  <input type="number" id="member_value" class="form-control" placeholder="Enter Value...">
									  
									  <div class="input-group-append">
									    <button class="btn btn-primary" id="btn_save_member" type="button" onclick="save_household();">Save</button>
									  </div>

									</div>

									<div class="form-group">
										<ul class="list-group" id="load_household_members"></ul>
									</div>
										
								</div>

								<div class="form-group">
									  <label><b> Who among the household members can provide instructional support to  the child's distance learning? Choose all that applies.:</b></label>

										<input type="hidden" id="teacher_home_val">

										<div id="teacher_home">

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="get_teacher_home();" value="Parents/Guardians" class="custom-control-input" id="t1" name="tp">
											    <label class="custom-control-label" for="t1">Parents/Guardians</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="get_teacher_home();" value="Elder siblings" class="custom-control-input" id="t2" name="tp">
											    <label class="custom-control-label" for="t2">Elder siblings</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="get_teacher_home();" value="Grandparents" class="custom-control-input" id="t3" name="tp">
											    <label class="custom-control-label" for="t3">Grandparents</label>
											</div>

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="get_teacher_home();" value="Extended members of the family" class="custom-control-input" id="t4" name="tp">
											    <label class="custom-control-label" for="t4">Extended members of the family</label>
											</div>

										</div>

										<div class="input-group mb-3">

											<div class="custom-control custom-checkbox">
											    <input type="checkbox" onclick="iba_pa_th();" value="" class="custom-control-input" id="iba_pa_th" name="tp">
											    <label class="custom-control-label" for="iba_pa_th">Other:</label>
											      <input type="text" id="other_th" placeholder="Other...">
										    </div>

										</div>

								</div>

								<div class="form-group">

								  <label><b>What devices are available at home that the learner can use for learning? Check all that applies:</b></label>
								  
									<input type="hidden" id="gadgets_val">

									<div id="gadgets">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Cable Tv" class="custom-control-input" id="a1" name="av_gadgets">
										    <label class="custom-control-label" for="a1">Cable TV</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Non cable TV" class="custom-control-input" id="a2" name="av_gadgets">
										    <label class="custom-control-label" for="a2">Non cable TV</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Radio" class="custom-control-input" id="a3" name="av_gadgets">
										    <label class="custom-control-label" for="a3">Radio</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Laptop" class="custom-control-input" id="a4" name="av_gadgets">
										    <label class="custom-control-label" for="a4">Laptop</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Cellphone" class="custom-control-input" id="a5" name="av_gadgets">
										    <label class="custom-control-label" for="a5">Cellphone</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Tablet" class="custom-control-input" id="a6" name="av_gadgets">
										    <label class="custom-control-label" for="a6">Tablet</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_gadgets();" value="Desktop" class="custom-control-input" id="a7" name="av_gadgets">
										    <label class="custom-control-label" for="a7">Desktop</label>
										</div>

										

									</div>

									<div class="input-group mb-3">
											<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="iba_pa_g();" value="Iba pa" class="custom-control-input" id="iba_pa_g" name="av_gadgets">
										    <label class="custom-control-label" for="iba_pa_g">Others:</label>
										      <input type="text" id="other_gadget" placeholder="Other...">
										    </div>
										</div>

								</div>

								<div class="form-group">
										
									  <label><b>Do you have a way to connect to the internet?:</b></label>
									  
										<input type="hidden" id="connected_to_internet_val">

										<div id="connected_to_internet">

											<div class="custom-control custom-radio">
											    <input type="radio" onclick="get_is_connected();" value="YES" class="custom-control-input" id="c1" name="internet_conn">
											    <label class="custom-control-label" for="c1">YES</label>
											</div>

											<div class="custom-control custom-radio">
											    <input type="radio" onclick="get_is_connected();" value="NO" class="custom-control-input" id="c2" name="internet_conn">
											    <label class="custom-control-label" for="c2">NO (if no please proceed to 8.)</label>
											</div>

										</div>

								</div>

								<div class="form-group">
									
								  <label><b>6. What type of internet connection do you have at home?</b></label>
								  
									<input type="hidden" id="internet_val">

									<div id="internet">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="No internet connection" class="custom-control-input" id="ia" name="example1">
										    <label class="custom-control-label" for="ia">No internet connection</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile data (prepaid)" class="custom-control-input" id="ib" name="example1">
										    <label class="custom-control-label" for="ib">Mobile data (prepaid)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile data (post paid or plan)" class="custom-control-input" id="ic" name="example1">
										    <label class="custom-control-label" for="ic">Mobile data (post paid or plan)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Mobile broadband" class="custom-control-input" id="i_d" name="example1">
										    <label class="custom-control-label" for="i_d">Mobile broadband</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Cable internet" class="custom-control-input" id="ie" name="example1">
										    <label class="custom-control-label" for="ie">Cable internet</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_ic();" value="Fiber internet" class="custom-control-input" id="i_f" name="example1">
										    <label class="custom-control-label" for="i_f">Fiber internet</label>
										</div>

									</div>

									<div class="input-group mb-3">
										<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="iba_pa_i();" value="Iba pa" class="custom-control-input" id="iba_pa_i" name="example1">
									    <label class="custom-control-label" for="iba_pa_i">Others:</label>
									      <input type="text" id="other_i" placeholder="Other...">
									    </div>
									</div>

								</div>

								<div class="form-group">

								  <label for="internet_rate"><b>7. Rate your internet connectivity from 1 to 5 (5 as the strongest 1 as the weakest):</b></label>

								  <select class="form-control" id="internet_rate">

								  	<option value=""></option>
								  	<option value="5">5</option>
								  	<option value="4">4</option>
								  	<option value="3">3</option>
								  	<option value="2">2</option>
								  	<option value="1">1</option>

								  </select>

								</div>

								<div class="form-group">
									
								  <label><b> 8. What setup do you prefer for your children for this coming school 2020-2021?</b></label>
								  
									<input type="hidden" id="learning_dmode">

									<div id="delivery_mode">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)" class="custom-control-input" id="l1" name="learning_delivery">
										    <label class="custom-control-label" for="l1">Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Purely online classes (all classes will be done online)" class="custom-control-input" id="l2" name="learning_delivery">
										    <label class="custom-control-label" for="l2">Purely online classes (all classes will be done online)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_delivery_mode();" value="Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)" class="custom-control-input" id="l3" name="learning_delivery">
										    <label class="custom-control-label" for="l3">Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)</label>
										</div>

									</div>

									<div class="input-group mb-3">
										<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="iba_pa_p();" value="" class="custom-control-input" id="iba_pa_p" name="learning_delivery">
									    <label class="custom-control-label" for="iba_pa_p">Others:</label>
									      <input type="text" id="other_p" placeholder="Other...">
									    </div>
									</div>

								</div>

								<div class="form-group">
									
								  <label>
								  	<b> 
								  		9. Are you interested to enroll in blended PMC classes? (for Elementary learners only)
								  	</b>
								  </label>
								  
									<input type="hidden" id="blended_pmc_val">

									<div id="blended_pmc">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="Yes" class="custom-control-input" id="pmc1" name="is_pmc">
										    <label class="custom-control-label" for="pmc1">Yes</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="No" class="custom-control-input" id="pmc2" name="is_pmc">
										    <label class="custom-control-label" for="pmc2">No</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_pmc();" value="Maybe" class="custom-control-input" id="pmc3" name="is_pmc">
										    <label class="custom-control-label" for="pmc3">Maybe</label>
										</div>

									</div>

									<div class="input-group mb-3">
										<div class="custom-control custom-checkbox">
									    <input type="checkbox" onclick="iba_pa_pmc();" value="" class="custom-control-input" id="iba_pa_pmc" name="is_pmc">
									    <label class="custom-control-label" for="iba_pa_pmc">Others:</label>
									      <input type="text" id="other_pmc" placeholder="Other...">
									    </div>
									</div>

								</div>

								<div class="form-group">
									
								  <label><b>10. What are the challenges that may affect your child's learning process through distance education? Choose all that applies:</b></label>
								  
									<input type="hidden" id="affects_DE_val">

									<div id="affects_DE">

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Lack of available gadgets/ equipment" class="custom-control-input" id="w1" name="affects">
										    <label class="custom-control-label" for="w1">Lack of available gadgets/ equipment</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Insufficient load/data allowance" class="custom-control-input" id="w2" name="affects">
										    <label class="custom-control-label" for="w2">Insufficient load/data allowance</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Unstable mobile/internet connection" class="custom-control-input" id="w3" name="affects">
										    <label class="custom-control-label" for="w3">Unstable mobile/internet connection</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Existing health condition's" class="custom-control-input" id="w4" name="affects">
										    <label class="custom-control-label" for="w4">Existing health condition's</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Difficulty in independent learning" class="custom-control-input" id="w5" name="affects">
										    <label class="custom-control-label" for="w5">Difficulty in independent learning</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Conflict with other activities(e.g house chores)" class="custom-control-input" id="w6" name="affects">
										    <label class="custom-control-label" for="w6">Conflict with other activities(e.g house chores)</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="High electrical consumptions" class="custom-control-input" id="w7" name="affects">
										    <label class="custom-control-label" for="w7">High electrical consumptions</label>
										</div>

										<div class="custom-control custom-checkbox">
										    <input type="checkbox" onclick="get_affects();" value="Distractions(i.e.| social media | noise from community/neighbor)" class="custom-control-input" id="w8" name="affects">
										    <label class="custom-control-label" for="w8">Distractions(i.e.,social media, noise from community/neighbor)</label>
										</div>

									</div>

									<div class="input-group mb-3">

										<div class="custom-control custom-checkbox">

										    <input type="checkbox" onclick="iba_pa_affects();" value="Iba pa" class="custom-control-input" id="iba_pa_affects" name="example1">
										    <label class="custom-control-label" for="iba_pa_affects">Others:
										    </label>
										    <input type="text" id="other_affects" placeholder="Other...">

									    </div>

									</div>

								</div>

								<div class="form-group">
									<label><b>11. Are there other suggestions and concerns that you may have?</b></label>
									<textarea class="form-control" rows="3" id="suggestions"></textarea>
								</div>
								
	  						</div>

	  					</div>

	  				</div>

	  			</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-success" onclick="update_student_info();"><span class="fa fa-save"></span> Save</button>
				<button class="btn btn-success" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
			</div>
		</div>

	</div>

</div>