<?php 

session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

//Terms
function update_term($con,$id,$term,$t_status){

	$sql = mysqli_query($con, "UPDATE tbl_terms set terms = '$term', is_active = '$t_status' where id = '$id'");

	if($sql){
		echo 1;
	}

}

function load_terms($con){

	$sql = mysqli_query($con, "SELECT * from tbl_terms");

	if(mysqli_num_rows($sql) > 0){

		while ($row = mysqli_fetch_assoc($sql)) { ?>
			
			<tr>

				<td><?php echo $row['terms'] ?></td>

				<td>
					<?php if($row['is_active'] == 1){
						echo '<span class="badge badge-success"> Active </span>';
					}else{
						echo '<span class="badge badge-danger"> Inactive </span>';
					} ?>
				</td>

				<td>
					<button class="btn btn-sm" onclick="edit_term('<?php echo $row['id'] ?>','<?php echo $row['terms'] ?>', '<?php echo $row['is_active'] ?>')"><span class="fa fa-edit fa-2x"></span></button>
				</td>

			</tr>

		<?php }

	}

}
//Terms

//edit student

function load_fam($con, $student_id){

	$father = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Father' ");

	$row_f = mysqli_fetch_assoc($father); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>FATHER INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_f" class="form-control text-capitalize" value="<?php echo $row_f['lastname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_f" class="form-control text-capitalize" value="<?php echo $row_f['firstname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
						<div class="form-group">
						  <label for="mname"><b>Middlename:</b></label>
						  <input type="text" id="mname_f" class="form-control text-capitalize" value="<?php echo $row_f['mi']; ?>">
						</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_f" class="form-control" value="<?php echo $row_f['contact']; ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_f" class="form-control text-capitalize" value="<?php echo $row_f['occupation']; ?>">
					</div>
				</div>
			</div>
		
		</div>

	</div>

    <?php 

    $mother = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Mother' ");

	$row_m = mysqli_fetch_assoc($mother); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>MOTHER'S INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">

				<div class="col-md-4">
					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_m" class="form-control text-capitalize" value="<?php echo $row_m['lastname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_m" class="form-control text-capitalize" value="<?php echo $row_m['firstname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Middlename:</b></label>
					  <input type="text" id="mname_m" class="form-control text-capitalize" value="<?php echo $row_m['mi']; ?>">
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_m" class="form-control" value="<?php echo $row_m['contact']; ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_m" class="form-control text-capitalize" value="<?php echo $row_m['occupation']; ?>">
					</div>
				</div>

			</div>

		</div>

	</div>

    <?php 

    $guardian = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Guardian' ");

	$row_g = mysqli_fetch_assoc($guardian); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>GUARDIAN'S INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">

				<div class="col-md-4">

					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_g" class="form-control text-capitalize" value="<?php echo $row_g['lastname']; ?>">
					</div>

				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_g" class="form-control text-capitalize" value="<?php echo $row_g['firstname']; ?>">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Middlename:</b></label>
					  <input type="text" id="mname_g" class="form-control text-capitalize" value="<?php echo $row_g['mi']; ?>">
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-4">

					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_g" class="form-control" value="<?php echo $row_g['contact']; ?>">
					</div>

				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_g" class="form-control text-capitalize" value="<?php echo $row_g['occupation']; ?>">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="relation_g"><b>Relation to the Learner:</b></label>
					  <input type="text" id="relation_g" class="form-control text-capitalize" value="<?php echo $row_g['relationship']; ?>">
					</div>
				</div>

			</div>
			
		</div>

	</div>

    <?php 

}

function save_member($con,$grade_id,$member,$student_id){

	$chk = mysqli_query($con, "SELECT * from tbl_households_members where grade_id = '$grade_id' and student_id = '$student_id'");

	if(mysqli_num_rows($chk) > 0){

		echo 2;

	}else{

		$sql = mysqli_query($con, "INSERT INTO tbl_households_members (student_id,grade_id,members) VALUES ('$student_id','$grade_id','$member')");

		if($sql){
			echo 1;
		}

	}

}

function load_household_members($con,$student_id,$grade_id){

	$sql = mysqli_query($con, "SELECT t1.*,t2.grade FROM tbl_households_members t1
				LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.ID where t1.student_id = '$student_id'");

	if (mysqli_num_rows($sql)>0) {
	?>

		<?php while ($row = mysqli_fetch_assoc($sql))  {  ?>

		  <li class="list-group-item d-flex justify-content-between align-items-left">

		    <?php echo $row['grade'] . ' = ' . $row['members']; ?>
		    <span class="fa fa-trash fa-2x w3-hover" onclick="remove_member('<?php echo $row['id'] ?>')"></span>
		  </li>		 
		
		<?php }
	 }

}

function remove_member($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_households_members where id = '$id'");
	if ($sql) {
		echo 1;
	}

}

function update_student_info($con,$s_id,$category,$lname,$fname,$mname,$suffix,$email,$fb_account,$lrn,$bdate,$age,$place_of_birth,$gender,$religion,$citizenship,$mother_tongue,$ethnicity_val,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g ,$contact_g,$occupation_g,$relation_g,$siblings,$with_medical_condition,$medical_condition_affects,$medical_condition,$other_mother_tongue,$other_ethnicity,$go_to_school_val,$teacher_home_val,$other_th,$gadgets_val,$other_gadget,$connected_to_internet_val,$internet_val,$other_i,$internet_rate,$learning_dmode,$other_p,$affects_DE_val,$other_affects,$blended_pmc_val,$other_pmc,$suggestions){

	$school_year = get_school_year_active($con);

	if(strlen($learning_dmode) == 0){
		$learning_delivery_mode = $learning_dmode;
	}else{
		$learning_delivery_mode = $learning_dmode .', '. $other_p;
	}

	if($lrn == 'undefined'){
		$lrn_val = '';
	}else{
		$lrn_val = $lrn;
	}

	$father_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$mother_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$guardian_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

	$sql = mysqli_query($con,"UPDATE tbl_students set lastname = '$lname',firstname = '$fname',mi = '$mname',gender = '$gender',age = '$age',address = '$address',contact = '$contact',birthday = '$bdate',place_of_birth = '$place_of_birth',religion = '$religion', citizenship = '$citizenship', siblings = '$siblings', with_medical_condition = '$with_medical_condition',medical_condition_affects = '$medical_condition_affects', medical_condition = '$medical_condition',suffix = '$suffix',email='$email',fb_account = '$fb_account',mother_tongue = '$mother_tongue',mother_tongue_other = '$other_mother_tongue',ethnicity = '$ethnicity_val',ethnicity_other = '$other_ethnicity',lrn  = '$lrn' WHERE student_id = '$s_id'");

	//Update father

	$chk_f = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Father'");

	if(mysqli_num_rows($chk_f) > 0){
		//update
		$sql2 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_f',firstname = '$fname_f',mi = '$mname_f',address = '$address',contact = '$contact_f',occupation = '$occupation_f' where student_id = '$s_id' and parent_type = 'Father'");
	}else{
		//insert
		$sql2 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$father_id','$lname_f','$fname_f','$mname_f','$address','$contact_f','$s_id','MALE','$occupation_f','Father')");
		$f = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$father_id')");
	}

	//Update mother

	$chk_m = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Mother'");

	if(mysqli_num_rows($chk_m) > 0){

		$sql3 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_m',firstname = '$fname_m',mi = '$mname_m',address = '$address',contact = '$contact_m',occupation = '$occupation_m' where student_id = '$s_id' and parent_type = 'Mother'");

	}else{
		$sql3 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$mother_id','$lname_m','$fname_m','$mname_m','$address','$contact_m','$s_id','FEMALE','$occupation_m','Mother')");
		$m = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$mother_id')");
	}

	//update guardian

	$chk_g = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Guardian'");

	if(mysqli_num_rows($chk_g) > 0){

		$sql4 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_g',firstname = '$fname_g',mi = '$mname_g',address = '$address',contact = '$contact_g',occupation = '$occupation_g',relationship = '$relation_g' where student_id = '$s_id' and parent_type = 'Guardian'");

	}else{

		$sql4 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,occupation,parent_type,relationship) values('$guardian_id','$lname_g','$fname_g','$mname_g','$address','$contact_g','$s_id','$occupation_g','Guardian','$relation_g')");
		$g = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$guardian_id')");

	}	


	$sql5 = mysqli_query($con, "UPDATE tbl_enrolled_students set category = '$category',available_gadgets = '$gadgets_val',available_gadgets_other = '$other_gadget',type_of_internet = '$internet_val',type_of_internet_other = '$other_i', internet_rate = '$internet_rate', learning_delivery_mode = '$learning_dmode',learning_delivery_mode_other = '$other_p', teacher_partner_home = '$teacher_home_val',teacher_partner_home_other = '$other_th', go_to_school = '$go_to_school_val', connected_to_internet = '$connected_to_internet_val', affects_DE = '$affects_DE_val', affects_DE_other = '$other_affects', is_blended_pmc = '$blended_pmc_val', is_blended_pmc_other = '$other_pmc',suggestions = '$suggestions' where student_id = '$s_id'");

	$sql6 =mysqli_query($con,"UPDATE tbl_households_members set is_active = 1, school_year = '$school_year' where student_id = '$s_id'");

	if($sql){
		echo 1;
	}

	if($sql2){
		echo 2;
	}
	if($sql3){
		echo 3;
	}
	if($sql4){
		echo 4;
	}
	if($sql5){
		echo 5;
	}
	if($sql6){
		echo 6;
	}

}

function load_supporting_files($con,$student_id){

	$docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$student_id'");

	if (mysqli_num_rows($docs)>0) {
		$num = 0; ?>
		<ul class="list-group" style="margin-top: 10px;">
				<li class="list-group-item d-flex justify-content-between list-group-item-info">
					<b> SUPPORTING DOCUMENTS AND CREDENTIALS</b>
				</li>
		<?php while ($d = mysqli_fetch_assoc($docs))  { $num++; ?>
			
			
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<b><a href="<?php echo $d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
					</b>
					<b>
						<a download="" href="<?php echo '../'.$d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> 
						</a>
						<a href="#" onclick="delete_doc('<?php echo $d['id'] ?>','<?php echo $d['url'] ?>')" title="Remove file"><span class="fa fa-trash"></span> </a>
					</b>
				</li>

		<?php } ?>
		</ul>
	<?php }

}

///edit student

function load_pass($con){
	
	$q = mysqli_query($con, "SELECT * from tbl_user_account where transid = '1'");

	if (mysqli_num_rows($q)>0) {

			$r = mysqli_fetch_assoc($q);
			$pass = $r['user_pwd'];
		}
	?>

	<input type="password" class="form-control" id="old_pass" required="" value="<?php echo $pass;?>">
	  
	  <div class="input-group-append">
	    <button class="btn btn-outline-secondary" onclick="showpass();">
	    	<span class="fa fa-eye" id="eye"></span><span class="fa fa-eye-slash" id="eye_slash"></span>
	    </button>
	  </div>

	  <script type="text/javascript">
	  		
	  		$(document).ready(function(e) {

	  			$("#eye_slash").css('display','none');

	  			$("#old_pass").keyup(function(e){

		        var old_pass = $("#old_pass").val();

		        if(old_pass != '')
		        {
		           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
		            $.ajax({
				    type:'POST',
				    url:url,
				    data:mydata,
				    cache:false,
				    success:function(data){
			
			    		if(data == 1){

				    		$('#new_pass').focus();
			                $('#old_pass_validator').text(' Your old password is correct.');
			                $('#old_pass_validator').removeClass('text-danger');
				    		$('#old_pass_validator').addClass('text-success');
				    		document.getElementById('btn_up_account').disabled = false;
			    		
				    	}else{

				    		$('#old_pass_validator').text(' Your old password is incorrect.');
				    		$('#old_pass_validator').removeClass('text-success');
				    		$('#old_pass_validator').addClass('text-danger');
				    		document.getElementById('btn_up_account').disabled = true;

				    	}

				    }
				    
				  });

		        } else {
		        	$("#oldpass").focus();      
		        }

		    });

	  		});

	  </script>

<?php }

function load_pic($con,$pic){

	echo '<img data-toggle="modal" data-target="#show_mypic" src="data:image/jpeg;base64,'.base64_encode($pic).'" class="img-fluid img-thumbnail" width="100"/>';

}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        return $row['id'];
    }
}

function get_company_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_company where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        return $row['companyid'];
    }
}

function payment_status($con,$student_id){

	$sql = mysqli_query($con, "SELECT * from tbl_payment_transactions where payment_status = '1' and student_id = '$student_id'");

	if(mysqli_num_rows($sql) > 0){
		echo '<span class="badge badge-success"> Paid </span>';
	}else{
		echo '<span class="badge badge-warning"> Unpaid </span>';
	}

}

function load_enrollees($con,$search,$grade_id,$school_year){

	//$school_year = get_school_year_active($con);

	$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$default_color = ' w3-'.$row['default_color'].' ';
		$school_name = ''.$row['company_name1'].'';
		$acronym = ''.$row['acronym'].'';
		$with_hs = ''.$row['with_highschool'].'';
		$with_payment = ''.$row['with_payment'].'';
	}
	
	if($search == "" || $search == null and $grade_id == ""){

		$sql = mysqli_query($con, "SELECT t1.*,t2.*,t3.grade,t2.is_active AS stat,t4.`section_name` FROM tbl_students t1
			LEFT JOIN tbl_enrolled_students t2 ON t1.`student_id` = t2.`student_id`
			LEFT JOIN tbl_grade t3 ON t1.grade = t3.ID
			LEFT JOIN tbl_sections t4 ON t2.`section_id` = t4.`id`
			LEFT JOIN tbl_accounts t5 on t1.`student_id` = t5.`student_id`
			WHERE t2.`school_year` = '$school_year' and t1.	is_active = 1 and t1.student_id <> '1001' ORDER BY t2.date_enrolled DESC ");

		

	}elseif ($search == "" || $search == null and $grade_id <> "") {

		$sql = mysqli_query($con, "SELECT t1.*,t2.*,t3.grade,t2.is_active as stat,t4.`section_name` FROM tbl_students t1
				LEFT JOIN tbl_enrolled_students t2 ON t1.`student_id` = t2.`student_id`
				left join tbl_grade t3 on t1.grade = t3.ID
				LEFT JOIN tbl_sections t4 ON t2.`section_id` = t4.`id`
				WHERE t1.`grade` = '$grade_id' AND t2.`school_year` = '$school_year' and is_delete=1 and t1.student_id <> '1001' ORDER BY t2.date_enrolled DESC");

	
	}elseif ($search <> "" || $search <> null and $grade_id == "") {

		$sql = mysqli_query($con, "SELECT t1.*,t2.*,t3.grade,t2.is_active as stat,t4.`section_name` FROM tbl_students t1
				LEFT JOIN tbl_enrolled_students t2 ON t1.`student_id` = t2.`student_id`
				left join tbl_grade t3 on t1.grade = t3.ID
				LEFT JOIN tbl_sections t4 ON t2.`section_id` = t4.`id`
				WHERE (t1.`firstname` LIKE '%$search%' OR t1.`lastname` LIKE '%$search%' OR t1.`grade` 
		LIKE '%$search%' OR t1.`section` LIKE '%$search%' OR t2.`date_trans` LIKE '%$search%' OR t4.`section_name` LIKE '%$search%')
		AND t2.`school_year` = '$school_year' and is_delete = 1 and t1.student_id <> '1001' ORDER BY t2.date_enrolled DESC");

		
	}else{

		$sql = mysqli_query($con, "SELECT t1.*,t2.*,t3.grade,t2.is_active AS stat,t4.`section_name` FROM tbl_students t1
		LEFT JOIN tbl_enrolled_students t2 ON t1.`student_id` = t2.`student_id`
		LEFT JOIN tbl_grade t3 ON t1.grade = t3.ID
		LEFT JOIN tbl_sections t4 ON t2.`section_id` = t4.`id` 
		WHERE (t1.`firstname` LIKE '%$search%' OR t1.`lastname` LIKE '%$search%' OR t1.`grade` 
		LIKE '%$search%' OR t1.`section` LIKE '%$search%' OR t2.`date_trans` LIKE '%$search%' OR t4.`section_name` LIKE '%$search%')
		AND t1.`grade` = '$grade_id' AND t2.`school_year` = '$school_year' and is_delete = 1 and t1.student_id <> '1001' ORDER BY t2.date_enrolled DESC");

	}

	if(mysqli_num_rows($sql)){ ?>

			<thead>
				<tr class="table-default">
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(1)')" style="cursor:pointer">Name <span class="fa fa-sort"></span></th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(2)')" style="cursor:pointer">Grade <span class="fa fa-sort"></span>	</th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(3)')" style="cursor:pointer">Category <span class="fa fa-sort"></span>	</th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(4)')" style="cursor:pointer">Gender <span class="fa fa-sort"></span>	</th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(5)')" style="cursor:pointer">Payment Status <span class="fa fa-sort"></span>	</th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(6)')" style="cursor:pointer">Enrollment Status <span class="fa fa-sort"></span>	</th>
			        <th onclick="w3.sortHTML('#tbl_enrollees', '.item', 'td:nth-child(7)')" style="cursor:pointer">Date <span class="fa fa-sort"></span>	</th>
			        <th>Action</th>
			    </tr>
			</thead>

			<tbody>

			<?php while ($row = mysqli_fetch_assoc($sql)) { ?>

				<tr class="item">
					<td><?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?></td>
					<td><?php echo $row['grade']?></td>
					<td><?php echo $row['category']; ?></td>
					<td><?php echo strtoupper($row['gender']); ?></td>
					<td><?php echo payment_status($con,$row['student_id']); ?></td>
					<td>
						
						<?php 

						if($row['stat'] == 1){
							echo '<span class="badge badge-pill badge-success">Enrolled</span>';
						}else{
							echo '<span class="badge badge-pill badge-warning">Waiting</span>';
						}

						?>

					</td>
					<td><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></td>
					<td>

						<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
					      <span class="fa fa-cogs"></span>
					    </button>
						<div class="dropdown-menu">
						  <a class="dropdown-item" onclick="student_profile('<?php echo $row['student_id'] ?>');"><span class="fa fa-id-card"></span> Student Profile</a>
						  <a class="dropdown-item" onclick="payments_info('<?php echo $row['student_id'] ?>');"><span class="fa fa-money-bill-wave"></span> Payments</a>
						  
						  <?php

							if($row['stat'] == 1){ ?>
								
								 <a class="dropdown-item" onclick="unenroll('<?php echo $row['student_id'] ?>');"><span class="fa fa-user-lock"></span> Unenroll</a>

							<?php }else{ ?>

								<a class="dropdown-item"onclick="enroll('<?php echo $row['student_id'] ?>','<?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?>','<?php echo $row['grade_id'] ?>');"><span class="fa fa-check"></span> Enroll
								</a>

							<?php
								}
							?>

							 <a class="dropdown-item" onclick="edit_student_profile('<?php echo $row['student_id'] ?>','<?php echo $row['category'] ?>', '<?php echo $row['grade'] ?>','<?php echo $row['lastname'] ?>','<?php echo $row['firstname'] ?>','<?php echo $row['mi'] ?>','<?php echo $row['suffix'] ?>','<?php echo $row['email'] ?>','<?php echo $row['fb_account'] ?>','<?php echo $row['esc_qvr_number'] ?>','<?php echo $row['lrn'] ?>','<?php echo $row['birthday'] ?>','<?php echo $row['age'] ?>','<?php echo $row['place_of_birth'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['religion'] ?>','<?php echo $row['citizenship'] ?>','<?php echo $row['mother_tongue'] ?>','<?php echo $row['ethnicity'] ?>','<?php echo $row['contact'] ?>','<?php echo $row['address'] ?>','<?php echo $row['siblings'] ?>','<?php echo $row['with_medical_condition'] ?>','<?php echo $row['medical_condition_affects'] ?>','<?php echo $row['medical_condition'] ?>', '<?php echo $row['go_to_school']; ?>', '<?php echo $row['teacher_partner_home']; ?>', '<?php echo $row['available_gadgets']; ?>', '<?php echo $row['connected_to_internet']; ?>', '<?php echo $row['type_of_internet']; ?>', '<?php echo $row['internet_rate']; ?>', '<?php echo $row['learning_delivery_mode']; ?>','<?php echo addslashes($row['affects_DE']); ?>','<?php echo $row['mother_tongue_other']; ?>','<?php echo $row['ethnicity_other']; ?>','<?php echo $row['teacher_partner_home_other']; ?>','<?php echo $row['available_gadgets_other']; ?>','<?php echo $row['type_of_internet_other']; ?>','<?php echo $row['learning_delivery_mode_other']; ?>','<?php echo $row['affects_DE_other']; ?>','<?php echo $row['is_blended_pmc']; ?>','<?php echo $row['is_blended_pmc_other']; ?>','<?php echo addslashes($row['suggestions']); ?>');"><span class="fa fa-edit"></span> Edit</a>

							 <a class="dropdown-item" onclick="del('<?php echo $row['student_id'] ?>','<?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?>');"><span class="fa fa-trash"></span> Delete</a>
						</div>

					</td>
				</tr>

			<?php } ?>
			<td class="text-success text-center" colspan="8">
				<b>
				<?php
				if(mysqli_num_rows($sql) > 1){

				 echo mysqli_num_rows($sql) . " students"; 

				}else{
				  echo mysqli_num_rows($sql) . " student"; 
				}
				?> 
				</b>
			</td>
			</tbody>

		<?php }else{
			echo '<td colspan="2">No Record Found!</td>';
	   }

	
}

function total_fees($con,$grade_id){

	$sql = mysqli_query($con, "SELECT sum(fee) as total from tbl_school_fees where grade_id = '$grade_id'");
	if(mysqli_num_rows($sql) > 0){

		$row = mysqli_fetch_assoc($sql);

		echo $row['total'];

	}
}

function load_fees($con,$grade_id){

	$sql = mysqli_query($con, "SELECT * from tbl_school_fees where grade_id = '$grade_id'");

	if(mysqli_num_rows($sql) > 0){ ?>

			<thead>
				<tr class="table-default">
			        <th width="70%">Fee</th>
			        <th>Amount</th>
			        <th>Action</th>
			    </tr>
			</thead>

			<tbody>
			
			<?php while ($row = mysqli_fetch_assoc($sql)) {?>

				<tr>
					<td><?php echo strtoupper($row['name']);?></td>
					<td><?php echo number_format($row['fee'],2);?></td>
					<td>
						<button class="btn btn-sm" title="Edit Fee" onclick="edit_fee('<?php echo $row['id'] ?>','<?php echo $row['name'] ?>','<?php echo $row['fee'] ?>','<?php echo $row['grade_id'] ?>');"><span class="fa fa-edit fa-2x"></span></button>

						<button class="btn btn-sm" title="Delete Fee" onclick="delete_fee('<?php echo $row['id'] ?>');"><span class="fa fa-trash fa-2x"></span></button>
					</td>
				</tr>

			<?php } ?>

			<tfoot class="table-info" style="font-weight: bolder;">
				<td colspan="1">Total:</td>
				<td colspan="2"><?php total_fees($con,$grade_id); ?></td>
			</tfoot>

			</tbody>

		<?php }else{
			echo '<td colspan="2">No Record Found!</td>';
	   }			

}

function save_update_fee($con,$fee_id,$fee_name,$fee_amount,$fee_grade_id){

	if($fee_id <> ''){
		//update
		$sql = "UPDATE tbl_school_fees set name = '$fee_name',fee = '$fee_amount' where id = '$fee_id'";
	}else{
		//insert
		$sql = "INSERT INTO tbl_school_fees (name,fee,grade_id) VALUES ('$fee_name','$fee_amount', '$fee_grade_id')";
	}

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

function delete_fee($con,$id){

	$sql = "DELETE FROM tbl_school_fees	where id = '$id'";

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

// Manage Terms of payment

function total_fees_term($con,$term_id,$grade_id){

	$sql = mysqli_query($con, "SELECT sum(amount) as total from tbl_terms_of_payment where term_id = '$term_id' and grade_id = '$grade_id'");
	if(mysqli_num_rows($sql) > 0){

		$row = mysqli_fetch_assoc($sql);

		echo number_format($row['total'],2);

	}
}

function load_terms_of_payment($con,$grade_id){

	$sql = mysqli_query($con, "SELECT * from tbl_terms");

	if(mysqli_num_rows($sql) > 0){

		while($row = mysqli_fetch_assoc($sql)){ 

			$term_id = $row['id']?>

			<ul class="list-group">
			  	<li class="list-group-item list-group-item-default"><b><?php echo $row['terms'] ?></b>
			   		<!-- <button class="btn btn-info" onclick="add_terms();"><span class="fa fa-plus"></span> Add</button> -->
				</li>
			</ul>

		<?php 

			$q = mysqli_query($con, "SELECT * from tbl_terms_of_payment where term_id = '$term_id' and grade_id = '$grade_id'");

			if(mysqli_num_rows($q) > 0){ ?>
				<div class="table-responsive">

				<table class="table table-bordered table-hover">

					<thead>
						<tr>
							<td width="60%">Type</td>
							<td>Amount</td>
							<td>Action</td>
						</tr>
					</thead>

					<tbody>

				<?php while($r = mysqli_fetch_assoc($q)){  ?>

					<tr>
						<td><?php echo $r['type']; ?></td>
						<td><?php echo number_format($r['amount'],2); ?></td>
						<td>
							<button class="btn btn-sm" title="Edit Fee" onclick="edit_term_payment('<?php echo $r['id'] ?>','<?php echo $term_id ?>','<?php echo $r['grade_id'] ?>');"><span class="fa fa-edit"></span> Update</button>
						</td>
					</tr>
					
				<?php } ?>
					</tbody>

					<tfoot class="table-success" style="font-weight: bolder;">
						<td colspan="1">Total:</td>
						<td colspan="2"><?php total_fees_term($con,$term_id,$grade_id); ?></td>
					</tfoot>

				</table>
				</div>
			<?php }

		}

	}

}

function save_update_term_fee($con,$t_id,$term_name,$term_amount,$term_grade_id){

	// if($t_id <> ''){
	// 	//update
	// 	$sql = "UPDATE tbl_terms_of_payment set type = '$term_name',amount = '$term_amount' where id = '$t_id'";
	// }else{
	// 	//insert
	// 	$sql = "INSERT INTO tbl_terms_of_payment (name,fee,grade_id) VALUES ('$fee_name','$fee_amount', '$fee_grade_id')";
	// }

	$sql = "UPDATE tbl_terms_of_payment set type = '$term_name',amount = '$term_amount' where id = '$t_id'";

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

function load_terms_fields($con,$id){

	$q = mysqli_query($con, "SELECT * from tbl_terms_of_payment where id = '$id'");
	$r = mysqli_fetch_assoc($q); ?>

	<div class="form-group">
		<label for="fee_name">Name:</label>
		<textarea class="form-control" rows="3" id="term_name"><?php echo $r['type']; ?></textarea>
	</div>

	<div class="form-group">
	  <label for="term_amount"><b>Amount:</b></label>
	  <input type="number" id="term_amount" class="form-control" value="<?php echo $r['amount']; ?>">
	</div>

<?php 
}

function save_update_terms_of_payment($con,$fee_id,$fee_name,$fee_amount,$fee_grade_id){

	if($fee_id <> ''){
		//update
		$sql = "UPDATE tbl_school_fees set name = '$fee_name',fee = '$fee_amount' where id = '$fee_id'";
	}else{
		//insert
		$sql = "INSERT INTO tbl_school_fees (name,fee,grade_id) VALUES ('$fee_name','$fee_amount', '$fee_grade_id')";
	}

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

// Manage Terms of payment

// Manage Company Details

function remove_file($con,$file_name){
	unlink($file_name);
}

function load_emails($con){

	$cid = get_company_active($con);

	$sql = mysqli_query($con,"SELECT * FROM tbl_company_emails WHERE company_id = '$cid ' AND is_active = '1' ");
	
	if(mysqli_num_rows($sql) > 0){

		echo mysqli_num_rows($sql);

		while ($row = mysqli_fetch_assoc($sql)) { ?>
			
			<tr>
				<td><?php echo $row['designation'] ?></td>
				<td><?php echo $row['email'] ?></td>
				<td>
					<?php 

						if($row['is_active'] == 1){
							echo '<div class="p-2 bg-success text-white text-center">Active</div>';
						}else{
							echo '<div class="p-2 bg-danger text-white text-center">Inactive</div>';
						}

					?>
				</td>
				<td>
					<button class="btn btn-sm" title="Edit email" onclick="edit_email('<?php echo $row['Id'] ?>','<?php echo $row['designation'] ?>','<?php echo $row['email'] ?>')"><span class="fa fa-edit fa-2x"></span></button>

					<button class="btn btn-sm" title="Delete email" onclick="delete_email('<?php echo $row['Id'] ?>')"><span class="fa fa-trash fa-2x"></span></button>
				</td>
			</tr>


		<?php }

	}

		
}

function load_banks($con){

	$cid = get_company_active($con);

	$sql = mysqli_query($con,"SELECT * FROM tbl_company_bank_account WHERE company_id = '$cid ' AND is_active = '1' ");
	
	if(mysqli_num_rows($sql) > 0){

		echo mysqli_num_rows($sql);

		while ($row = mysqli_fetch_assoc($sql)) { ?>
			
			<tr>
				<td><?php echo $row['bank_name'] ?></td>
				<td><?php echo $row['account_name'] ?></td>
				<td><?php echo $row['account_number'] ?></td>
				<td>
					<?php 

						if($row['is_active'] == 1){
							echo '<div class="p-2 bg-success text-white text-center">Active</div>';
						}else{
							echo '<div class="p-2 bg-danger text-white text-center">Inactive</div>';
						}

					?>
				</td>
				<td>
					<button class="btn btn-sm" title="Edit email" onclick="edit_bank('<?php echo $row['id'] ?>','<?php echo $row['bank_name'] ?>','<?php echo $row['account_name'] ?>','<?php echo $row['account_number'] ?>')"><span class="fa fa-edit fa-2x"></span></button>

					<button class="btn btn-sm" title="Delete email" onclick="delete_bank('<?php echo $row['id'] ?>')"><span class="fa fa-trash fa-2x"></span></button>
				</td>
			</tr>


		<?php }

	}

}

function save_update_bank($con,$bank_id,$bank_name,$account_name,$account_number){

	$cid = get_company_active($con);
	$date = date('Y-m-d H:i:s');
	if($bank_id == ''){
		//insert

		$sql = "INSERT INTO tbl_company_bank_account (company_id,bank_name,account_name,account_number,date_added) VALUES ('$cid','$bank_name','$account_name','$account_number','$date')";

	}else{
		//update
		$sql = "UPDATE tbl_company_bank_account set bank_name = '$bank_name', account_name = '$account_name', account_number = '$account_number',date_modified = '$date' where id ='$bank_id'";
	}

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

function delete_bank($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_company_bank_account where id = '$id'");

	if($sql){
		echo 1;
	}else{
		echo 404;
	}

}

function save_update_company($con,$company_name,$company_name2,$address,$telephone,$cellphone,$acronym){

	$cid = get_company_active($con);

	$sql = "UPDATE tbl_company set company_name1 = '$company_name',company_name2 = '$company_name2', address = '$address',telephone = '$telephone',cellphone ='$cellphone', acronym = '$acronym'  where companyid = '$cid'";

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

//Manage Email
function save_update_email($con,$email_id,$designation,$email){

	$cid = get_company_active($con);
	$date = date('Y-m-d H:i:s');
	if($email_id == ''){
		//insert

		$sql = "INSERT INTO tbl_company_emails (company_id,designation,email,is_active) VALUES ('$cid','$designation','$email','1')";

	}else{
		//update
		$sql = "UPDATE tbl_company_emails set designation = '$designation', email = '$email' where Id ='$email_id'";
	}

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

function delete_email($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_company_emails where Id = '$id'");

	if($sql){
		echo 1;
	}else{
		echo 404;
	}

}

// Manage Email

function load_logo($con){

	$cid = get_company_active($con);
	$sql = mysqli_query($con,"SELECT * FROM tbl_company where companyid = '$cid'");
	if(mysqli_num_rows($sql) > 0){

	$row = mysqli_fetch_assoc($sql); ?>

	<img width="200" src="data:image/jpeg;base64,<?php echo base64_encode($row['logo']) ?>" class="rounded img-fluid"/>

	<?php }
}

function load_company_details($con){

	$sql = mysqli_query($con,"SELECT * FROM tbl_company where is_active = 1");

	if(mysqli_num_rows($sql) > 0){

		$row = mysqli_fetch_assoc($sql); ?>

				<div class="form-group">
				  <label for="company_name"><b>Company Name:</b></label>
				  <input type="text" id="company_name" class="form-control text-capitalize" id="company_name" value="<?php echo $row['company_name1']; ?>">
				</div>

				<div class="form-group">
				  <label for="company_name2"><b>Other detail:</b></label>
				  <input type="text" id="company_name2" class="form-control text-capitalize" id="company_name2" value="<?php echo $row['company_name2']; ?>">
				</div>

				<div class="form-group">
				  <label for="address"><b>Address:</b></label>
				  <textarea class="form-control" rows="2" id="address"><?php echo $row['address']; ?></textarea>
				</div>

				<div class="form-group">
				  <label for="telephone"><b>Telephone:</b></label>
				  <input type="text" id="telephone" class="form-control text-capitalize" id="telephone" value="<?php echo $row['telephone']; ?>">
				</div>

				<div class="form-group">
				  <label for="cellphone"><b>Cellphone:</b></label>
				  <input type="text" id="cellphone" class="form-control text-capitalize" id="cellphone" value="<?php echo $row['cellphone']; ?>">
				</div>

				<div class="form-group">
				  <label for="acronym"><b>Acronym:</b></label>
				  <input type="text" id="acronym" class="form-control text-capitalize" id="acronym" value="<?php echo $row['acronym']; ?>">
				</div>

				<button class="btn btn-primary btn-block" id="btn_save_update_c" onclick="save_update_company();">
				<span class="fa fa-save"></span> Save
				</button>
		
	<?php }

}

// Manage Sections 
function load_sections($con,$grade_id){

	if($grade_id == '' || $grade_id == null){

		$sql = mysqli_query($con,"SELECT t1.*,t2.`grade` FROM tbl_sections t1
								LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.`ID`
								ORDER BY section_name ASC ");
	
		if(mysqli_num_rows($sql) > 0){

			while ($row = mysqli_fetch_assoc($sql)) { ?>
				
				<tr>
					<td><?php echo $row['grade'] ?></td>
					<td><?php echo $row['section_name'] ?></td>
					<td>
						<button class="btn btn-sm" title="Edit Section" onclick="edit_section('<?php echo $row['id'] ?>','<?php echo $row['section_name'] ?>','<?php echo $row['grade_id'] ?>')"><span class="fa fa-edit fa-2x"></span></button>

						<button class="btn btn-sm" title="Delete Section" onclick="delete_section('<?php echo $row['id'] ?>')"><span class="fa fa-trash fa-2x"></span></button>
					</td>
				</tr>

			<?php }

		}else{
			echo '<tr> <td colspan="3" class="text-danger"> No data </td> </tr>';
		}

	}else{

		$sql = mysqli_query($con,"SELECT t1.*,t2.`grade` FROM tbl_sections t1
								LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.`ID` where t1.grade_id = '$grade_id' order by t1.section_name asc ");
	
		if(mysqli_num_rows($sql) > 0){

			while ($row = mysqli_fetch_assoc($sql)) { ?>
				
				<tr>
					<td><?php echo $row['grade'] ?></td>
					<td><?php echo $row['section_name'] ?></td>
					<td>
						<button class="btn btn-sm" title="Edit Section" onclick="edit_section('<?php echo $row['id'] ?>','<?php echo $row['section_name'] ?>','<?php echo $row['grade_id'] ?>')"><span class="fa fa-edit fa-2x"></span></button>

						<button class="btn btn-sm" title="Delete Section" onclick="delete_section('<?php echo $row['id'] ?>')"><span class="fa fa-trash fa-2x"></span></button>
					</td>
				</tr>

			<?php }

		}else{
			echo '<tr> <td colspan="3" class="text-danger"> No data </td> </tr>';
		}

	}

}

function save_update_section($con,$sec_id,$sec_grade,$sec_name){

	if($sec_id == ''){
		//insert

		$sql = "INSERT INTO tbl_sections (section_name,grade_id) VALUES ('$sec_name','$sec_grade')";

	}else{
		//update
		$sql = "UPDATE tbl_sections set section_name = '$sec_name', grade_id = '$sec_grade' where id ='$sec_id'";
	}

	if(mysqli_query($con,$sql)){
		echo 1;
	}else{
		echo 404;
	}

}

function delete_section($con,$id){

	$chk = mysqli_query($con, "SELECT * from tbl_enrolled_students where section_id = '$id'");

	if(mysqli_num_rows($chk) > 0){
		echo 2;
	}else{

		$sql = mysqli_query($con, "DELETE FROM tbl_sections where id = '$id'");

		if($sql){
			echo 1;
		}else{
			echo 404;
		}

	}

}

function load_grade_sec($con,$grade_id_section){

	$sql = mysqli_query($con, "SELECT * from tbl_sections where grade_id = '$grade_id_section'");

	if(mysqli_num_rows($sql) > 0){ ?>

		 <select class="form-control" id="sec_grade">
				<option selected="" value="">-- Select Section --</option>

		<?php while ($row = mysqli_fetch_assoc($sql)) { ?>
			
				<option value="<?php echo $row['id'] ?>"><?php echo $row['section_name'] ?></option>
				
		<?php } ?>
		</select>

	<?php }else{
		echo '<span class="badge badge-danger">No section found! </span>';
	}


}

// Manage Sections 

// Manage Compnay details

function load_student_info($con,$student_id){

	$sy = get_school_year_active($con);

	$sql = mysqli_query($con,"SELECT t1.*,t2.grade AS grade_name,t4.section_name FROM tbl_students t1 
								LEFT JOIN tbl_grade t2 ON t1.`grade` = t2.`ID`
								LEFT JOIN tbl_enrolled_students t3 ON t1.`student_id` = t3.`student_id`
								LEFT JOIN tbl_sections t4 ON t3.`section_id` = t4.`id`
								WHERE t1.student_id = '$student_id' AND t3.`school_year` = '$sy'");

    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);

        $name = $row['lastname'].', '.$row['firstname'].' '.$row['mi'];

    } 
?>

	<ul class="nav nav-pills nav-justified" style="font-weight: bolder;">
	  <li class="nav-item">
	    <a class="nav-link active" data-toggle="pill" href="#info"><span class="fa fa-registered"></span> Personal Information</a>
	  </li>
	  <li class="nav-item	">
	    <a class="nav-link" data-toggle="pill" href="#fam"><span class="fa fa-home"></span> Family Information</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" data-toggle="pill" href="#other_info"><span class="fa fa-money-bill-wave"></span> Other Information</a>
	  </li>
	</ul>
	<br>
	<div class="tab-content">

	  <div class="tab-pane container active" id="info">

	  	<ul class="list-group">
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Name:
				<b><?php echo strtoupper($name); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Grade & Section:
				<b><?php echo strtoupper($row['grade_name']) .' - '. strtoupper($row['section_name']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Gender:
				<b><?php echo strtoupper($row['gender']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Place of birth:
				<b><?php echo strtoupper($row['place_of_birth']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Birth date:
				<b><?php echo strtoupper($row['birthday']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Age:
				<b><?php echo strtoupper($row['age']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Address:
				<b><?php echo strtoupper($row['address']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Contact:
				<b><?php echo strtoupper($row['contact']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Religion:
				<b><?php echo strtoupper($row['religion']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Citizenship:
				<b><?php echo strtoupper($row['citizenship']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Mother Tongue(Dialect Use at Home):
				<b><?php echo strtoupper($row['mother_tongue']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Ethnicity:
				<b><?php echo strtoupper($row['ethnicity']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Email:
				<b><?php echo strtoupper($row['email']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Facebook:
				<b><?php echo strtoupper($row['fb_account']); ?></b>
			</li>

		</ul>

	  </div>
	  <div class="tab-pane container" id="fam">

	  	<?php  

	  		$father= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Father' and student_id = '$student_id'");

	  		if (mysqli_num_rows($father)>0) {
		        $f = mysqli_fetch_assoc($father);

		        $fname = $f['lastname'].', '.$f['firstname'].' '.$f['mi'];
		        ?>

		        <ul class="list-group">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>FATHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($fname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($f['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($f['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($f['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{

		    	echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>FATHER INFORMATION</b>
					</li> <br> <span class="badge badge-warning">No data.</span>';
		    } 

	  	?>

	  	<?php  

	  		$mother= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Mother' and student_id = '$student_id'");

	  		if (mysqli_num_rows($mother)>0) {
		        $m = mysqli_fetch_assoc($mother);

		        $mname = $m['lastname'].', '.$m['firstname'].' '.$m['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>MOTHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($mname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($m['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($m['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($m['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>MOTHER INFORMATION</b>
					</li><br><span class="badge badge-warning">No data.</span>';
		    } 

	  	?>
	  	<?php  

	  		$guardian= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Guardian' and student_id = '$student_id'");

	  		if (mysqli_num_rows($guardian)>0) {
		        $g = mysqli_fetch_assoc($guardian);

		        $gname = $g['lastname'].', '.$g['firstname'].' '.$g['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>GUARDIAN'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($gname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($g['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($g['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($g['occupation']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Relationship:
						<b><?php echo strtoupper($g['relationship']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo '<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>GUARDIAN INFORMATION</b>
					</li><br><span class="badge badge-warning">No data.</span>';
		    } 

	  	?>
	  		<ul class="list-group" style="margin-top: 10px;">
			  	<li class="list-group-item d-flex justify-content-between align-items-center">
					Siblings:
					<b><?php echo strtoupper($row['siblings']); ?></b>
				</li>
			</ul>

	  </div>

	  <div class="tab-pane container" id="other_info">

	  	<ul class="list-group">
	  		<li class="list-group-item d-flex justify-content-between list-group-item-info">
				<b>A. SCHOLASTIC INFORMATION</b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Kindergarten:
				<b><?php echo strtoupper($row['si_kindergarten']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Grade School:
				<b><?php echo strtoupper($row['si_gradeschool']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Junior High School:
				<b><?php echo strtoupper($row['si_junior_hs']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Senior High School:
				<b><?php echo strtoupper($row['si_junior_hs']); ?></b>
			</li>

		</ul>

	  	<ul class="list-group" style="margin-top: 10px;">
			<li class="list-group-item d-flex justify-content-between list-group-item-info">
				<b>B. HEALTH RECORDS</b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Do you have impairment, disability, or long term medical condition?
				<b><?php echo strtoupper($row['with_medical_condition']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Does your impairment, disability, or long-term medical condition affect your study?
				<b><?php echo strtoupper($row['medical_condition_affects']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				What is your impairment, disability, or long-term medical condition?
				<b><?php echo strtoupper($row['medical_condition']); ?></b>
			</li>

		</ul>

		<?php

			$docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$student_id'");

			if (mysqli_num_rows($docs)>0) {
				$num = 0;
				while ($d = mysqli_fetch_assoc($docs))  { $num++; ?>
					
					<ul class="list-group" style="margin-top: 10px;">
						<li class="list-group-item d-flex justify-content-between list-group-item-info">
							<b>C. SUPPORTING DOCUMENTS AND CREDENTIALS</b>
						</li>
						<li class="list-group-item d-flex justify-content-between align-items-center">
							<b><a href="<?php echo '../'.$d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
							</b>
							<b><a download="" href="<?php echo '../'.$d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> </a></b>
						</li>

					</ul>


				<?php }

			}

		?>


	  </div>

	</div>

<?php }


function max_balance($con,$student_id){

	$sql = mysqli_query($con, "SELECT max(t1.`balance`) as total_balance FROM tbl_payment_transactions t1 
							   LEFT JOIN tbl_school_year t2 ON t1.`school_year` = t2.`id` where t1.student_id = '$student_id'");
	if(mysqli_num_rows($sql) > 0){

		$row = mysqli_fetch_assoc($sql);

		echo $row['total_balance'];

	}

}

function load_payment_info($con,$student_id){

	$sql = mysqli_query($con, "SELECT t1.*,t2.`school_year` FROM tbl_payment_transactions t1 
							   LEFT JOIN tbl_school_year t2 ON t1.`school_year` = t2.`id` where t1.student_id = '$student_id' and t1.confirmation_status !=0 ORDER BY balance DESC");

		if(mysqli_num_rows($sql)){ ?>

			<thead>
				<tr class="table-info">
					<th>Year</th>
					<th>Amount Due</th>
			        <th>Payment Due</th>
					<th>Due Balance</th>
			        <th>Balance</th>
			        <th>View</th>
					
			    </tr>
			</thead>

			<tbody>

			<?php while ($row = mysqli_fetch_assoc($sql)) {
				$before_update_balance=$row['balance']+$row['due_payment'];
			 ?>

				<tr>
					<td><?php echo $row['school_year']?></td>
					<!-- <td><?php echo date('F d, Y',strtotime($row['date_accepted'])); ?></td> -->
						<?php 

						if($row['due_payment'] > 0){ ?>

							<td><?php echo number_format($row['amount_due'],2)?></td>
							<td><?php echo number_format($row['due_payment'],2)?></td>
							
							<td><?php echo number_format($row['due_balance'],2)?></td>
					<?php 	}else{ ?>
							<td class="text-center" colspan="3">No Payment</td>
					<?php	}

					?>
					<td class="text-right"><?php echo number_format($row['balance'],2)?></td>
					<td><button class="btn btn-dark" onclick="view_payment_details('<?php echo $row['due_payment']?>','<?php echo $row['bank']?>','<?php echo $row['ref_no']?>')"><i class="fa fa-eye"></i></button></td>
				</tr>

			<?php } 
			?>
			</tbody>

			

		<?php }
		$sql2 = mysqli_query($con, "SELECT t1.`balance` AS total_balance FROM tbl_accounts t1 LEFT JOIN tbl_school_year t2 ON t1.`school_year`=t2.`id` WHERE student_id='$student_id'");

		if(mysqli_num_rows($sql2) > 0){

		$t = mysqli_fetch_assoc($sql2);
		$total = $t['total_balance'];

		}
		?>
		<tfoot class="w3-gray text-white">
			<tr>
				<td colspan="4" class="text-right">Total Balance</td>
				<td class="text-right"><?php echo number_format($total,2); ?></td>
				<td>View</td>
			</tr>
		</tfoot>
		<?php

}

function enroll_student($con,$stud_id,$sec_id,$school_year){

	if($sec_id <> ''){
		$sql = mysqli_query($con, "UPDATE tbl_enrolled_students set is_active = 1, section_id = '$sec_id' where student_id = '$stud_id' and school_year = '$school_year'");
	}else{

		$sql = mysqli_query($con, "UPDATE tbl_enrolled_students set is_active = 1 where student_id = '$stud_id' and school_year = '$school_year'");
	}

	if ($sql) {
		echo 1;
	}
}

function unenroll_student($con,$stud_id,$school_year){

	$sql = mysqli_query($con, "UPDATE tbl_enrolled_students set is_active = 0,section_id = '' where student_id = '$stud_id' and school_year = '$school_year'");
	if ($sql) {
		echo 1;
	}
}

function delete_student($con,$stud_id){

	$school_year = get_school_year_active($con);

	//check if student has transactions
	$chk = mysqli_query($con, "SELECT * from tbl_enrolled_students where school_year = '$school_year' and student_id = '$stud_id' and is_active = 1");

	if(mysqli_num_rows($chk) > 0){
		//with transactions
		echo 2;
		
	}else{
		$sql = mysqli_query($con, "DELETE FROM tbl_enrolled_students where student_id = '$stud_id' and school_year = '$school_year'");
		if ($sql) {
			echo 1;
		}
	}

}
//check the last comment <<<-----
function load_payments($con){
	$sql=mysqli_query($con,"SELECT t1.lastname,t1.firstname,t1.mi,t2.* FROM tbl_students t1 
	LEFT JOIN tbl_payment_transactions t2 ON t1.`student_id`=t2.`student_id` WHERE due_payment!=0 AND t2.`id` IS NOT NULL");
	if(mysqli_num_rows($sql)>0){
		
			?>
			<thead>
				<tr>
					<th>Name</th>
					<th>Payment</th>
					<th>Pay</th>
					<th>Balance</th>
					<th>Date Updated</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<?php
				while($row=mysqli_fetch_assoc($sql)){
					?>
					<td><?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?></td>
					<td><?php echo number_format($row['amount_due'])?></td>
					<td><?php echo number_format($row['due_payment'])?></td>
					<td><?php echo number_format($row['due_balance'])?></td>
					<td><?php echo date('F d,Y',strtotime($row['date_trans']))?></td>
					<td>
						<?php
							if($row['confirmation_status']==0){
								?>	
								<button class="btn btn-sm" onclick="view_payment('<?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?>','<?php echo $row['id']?>','<?php echo $row['file']?>','<?php echo $row['file_type']?>','<?php echo $row['due_payment']?>','<?php echo number_format($row['due_payment']) ?>','<?php echo $row['student_id'] ?>','<?php echo $row['amount_due'] ?>','<?php echo $row['bank'] ?>','<?php echo $row['account_no'] ?>','<?php echo $row['account_name'] ?>','<?php echo $row['contact_no'] ?>','<?php echo $row['address'] ?>','<?php echo $row['ref_no'] ?>')" title="View Payment">
									<span class="fa fa-eye fa-2x"></span>
								</button>
								<button class="btn btn-sm" onclick="delete_payment('<?php echo strtoupper($row['lastname'].', '.$row['firstname'].' '.$row['mi']);?>','<?php echo $row['id']?>')" title="Delete Payment">
									<span class="fa fa-trash fa-2x"></span>
								</button>
								<?php
							}else{
								?>
								<span class="font-weight-bold"><i class="fa fa-check"></i> Accepted</span>
								<?php
							}
						?>
					</td>
				</tr>
				<?php } ?>
			</tbody>

			<?php
		
	}else{
		echo '<td colspan="2">No Record Found!</td>';
	}
}
function accept_payment($con,$id,$amount,$student_id,$user,$date_time,$amount_due){

	$account_balance=select_this_field_value($con,'tbl_accounts','balance','student_id',$student_id);
	$new_amount=$account_balance-$amount;
	$update=update_this_field_value($con,'tbl_accounts','balance',$new_amount,'student_id',$student_id);
	if($amount_due<$amount){
		$status = 2;
	}else if($amount_due>$amount){
		$status = 1;
	}else if($amount_due==$amount){
		$status = 3;
	}
	$transaction=mysqli_query($con,"UPDATE tbl_payment_transactions SET balance='$new_amount',accepted_by='$user',date_accepted='$date_time',payment_status='$status',confirmation_status='1' where id='$id' ");
	echo 1;
	
}
function select_this_field_value($con,$table,$field,$where,$id){
	$sql=mysqli_query($con,"SELECT * FROM $table where $where='$id'");
	$row=mysqli_fetch_assoc($sql);
	return $row[$field];
}
function update_this_field_value($con,$table,$set,$value,$where,$id){
	$sql=mysqli_query($con,"UPDATE $table SET $set=$value where $where=$id");
	return 1;
}
function delete_payment($con,$id){
	$delete=delete_function($con,'tbl_payment_transactions','id',$id);
	if($delete==1){
		echo 1;
	}
}
function delete_function($con,$table,$where,$id){
	$sql=mysqli_query($con,"DELETE FROM $table where $where='$id'");
	return 1;
}

function select_count($con,$query){

	$sql=mysqli_query($con,$query);
	$row=mysqli_fetch_assoc($sql);
	echo $row['count'];

}
function count_studentgrade($con,$id,$limit,$offset,$sy_id){
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT t1.*,s.* FROM tbl_grade t1 LEFT JOIN (
		SELECT COUNT(*)AS 'count',`grade` AS id FROM tbl_students a LEFT JOIN
		 tbl_enrolled_students b ON a.`student_id`=b.`student_id`
		  WHERE b.`school_year`=$sy_id AND b.`is_active`=$id GROUP BY grade) AS s ON t1.`ID`=s.id  ORDER BY t1.`ID` ASC limit $limit OFFSET $offset ");

	?>
	<div class="form-row">
	<?php
	while($row=mysqli_fetch_assoc($sql)){
		?>
		<div class="col-sm-4 col-md-4 col-xl-4">
			<div class="card  mt-2">
				<div class=" cards-image w3-text-white h-100">
					<div class=" form-row">
						<div class="col-xl-2 col-sm-12 text-center">
							<i class="fa fa-user fa-5x"></i>
						</div>
						<div class="col-xl-10 col-sm-12 text-center"><h4 class="mt-2"><?php echo $row['grade']?></h4> </div>
					</div>
					<div class="dropdown-divider"></div>
					<div class="text-center ">
						
						
					</div>
				</div>
				<div class="card-body text-center">
					<h3><?php echo number_format($row['count']) ?></h3>
				</div>
				<div class="card footer text-center">Students</div>
			</div>
		</div>
										
																
		<?php
	}
	?>
	</div>
	<?php
}

function count_device_survey($con,$sy_id){
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(VALUE='','None',VALUE) as VALUE,COUNT(*) AS 'count' FROM
	(SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.available_gadgets, ',', n.n), ',', -1)) VALUE
	  FROM tbl_enrolled_students CROSS JOIN 
	  (
	   SELECT a.N + b.N * 10 + 1 n
		 FROM 
		(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) a
	   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 ) b
		ORDER BY n 
	   ) n
	 WHERE school_year=$sy_id AND n.n <= 1 + (LENGTH(tbl_enrolled_students.available_gadgets) - LENGTH(REPLACE(tbl_enrolled_students.available_gadgets, ',', '')))
	 ORDER BY VALUE) nt0 GROUP BY VALUE");
	?>
	<ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Student Gadgets</li>
    <?php
    if(mysqli_num_rows($sql)>0){
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['VALUE']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
    	<?php
    	}
    }else{
    	?>
    	<li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  No Record
              </span>
            </li>
    	<?php
    	}
    ?>
    </ul>
	<?php
}
function count_connection_survey($con,$sy_id){
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(VALUE='','None',VALUE) as VALUE,COUNT(*) AS 'count' FROM
	(SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.type_of_internet, ',', n.n), ',', -1)) VALUE
	  FROM tbl_enrolled_students CROSS JOIN 
	  (
	   SELECT a.N + b.N * 10 + 1 n
		 FROM 
		(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) a
	   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) b
		ORDER BY n 
	   ) n
	 WHERE school_year=$sy_id AND n.n <= 1 + (LENGTH(tbl_enrolled_students.type_of_internet) - LENGTH(REPLACE(tbl_enrolled_students.type_of_internet, ',', '')))
	 ORDER BY VALUE) nt0 GROUP BY VALUE");
	?>
	<ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Student Connection</li>
    <?php
    if (mysqli_num_rows($sql)>0) {
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['VALUE']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          
    	<?php
    	}
    }else{
    	?>
        <li class="list-group-item list-group-item-action">
         <span style="text-align: justify;">
              No Record 
          </span>
        </li>
      
	<?php
    }
    ?>
    </ul>
	<?php
}
function tab_button($con,$id,$displayId){
	$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT t1.*,s.* FROM tbl_grade t1 LEFT JOIN (
		SELECT COUNT(*)AS 'count',`grade` AS id FROM tbl_students a LEFT JOIN
		 tbl_enrolled_students b ON a.`student_id`=b.`student_id`
		  WHERE b.`school_year`=$school_yearId AND b.`is_active`=$id GROUP BY grade) AS s ON t1.`ID`=s.id  ORDER BY t1.`ID` ASC");

	$num=mysqli_num_rows($sql);
	$divide=$num/3;
	if ($divide==floor($divide)) {
		$round=$divide;
	}else{
		$round=$divide+1;
	}
	for ($i=1; $i <= floor($round); $i++) {
		if ($i==1) {
			$numer=3;
			$number=0;
		 }else{
		 	$numer=3;
		 	$numbkk=$i-1;
		 	$number=$numbkk*3;
		 } 
		?>
		<button class="btn w3-border tab-btn" id="tabbtn<?php echo $i?>-<?php echo $id?>" onclick="count_grade_r('<?php echo $id?>','<?php echo $numer;?>','<?php echo $number;?>','<?php echo $displayId?>','tabbtn<?php echo $i?>-<?php echo $id?>')"><?php echo $i;?></button>
		<?php
	}
}

function load_speed($con,$sy_id){
	// $school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(numbering.id='','None',numbering.id) AS 'name',COUNT(a.`internet_rate`) AS 'count',numbering.type FROM (SELECT 1 AS id,'Very Slow' AS 'type' UNION SELECT 2,'Slow' UNION SELECT 3,'Good' UNION SELECT 4,'Fast' UNION SELECT 5,'Very Fast' UNION SELECT '','Ignore')AS numbering LEFT JOIN tbl_enrolled_students a
                    ON numbering.id=a.`internet_rate` WHERE a.`school_year`=$sy_id OR a.`school_year` IS NULL GROUP BY numbering.id");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Internet Speed</li>
    <?php
    while ($row=mysqli_fetch_assoc($sql)) {
    	
    	?>
    	
            
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['type']?>
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          
    	<?php
    }
    ?>
    </ul>
    <?php
}
function load_delivery($con,$sy_id)
{
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(VALUE='','None',VALUE) as VALUE,COUNT(*) AS 'count' FROM
	(SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.learning_delivery_mode, ',', n.n), ',', -1)) VALUE
	  FROM tbl_enrolled_students CROSS JOIN 
	  (
	   SELECT a.N + b.N * 10 + 1 n
		 FROM 
		(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) a
	   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) b
		ORDER BY n 
	   ) n
	 WHERE school_year=$sy_id AND n.n <= 1 + (LENGTH(tbl_enrolled_students.learning_delivery_mode) - LENGTH(REPLACE(tbl_enrolled_students.learning_delivery_mode, ',', '')))
	 ORDER BY VALUE) nt0 GROUP BY VALUE");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Learning Delivery</li>
    <?php
    if (mysqli_num_rows($sql)>0) {
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['VALUE']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
    	<?php
    	}
    }else{
    	?>
    	<li class="list-group-item list-group-item-action">
         <span style="text-align: justify;">
              No Record
          </span>
        </li>
    	<?php
    }
    ?>
    </ul>
    <?php
}
function load_learning_day($con,$sy_id)
{
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT COUNT(*) AS 'count',IF(day_learning='','None',day_learning) AS 'day_learning' FROM tbl_enrolled_students WHERE school_year=$sy_id GROUP BY day_learning");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Learning Schedule</li>
    <?php
    if(mysqli_num_rows($sql)>0){
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['day_learning']?>
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          </ul>
    	<?php
   		}
    }else{
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  No Record
              </span>
            </li>
          </ul>
    	<?php
    }
    ?>
    <ul class="list-group">
    <?php
}
function load_financial_cap($con,$sy_id)
{
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT COUNT(*) AS 'count',IF(financial_cap='','None',financial_cap) AS 'financial_cap' FROM tbl_enrolled_students WHERE school_year=$sy_id GROUP BY financial_cap");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Financial Capability</li>
    <?php
    if(mysqli_num_rows($sql)>0){
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['financial_cap']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          
    	<?php
    	}
    }else{
    	?>
        <li class="list-group-item list-group-item-action">
         <span style="text-align: justify;">
              No Record 
          </span>
        </li>
          
    	<?php
    }
    ?>
    </ul>
    <?php
}
function load_teacher_partner($con,$sy_id)
{
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(VALUE='','None',VALUE) as VALUE,COUNT(*) AS 'count' FROM
	(SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.teacher_partner_home, ',', n.n), ',', -1)) VALUE
	  FROM tbl_enrolled_students CROSS JOIN 
	  (
	   SELECT a.N + b.N * 10 + 1 n
		 FROM 
		(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) a
	   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) b
		ORDER BY n 
	   ) n
	 WHERE school_year=$sy_id AND n.n <= 1 + (LENGTH(tbl_enrolled_students.teacher_partner_home) - LENGTH(REPLACE(tbl_enrolled_students.teacher_partner_home, ',', '')))
	 ORDER BY VALUE) nt0 GROUP BY VALUE");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Teacher Partner</li>
    <?php
    if (mysqli_num_rows($sql)>0) {
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['VALUE']?>
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          </ul>
    	<?php
    	}	# code...
    }else{
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                 No Record
              </span>
            </li>
          </ul>
    	<?php
    }
    ?>
    <ul class="list-group">
    <?php
}
function load_transpo($con,$sy_id){
	$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT arrive.*,COUNT(*) AS 'count' FROM (
	SELECT 'Walking' AS 'name',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%Walking%' UNION
	SELECT 'public commute (land/water)',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%public commute (land/water)%' UNION
	SELECT 'family-owned vehicle',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%family-owned vehicle%' UNION 
	SELECT 'school',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%school%')
	AS arrive WHERE arrive.school_year=$sy_id GROUP BY arrive.name");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">School Transportation</li>
    <?php
    if(mysqli_num_rows($sql)>0){
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['name']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          
    	<?php
    	}
    }else{
    	?>
        <li class="list-group-item list-group-item-action">
         <span style="text-align: justify;">
              No Record 
          </span>
        </li>
          
    	<?php
    }
    ?>
    </ul>
    <?php
}
function load_is_connected($con,$sy_id){
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT connected.*,COUNT(t1.`connected_to_internet`) AS 'count' FROM (SELECT 'YES' AS 'name' UNION SELECT 'NO') AS connected LEFT JOIN tbl_enrolled_students t1 ON connected.name=t1.`connected_to_internet` WHERE t1.`school_year`=$sy_id GROUP BY connected.name");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Student Internet Connection</li>
    <?php
    if(mysqli_num_rows($sql)>0){
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['name']?> 
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
          </ul>
    	<?php
    	}
    }else{
    	?>
    	<li class="list-group-item list-group-item-action">
	    	<span style="text-align: justify;">
	            No Record
	        </span>
    	</li>
    	<?php
    }
}
function load_interfere($con,$sy_id){
	//$school_yearId=select_this_field_value($con,'tbl_school_year','id','is_active',1);
	$sql=mysqli_query($con,"SELECT IF(VALUE='','None',VALUE)  as VALUE,COUNT(*) AS 'count' FROM
	    (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.affects_DE, ',', n.n), ',', -1)) VALUE
	      FROM tbl_enrolled_students CROSS JOIN 
	      (
	       SELECT a.N + b.N * 10 + 1 n
	         FROM 
	        (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8) a
	       ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8) b
	        ORDER BY n 
	       ) n
	     WHERE school_year=$sy_id AND n.n <= 1 + (LENGTH(tbl_enrolled_students.affects_DE) - LENGTH(REPLACE(tbl_enrolled_students.affects_DE, ',', '')))
	     ORDER BY VALUE) nt0 GROUP BY VALUE");
    ?>
    <ul class="list-group">
    	<li class="list-group-item list-group-item-info bg-dark w3-text-white text-center">Online Learning Interruption</li>
    <?php
    if (mysqli_num_rows($sql)>0) {
    	while ($row=mysqli_fetch_assoc($sql)) {  	
    	?>
            <li class="list-group-item list-group-item-action">
             <span style="text-align: justify;">
                  <?php echo $row['VALUE']?>
              </span>
              <span style="text-align: justify;" class="w3-right">
                  <?php echo $row['count']?>
              </span>
            </li>
    	<?php
    	}
    }else{
    	?>
        <li class="list-group-item list-group-item-action">
         <span style="text-align: justify;">
              No Record
          </span>
        </li>
	<?php
    }
    ?>
    </ul>
    <?php
}
//>>>> Portal Setting <<<<

function display_portal_content($con,$comp_id){
	$sql=mysqli_query($con,"SELECT * FROM tbl_mission_vision WHERE company_id='$comp_id'");
	
	if(mysqli_num_rows($sql)>0){
		
	?>
			<thead>
				<tr>
					<th>Content</th>
					<th width="65%;">Description</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while($row=mysqli_fetch_assoc($sql)){
			?>
				<tr>
					<?php
						if ($row['categ'] == 1) {
							echo '<td>Mission</td>';
						}
						else if ($row['categ'] == 2) {
							echo '<td>Vision</td>';
						}
						else{
							echo '<td>Philosophy</td>';
						}
					?>
					<td width="65%;"><?php echo $row['item']; ?></td>
					<?php
						if ($row['is_active'] == 1) {
					?>
						<td><button class="btn btn-success btn-small btn-block" onclick="set_status_inactive_pc('<?php echo $row['id']?>','<?php echo $comp_id;?>')">Active</button></td>
					<?php
						}
						else{
					?>
						<td><button class="btn btn-danger btn-small btn-block" onclick="set_status_active_pc('<?php echo $row['id']?>','<?php echo $comp_id;?>')">Inactive</button></td>
					<?php	
						}
					?>
					<td>
						<button class="btn btn-sm" onclick="get_portal_content_data('<?php echo $row['id']?>','<?php echo $row['categ']?>','<?php echo $row['item']?>')"><span class="fa fa-edit fa-2x"></span></button>

						<button class="btn btn-sm" onclick="delete_portal_content('<?php echo $row['id']?>','<?php echo $comp_id; ?>')"><span class="fa fa-trash fa-2x"></span></button>
					</td>
				</tr>
			<?php 
				} 
			?>
			</tbody>
<?php
		
	}else{
		echo '<td colspan="2">No Record Found!</td>';
	}
}

function save_portal_content($con,$id,$comp_id,$categ,$item,$option) {

	if ($option == 1) {

		$save_sql = mysqli_query($con, "INSERT INTO tbl_mission_vision VALUES (Id,'$comp_id','$categ','$item','1')");

		if ($save_sql) {
			echo 1;
		}
		else{
			echo 2;
		}

	}
	else{

		$update_sql = mysqli_query($con, "UPDATE tbl_mission_vision SET categ='$categ', item='$item' WHERE id='$id'");

		if ($update_sql) {
			echo 3;
		}
		else{
			echo 4;
		}
	}
	
}

function change_status($con,$id,$status){

	$status_sql = mysqli_query($con, "UPDATE tbl_mission_vision SET is_active='$status' WHERE Id='$id'");

	if ($status_sql) {
		echo 1;
	}
	else{
		echo 2;
	}
}

function delete_portal_content($con,$id) {

		$delete_sql = mysqli_query($con, "DELETE FROM tbl_mission_vision WHERE id='$id'");

		if ($delete_sql) {
			echo 1;
		}
		else{
			echo 2;
		}
	
}

function display_portal_banner($con,$comp_id){
	$sql=mysqli_query($con,"SELECT * FROM tbl_image_slider WHERE company_id='$comp_id'");
	
	if(mysqli_num_rows($sql)>0){
		
	?>
			<thead>
				<tr>
					<th>Banner Image</th>
					<th width="20%">Status</th>
					<th width="10%">Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while($row=mysqli_fetch_assoc($sql)){
			?>
				<tr>
					<td>
						<img width="100px;" src="data:image/jpeg;base64,<?php echo base64_encode($row['slider_image']) ?>" class="rounded img-fluid"/>
					</td>
					<?php
						if ($row['is_active'] == 1) {
					?>
						<td width="20%"><button class="btn btn-success btn-small btn-block" onclick="set_status_inactive_pb('<?php echo $row['Id']?>','<?php echo $comp_id;?>')">Active</button></td>
					<?php
						}
						else{
					?>
						<td width="20%"><button class="btn btn-danger btn-small btn-block" onclick="set_status_active_pb('<?php echo $row['Id']?>','<?php echo $comp_id;?>')">Inactive</button></td>
					<?php	
						}
					?>
					<td width="10%">
						<button class="btn btn-sm" onclick="delete_portal_banner('<?php echo $row['Id']?>','<?php echo $row['company_id']?>')"><span class="fa fa-trash fa-2x"></span></button>
					</td>
				</tr>
			<?php 
				} 
			?>
			</tbody>
<?php
		
	}else{
		echo '<td colspan="2">No Record Found!</td>';
	}
}

function change_status_pb($con,$id,$status){

	$status_sql = mysqli_query($con, "UPDATE tbl_image_slider SET is_active='$status' WHERE Id='$id'");

	if ($status_sql) {
		echo 1;
	}
	else{
		echo 2;
	}
}

function delete_portal_banner($con,$id) {

		$delete_sql = mysqli_query($con, "DELETE FROM tbl_image_slider WHERE id='$id'");

		if ($delete_sql) {
			echo 1;
		}
		else{
			echo 2;
		}
	
}
//--->>>.
?>