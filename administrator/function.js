var url = 'function.php';

//terms

function load_terms(){

	var mydata = 'action=load_terms' + '&student_id=' + student_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#tbl_terms").html(data);
		}
	});
	
}

function edit_term(id,term,is_active){

	$('#t_id').val(id);
	$('#term_name').val(term);
	$('#t_status').val(is_active);
	$('#edit_term').modal('show');

}

function update_term(){

	var t_id = $('#t_id').val();
	var term_name = $('#term_name').val(); 
	var t_status = $('#t_status').val();
	var mydata = 'action=update_term' + '&t_id=' + t_id + '&term_name=' + term_name + '&t_status=' + t_status;

	if(term_name == ''){
		term_name.focus();
	}else{

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_term").text('Saving Term...');
			},
			success:function(data){
				
				if(data == 1){
					showToast.show("Data has been updated!",1000);
					$("#btn_term").text('Save');
					$('#edit_term').modal('hide');
					load_terms();
				}else{

				}

			}
		});

	}

}

//terms

function edit_student_profile(student_id,category,grade_id,lname,fname,mname,suffix,email,fb_account,esc_qvr_number,lrn,bdate,age,place_of_birth,gender,religion,citizenship,mother_tongue,ethnicity,contact,address,siblings,with_medical_condition,medical_condition_affects,medical_condition,go_to_school_val,teacher_partner_home,available_gadgets,connected_to_internet,type_of_internet,internet_rate,learning_delivery_mode,affects_DE,mother_tongue_other,ethnicity_other,teacher_partner_home_other,available_gadgets_other,type_of_internet_other,learning_delivery_mode_other,affects_DE_other,is_blended_pmc,is_blended_pmc_other,suggestions){
	
	//alert(medical_condition);
	$('#header_stud').html(lname +', '+ fname + ' '+ mname);
	//alert(grade_id);
	$('#s_id').val(student_id);
	$('#s_id_doc').val(student_id);
	$('#category').val(category);
	$('#grade_id').val(grade_id);
	$('#lname').val(lname);
	$('#fname').val(fname);
	$('#mname').val(mname);
	$('#suffix').val(suffix);
	$('#email').val(email);

	$('#fb_account').val(fb_account);
	$('#esc_qvr_number').val(esc_qvr_number);
	$('#lrn').val(lrn);
	
	$('#bdate').val(bdate);
	$('#age').val(age);
	$('#place_of_birth').val(place_of_birth);
	$('#gender').val(gender);
	$('#religion').val(religion);
	$('#citizenship').val(citizenship);

	//mother tongue
	$('#mother_tongue').val(mother_tongue);

	var tag = mother_tongue.includes("Tagalog");
	var eng = mother_tongue.includes("English");
	var ilo = mother_tongue.includes("Ilocano");
	var yba = mother_tongue.includes("Ybanag");
	var yo = mother_tongue.includes("Yogad");

	if(tag == true){
		$('#Tagalog').prop('checked', true);
	}else{
		$('#Tagalog').prop('checked', false);
	}

	if(eng == true){
		$('#English').prop('checked', true);
	}else{
		$('#English').prop('checked', false);
	}

	if(ilo == true){
		$('#Ilocano').prop('checked', true);
	}else{
		$('#Ilocano').prop('checked', false);
	}

	if(yba == true){
		$('#Ybanag').prop('checked', true);
	}else{
		$('#Ybanag').prop('checked', false);
	}

	if(yo == true){
		$('#Yogad').prop('checked', true);
	}else{
		$('#Yogad').prop('checked', false);
	}

	if(mother_tongue_other == ''){
		$('#iba_pa').prop('checked', false);
		$('#other_mother_tongue').val('');
	}else{
		$('#iba_pa').prop('checked', true);
		$('#other_mother_tongue').val(mother_tongue_other);
	}

	//mother tongue

	$('#ethnicity_val').val(ethnicity);

	var gad = ethnicity.includes("Gaddang");
	var ifu = ethnicity.includes("Ifugao");
	var iloc = ethnicity.includes("Ilocano");
	var yban = ethnicity.includes("Ybanag");
	var yog = ethnicity.includes("Yogad");

	if(gad == true){
		$('#Gaddang').prop('checked', true);
	}else{
		$('#Gaddang').prop('checked', false);
	}

	if(ifu == true){
		$('#Ifugao').prop('checked', true);
	}else{
		$('#Ifugao').prop('checked', false);
	}

	if(iloc == true){
		$('#Ilocano_e').prop('checked', true);
	}else{
		$('#Ilocano_e').prop('checked', false);
	}

	if(yban == true){
		$('#Ybanag_e').prop('checked', true);
	}else{
		$('#Ybanag_e').prop('checked', false);
	}

	if(yog == true){
		$('#Yogad_e').prop('checked', true);
	}else{
		$('#Yogad_e').prop('checked', false);
	}

	if(ethnicity_other == ''){
		$('#iba_pa_e').prop('checked', false);
		$('#other_ethnicity').val('');
	}else{
		$('#iba_pa_e').prop('checked', true);
		$('#other_ethnicity').val(mother_tongue_other);
	}


	$('#contact').val(contact);
	$('#address').val(address);
	$('#siblings').val(siblings);
	
	$('#with_medical_condition').val(with_medical_condition);
	$('#medical_condition_affects').val(medical_condition_affects);
	$('#medical_condition').val(medical_condition);

	//SURVEY
	$('#go_to_school_val').val(go_to_school_val);

	var g1 = go_to_school_val.includes("Walking");
	var g2 = go_to_school_val.includes("public commute (land/water)");
	var g3 = go_to_school_val.includes("family-owned vehicle");
	var g4 = go_to_school_val.includes("school service");

	if(g1 == true){
		$('#g1').prop('checked', true);
	}else{
		$('#g1').prop('checked', false);
	}

	if(g2 == true){
		$('#g2').prop('checked', true);
	}else{
		$('#g2').prop('checked', false);
	}

	if(g3 == true){
		$('#g3').prop('checked', true);
	}else{
		$('#g3').prop('checked', false);
	}

	if(g4 == true){
		$('#g4').prop('checked', true);
	}else{
		$('#g4').prop('checked', false);
	}

	$('#teacher_home_val').val(teacher_partner_home);

	var t1 = teacher_partner_home.includes("Parents/Guardians");
	var t2 = teacher_partner_home.includes("Elder siblings");
	var t3 = teacher_partner_home.includes("Grandparents");
	var t4 = teacher_partner_home.includes("Extended members of the family");

	if(teacher_partner_home_other == ''){
		$('#iba_pa_th').prop('checked', false);
		$('#other_th').val('');
	}else{
		$('#iba_pa_th').prop('checked', true);
		$('#other_th').val(teacher_partner_home_other);
	}	

	if(t1 == true){
		$('#t1').prop('checked', true);
	}else{
		$('#t1').prop('checked', false);
	}

	if(t2 == true){
		$('#t2').prop('checked', true);
	}else{
		$('#t2').prop('checked', false);
	}

	if(t3 == true){
		$('#t3').prop('checked', true);
	}else{
		$('#t3').prop('checked', false);
	}

	if(t4 == true){
		$('#t4').prop('checked', true);
	}else{
		$('#t4').prop('checked', false);
	}

	$('#gadgets_val').val(available_gadgets);

	var a1 = available_gadgets.includes("Cable Tv");
	var a2 = available_gadgets.includes("Non cable TV");
	var a3 = available_gadgets.includes("Radio");
	var a4 = available_gadgets.includes("Laptop");
	var a5 = available_gadgets.includes("Cellphone");
	var a6 = available_gadgets.includes("Tablet");
	var a7 = available_gadgets.includes("Desktop");

	if(available_gadgets_other == ''){
		$('#iba_pa_g').prop('checked', false);
		$('#other_gadget').val('');
	}else{
		$('#other_gadget').val(available_gadgets_other);
		$('#iba_pa_g').prop('checked', true);
	}

	if(a1 == true){
		$('#a1').prop('checked', true);
	}else{
		$('#a1').prop('checked', false);
	}

	if(a2 == true){
		$('#a2').prop('checked', true);
	}else{
		$('#a2').prop('checked', false);
	}

	if(a3 == true){
		$('#a3').prop('checked', true);
	}else{
		$('#a3').prop('checked', false);
	}

	if(a4 == true){
		$('#a4').prop('checked', true);
	}else{
		$('#a4').prop('checked', false);
	}
	if(a5 == true){
		$('#a5').prop('checked', true);
	}else{
		$('#a5').prop('checked', false);
	}

	if(a6 == true){
		$('#a6').prop('checked', true);
	}else{
		$('#a6').prop('checked', false);
	}

	if(a7 == true){
		$('#a7').prop('checked', true);
	}else{
		$('#a7').prop('checked', false);
	}

	$('#connected_to_internet_val').val(connected_to_internet);

	var c1 = connected_to_internet.includes("YES");
	var c2 = connected_to_internet.includes("NO");

	if(c1 == true){
		$('#c1').prop('checked', true);
	}else{
		$('#c1').prop('checked', false);
	}

	if(c2 == true){
		$('#c2').prop('checked', true);
	}else{
		$('#c2').prop('checked', false);
	}

	$('#internet_val').val(type_of_internet);

	var ia = type_of_internet.includes("No internet connection");
	var ib = type_of_internet.includes("Mobile data (prepaid)");
	var ic = type_of_internet.includes("Mobile data (post paid or plan)");
	var i_d = type_of_internet.includes("Mobile broadband");
	var ie = type_of_internet.includes("Cable internet");
	var i_f = type_of_internet.includes("Fiber internet");

	if(ia == true){
		$('#ia').prop('checked', true);
	}else{
		$('#ia').prop('checked', false);
	}

	if(ib == true){
		$('#ib').prop('checked', true);
	}else{
		$('#ib').prop('checked', false);
	}

	if(ic == true){
		$('#ic').prop('checked', true);
	}else{
		$('#ic').prop('checked', false);
	}

	if(i_d == true){
		$('#i_d').prop('checked', true);
	}else{
		$('#i_d').prop('checked', false);
	}

	if(i_d == true){
		$('#ie').prop('checked', true);
	}else{
		$('#ie').prop('checked', false);
	}

	if(i_d == true){
		$('#i_f').prop('checked', true);
	}else{
		$('#i_f').prop('checked', false);
	}

	if(type_of_internet_other == ''){
		$('#iba_pa_i').prop('checked', false);
		$('#other_i').val('');
	}else{
		$('#other_i').val(type_of_internet_other);
		$('#iba_pa_i').prop('checked', true);
	}

	$('#internet_rate').val(internet_rate);

	$('#learning_dmode').val(learning_delivery_mode);

	var l1 = learning_delivery_mode.includes("Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)");
	var l2 = learning_delivery_mode.includes("Purely online classes (all classes will be done online)");
	var l3 = learning_delivery_mode.includes("Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)");
	
	if(learning_delivery_mode_other == ''){
		$('#iba_pa_p').prop('checked', false);
		$('#other_p').val('');
	}else{
		$('#other_p').val(learning_delivery_mode_other);
		$('#iba_pa_p').prop('checked', true);
	}

	if(l1 == true){
		$('#l1').prop('checked', true);
	}else{
		$('#l1').prop('checked', false);
	}

	if(l2 == true){
		$('#l2').prop('checked', true);
	}else{
		$('#l2').prop('checked', false);
	}

	if(l3 == true){
		$('#l3').prop('checked', true);
	}else{
		$('#l3').prop('checked', false);
	}

	$('#blended_pmc_val').val(is_blended_pmc);

	var pmc1 = is_blended_pmc.includes("Yes");
	var pmc2 = is_blended_pmc.includes("No");
	var pmc3 = is_blended_pmc.includes("Maybe");

	if(pmc1 == true){
		$('#pmc1').prop('checked', true);
	}else{
		$('#pmc1').prop('checked', false);
	}

	if(pmc2 == true){
		$('#pmc2').prop('checked', true);
	}else{
		$('#pmc2').prop('checked', false);
	}

	if(pmc3 == true){
		$('#pmc3').prop('checked', true);
	}else{
		$('#pmc3').prop('checked', false);
	}

	if(is_blended_pmc_other == '' || is_blended_pmc_other == 'undefined'){
		$('#iba_pa_pmc').prop('checked', false);
		$('#other_pmc').val('');
	}else{
		$('#other_pmc').val(is_blended_pmc_other);
		$('#iba_pa_pmc').prop('checked', true);
	}

	$('#affects_DE_val').val(affects_DE);

	var w1 = affects_DE.includes("Lack of available gadgets/ equipment");
	var w2 = affects_DE.includes("Insufficient load/data allowance");
	var w3 = affects_DE.includes("Unstable mobile/internet connection");
	var w4 = affects_DE.includes("Existing health condition's");
	var w5 = affects_DE.includes("Difficulty in independent learning");
	var w6 = affects_DE.includes("Conflict with other activities(e.g house chores)");
	var w7 = affects_DE.includes("High electrical consumptions");
	var w8 = affects_DE.includes("Distractions(i.e.| social media | noise from community/neighbor)");

	if(w1 == true){
		$('#w1').prop('checked', true);
	}else{
		$('#w1').prop('checked', false);
	}

	if(w2 == true){
		$('#w2').prop('checked', true);
	}else{
		$('#w2').prop('checked', false);
	}

	if(w3 == true){
		$('#w3').prop('checked', true);
	}else{
		$('#w3').prop('checked', false);
	}

	if(w4 == true){
		$('#w4').prop('checked', true);
	}else{
		$('#w4').prop('checked', false);
	}

	if(w5 == true){
		$('#w5').prop('checked', true);
	}else{
		$('#w5').prop('checked', false);
	}

	if(w6 == true){
		$('#w6').prop('checked', true);
	}else{
		$('#w6').prop('checked', false);
	}

	if(w7 == true){
		$('#w7').prop('checked', true);
	}else{
		$('#w7').prop('checked', false);
	}

	if(w8 == true){
		$('#w8').prop('checked', true);
	}else{
		$('#w8').prop('checked', false);
	}

	if(affects_DE_other == ''){
		$('#iba_pa_affects').prop('checked', false);
		$('#other_affects').val('');
	}else{
		$('#other_affects').val(affects_DE_other);
		$('#iba_pa_affects').prop('checked', true);
	}

	$('#suggestions').val(suggestions);

	//SURVEY
	load_fam(student_id);
	load_supporting_files();
	load_household_members();
	$('#manage_account_edit').modal('show');

}

function update_student_info(){

	var s_id = $('#s_id').val();
	var category = $('#category').val();
	var lname = $('#lname').val();      
	var fname = $('#fname').val();      
	var mname = $('#mname').val();      
	var suffix = $('#suffix').val();      
	var email = $('#email').val();      
	var fb_account = $('#fb_account').val();         
	var lrn= $('#lrn').val();      
	var bdate= $('#bdate').val();      
	var age= $('#age').val();      
	var place_of_birth = $('#place_of_birth').val();      
	var gender = $('#gender').val();      
	var religion = $('#religion').val();      
	var citizenship = $('#citizenship').val();      
	var mother_tongue = $('#mother_tongue').val(); 
	var other_mother_tongue = $('#other_mother_tongue').val();
	var other_ethnicity = $('#other_ethnicity').val();     
	var ethnicity_val = $('#ethnicity_val').val();      
	var contact = $('#contact').val();      
	var address = $('#address').val();      

	var lname_f = $('#lname_f').val();  
	var fname_f = $('#fname_f').val();  
	var mname_f = $('#mname_f').val();  
	var contact_f = $('#contact_f').val();  
	var occupation_f = $('#occupation_f').val();  
	var lname_m = $('#lname_m').val();  
	var fname_m = $('#fname_m').val();  
	var mname_m = $('#mname_m').val();  
	var contact_m = $('#contact_m').val();  
	var occupation_m = $('#occupation_m').val();  
	var lname_g = $('#lname_g').val();  
	var fname_g = $('#fname_g').val();  
	var mname_g = $('#mname_g').val();  
	var contact_g = $('#contact_g').val();  
	var occupation_g = $('#occupation_g').val();  
	var relation_g = $('#relation_g').val();  
	var siblings = $('#siblings').val(); 

	var with_medical_condition = $('#with_medical_condition').val();
	var medical_condition_affects = $('#medical_condition_affects').val();
	var medical_condition = $('#medical_condition').val();

	var go_to_school_val = $('#go_to_school_val').val();
	var teacher_home_val = $('#teacher_home_val').val();
	var other_th = $('#other_th').val();
	var gadgets_val = $('#gadgets_val').val();
	var other_gadget = $('#other_gadget').val();
	var connected_to_internet_val = $('#connected_to_internet_val').val();
	var internet_val = $('#internet_val').val();
	var other_i = $('#other_i').val();
	var internet_rate = $('#internet_rate').val();
	var learning_dmode = $('#learning_dmode').val();
	var other_p = $('#other_p').val();
	var affects_DE_val = $('#affects_DE_val').val();
	var other_affects = $('#other_affects').val();

	var blended_pmc_val = $('#blended_pmc_val').val();
	var other_pmc = $('#other_pmc').val();

	var suggestions = $('#suggestions').val();

	var mydata = 'action=update_student_info' + '&s_id=' + s_id + '&category=' + 	category + '&lname=' + 	lname + '&fname=' + fname + '&mname=' + mname + '&suffix=' + suffix + '&email=' + email + '&fb_account=' + fb_account + '&lrn=' + lrn + '&bdate=' + 	bdate + '&age=' + age + '&place_of_birth=' + place_of_birth + '&gender=' + 	gender + '&religion=' + religion + '&citizenship=' + citizenship + '&mother_tongue=' + 	mother_tongue + '&ethnicity_val=' + ethnicity_val + '&contact=' + contact + '&address='+ address + '&lname_f=' + lname_f + '&fname_f=' + fname_f + '&mname_f=' + mname_f + '&contact_f=' + contact_f + '&occupation_f=' + occupation_f + '&lname_m=' + lname_m + '&fname_m=' + fname_m + '&mname_m=' + mname_m + '&contact_m=' + contact_m + '&occupation_m=' + occupation_m + '&lname_g=' + lname_g + '&fname_g=' + fname_g + '&mname_g=' + mname_g + '&contact_g=' + contact_g + '&occupation_g=' + occupation_g + '&relation_g=' + relation_g + '&siblings=' + siblings + '&with_medical_condition=' + with_medical_condition + '&medical_condition_affects=' + medical_condition_affects + '&medical_condition=' + medical_condition + '&other_mother_tongue=' + other_mother_tongue + '&other_ethnicity=' + other_ethnicity + '&go_to_school_val=' + go_to_school_val + '&teacher_home_val=' + teacher_home_val + '&other_th=' + other_th + '&gadgets_val=' + gadgets_val + '&other_gadget=' + other_gadget + '&connected_to_internet_val=' + connected_to_internet_val + '&internet_val=' + internet_val + '&other_i=' + other_i + '&internet_rate=' + internet_rate + '&learning_dmode=' + learning_dmode + '&other_p=' + other_p + '&affects_DE_val=' + affects_DE_val + '&other_affects=' + other_affects + '&blended_pmc_val=' + blended_pmc_val + '&other_pmc=' + other_pmc + '&suggestions=' + suggestions;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 123456) 
	        {
	         	$('#manage_account_edit').modal('hide');
	         	$("#grade_id").val('');
	         	swal("Success","Student Successfully Updated","success");
	         	load_enrollees2();

	        }
	        else{
	          swal("Error","Error on updating data!","error");
	        }
	      }
	});

}

function load_fam(student_id){
	//alert(student_id);
	var mydata = 'action=load_fam' + '&student_id=' + student_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_fam").html(data);
		}
	});	

}

function load_supporting_files(){
	var student_id = $('#s_id').val();
	var mydata = 'action=load_supporting_files' + '&s_id=' + student_id;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#load_files").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#load_files").html(data);
		}
	});
}

function delete_doc(id,path){

	var mydata = 'action=delete_doc' + '&id=' + id + '&path=' + path;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this file?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#1edb6d",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("file has been removed!",3000);
              	 load_supporting_files();
                }
                else
                {
                    swal("Error","Error on deleting file.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

function save_household(){

	var mydata,grade_id,member,student_id;

	grade_id = $('#grade_id_h').val();
	member = $('#member_value').val();
	student_id = $('#s_id').val();
	mydata = 'action=save_member' + '&grade_id_h=' + grade_id + '&member=' + member + '&s_id=' + student_id;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 1) 
	        {
	         	load_household_members();
	         	$('#member_value').val('');

	        }else if(data == 2){
	        	swal("Info","Grade already exists. Please select another grade level.","info");
	        	$('#member_value').val('');
	        }
	        else{
	          swal("Error","Error on adding data!","error");
	        }
	      }
	    });

}

function load_household_members(){

	var student_id = $('#s_id').val();
	var grade_id = $('#grade_id_h').val();
	var mydata = 'action=load_household_members' + '&s_id=' + student_id + '&grade_id_h=' + grade_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_household_members").html(data);
		}
	});
}

function remove_member(id){

	var mydata = 'action=remove_member' + '&id=' + id;

	$.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 	showToast.show("Data has been removed!",1000);
	              	load_household_members();
                }
                else
                {
                    //alert(data);
                }
            }
        });

}

function get_affects(){

  var chkArray = [];
  
  $("#affects_DE input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#affects_DE_val").val(selected);

}

function iba_pa_affects() {
	  var checkBox = document.getElementById("iba_pa_affects");
  if (checkBox.checked == true){
  	document.getElementById('other_affects').disabled = false;
  } else {
    document.getElementById('other_affects').disabled = true;
    $('#other_affects').val('');
  }
}

function get_gotoschool(){

  var chkArray = [];
  
  $("#go_to_school input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#go_to_school_val").val(selected);

  }

function iba_pa() {
	  var checkBox = document.getElementById("iba_pa");
  if (checkBox.checked == true){
  	//alert(1);
  	document.getElementById('other_mother_tongue').disabled = false
  } else {
  	$('#other_mother_tongue').val('');
    document.getElementById('other_mother_tongue').disabled = true;
    //alert(2);
    
  }
}

function get_style_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#m_tongue input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#mother_tongue").val(selected);
}

function get_ethnicity(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#ethnicity input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#ethnicity_val").val(selected);
}

function iba_pa_e() {
	  var checkBox = document.getElementById("iba_pa_e");
  if (checkBox.checked == true){
  	document.getElementById('other_ethnicity').disabled = false;
  } else {
    document.getElementById('other_ethnicity').disabled = true;
    $('#other_ethnicity').val('');
  }
}

function get_is_connected(){

  var chkArray = [];
  
  $("#connected_to_internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#connected_to_internet_val").val(selected);

}

function get_gadgets(){

  var chkArray = [];
  
  $("#gadgets input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#gadgets_val").val(selected);

}

function iba_pa_g() {
	  var checkBox = document.getElementById("iba_pa_g");
  if (checkBox.checked == true){
  	document.getElementById('other_gadget').disabled = false;
  } else {
    document.getElementById('other_gadget').disabled = true;
    $('#other_gadget').val('');
  }
}

function get_ic(){

  var chkArray = [];
  
  $("#internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#internet_val").val(selected);

}

function get_delivery_mode(){

  var chkArray = [];
  
  $("#delivery_mode input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#learning_dmode").val(selected);

}

function get_pmc(){

  var chkArray = [];
  
  $("#blended_pmc input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#blended_pmc_val").val(selected);

}

function iba_pa_pmc() {
	  var checkBox = document.getElementById("iba_pa_pmc");
  if (checkBox.checked == true){
  	document.getElementById('other_pmc').disabled = false
  } else {
    document.getElementById('other_pmc').disabled = true
  }
}

function get_preferred_days(){

  var chkArray = [];
  
  $("#preferred_days input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#preferred_days_val").val(selected);
  $('#other_gp').val('');

}

function iba_pa_gp() {
	  var checkBox = document.getElementById("iba_pa_gp");
  if (checkBox.checked == true){
  	$("#preferred_days_val").val('');
  	document.getElementById('other_gp').disabled = false
  } else {
    document.getElementById('other_gp').disabled = true
  }
}

function iba_pa_i() {
	  var checkBox = document.getElementById("iba_pa_i");
  if (checkBox.checked == true){
  	document.getElementById('other_i').disabled = false;
  } else {
    document.getElementById('other_i').disabled = true;
     $('#other_i').val('');
  }
}

function iba_pa_p() {
	  var checkBox = document.getElementById("iba_pa_p");
  if (checkBox.checked == true){
  	document.getElementById('other_p').disabled = false;
  } else {
    document.getElementById('other_p').disabled = true;
     $('#other_p').val('');
  }
}

function get_financial_capability(){

  var chkArray = [];
  
  $("#financial_capability input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#financial_capability_val").val(selected);
  $('#other_fc').val('');

}

function iba_pa_fc() {
	  var checkBox = document.getElementById("iba_pa_fc");
  if (checkBox.checked == true){
  	$("#financial_capability_val").val('');
  	document.getElementById('other_fc').disabled = false
  } else {
    document.getElementById('other_fc').disabled = true
  }
}

function get_teacher_home(){

  var chkArray = [];
  
  $("#teacher_home input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#teacher_home_val").val(selected);

}

function iba_pa_th() {
	  var checkBox = document.getElementById("iba_pa_th");
  if (checkBox.checked == true){
  	document.getElementById('other_th').disabled = false;
  } else {
    document.getElementById('other_th').disabled = true;
    $('#other_th').val('');
  }
}

//edit student

$(document).ready(function(e) {

	//$("#eye_slash").css('display','none');
	$("#old_pass").keyup(function(e){

        var old_pass = $("#old_pass").val();

        if(old_pass != '')
        {
           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
            $.ajax({
		    type:'POST',
		    url:url,
		    data:mydata,
		    cache:false,
		    success:function(data){
	
	    		if(data == 1){

		    		$('#new_pass').focus();
	                $('#old_pass_validator').text(' Your old password is correct.');
	                $('#old_pass_validator').removeClass('text-danger');
		    		$('#old_pass_validator').addClass('text-success');
		    		document.getElementById('btn_up_account').disabled = false;
	    		
		    	}else{

		    		$('#old_pass_validator').text(' Your old password is incorrect.');
		    		$('#old_pass_validator').removeClass('text-success');
		    		$('#old_pass_validator').addClass('text-danger');
		    		document.getElementById('btn_up_account').disabled = true;

		    	}

		    }
		    
		  });

        } else {
        	$("#oldpass").focus();      
        }

    });

	$("#retype_pass").keyup(function(e){

        var new_pass = $("#new_pass").val();
        var retype_pass = $("#retype_pass").val();

        if(new_pass === retype_pass)
        {
        	$('#retype_pass_validator').text('');
        	document.getElementById('btn_up_account').disabled = false;
        }else {

        	document.getElementById('btn_up_account').disabled = true;
        	$('#btn_update_account').attr('disabled','disabled'); 
        	$('#retype_pass_validator').text(' Password is not equal.');
		    $('#retype_pass_validator').addClass('text-danger');
 
        }

    });

});

function load_pass(){

	var mydata = 'action=load_pass';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#show_pass").html(data);
		}
	});

}

function showpass() {

    var x = document.getElementById("old_pass");

    if (x.type === "password") {
        x.type = "text";
        $("#eye").css('display','none');
        $("#eye_slash").css('display','block');

    } else {

        x.type = "password";
        $("#eye").css('display','block');
        $("#eye_slash").css('display','none');

    }
}

function update_password(){

	var old_pass = $('#old_pass').val();
	var retype_pass = $('#retype_pass').val();

	var mydata = 'action=update_password' + '&old_pass=' + old_pass + '&retype_pass=' + retype_pass;

	if(retype_pass == ''){

	$('#retype_pass').focus();

	}else{

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_up_account").html('Saving data....');
				document.getElementById("btn_up_account").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Password Successfully Updated.","success");
					$('#manage_account').modal('hide');
					document.getElementById("btn_up_account").disabled = false;
					$("#btn_up_account").html('Save');
					$('#old_pass').val('');
					$('#new_pass').val('');
					$('#retype_pass').val('');
					load_pass();
				}
				else
				{
					swal("Error","Error on updating password.","error");
				}
			}
			
		});
	
	}

	
}


function delete_term_payment(id){

	var mydata = 'action=delete_term_payment' + '&id=' + id;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this term payment?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("Data has been removed!",3000);
              	 load_terms_of_payment();
                }
                else
                {
                    swal("Error","Error on deleting data.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
   });

}

$(document).ready(function(e){

	$(function() {
	    $('#addedit_email').on('hidden.bs.modal', function(){
	      	$('#email_id').val('');
			$('#designation').val('');
			$('#company_email').val('');
			$('#btn_update_email').text('Save');
	    });

	    $('#addedit_bank').on('hidden.bs.modal', function(){
	      	$('#bank_id').val('');
			$('#bank_name').val('');
			$('#account_name').val('');
			$('#account_number').val('');
			$('#btn_update_bank').text('Save');
	    });

	      $('#addedit_section').on('hidden.bs.modal', function(){
	      	$('#sec_id').val('');
			$('#sec_name').val('');
			$('#sec_grade').val('');
			$('#btn_update_section').text('Save');
	    });

	    $('#manage_account_edit').on('hidden.bs.modal', function(){
	      	$('#header_stud').html('Edit Student');
			//alert(grade_id);
			$('#s_id').val('');
			$('#s_id_doc').val('');
			$('#category').val('');
			$('#grade_id').val('');
			$('#lname').val('');
			$('#fname').val('');
			$('#mname').val('');
			$('#suffix').val('');
			$('#email').val('');

			$('#fb_account').val('');
			$('#esc_qvr_number').val('');
			$('#lrn').val('');
			
			$('#bdate').val('');
			$('#age').val('');
			$('#place_of_birth').val('');
			$('#gender').val('');
			$('#religion').val('');
			$('#citizenship').val('');

			//mother tongue
			$('#mother_tongue').val('');
			$('#ethnicity_val').val('');
			$('#contact').val('');
			$('#address').val('');
			$('#siblings').val('');
			$('#si_kindergarten').val('');
			$('#si_gradeschool').val('');
			$('#si_junior_hs').val('');
			$('#si_senior_hs').val('');
			
			$('#with_medical_condition').val('');
			$('#medical_condition_affects').val('');
			$('#medical_condition').val('');
			$('#reg_learn_options').val('');
			$('#go_to_school_val').val('');
			$('#teacher_home_val').val('');
			$('#gadgets_val').val('');
			$('#connected_to_internet_val').val('');
			$('#internet_val').val('');
			$('#internet_rate').val('');
			$('#learning_dmode').val('');
			$('#affects_DE_val').val('');
			$('#con_by').val('');

			$('#info').addClass('active');
			$('#fam').removeClass('active');
			$('#other_info').removeClass('active');
			$('#survey_info').removeClass('active');
				
			$('#info_tab').addClass('active');
			$('#fam_tab').removeClass('active');
			$('#other_info_tab').removeClass('active');
			$('#survey_info_tab').removeClass('active');
			
	    });

	    
	});

});

function get_up_pics(){

	var mydata = 'action=load_pic';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#my_pic").html(data);
		}
	});
}
function load_pics(){
	var mydata = 'action=load_pic';
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#my_pic").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#my_pic").html(data);
		}
	});
}

function load_enrollees(){

	var grade_id,search,mydata;
	  search = $("#search").val();
	  grade_id = $("#grade_id").val();
	  var school_year = $("#school_year").val();
	  mydata = 'action=load_enrollees' + '&grade_id=' + grade_id + '&search=' + search + '&school_year=' + school_year;
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    beforeSend:function(){
				$("#tbl_enrollees").html('<center><h4 class="animated pulse infinite">loading enrollees...</h4></center>');
			},
	    success:function(data){
	      setTimeout(function(){$("#tbl_enrollees").html(data);},500);
	    }
	  });

}

function load_enrollees2(){

	var grade_id,search,mydata;
	  search = $("#search").val();
	  grade_id = $("#grade_id").val();
	  var school_year = $("#school_year").val();

	  mydata = 'action=load_enrollees' + '&grade_id=' + grade_id + '&search=' + search + '&school_year=' + school_year;
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_enrollees").html(data);},100);
	    }
	  });

}

function student_profile(stud_id){

	$('#student_id').val(stud_id);
	
	$('#student_info').modal('show');
	load_student_info();
}

function load_student_info(){

	var student_id = $('#student_id').val();

	var mydata = 'action=load_student_info' + '&student_id=' + student_id;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#stud_info").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#stud_info").html(data);
		}
	});
}

function payments_info(stud_id){

	$('#student_id_2').val(stud_id);
	
	$('#payments').modal('show');
	load_payment_info();
}

function load_payment_info(){

	var student_id_2 = $('#student_id_2').val();

	var mydata = 'action=load_payment_info' + '&student_id_2=' + student_id_2;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#payment_info").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#payment_info").html(data);
		}
	});
}

function enroll_student(){

	var student_id_3 = $('#student_id_3').val();
	var sec_grade = $('#sec_grade').val();
	var school_year = $('#school_year').val();
	var mydata = 'action=enroll_student' + '&student_id_3=' + student_id_3 + '&sec_grade=' + sec_grade + '&school_year=' + school_year;

	$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_enroll").html('Saving data....');
				document.getElementById("btn_enroll").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Student successfully enrolled.","success");
					$('#enroll').modal('hide');
					document.getElementById("btn_enroll").disabled = false;
					$("#btn_enroll").html('Enroll');
					load_enrollees();
					dashboard_load();
				}
				else
				{
					//alert(data);
				}
			}
			
		});

}

function enroll(student_id,name,grade_id){

	$('#s_name').html(name);
	$('#student_id_3').val(student_id);
	$('#grade_id_section').val(grade_id);
	load_grade_sec();
	$('#enroll').modal('show');

}

function load_grade_sec(){

	var grade_id_section = $('#grade_id_section').val();

	var mydata = 'action=load_grade_sec' + '&grade_id_section=' + grade_id_section;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_section").html(data);
		}
	});

}

function unenroll(student_id,name){

	$('#s_name2').html(name);
	$('#student_id_4').val(student_id);
	$('#unenroll').modal('show');

}

function unenroll_student(){

	var student_id_3 = $('#student_id_4').val();
	var school_year = $('#school_year').val();
	var mydata = 'action=unenroll_student' + '&student_id_4=' + student_id_3 + '&school_year=' + school_year;

	$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_unenroll").html('Saving data....');
				document.getElementById("btn_unenroll").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Student successfully unenrolled.","success");
					$('#unenroll').modal('hide');
					document.getElementById("btn_unenroll").disabled = false;
					$("#btn_unenroll").html('Unenroll');
					load_enrollees();
				}
				else
				{
					//alert(data);
				}
			}
			
		});

}

function del(student_id,name){

	$('#s_name4').html(name);
	$('#student_id_5').val(student_id);
	$('#delete_student').modal('show');

}

function delete_student(){

	var student_id = $('#student_id_5').val();

	var mydata = 'action=delete_student' + '&student_id_5=' + student_id

	$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_delete").html('Saving data....');
				document.getElementById("btn_delete").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Student successfully deleted.","success");
					$('#delete_student').modal('hide');
					document.getElementById("btn_delete").disabled = false;
					$("#btn_delete").html('Delete');
					load_enrollees();
				}
				else if(data == 2)
				{
					swal("Information","Student can't be remove because she/he has existing transactions.","info");
					document.getElementById("btn_delete").disabled = false;
					$('#delete_student').modal('hide');
					$("#btn_delete").html('Delete');
					//alert(data);
				}else{

				}
			}
			
		});
}


//mange terms of payments

function load_terms_of_payment(){

	var grade_id,learn_opt,mydata;
	grade_id = $("#grade_id_terms").val();
	learn_opt = $("#tp_learn_option").val();
	mydata = 'action=load_terms_of_payment' + '&grade_id_terms=' + grade_id + '&tp_learn_option=' + learn_opt;
	$.ajax({
		type:'POST',
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			setTimeout(function(){$("#tbl_terms_payment").html(data);},500);
		}
	});

}

function add_term_payment(term_id){

	var grade_id = $('#grade_id_fee').val();
	var tp_ln_opt = $('#tp_learn_option').val();

	$('#t_id').val();
	$('#term_id').val(term_id);
	$('#term_grade_id').val(grade_id);
	$('#term_learn_options').val(tp_ln_opt);

	load_terms_fields();
	$('#header_term').html('<span class="fa fa-edit"></span> Add Terms of Payment');
	$("#btn_update_term").html('Add');
	$('#addedit_term_fee').modal('show');

}

function edit_term_payment(id,term_id,grade_id,learn_opt){

	$('#t_id').val(id);
	$('#term_id').val(term_id);
	$('#term_grade_id').val(grade_id);

	load_terms_fields();
	$('#term_learn_options').val(learn_opt);
	
	$('#header_term').html('<span class="fa fa-edit"></span> Edit Terms of Payment')
	$("#btn_update_term").html('Update');
	$('#addedit_term_fee').modal('show');

}

function update_term_fee(){

	var t_id,terms,term_name,term_amount,term_learn_opt,term_grade_id;

	t_id = $('#t_id').val();
	terms = $('#term_id').val();
	term_grade_id = $('#term_grade_id').val();
	term_name = $('#term_name').val();
	term_amount = $('#term_amount').val();
	term_learn_opt = $('#term_learn_options').val();
	
	if(term_name == ''){
		document.getElementById("term_name").focus();
	}
	else if(term_amount == ''){
		document.getElementById("term_amount").focus();
	}
	else if (term_learn_opt == '' || term_learn_opt == 0) {
		document.getElementById("term_learn_options").focus();
	}
	else{

		var mydata = 'action=save_update_term_fee' + '&t_id=' + t_id + '&term_name=' + term_name + '&term_amount=' + term_amount + 
					 '&term_grade_id=' + term_grade_id + '&term_learning_option=' + term_learn_opt + '&terms=' + terms;

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				if (t_id == "") {
					$("#btn_update_term").html('Adding fee....');
				}
				else{
					$("#btn_update_term").html('Updating fee....');
				}
				document.getElementById("btn_update_term").disabled = true;
			},
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					showToast.show('Term fee Successfully Saved.',3000);
					//swal("Success","Student successfully deleted.","success");
					$('#addedit_term_fee').modal('hide');
					document.getElementById("btn_update_term").disabled = false;
					$("#btn_update_term").html('Update');
					load_terms_of_payment();
				}
				else{
					swal("Error","Error on updating data.","error");
				}
			}
			
		});

	}

}

function load_terms_fields(){

	var id = $('#t_id').val();

	var mydata = 'action=load_terms_fields' + '&t_id=' + id;
	$.ajax({
		type:'POST',
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			//alert(data);
			setTimeout(function(){$("#load_term_field").html(data);},500);
		}
	});
}



//manage Fees

function load_fees(){

	var grade_id,learn_opt,mydata;
	grade_id = $("#grade_id_fee").val();
	learn_opt = $("#learn_option").val();

	mydata = 'action=load_fees' + '&grade_id_fee=' + grade_id + '&learn_option=' + learn_opt;

	$.ajax({
		type:'POST',
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
		  setTimeout(function(){$("#tbl_fees").html(data);},500);
		}
	});
}

function edit_fee(id,name,fee,grade_id,learn_opt){

	$('#fee_id').val(id);
	$('#fee_name').val(name);
	$('#fee_amount').val(fee);
	$('#fee_grade_id').val(grade_id);
	$('#fee_learn_options').val(learn_opt);
	$('#header_fee').html('<span class="fa fa-edit"></span> Edit Fee');
	$("#btn_update_fee").html('Update');
	$('#addedit_fee').modal('show');
}


function add_fee(){

	var grade_id = $('#grade_id_fee').val();

	$('#fee_id').val('');
	$('#fee_name').val('');		
	$('#fee_amount').val('');
	$('#fee_grade_id').val(grade_id);
	$("#btn_update_fee").html('Save');
	$('#header_fee').html('<span class="fa fa-plus"></span> New Fee')
	$('#addedit_fee').modal('show');

}

function update_fee(){

	var fee_id,fee_name,fee_amount,fee_grade_id;

	 fee_id = $('#fee_id').val();
	 fee_name = $('#fee_name').val();
	 fee_amount = $('#fee_amount').val();
	 fee_grade_id = $('#fee_grade_id').val();

	if(fee_name == ''){
		document.getElementById("fee_name").focus();
	}else if(/^[a-zA-Z0-9-,/\.: ,_]*$/.test(fee_name) == false){
		swal("Error","Please remove & on the fee name!","error");
	}else if(fee_amount == ''){
		document.getElementById("fee_amount").focus();
	}else{

		var mydata = 'action=save_update_fee' + '&fee_id=' + fee_id + '&fee_name=' + fee_name + '&fee_amount=' + fee_amount + 
					 '&fee_grade_id=' + fee_grade_id;

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_update_fee").html('Updating fee....');
				document.getElementById("btn_update_fee").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					showToast.show('Fee Successfully Saved.',3000);
					//swal("Success","Student successfully deleted.","success");
					$('#addedit_fee').modal('hide');
					document.getElementById("btn_update_fee").disabled = false;
					$("#btn_update_fee").html('Update');
					load_fees();
				}
				else{
					swal("Error","Error on updating data.","error");
				}
			}
			
		});

	}


}

function delete_fee(id){

	var mydata = 'action=delete_fee' + '&id=' + id;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this school fee?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("School fee has been removed!",3000);
              	 load_fees();
                }
                else
                {
                    swal("Error","Error on deleting data.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}


//Manage Company details

function del_temp_file(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;
    
    swal({
        title: "Are you sure ?",
        text: "You want to delete this file ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == "") 
                {
              	 showToast.show("File has been removed!",1000);
              	 $('.hide_logo').css('display','block');
              	 load_logo();
              	  $('#fupForm')[0].reset();
              	  $('#preview_file').html("");
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

function load_logo(){

	var mydata;

	  mydata = 'action=load_logo';
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_logo").html(data);},500);
	    }
	  });

}

function load_company_details(){

	var mydata;

	  mydata = 'action=load_company_details';
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_company_details").html(data);},500);
	    }
	  });

}

function save_update_company(){

	var company_name,company_name2,address,telephone,cellphone,acronym;

	 company_name = $('#company_name').val();
	 company_name2 = $('#company_name2').val();
	 address = $('#address').val();
	 telephone = $('#telephone').val();
	 cellphone = $('#cellphone').val();
	 acronym = $('#acronym').val();

	if(company_name == ''){
		document.getElementById("company_name").focus();
	}else{

		var mydata = 'action=save_update_company' + '&company_name=' + company_name + '&company_name2=' + company_name2 + '&address=' + address + '&telephone=' + telephone + '&cellphone=' + cellphone + '&acronym=' + acronym;

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_save_update_c").html('Updating data....');
				document.getElementById("btn_save_update_c").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					showToast.show('Company data successfully updated.',3000);

					document.getElementById("btn_save_update_c").disabled = false;
					$("#btn_save_update_c").html('Update');
					load_fees();
				}
				else{
					swal("Error","Error on updating data.","error");
				}
			}
		});
	}
}

//Email

function load_emails(){

	var mydata;

	mydata = 'action=load_emails';
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_emails").html(data);},500);
	    }
	});

}

function edit_email(id,designation,email){

	var email_id = $('#email_id').val(id);
	var designation = $('#designation').val(designation);
	var c_email = $('#company_email').val(email);
	$('#header_email').text('Update Email');
	$('#addedit_email').modal('show');

}

function save_update_email(){

	var email_id = $('#email_id').val();
	var designation = $('#designation').val();
	var c_email = $('#company_email').val();

	var mydata = 'action=save_update_email' + '&email_id=' + email_id + '&designation=' + designation + '&company_email=' + c_email;

	if(designation == ''){
		$('#designation').focus();
	}else if(c_email == ''){
		$('#company_email').focus();
	}else{

		$.ajax({

			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
			$("#btn_update_email").html('Updating email....');
			document.getElementById("btn_update_email").disabled = true;
			},
			success:function(data){

			if (data == 1) 
			{
			showToast.show('Email successfully updated.',3000);
			$('#addedit_email').modal('hide');
			document.getElementById("btn_update_email").disabled = false;
			$("#btn_update_email").html('Update');
			load_emails();
			}
			else{
			swal("Error","Error on updating data.","error");
			}
			}

		});

	}

}

function delete_email(id){

	var mydata = 'action=delete_email' + '&id=' + id;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this data ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("Email has been deleted!",1000);
              	 load_emails();
                }
                else
                {
                 swal("Error","Error on deleting data.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

//Email
//Bank Details

function load_banks(){

	var mydata;

	mydata = 'action=load_banks';
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_banks").html(data);},500);
	    }
	});

}

function edit_bank(bank_id,bank_name,account_name,account_no){

	$('#bank_id').val(bank_id);
	$('#bank_name').val(bank_name);
	$('#account_name').val(account_name);
	$('#account_number').val(account_no);
	$('#header_bank').text('Update Bank');
	$('#btn_update_bank').text('Update');
	$('#addedit_bank').modal('show');

}

function delete_bank(id){

	var mydata = 'action=delete_bank' + '&id=' + id;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this data ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("Bank has been deleted!",1000);
              	 load_banks();
                }
                else
                {
                 swal("Error","Error on deleting data.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

function save_update_bank(){

	var bank_id = $('#bank_id').val();
	var bank_name = $('#bank_name').val();
	var account_name = $('#account_name').val();
	var account_number = $('#account_number').val();

	var mydata = 'action=save_update_bank' + '&bank_id=' + bank_id + '&bank_name=' + bank_name + '&account_name=' + account_name + '&account_number=' + account_number;

	$.ajax({
			
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#btn_update_bank").html('Saving data....');
			document.getElementById("btn_update_bank").disabled = true;
		},
		success:function(data){

			if (data == 1) 
			{
				$('#addedit_bank').modal('hide');
				$('#bank_id').val('');
				$('#bank_name').val('');
				$('#account_name').val('');
				$('#account_number').val('');
				showToast.show('Bank details successfully saved.',3000);
				document.getElementById("btn_update_bank").disabled = false;
				$("#btn_update_bank").html('Save');
				load_banks();

			}
			else{
				swal("Error","Error on updating data.","error");
			}
		}
	});
}

//Bank Details

//Manage Sections
function load_sections(){

	var mydata;
	var grade_id_sec = $('#grade_id_sec').val();
	mydata = 'action=load_sections' + '&grade_id_sec=' + grade_id_sec;
	  $.ajax({
	    type:'POST',
	    url:url,
	    data:mydata,
	    cache:false,
	    success:function(data){
	      setTimeout(function(){$("#tbl_sections").html(data);},500);
	    }
	});

}

function save_update_section(){

	var sec_id = $('#sec_id').val();
	var sec_grade = $('#sec_grade').val();
	var sec_name = $('#sec_name').val();

	var mydata = 'action=save_update_section' + '&sec_id=' + sec_id + '&sec_grade=' + sec_grade + '&sec_name=' + sec_name;

	if(sec_grade == ''){

	}else if(sec_name == ''){

	}else{

		$.ajax({
			
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_update_section").html('Saving data....');
				document.getElementById("btn_update_section").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					$('#addedit_section').modal('hide');
					$('#sec_id').val('');
					$('#sec_grade').val('');
					$('#sec_name').val('');
					showToast.show('Section successfully saved.',3000);
					document.getElementById("btn_update_section").disabled = false;
					$("#btn_update_section").html('Save');
					load_sections();

				}
				else{
					swal("Error","Error on updating data.","error");
				}
			}
		});

	}
}

function edit_section(sec_id,sec_name,sec_grade){

	$('#sec_id').val(sec_id);
	$('#sec_name').val(sec_name);
	$('#sec_grade').val(sec_grade);
	$('#header_section').text('Update Section');
	$('#btn_update_section').text('Update');
	$('#addedit_section').modal('show');

}

function delete_section(id){

	var mydata = 'action=delete_section' + '&id=' + id;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this section ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	 //alert(data);
                if (data == 1) 
                {
              	 showToast.show("Section has been deleted!",1000);
              	 load_sections();
                }else if(data == 2){
                 showToast.show("Section can't be deleted because it has related data.",3000);
                }else
                {
                 swal("Error","Error on deleting data.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

//Manage Sections

//Manage Company details

//check the last comment <<<<----
function load_payments(){
	$.ajax({
		type:"POST",
		url:url,
		data:'action=load_payments',
		cache:false,
		beforeSend:function(){
			$("#tbl_payments").html('<center><h4 class="animated pulse infinite">loading enrollees...</h4></center>');
		},
		success:function(data){
			$('#tbl_payments').html(data);
		}

	})
}
function view_payment(name,id,file,filetype,payment,paymentFormat,student_id,amount_due,bank,account_no,account_name,contact_no,address,ref_no){
	$('#viewPayment_modal').modal('show');
	$('#viewPayment_name').html(name);
	if(filetype=='image/jpeg'||filetype=='image/png'||filetype=='image/jpg'){
		$('#uploadFile').html('<img src="'+file+'" style="height:150px;width:150px"><div class="mt-3 mb-3"><a class="btn btn-dark mr-2" href="'+file+'" target="_blank">Preview</a><a class="btn btn-dark" href="'+file+'" download>Download</a></div>');
	}else{
		$('#uploadFile').html('<span class="fa fa-file fa-5x"></span><div class="mt-3 mb-3"><a class="btn btn-dark" href="'+file+'" download>Download</a></div>')
	}
	$('#viewPayment_amount').html(paymentFormat);
	$('#viewPayment_text').val(payment);
	$('#viewId_text').val(id);
	$('#viewStudent_Id').val(student_id);
	$('#viewPayment_amount_due').val(amount_due);
	$('#view_bank').html(bank);
	$('#view_acctno').html(account_no);
	$('#view_acctname').html(account_name);
	$('#view_cp').html(contact_no);
	$('#view_address').html(address);
	$('#view_ref').html(ref_no);
}
function accept_payment(){
	var amountText = $('#viewPayment_amount').html();
	var id=$('#viewId_text').val();
	var amount = $('#viewPayment_text').val();
	var studentId= $('#viewStudent_Id').val();
	var payerName = $('#viewPayment_name').html();
	var amount_due = $('#viewPayment_amount_due').val();
	swal({
		title:"Accept the " +amountText+" Payment?",
		text:"Click Accept to Pay the Balance",
		type:"warning",
		showCancelButton:true,
		confirmButtonColor:'#3085d6',
		confirmButtonText:'Accept',
		cancelButtonColor:'#d33'
		},
		function(){
			$.ajax({
				type:"POST",
				url:url,
				data:'action=accept_payment'+ '&id=' + id+ '&amount=' + amount+ '&studentId=' + studentId+ '&amount_due=' + amount_due,
				cache:false,
				success:function(data){
					if(data==1){
						setTimeout(function(){
							swal("Payment Accepted","Successfully","success");
							load_payments();
							dashboard_load();
							$('#viewPayment_modal').modal('hide');
						},1000)
						
					}else if(data==2){
						
						setTimeout(function(){
							swal(payerName+" is already paid","Balance is Zero","info");
							$('#viewPayment_modal').modal('hide');
						},1000)
					}else if (data==3){
						setTimeout(function(){
							swal(amountText+" is greater than the Balance","Check the student balance!","info");
							$('#viewPayment_modal').modal('hide');
						},1000)
					}
					else{
						alert(data);
					}
				}
			})	
		})
}
function delete_payment(name,id){
	swal({
	title:"Delete the payment of "+name+"?",
	text:"Are you sure?",
	type:"warning",
	showCancelButton:true,
	confirmButtonColor:'#f51818',
	confirmButtonText:'Delete',
	cancelButtonColor:'#d33'
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:'action=delete_payment'+ '&id=' + id,
			cache:false,
			success:function(data){
				if(data==1){
					setTimeout(function(){
						swal("Payment Deleted","Successfully","success");
						load_payments();
						$('#viewPayment_modal').modal('hide');
					},1000)
					
				}
				else{
					alert(data);
				}
			}
		})	
	})
}
function dashboard_load(){

	var sy_id = $('#sy_id').val();

	public_select("SELECT COUNT(*) as count FROM tbl_enrolled_students where is_active=0 and school_year = "+sy_id+"",'count_registered');
	public_select("SELECT COUNT(*) AS count FROM tbl_students t1 LEFT JOIN tbl_enrolled_students t2 ON t1.student_id=t2.student_id WHERE t2.`is_active`=1 and t2.school_year = "+sy_id+"" ,'count_enrolled');
	public_select("SELECT COUNT(*) as count FROM tbl_accounts where status=1 and school_year = "+sy_id+"",'count_paid');
	//select count grade register
	count_grade_r(0,3,0,'count_grade_student_r','tabbtn1-0');
	tab_button(0,'count_grade_student_r','tabId_r');
	//select count grade enrolled
	count_grade_r(1,3,0,'count_grade_student_e','tabbtn1-0');
	tab_button(1,'count_grade_student_e','tabId_e');

	load_device_survey();
	load_connection_survey();
	load_speed();
	load_delivery();
	load_learning_day();
	load_financial_cap();
	load_teacher_partner();
	load_transpo();
	load_is_connected();
	load_interfere();

}
function public_select(query,id){
	$.ajax({
		type:"POST",
		url:url,	
		data:'action=public_select'+'&query='+query,
		cache:false,
		success:function(data){
			$('#'+id).html(data);
		}
	})
}
function view_payment_details(payment,bank,ref_no){
	if (payment>0) {
		$('#bank_detail').html(bank);
		$('#ref_no_details').html(ref_no);
	}else{
		swal("No Payment Accepted","Accept the payment first","error")
	}
}

function tab_button(id,displayId,tabId){

	$.ajax({
		type:"POST",
		url:url,
		data:'action=tab_button'+'&id='+id+'&displayId='+displayId,
		cache:false,
		success:function(data){
			$('#'+tabId).html(data);
		}
	})
}

function count_grade_r(id,limit,offset,div_id,tabbtn){

	var sy_id = $('#sy_id').val();

	$.ajax({
		type:"POST",
		url:url,
		data:'action=count_studentgrade'+'&id='+id+'&limit='+limit+'&offset='+offset+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#'+div_id).html(data);
			$('.tab-btn').removeClass('active');
			$('#'+tabbtn).addClass('active');
		}
	})
}

function load_device_survey(){

	var sy_id = $('#sy_id').val();

	$.ajax({
		type:"POST",
		url:url,
		data:'action=device_survey'+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#device_survey').html(data);
		}
	})
}

function load_connection_survey(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:'action=connection_survey'+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#connection_survey').html(data);
		}
	})
}

function load_speed(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_speed"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#internet_speed').html(data)
		}
	})
}
function load_delivery(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_delivery"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#learning_delivery').html(data)
		}
	})
}
function load_learning_day(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_learning_day"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#day_learning').html(data)
		}
	})
}
function load_financial_cap(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_financial_cap"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#financial_cap').html(data)
		}
	})
}
function load_teacher_partner(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_teacher_partner"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#teacher_partner').html(data)
		}
	})
}
function load_transpo(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_transpo"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#student_transpo').html(data)
		}
	})
}
function load_is_connected(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_is_connected"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#is_connected').html(data)
		}
	})
}
function load_interfere(){
	var sy_id = $('#sy_id').val();
	$.ajax({
		type:"POST",
		url:url,
		data:"action=load_interfere"+'&sy_id='+sy_id,
		cache:false,
		success:function(data){
			$('#learning_interruption').html(data)
		}
	})
}
function hide_this_payment(name,id){
	swal({
	title:"Delete the payment of "+name+"?",
	text:"Are you sure?",
	type:"warning",
	showCancelButton:true,
	confirmButtonColor:'#f51818',
	confirmButtonText:'Delete',
	cancelButtonColor:'#d33'
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:'action=hide_payment'+ '&id=' + id,
			cache:false,
			success:function(data){
				if(data==1){
					setTimeout(function(){
						swal("Payment Deleted","Successfully","success");
						load_payments();
						$('#viewPayment_modal').modal('hide');
					},1000)
					
				}
				else{
					alert(data);
				}
			}
		})	
	})

}
function view_p_details(name,amount_due,due_payment,due_balance,balance,date_trans,bank,account_no,account_name,ref_no,file,date_accepted,date_due){
	$('#detail_of_payment_modal').modal({backdrop:"static"});
	$('#p_info_name').html(name);
	$('#p_info_amount').html(amount_due);
	$('#p_info_due_payment').html(due_payment);
	$('#p_info_due_balance').html(due_balance);
	$('#p_info_date_transaction').html(date_trans);
	$('#p_info_bank').html(bank);
	$('#p_info_account_no').html(account_no);
	$('#p_info_account_name').html(account_name);
	$('#p_info_reference_no').html(ref_no);
	$('#p_info_file').html(file);
	$('#p_info_file').attr('href',file);
	$('#p_info_date_accepted').html(date_accepted);
	$('#p_info_due_date').html(date_due);
}
//>>>>Portal Setting<<<<

//Portal Banner
function del_temp_file_banner(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;
    
   swal({
      title: "Are you sure ?",
      text: "You want to delete this post ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete",
      closeOnConfirm: true
   },
   function(isConfirm){
   	if (isConfirm) 
   	{
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	//alert(data);

               if (data == "") 
               {
               	showToast.show("File has been removed!",1000);
               	$('#preview_banner').html("");
               	$('#banner_pic').val("");
               	$('#btn_browse').css('display','block');
               	$('#btn_save_banner').attr("disabled","disabled");
               }
               else
               { 
              		alert("Not Remove");
               }  
            }
        });
   	} 
   	else 
   	{

   	}
   });
}

function load_portal_banner(comp_id){

	var mydata = 'action=load_portal_banner' + '&company_id=' + comp_id;

	$.ajax({	
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			//alert(data);
			$('#tbl_banner').html(data);
		}
	})
}

function set_status_active_pb(id,comp_id) {

	var mydata = 'action=set_status_active_pb' + '&id=' + id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			if (data == 1) {
				swal('Success','Successfully Set Portal Banner Image Active','info');
				load_portal_banner(comp_id);
			}
			else{
				swal('Error','Something Went Wrong','error');
			}	
		}
	})
}

function set_status_inactive_pb(id,comp_id) {

	var mydata = 'action=set_status_inactive_pb' + '&id=' + id;

	swal({
		title: "Are you sure ?",
		text: "You want to set this banner image inactive ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: true
   },
   function(isConfirm){

	   	if (isConfirm) 
	   	{
	   		$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				success:function(data){
					if (data == 1) {
						swal('Success','Successfully Set Portal Banner Image Inactive','info');
						load_portal_banner(comp_id);
					}
					else{
						swal('Error','Something Went Wrong','error');
					}	
				}
			})
	   	}
	})
}

function delete_portal_banner(id,comp_id){

	var mydata = 'action=delete_portal_banner' + '&id=' + id;

	swal({
      title: "Are you sure ?",
      text: "You want to delete this banner image ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete",
      closeOnConfirm: true
   },
   function(isConfirm){

	   	if (isConfirm) 
	   	{
	   		$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				success:function(data){
					if (data == 1) {
						swal('Success','Successfully Deleted Portal Banner','info');
						load_portal_banner(comp_id);
					}
					else{
						swal('Error','Something Went Wrong','error');
					}	
				}
			})
	   	}
	})
}


//Portal Content
function load_portal_content(comp_id){

	var mydata = 'action=load_portal_content' + '&company_id=' + comp_id;

	$.ajax({	
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			//alert(data);
			$('#tbl_mission').html(data);
		}
	})
}

function clear_portal_content_data() {
	$("#pc_id").val("");
	$("#categ").val("- Select Category -");
	$("#item").val("");
	display_content_area();
	$("#btn_save_pcontent").text('Save');
	$("#btn_save_pcontent").attr('value', 'Save');

	$("#addedit_content").modal('hide');
}

function display_content_area(id) {

	var mydata = 'action=load_content_area' + '&id=' + id;

	$.ajax({	
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			//alert(data);
			setTimeout(function(){$('#cont_area').html(data);},500);
		}
	});
}

function get_portal_content_data(id,categ,temp,img_head,cont_head){

	$("#pc_id").val(id);
	$("#template").val(temp);
	$("#categ").val(categ);
	$("#img_header").val(img_head);
	$("#cont_header").val(cont_head);
	//$("#item").val(item);
	display_content_area(id);
	$("#btn_save_pcontent").text('Edit');
	$("#btn_save_pcontent").attr('value', 'Edit');
	$("#addedit_content").modal('toggle');
}

function hide_show_temp2(){
	var cont = $("#template").val();

	if (cont == 1) {
		$("#temp2").hide('fast');
	}
	else{
		$("#temp2").show('fast');
	}
}

function save_portal_content(comp_id){

	var btn_text = $("#btn_save_pcontent").attr('value');
	var id = $("#pc_id").val();
	var temp = $("#template").val();
	var categ = $("#categ").val();
	var img_header = $("#img_header").val();
	var cont_header = $("#cont_header").val();
	var items = $("#item").val();
	
	if (btn_text == "Save") {
		var opt = 1;
	}
	else{
		var opt = 2;
	}

	var mydata = 'action=save_portal_content' + '&company_id=' + comp_id + '&categ=' + categ + '&items=' + items + '&opt=' + opt + 
					'&id=' + id + '&temp=' + temp + '&img_header=' + img_header + '&cont_header=' + cont_header ;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			//alert(data)
			if (data == 1) {
				swal('Success','Successfully Added Portal Content','info');
				load_portal_content(comp_id);
 				clear_portal_content_data();
				
			}
			else if (data == 3) {
				swal('Success','Successfully Updated School Portal Content','info');
				load_portal_content(comp_id);
				clear_portal_content_data();
			}
			else{
				swal('Error','Something Went Wrong','error');
			}	
		}
	})
}

function set_status_active_pc(id,comp_id) {

	var mydata = 'action=set_status_active_pc' + '&id=' + id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			if (data == 1) {
				swal('Success','Successfully Set Portal Content Active','info');
				load_portal_content(comp_id);
			}
			else{
				swal('Error','Something Went Wrong','error');
			}	
		}
	})
}

function set_status_inactive_pc(id,comp_id) {

	var mydata = 'action=set_status_inactive_pc' + '&id=' + id;

	swal({
      title: "Are you sure ?",
      text: "You want to set this content inactive ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: true
   },
   function(isConfirm){

	   	if (isConfirm) 
	   	{
	   		$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				success:function(data){
					if (data == 1) {
						swal('Success','Successfully Set Portal Content Inactive','info');
						load_portal_content(comp_id);
					}
					else{
						swal('Error','Something Went Wrong','error');
					}	
				}
			})
	   	}
	})
}

function delete_portal_content(id,comp_id){

	var mydata = 'action=delete_portal_content' + '&id=' + id;

	swal({
      title: "Are you sure ?",
      text: "You want to delete this content ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete",
      closeOnConfirm: true
   },
   function(isConfirm){

	   	if (isConfirm) 
	   	{
	   		$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				success:function(data){
					if (data == 1) {
						swal('Success','Successfully Deleted Portal Content','info');
						load_portal_content(comp_id);
					}
					else{
						swal('Error','Something Went Wrong','error');
					}	
				}
			})
	   	}
	})
}

// ---->>>