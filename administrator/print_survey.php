<html>

    <?php
    include_once('../config.php');
    $sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

    if (mysqli_num_rows($sql)>0) {

        $row = mysqli_fetch_assoc($sql);

        $default_color = ' w3-'.$row['default_color'].' ';
        $school_name = ''.$row['company_name1'].'';
        $acronym = ''.$row['acronym'].'';
        $with_payment = ''.$row['with_payment'].'';
        $logo = $row['logo'];
        $cid = $row['companyid'];

    }?>
    <head>
        <title><?php echo $school_name;  ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link href="../css/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
        <link rel="stylesheet" type="text/css" href="../css/w3.css">
        <link rel="stylesheet" type="text/css" href="../css/animate.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.css">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="../css/showToast.css">
    </head>
   
    <body>
        <?php 
    
                include('../config.php');

                $sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

                if (mysqli_num_rows($sql)>0) {

                    $row = mysqli_fetch_assoc($sql);

                    $company_name = '<b class="img-responsive" style="font-size:25px;"> '.$row['company_name1'].' </b>';
                    $address = '<small style="font-size:30px;"> '.$row['address'].' </small>';
                    $default_color = ' w3-'.$row['default_color'].' ';

                     $header = '<img width="100px;" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
                    $header_bg = ''.$row['header_bg'].'';
                    $navbar_bg = $row['navbar_bg'];
                    $school_name2 = ''.$row['company_name2'].'';
                     $logo = $row['logo'];

                }

            ?>
        <?php
            $sql=mysqli_query($con,"SELECT * FROM tbl_school_year WHERE is_active=1");
            $data=mysqli_fetch_assoc($sql);
            $school_yearId=$data['id'];
        ?>
        
        <div class="">
            <img src="../img/print_header.png" class="img-fluid">
        </div>
       <div class="container-fluid">
            <h3 class="text-center font-weight-bold">Household Capacity and Access to Distance Learning</h3>
            <div class="text-center"><?php echo date('F d, Y H:i:s')?></div>
            <h2>Enrolled</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Student Status</td>
                        <td  style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT student_count.*,COUNT(t1.is_active) AS 'count' FROM (SELECT 1 AS id,'Enrolled' AS 'name' UNION SELECT 0,'Registered') AS student_count LEFT JOIN tbl_enrolled_students t1 ON student_count.id=t1.is_active WHERE school_year=$school_yearId GROUP BY student_count.id");
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                        ?>
                            <tr>
                                <td><?php echo $row['name']?></td>
                                <td><?php echo $row['count']?></td>
                            </tr>
                        <?php
                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <h2>Device</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Gadgets</td>
                        <td  style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT VALUE,COUNT(*) AS 'count' FROM
    (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.available_gadgets, ',', n.n), ',', -1)) VALUE
      FROM tbl_enrolled_students CROSS JOIN 
      (
       SELECT a.N + b.N * 10 + 1 n
         FROM 
        (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) a
       ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 ) b
        ORDER BY n 
       ) n
     WHERE school_year=$school_yearId AND n.n <= 1 + (LENGTH(tbl_enrolled_students.available_gadgets) - LENGTH(REPLACE(tbl_enrolled_students.available_gadgets, ',', '')))
     ORDER BY VALUE) nt0 GROUP BY VALUE");
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                            if(strlen($row['VALUE'])==0){

                            }else{
                                ?>
                                <tr>
                                    <td><?php echo $row['VALUE'];?></td>
                                    <td><?php echo $row['count']?></td>
                                </tr>
                                <?php
                                 
                            }
                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            
            <h2>School Transportation</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Transportation</td>
                        <td  style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT arrive.*,COUNT(*) AS 'count' FROM (
SELECT 'Walking' AS 'name',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%Walking%' UNION
SELECT 'public commute (land/water)',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%public commute (land/water)%' UNION
SELECT 'family-owned vehicle',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%family-owned vehicle%' UNION 
SELECT 'school',go_to_school,school_year FROM tbl_enrolled_students WHERE go_to_school LIKE '%school%')
AS arrive WHERE arrive.school_year=$school_yearId GROUP BY arrive.name");
                    
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                        ?>
                            <tr>
                                <td><?php echo $row['name']?></td>
                                <td><?php echo $row['count']?></td>
                            </tr>
                        <?php
                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <h2>Student Internet Status</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Gadgets</td>
                        <td style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT connected.*,COUNT(t1.`connected_to_internet`) AS 'count' FROM (SELECT 'YES' AS 'name' UNION SELECT 'NO') AS connected LEFT JOIN tbl_enrolled_students t1 ON connected.name=t1.`connected_to_internet` WHERE t1.`school_year`=$school_yearId GROUP BY connected.name");
                    while($row=mysqli_fetch_assoc($sql)){
                        ?>
                            <tr>
                                <td><?php echo $row['name']?></td>
                                <td><?php echo $row['count']?></td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <h2>Connection</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Internet Device</td>
                        <td style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT VALUE,COUNT(*) AS 'count' FROM
                    (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.type_of_internet, ',', n.n), ',', -1)) VALUE
                    FROM tbl_enrolled_students CROSS JOIN 
                    (
                    SELECT a.N + b.N * 10 + 1 n
                        FROM 
                        (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) a
                    ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) b
                        ORDER BY n 
                    ) n
                    WHERE school_year=$school_yearId AND n.n <= 1 + (LENGTH(tbl_enrolled_students.type_of_internet) - LENGTH(REPLACE(tbl_enrolled_students.type_of_internet, ',', '')))
                    ORDER BY VALUE) nt0 GROUP BY VALUE");
                    
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                            if(strlen($row['VALUE'])==0){

                            }else{
                                ?>
                                <tr>
                                    <td><?php echo $row['VALUE'];?></td>
                                    <td><?php echo $row['count']?></td>
                                </tr>
                                <?php
                                 
                            }
                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
           
            <h2>Internet Speed</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Internet Speed</td>
                        <td  style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT COUNT(a.`internet_rate`) AS 'count',numbering.type FROM (SELECT 1 AS id,'Very Slow' AS 'type' UNION SELECT 2,'Slow' UNION SELECT 3,'Good' UNION SELECT 4,'Fast' UNION SELECT 5,'Very Fast' UNION SELECT '','Ignore')AS numbering LEFT JOIN tbl_enrolled_students a
                    ON numbering.id=a.`internet_rate` WHERE a.`school_year`=$school_yearId OR a.`school_year` IS NULL GROUP BY numbering.id");
                    
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                        ?>
                            
                        <?php
                         while($row=mysqli_fetch_assoc($sql)){
                            if(strlen($row['type'])==0){

                            }else{
                                ?>
                                <tr>
                                    <td><?php echo $row['type']?></td>
                                    <td><?php echo $row['count']?></td>
                                </tr>
                                <?php
                                 
                            }

                        }
                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }

                    ?>
                </tbody>
            </table>
            <h2>Learning Delivery</h2>
            <table class="table table-bordered">
                <thead>
                    <tr class="w3-gray">
                        <td>Learning Mode</td>
                        <td  style="width:30%">Number of student</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql=mysqli_query($con,"SELECT VALUE,COUNT(*) AS 'count' FROM
                            (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.learning_delivery_mode, ',', n.n), ',', -1)) VALUE
                              FROM tbl_enrolled_students CROSS JOIN 
                              (
                               SELECT a.N + b.N * 10 + 1 n
                                 FROM 
                                (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) a
                               ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) b
                                ORDER BY n 
                               ) n
                             WHERE school_year=$school_yearId AND n.n <= 1 + (LENGTH(tbl_enrolled_students.learning_delivery_mode) - LENGTH(REPLACE(tbl_enrolled_students.learning_delivery_mode, ',', '')))
                             ORDER BY VALUE) nt0 GROUP BY VALUE");
                    
                    if (mysqli_num_rows($sql)>0) {
                        while($row=mysqli_fetch_assoc($sql)){
                            if(strlen($row['VALUE'])==0){

                            }else{
                                ?>
                                <tr>
                                    <td><?php echo $row['VALUE'];?></td>
                                    <td><?php echo $row['count']?></td>
                                </tr>
                                <?php
                                 
                            }

                        }
                    }else{
                        ?>
                            <tr>
                                <td colspan="2">No Record</td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            
            <div >
                <h2 style="display: none;">Days of Learning</h2>
                <table class="table table-bordered" style="display: none;">
                    <thead>
                        <tr class="w3-gray">
                            <td>Day Learning</td>
                            <td style="width:30%">Number of student</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql=mysqli_query($con,"SELECT COUNT(*) AS 'count',day_learning FROM tbl_enrolled_students WHERE school_year=$school_yearId GROUP BY day_learning");
                        
                        if (mysqli_num_rows($sql)>0) {
                            while($row=mysqli_fetch_assoc($sql)){
                                if(strlen($row['day_learning'])==0){

                                }else{
                                    ?>
                                    <tr>
                                        <td><?php echo $row['day_learning']?></td>
                                        <td ><?php echo $row['count']?></td>
                                    </tr>
                                    <?php
                                     
                                }
                            }
                        }else{
                            ?>
                                <tr>
                                    <td colspan="2">No Record</td>
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <h2 style="display: none;">Financial Capacity</h2>
                <table class="table table-bordered" style="display: none;">
                    <thead>
                        <tr class="w3-gray">
                            <td>Capacity</td>
                            <td style="width:30%">Number of student</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql=mysqli_query($con,"SELECT COUNT(*) AS 'count',financial_cap FROM tbl_enrolled_students WHERE school_year=$school_yearId GROUP BY financial_cap");
                        
                        if (mysqli_num_rows($sql)>0) {
                            while($row=mysqli_fetch_assoc($sql)){
                
                                if(strlen($row['financial_cap'])==0){

                                }else{
                                    ?>
                                    <tr>
                                        <td><?php echo $row['financial_cap']?></td>
                                        <td ><?php echo $row['count']?></td>
                                    </tr>
                                    <?php
                                     
                                }
                            }
                        }else{
                            ?>
                                <tr>
                                    <td colspan="2">No Record</td>
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <h2>Teacher Partner</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr class="w3-gray">
                            <td>Teacher Partner</td>
                            <td  style="width:30%">Number of student</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql=mysqli_query($con,"SELECT VALUE,COUNT(*) AS 'count' FROM
    (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.teacher_partner_home, ',', n.n), ',', -1)) VALUE
      FROM tbl_enrolled_students CROSS JOIN 
      (
       SELECT a.N + b.N * 10 + 1 n
         FROM 
        (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) a
       ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) b
        ORDER BY n 
       ) n
     WHERE school_year=$school_yearId AND n.n <= 1 + (LENGTH(tbl_enrolled_students.teacher_partner_home) - LENGTH(REPLACE(tbl_enrolled_students.teacher_partner_home, ',', '')))
     ORDER BY VALUE) nt0 GROUP BY VALUE");
                        
                        if (mysqli_num_rows($sql)>0) {
                            while($row=mysqli_fetch_assoc($sql)){
                                if(strlen($row['VALUE'])==0){

                                }else{
                                    ?>
                                    <tr>
                                        <td><?php echo $row['VALUE'];?></td>
                                        <td><?php echo $row['count']?></td>
                                    </tr>
                                    <?php
                                     
                                }
                            }
                        }else{
                            ?>
                                <tr>
                                    <td colspan="2">No Record</td>
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <h2>Online Learning Interruption</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr class="w3-gray">
                            <td>Intefere</td>
                            <td style="width:30%">Number of student</td>
                        </tr>
                    </thead>
                     <tbody>
                        <?php
                        $sql=mysqli_query($con,"SELECT VALUE,COUNT(*) AS 'count' FROM
        (SELECT TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(tbl_enrolled_students.affects_DE, ',', n.n), ',', -1)) VALUE
          FROM tbl_enrolled_students CROSS JOIN 
          (
           SELECT a.N + b.N * 10 + 1 n
             FROM 
            (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8) a
           ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8) b
            ORDER BY n 
           ) n
         WHERE school_year=$school_yearId AND n.n <= 1 + (LENGTH(tbl_enrolled_students.affects_DE) - LENGTH(REPLACE(tbl_enrolled_students.affects_DE, ',', '')))
         ORDER BY VALUE) nt0 GROUP BY VALUE");
                        
                        if (mysqli_num_rows($sql)>0) {
                            while($row=mysqli_fetch_assoc($sql)){
                                if(strlen($row['VALUE'])==0){

                                }else{
                                    ?>
                                    <tr>
                                        <td><?php echo $row['VALUE'];?></td>
                                        <td><?php echo $row['count']?></td>
                                    </tr>
                                    <?php
                                     
                                }
                            }
                        }else{
                            ?>
                                <tr>
                                    <td colspan="2">No Record</td>
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>