<?php
session_start();    // Increase 'timeout' time<br />
  
  if(!empty($_FILES["file"]["type"])){
        $fileName = time().'_'.$_FILES['file']['name'];
        $valid_extensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES["file"]["name"]);
        $file_extension = end($temporary);
        if(($_FILES["file"]["size"] < 1000000000) && in_array($file_extension, $valid_extensions)){

            $sourcePath = $_FILES['file']['tmp_name'];

            $targetPath = "../files/".$fileName;

            if(move_uploaded_file($sourcePath,$targetPath)){

                $uploadedFile = "../files/".$fileName; ?>

                <?php 
                 //echo  $_FILES["file"]["type"];

                 if($_FILES["file"]["type"] == "image/jpeg" || $_FILES["file"]["type"] == "image/jpg" || $_FILES["file"]["type"] == "image/png"){ ?>

                  <a href="<?php echo $uploadedFile; ?>" target="_blank"><img src="<?php echo $uploadedFile; ?>" class="img img-fluid img_size img-thumbnail" width='200'></a> 
                  <br>
                  <br>
                  <span class="btn btn-danger text-center" onclick="del_temp_file('<?php echo $uploadedFile; ?>')">Cancel</span>
                  <input type="hidden" id="file_name" name="file_name" value="<?php echo $uploadedFile; ?>">
                  <hr>
                <?php
                 }
            }
        }else{
          echo '404';
        }
    }
?>
