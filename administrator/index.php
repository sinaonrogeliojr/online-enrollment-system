<?php
session_start();
if (!isset($_SESSION['admin_pane'])) {
  @header('location:../');  
}

include_once('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

  if (mysqli_num_rows($sql)>0) {

    $row = mysqli_fetch_assoc($sql);

    $company_id = $row['companyid'];
    $default_color = 'w3-light-blue';
    $school_name = ''.$row['company_name1'].'';
    $acronym = $row['acronym'];
    $with_payment = ''.$row['with_payment'].'';
    $cid = $row['companyid'];
    $navbar_bg = $row['navbar_bg'];
    $logo = '<img width="60" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
    $company_name = ''.$row['company_name1'].'';

  }

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        echo $row['school_year'];
    }
}

function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade order by orderno asc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['ID'].'">'.$row['grade'].'</option>';
        }
    }
}

function get_school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by is_active desc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $school_name; ?></title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/w3.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="../css/showToast.css">

  <style type="text/css">
    /*css new file*/
  .header-image{
    border-bottom: none;
      padding: 15px 15px 0 15px;
      background-image: url(../img/modal-header.jpg);
      height: 100px;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center
  }
  .cards-image{
    border-bottom: none;
      padding: 15px 15px 0 15px;
      background-image: url(../img/card.jpg);
      height: 100px;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center
  }
  .img50{
    width: 50px;
    height: 50px;
  }
  .tab-btn.active{
      background-color: blue;
      color: white;
  }
  </style>
</head>

<body id="page-top" onload="dashboard_load(); load_pass();">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <!-- <i class="fas fa-laugh-wink"></i> -->
          <?php echo $logo; ?>
        </div>
        <div class="sidebar-brand-text mx-1"><?php echo $acronym; ?> Admin <!-- <sup>2</sup> --></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>

      </li>
        <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="enrollment.php">
          <i class="fas fa-fw fa-edit"></i>
          <span>Enrollment <?php echo get_school_year_active($con); ?></span></a>
      </li>

     
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Administrator</span>
                <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
                <span class="fa fa-user-secret fa-2x"></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#manage_account">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logout_me">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard  / <span class="w3-large">Select School Year:</span>
              <div class="form-check-inline">
                   <select class="form-control" id="sy_id" onchange="dashboard_load();">
                    <?php echo get_school_year($con); ?>
                  </select>
              </div>
            </h1>

          </div>

          <!-- Content Row -->
          <div class="row">

            <?php if($with_payment == 'YES'){ ?> 

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Registered Students</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><strong id="count_registered"></strong></div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Enrolled Students -->
              <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Enrolled Students</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><strong id="count_enrolled"></strong></div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-check fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Fully Paid -->
              <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Fully Paid Students</div>
                        <div class="row no-gutters align-items-center">
                          <div class="col-auto">
                            <div class="h1 mb-0 mr-3 font-weight-bold text-gray-800"><strong id="count_paid"></strong></div>
                          </div>
                          
                        </div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-hand-holding-usd fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            <?php }else{ ?>

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-6 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Registered Students</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><strong id="count_registered"></strong></div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Earnings (Monthly) Card Example -->
              <div class="col-xl-6 col-md-6 mb-4 ">
                <div class="card border-left-success  shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Enrolled Students</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><strong id="count_enrolled"></strong></div>
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-check fa-2x text-gray-300"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            <?php } ?>

          </div>

          <!-- Content Row -->
          
          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"></h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <ul class="nav nav-pills nav-justified " style="font-weight: bolder;">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#register_countbtn"><img src="../img/enrolled.png" class="img50"> Registered</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#enrolled_countbtn"><img src="../img/register.png" class="img50"> Enrolled</a>
                      </li>
                    </ul>
                    <div class="dropdown-divider"></div>
                    <div class="tab-content">
                      <div class="tab-pane active" id="register_countbtn">
                        <div id="count_grade_student_r"></div>
                        <div class="text-center mt-2" id="tabId_r"></div>
                        
                      </div>
                      <div class="tab-pane" id="enrolled_countbtn">
                        <div id="count_grade_student_e"></div>
                        <div class="text-center mt-2" id="tabId_e"></div>
                        </div>
                        
                    </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-12 mb-4">

              <div class="card">
                <div class="card-body">
                  <div class="form-row">
                    <div class="col-xl-10 col-sm-12 col-md-12"><h2 class="text-center">Household Capacity and Access to Distance Learning</h2></div>
                    <div class="col-xl-2" id="removePrint">
                      <div class="mt-2 text-center">
                      <input type="button" class="btn btn-primary" onclick="frames['frame'].print()" value="Print">
                      </div>
                    </div>
                  </div>
                
                  <iframe src="print_survey.php" style="display:none;" name="frame"></iframe>
                  <div>
                    <span class="mt-5">Students with available gadgets</span>
                    <div class="dropdown-divider"></div>
                    <div id="device_survey"></div>
                  </div>
                </div>
                <div class="card-body">
                  <span>Students Internet Connection </span>
                  <div class="dropdown-divider"></div>
                  <div id="connection_survey"></div>
                </div>
                <div class="card-body">
                  <div class="form-row">
                    <div class="col-sm-4 col-xl-4 col-md-4">
                      <div id="internet_speed"></div>
                    </div>
                    <div class="col-sm-8 col-xl-8 col-md-8">
                      <div id="learning_delivery"></div>
                    </div>
                  </div>
                </div>
                <div class="card-body mt-3" style="display: none;">
                  <div id="day_learning"></div>
                </div>
                <div class="card-body mt-3" style="display: none;">
                  <div id="financial_cap"></div>
                </div>
                <div class="card-body mt-3">
                  <div id="teacher_partner"></div>
                </div>
                <div class="card-body mt-3">
                  <div id="student_transpo"></div>
                </div>
                <div class="card-body mt-3">
                  <div id="is_connected"></div>
                </div>
                <div class="card-body mt-3">
                  <div id="learning_interruption"></div>
                </div>
                
              </div>

            </div>

          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include_once("modals.php");?>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

 <script src="../js/sweetalert.min.js"></script>
  <script src="../js/showToast.js"></script>
  <script src="function.js"></script>

</body>

</html>