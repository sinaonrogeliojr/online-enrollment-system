<html>

    <?php
    include_once('../config.php');
    $sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

    if (isset($_REQUEST['sid'])) {
        $student_id = $_REQUEST['sid'];
    }
    else
    {   
        echo "No Student ID Selected !!!";
    }

    if (mysqli_num_rows($sql)>0) {

        $row = mysqli_fetch_assoc($sql);

        $default_color = ' w3-'.$row['default_color'].' ';
        $school_name = ''.$row['company_name1'].'';
        $acronym = ''.$row['acronym'].'';
        $with_payment = ''.$row['with_payment'].'';
        $logo = $row['logo'];
        $cid = $row['companyid'];

        $company_name = '<b class="img-responsive" style="font-size:25px;"> '.$row['company_name1'].' </b>';
        $address = '<small style="font-size:30px;"> '.$row['address'].' </small>';
        $default_color = ' w3-'.$row['default_color'].' ';

        $header = '<img width="100px;" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
        $header_bg = ''.$row['header_bg'].'';
        $navbar_bg = $row['navbar_bg'];
        $school_name2 = ''.$row['company_name2'].'';

    }?>

    <?php
        $sql=mysqli_query($con,"SELECT * FROM tbl_school_year WHERE is_active=1");
        $data=mysqli_fetch_assoc($sql);
        $school_yearId=$data['id'];
    ?>

    <head>
        <title><?php echo $school_name; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link href="../css/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
        <link rel="stylesheet" type="text/css" href="../css/w3.css">
        <link rel="stylesheet" type="text/css" href="../css/animate.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="../font/css/all.css">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="../css/showToast.css">
    </head>
   
    <body>
        
        <div class="">
            <img src="../img/print_header.png" class="img-fluid">
        </div>
        <div class="container-fluid">
          <h3 class="text-center font-weight-bold">MEMORAMDUM OF AGREEMENT</h3>
            <div class="text-center"><?php echo date('F d, Y H:i:s')?></div>

            <br><br>
            <div class="tab-pane container" id="moa">
              <style type="text/css">
                .ml-7{
                  margin-left: 7%;
                }
                .ml-15{
                  margin-left: 15%;
                }
                .text-default{
                  font-size: 16px;
                }
              </style>
              <?php

                $moa_sql = mysqli_query($con, "SELECT * FROM tbl_moa WHERE student_id ='$student_id'");

                if (mysqli_num_rows($sql)>0) {
                      $row_moa = mysqli_fetch_assoc($moa_sql);
                    }
              ?>
              <p class="text-default">Dear Ma’am/Sir.</p>

              <p class="text-default">Greetings of peace and love!</p>

              <p class="text-default">Enrolling your child/children at <strong>CHILDREN FIRST SCHOOL, INC.,</strong> bespeaks of your TRUST and CONFIDENCE in us. We appreciate and value this very deeply. This drives us even harder to help your child heighten his/her total wellbeing as CFS envisions.</p>

              <p class="text-default">We, at CHILDREN FIRST SCHOOL, INC., cannot do this alone. WE NEED YOU TO HELP US, particularly in the following areas:</p>
              <p class="ml-3 text-default">A. <u>Developing Self-discipline:</u> We need your support in developing these traits. Please keep abreast with and abide by the school policies, rules and regulations and be able to follow up their development beyond the school. Please be aware on thebelow prohibitions for we will strictly monitor:</p>


              <p class="ml-5 text-default">1. Bringing/Driving private vehicles for school. Minors and students without professional driver's license are not allowed to drive and park vehicles in the parking lot and the loading and unloading bay in front of the school.</p>

              <p class="ml-5 text-default">2. Improper use of gadgets inside the classroom. Students are not allowed to use cellphones, tablets and other electronic gadgets during class hours. They are also not allowed to charge gadgets in the electrical outlets of the school anytime during the day.</p>

              <p class="ml-5 text-default">3. Offensive behavior to the general public. CFS recognizes that many cultures and religions exist in the school. Students should therefore refrain from inappropriate behavior such as public display of affection, shouting which may cause public disturbance and the like, that are offensive to the general public while in the school premises, while on official school trips, while wearing official school uniform, or any other school related activity.</p>

              <p class="ml-5 text-default">4. Bullying of any kind.</p>

              <p class="ml-5 text-default">5. Drinking alcoholic beverages/Smoking inside the school and in public places.</p>
              <p class="ml-5 text-default">6. Punctuality and Attendance: Pupils and Students are expected to be punctual in reporting to the school and absences should be limited to what is necessary and
              permissible.</p>

              <p class="ml-5 text-default">7. School Uniform and other Uniforms: There are prescribed daily school uniforms and other uniforms such as PE and Laboratory wearing, these uniforms must only be used during the scheduled day of wearing them. </p>

              <p class="ml-3 text-default">B. Developing Self-reliance: Once one says "I CAN", it will not be difficult for him/her to be the BEST he/she can. This "I CAN" attitude is best developed when the child is allowed to do things by himself/herself at every chance possible, such as the following:</p>

              <p class="ml-7 text-default"><u>1. Coming to School:</u> if you are accompanying your child to school, please accompany
              him/her TO THE GATE ONLY. If you have to wait for him/her; please STAY ONLY
              AT THE DESIGNATED WAITING AREA. Entry time is 7:00 A.M </p>

              <p class="ml-2 text-default"><b>Section 159 of the 2010 Revised Manual for Private Schools</b> amended and shall read as follows:</p>
              <span class="ml-5 text-default">Campus Security. Only bonafide students of the school shall be allowed inside the school campus. They shall be required to sign the logbook of the security service. Teachers shall confer with parents/guardians or entertain visitors in the Principal's Office during their off period if the parents have scheduled appointments with them.</span><br><br>

              <p class="ml-5 text-default">2. <u>Going Home.</u> If you are fetching your child, please meet him/her at the gate or waiting area. Dismissal time is at 12:00 for the morning session and at
              3:50 for the afternoon session for the regular school days.
              Please fetch your child ON TIME. After this time, the homeroom adviser will be busy with the next day's preparations for other school activities and therefore, can no longer look after your child. Extended time for your child supervision will not qualify for any liability in case of unexpected bad circumstances. Students shall wait for their fetchers at the designated areas (waiting lounge and preschool play area).</p>
              
              <p class="ml-5 text-default">3. <u>Self-development:</u> The child learns to be organized, responsible for specific things, forms good personal habits, relies on self and be independent and self-disciplined. Please encourage your child to observe this and check for any laxity or dishonesty.</p>
              
              <p class="ml-5 text-default">4. <u>Admission Slips:</u> Whenever your child gets tardy or not in the prescribed uniform, he/she needs to get an admission slip from the Prefect of discipline to enable him/her to be admitted to his/her class/classes. In case of absences, the pupil or student should present an excuse letter duly signed by the parent/guardian to the Prefect of Discipline as soon as he/she resumes attending classes. Parent's or Guardian's excuse letter will be our basis for determining the reasonableness of your child's absence/tardiness. Please support us on this practice.</p>
              
              <p class="ml-5 text-default">5. <u>Examination Permits/Clearance:</u> A way to directly learn about the value of being careful with important documents is to encourage the child to do things by himself EXCEPT the following:</p>
              <p class="ml-7 text-default">a. Examination Permits/Clearance (should be taken care by the parents the least should be 2 days before the examination. No Permit No Exam Policy will be implemented).</p>
              <p class="ml-7 text-default">b. Clearance (at the end of the school year).</p>
              <p class="ml-7 text-default">c. For the Preschool kids, you may feel more secure if you do this for your children.</p>

              <p class="ml-3 text-default">C. School Activities/Events: The following are school activities where the parent's involvement shall be required:</p>
              <p class="ml-5 text-default">I. Card Days — Here, parents get the chance of conferring with the child's Homeroom adviser as report cards are given. The parents get to join the PARENT-TEACHER CONFERENCE, the venue for sharing and learning together about the children's concerns.</p>
              <p class="ml-7 text-default">
              2.  Foundation Days<br>
              3.  Dad's day/Mom's Day<br>
              4.  Other school activities, programs or meetings, of which you are informed such as Family Day, Recollections and Retreat for Values transformation conforming to the CEAP values approach and educational purposes.</p>

              <p class="ml-3 text-default">D. Class visits/Suggestions/Complaints: Should parents want to give suggestions, or criticism or complaints, parents MUST SEE THE PRINCIPAL FIRST, who will call or get together those concerned and arrange that those concerned get a proper answer/resolution to their need or problem.</p>
              <p class="ml-5 text-default">*Parents are discouraged in going directly to the classroom teacher during class time. An appointment may be scheduled to talk with the concerned teacher and/or Principal. Parents are also DISCOURAGED IN POSTING ISSUES/COMPLAINTS IN ANY SOCIAL MEDIA such as Facebook, Twitter, Messenger, etc. There are proper ways, venues and protocols to achieve better ends.</p>

              <p class="ml-3 text-default">
                E. Parents need to provide the school with the information about the student by checking the right blank or providing the answer: 
              </p>

               <p class="ml-5">
                  &#9745; 1. My child goes home by himself/herself.
              </p>

              <p class="ml-5">
                &#9745; 2. My child will be fetched at time/by: <u><?php echo $row_moa['q2_a'];?> / <?php echo $row_moa['q2_b'];?></u>
              </p>

              <p class="ml-5">
                  &#9745; 3. My child goes home for lunch, hence should be allowed to go home at lunch break.
              </p>

              <p class="ml-5">
                  &#9745; 4. My child eats lunch in school with his/her guardian/fetcher, hence should not be allowed to go out at lunch break.
              </p>

              <p class="ml-5">
                 &#9745; 5. On occasion that he/she needs to go home during school hours, a note or an exit slip to this effect will be given by the School Principal or in her absence; the Prefect of Discipline. Without the exit slip, the guard will not allow the child to get outside the school vicinity before dismissal time.
              </p>

              <p class="ml-5">
                 &#9745; 6. Any information the school administration should know, should be indicated as follows: medical, health, allergy(ies), other concern(s):
              </p>
              <p class="ml-5"><u><?php echo $row_moa['q6_a'];?></u></p>

              <p class="ml-5">
                 &#9745; 7. Please write your comments, suggestions and recommendations on the space provided for so we can improve our services for you and our pupils.
              </p>
              <p class="ml-5"><u><?php echo $row_moa['q6_a'];?></u> </p>

              <br>
              <div class="card" style="margin-top: 150px;">
                
                <div class="card-header w3-blue text-center text-white">
                  <b>AGREEMENT PAGE <span class="text-danger">*</span></b>
                </div>


                <div class="card-body">
                  
                  <p class="text-center text-default">
                    This MEMORANDUM OF AGREEMENT is presented to us as a PARENT/GUARDIAN upon enrollment for School Year 2020-2021 in Children First School, Inc. conforming that upon submitting this form:
                  </p>

                  <div id="memo">

                     <p class="ml-5">
                         &#9745; We have read the Children First School, Inc. policies and procedures, parents’ guidelines, other school rules and regulations and we AGREE with all of them;
                     </p>

                     <p class="ml-5">
                        &#9745; All student credentials and pertinent documents that we submitted to the Children First School, Inc. Registrar Office will and may be used to any legal, procedural, and official purposes that the Children First School, Inc. needs to accomplish that may result to the request for a second copy;
                     </p>

                     <p class="ml-5">
                        &#9745; We have original / photocopy copies of all student credentials and pertinent documents that we submitted to the Children First School, Inc. Registrar Office that is available anytime when the school needs or asks for a second copy;
                     </p>

                     <p class="ml-5">
                        &#9745; We allow Children First School, Inc. to use the image, likeness, personal identification such as but not limited to LRN, Name, Age, Address, Contact Details, and the like to any legal, procedural, school program, and all other official purposes that the Children First School, Inc. needs to accomplish;
                     </p>

                     <p class="ml-5">
                         &#9745; We agree to pay all school fees necessary for enrolling and promoting my child that includes all financial liabilities that my child is subject for within the inclusive school year. Furthermore, we agree that we will to pay all school fees on or before the current school year ends respective to my chosen terms of payments. Otherwise, we understand that all credentials and certifications such as but not limited to: Diploma, Certificates, Final SF-9 (Report Card), and SF-10 (Form 137) shall not be released from the registrar’s office until the completion/clearance of financial liabilities.
                     </p>

                  </div>

                </div>

              </div>

              <br><br>
              <p>
                <label class="float-right">Conforme By : <br/> <?php echo $row_moa['conforme_by'];?> </label>
              </p>

         </div>
      </div>
  </body>
</html>