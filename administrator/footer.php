<?php
include_once('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

  if (mysqli_num_rows($sql)>0) {

    $row = mysqli_fetch_assoc($sql);

    $company_id = $row['companyid'];
    $default_color = 'w3-light-blue';
    $school_name = ''.$row['company_name1'].'';
    $acronym = $row['acronym'];
    $with_payment = ''.$row['with_payment'].'';
    $cid = $row['companyid'];
    $navbar_bg = $row['navbar_bg'];
    $logo = '<img width="60" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
     $company_name = $row['company_name1'];

  }
 ?>

<footer class="sticky-footer bg-white">
<div class="container my-auto">
  <div class="copyright text-center my-auto" style="line-height: 20px;">
    <span>&copy; Copyright <strong><?php echo $company_name; ?></span><br>
     Designed by <a href="">Greathome IT Services and Consultancy</a>
  </div>
</div>
</footer>