	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link href="css/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
	<link href="https://fonts.googleapis.com/css?family=Lobster:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="font/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="font/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="css/showToast.css">
	

	<!-- Libraries CSS Files -->
	<link href="lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
	<link href="lib/owlcarousel/owl.carousel.css" rel="stylesheet">
	<link href="lib/owlcarousel/owl.transitions.css" rel="stylesheet">
	<link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="lib/animate/animate.min.css" rel="stylesheet">
	<link href="lib/venobox/venobox.css" rel="stylesheet">

	<!-- Nivo Slider Theme -->
	<link href="css/nivo-slider-theme.css" rel="stylesheet">

	<!-- Main Stylesheet File -->
	<link href="css/style.css" rel="stylesheet">

	<!-- Responsive Stylesheet File -->
	<link href="css/responsive.css" rel="stylesheet">
	<link href="timeline.css" rel="stylesheet">