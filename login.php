<?php
 session_start();
 if (isset($_SESSION['id'])) {
 	@header('location:client/');
 }
 include_once('config.php');

function with_highschool($con){
	$sql = mysqli_query($con,"SELECT * from tbl_company where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
         $row = mysqli_fetch_assoc($sql);
         return $row['with_highschool'];
    }
}

function get_terms($con){
    $sql = mysqli_query($con,"SELECT * from tbl_terms order by id asc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['terms'].'</option>';
        }
    }
}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        echo $row['school_year'];
    }
}

function get_school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by is_active desc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$default_color = 'w3-light-blue';
		$school_name = ''.$row['company_name1'].'';
		$school_name2 = ''.$row['company_name2'].'';
		$acronym = ''.$row['acronym'].'';
		$with_hs = ''.$row['with_highschool'].'';
		$with_gs = ''.$row['with_gradeschool'].'';
		$with_payment = ''.$row['with_payment'].'';
		$telephone = ''.$row['telephone'].'';
		$cp = ''.$row['cellphone'].'';
		$add = ''.$row['address'].'';
		$p_userid = "P-".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
		$p_pass = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
		$company_name = ''.$row['company_name1'].'';
		$header = '<img width="100px;" src="data:image/jpeg;base64,'.base64_encode($row['logo']).'" class="rounded img-fluid"/>';
		$header_logo = '<img style=" max-width: 100%;" src="data:image/jpeg;base64,'.base64_encode($row['header_logo']).'" class="rounded img-fluid w-100"/>';
   		$navbar_bg = ''.$row['header_bg'].'';
		$login_img = '<img width="500px;" src="data:image/jpeg;base64,'.base64_encode($row['login_img']).'" class="rounded img-fluid"/>';

	}

function get_grade_level($con,$with_hs,$with_gs){

    $sql = mysqli_query($con,"SELECT * from tbl_grade where is_highschool = '$with_hs' or is_gradeschool = '$with_gs' order by orderno asc");

    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['ID'].'">'.$row['grade'].'</option>';
        }
    }

}


?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $school_name;  ?></title>
	<?php include_once('links.php'); ?>
</head>

<style type="text/css">
	body {

		background-color: #f2f2f2;
	 
	  /*background-color: #cccccc;*/
	}

	.display-none{
		display: none;
	}

</style>

<body onload=" load_school_fees(); load_terms_of_payment();load_mobile_index()">
<nav class="navbar navbar-expand-lg navbar navbar-expand-sm navbar-dark" style="background: <?php echo $navbar_bg?>">
  <div id="header_image" style="display: none;" class="w-100"><?php echo $header_logo; ?></div>
  <a class="navbar-brand" id="navbar-brand-logo" href="#"><?php echo $header ?></a>
  <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon text-white"></span>
  </button>

  <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      </li>
    </ul>

    <ul class="navbar-nav" id="navbar-header">
      <li class="nav-item active">
          <a class="nav-link page-scroll w3-allerta" href="index.php"><b>HOME</b></a>
	  </li>
	  <li class="nav-item active">
	      <a class="nav-link page-scroll w3-allerta" href="register.php"><b>REGISTER</b></a>
	  </li>
    </ul>
  </div>
</nav>
 <nav class="navbar navbar-expand-md w3-pale-red navbar-light" id="desktop_btn_navbar" style="display: none">
 
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            
            
        </ul>
        <ul class="navbar-nav">
           <li class="nav-item active">
              <a class="nav-link page-scroll" href="index.php"><b> <span class="fa fa-home"></span> HOME</b></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link page-scroll" href="register.php"><b><span class="fa fa-edit"></span> REGISTER</b></a>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid" style="margin-top: 10px;">

	<div class="row">

		<div class="col-md-6">

			<div class="row">

				<div class="col-md-3">
					
				</div>
				<div class="col-md-9">
					<h5 style="font-weight: bolder;"><?php echo $school_name; ?></h5>

					<?php if($telephone <> ''){
						echo '<h5>Telephone: <b><?php echo $telephone;?></b>  </h5>';
					}else{
						echo '';
					} ?>

					
					<h5>Cellphone: <b><?php echo $cp;?></b>  </h5>
					<h5>Address: <b><?php echo $add; ?></b> </h5>
					<br>

					<?php echo $login_img; ?>

				</div>
				
			</div>

		</div>

		<div class="col-md-6" style="background-color: #e9ecef;">
			<div class="jumbotron">
			  <h2><b>Login to your account</b></h2>
			  <p>Enter your details to login.</p>

			  <br>

			  <form onsubmit="event.preventDefault();">
			  		<div class="input-group mb-3 input-group-lg">
				    <div class="input-group-prepend">
				      <span class="input-group-text"><span class="fa fa-user"></span> </span>
				    </div>
				    <input type="text" class="form-control" id="uname" name="uname" placeholder="Username" required="">
				</div>
				<div class="input-group mb-3 input-group-lg">
				    <div class="input-group-prepend">
				      <span class="input-group-text"><span class="fa fa-key"></span></span>
				    </div>
				    <input type="password" class="form-control" placeholder="Password" id="pwd" name="pwd" required="">
				</div>

				<div class="btn btn-group">
					<div class="input-group mb-3 " style="margin-left: -13px;">
				    <input type="submit" id="btn_log" class="btn <?php echo $default_color; ?> btn-lg" onclick="login();" value="Login">
				</div>
				
				</div>
			  	</form>

			  	<footer class="">Dont have an account? <a href="register.php" style="color:red;">Register here</a></footer>

			</div>
		</div>

	</div>
	<br>
	<footer>
		<br>
		<p class="text-center">
				Design by <b>GreatHome I.T. Service and Consultancy</b>
		</p>
	</footer>

</div>

</body>

<?php include_once('scripts.php'); ?>

</html>