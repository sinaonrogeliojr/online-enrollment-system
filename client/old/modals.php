<?php 

include('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$default_color = ' w3-'.$row['default_color'].' ';
		$school_name = ''.$row['company_name1'].'';
		$acronym = ''.$row['acronym'].'';
	}

?>

<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block <?php echo $default_color; ?>">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">No</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="payment_details">
	<div class="modal-dialog modal-lg">
		<div class="modal-body" id="view_payment_details">
			
		</div>
		<!-- 	<div class="modal-footer">
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div> -->
		<!-- <div class="modal-content">-->
	</div>
</div>

<!-- Add Payment-->
<div class="modal fade" role="dialog" id="payment">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Add Payment<input type="hidden" id="payment_id">
			<button type="button" class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">

				<div class="row"> 
					<div class="col-sm-6" style="margin-top: 5px;">
						<ul class="list-group">
						  <li class="list-group-item list-group-item-primary"><b><span class="fa fa-money-bill-wave"></span> Balance </b></li>
						  <li class="list-group-item list-group-item-action"><center><h2><b id="p_amt_due"></b></h2></center></li>
						  <input type="hidden" id="p_amt_due1">
						</ul>
					</div>

					<div class="col-sm-6" style="margin-top: 5px;">
						<ul class="list-group">
						  <li class="list-group-item list-group-item-success"><b><span class="fa fa-money-bill-wave"></span> Amount Due </b></li>
						  <li class="list-group-item list-group-item-action"><center><h2><b id="p_rem_bal"></b></h2></center></li>
						</ul>
					</div>
				</div>
				<br/>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="lname"><b>Amount: </b></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="amt" class="form-control text-capitalize">
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						  <label for="lname"><b>Bank: </b></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="bank" class="form-control text-capitalize" >
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						  <label for="lname"><b>Account No.: </b></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="acct_no" class="form-control text-capitalize" >
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						  <label for="lname"><b>Account Name: </b></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="acct_name" class="form-control text-capitalize" >
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						  <label for="lname"><b>Reference No: </b></label> <i class="display-none"> This field is required!</i>
						  <input type="text" id="ref_no" class="form-control text-capitalize" >
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						 	<label for="lname"><b>Upload Receipt: </b></label> <i class="display-none"> This field is required!</i>
						 	<form enctype="multipart/form-data" id="upload_receipt" >
								<input type="hidden" name="pid_pic" id="pid_pic">
				  				<input type="hidden" name="doc_type" id="doc_type">
								<input type="hidden" id="f_name" name="f_name">
						
								<input type="file" class="form-control" id="receipt1" name="receipt1" style="display: none;"/>

								<div class="row">
									<div class="col-sm-6" style="margin-bottom: 5px;">
										<button type="button" class="btn <?php echo $default_color; ?> btn-sm btn-block" id="btn-select" onclick="$('#receipt1').click();"> Browse</button>
									</div>

									<div class="col-sm-6">
										<input type="submit" name="submit" id="btn_attach" class="btn <?php echo $default_color; ?> btn-sm btn-block submitBtn disabled" value="Attach"/>
									</div>
								</div>	
							</form>
						</div>
					</div>
				</div>	
				<div class="text-center" id="preview_file"></div>

				<!-- <div class="col-sm-6">
					<div class="form-group">
					  <label for="lname"><b>Mode of Payment: </b></label> <i class="display-none"> This field is required!</i>
					  <select class="form-control text-capitalize" id="mop" onchange="show_mop();" onload="show_mop();">
					  	<option value="0"></option>
					  	<option value="1">Bank Transfer</option>
					  	<option value="2">Remittance</option>
					  </select>
					</div>
				</div> -->

				<!-- <div id="bt" style="display: none;">
					<div class="row">
												
					</div>
				</div> -->

				<!-- div id="rmt" style="display: none;">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="lname"><b>Sender Name: </b></label> <i class="display-none"> This field is required!</i>
							  <input type="text" id="sender_name" class="form-control text-capitalize" >
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="lname"><b>Contact No: </b></label> <i class="display-none"> This field is required!</i>
							  <input type="text" id="cno" class="form-control text-capitalize" >
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="lname"><b>Address: </b></label> <i class="display-none"> This field is required!</i>
							  <input type="text" id="address" class="form-control text-capitalize" >
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="lname"><b>Reference No: </b></label> <i class="display-none"> This field is required!</i>
							  <input type="text" id="ref_no" class="form-control text-capitalize" >
							</div>
						</div>
						<div class="col-sm-12">
							<form enctype="multipart/form-data" id="upload_receipt" >
								<input type="hidden" name="doc_id" id="doc_id">
					  			<input type="hidden" name="doc_type" id="doc_type">
								<input type="hidden" id="f_name" name="f_name">

								<div class="form-group">
									<label for="lname"><b>Upload Receipt: </b></label> <i class="display-none"> This field is required!</i>
							        <input type="file" class="form-control" id="receipt2" name="receipt2" style="display: none;"/>
							        <button type="button" class="btn btn-primary btn-small btn-block" id="btn-select" onclick="$('#receipt2').click();"><span class="fa fa-upload"></span> Browse Receipt</button>
							    </div>
							</form>
							
						</div>
					</div>
				</div> -->
				 
			    <br/>
			    <button type="button" class="btn btn-success btn-block btn-lg" id="btn_pays" disabled id="btn-select" onclick="add_payment();">
			    <span class="fa fa-upload"></span> PAY</button>
			</div>
			<!-- <div class="modal-footer">
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Close</a>
			</div> -->
		</div>
	</div>
</div>

<!-- MANAGE USER ACCOUNT -->

<div class="modal fade" role="dialog" id="manage_account">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" >
				<h3 id="header_bank"><span class="fa fa-edit"></span> Manage Account</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<ul class="nav nav-tabs">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="tab" href="#manage_acc">Manage User Account</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#manage_img">Change Picture</a>
				  </li>
				</ul>

				<div class="tab-content">
					
				  <div class="tab-pane container active" id="manage_acc">

				  	<div class="form-group">
						<label for="userid"><b>Username:</b></label>
						<input type="text" class="form-control" id="userid" value="<?php echo $_SESSION['user_name'] ?>" disabled="">
					</div>

					<label for="old_pass"><b>Old Password:<span id="old_pass_validator"></span></b></label>
					<div class="input-group mb-3" id="show_pass">
					  

					  
					</div>

					<div class="form-group">
						<label for="new_pass"><b>New Password</b></label>
						<input type="password" class="form-control" id="new_pass" required="">
					</div>
					<div class="form-group">
						<label for="retype_pass"><b>Retype Password:</b> <span id="retype_pass_validator"></span></label>
						<input type="password" class="form-control" id="retype_pass" required="">
					</div>

					<button class="btn btn-block btn-info" id="btn_up_account" onclick="update_password();">
						Save
					</button>

				  </div>

				  <div class="tab-pane container fade" id="manage_img">
				  		
				  	<form enctype="multipart/form-data" id="image_form" >

							<div class="card-body text-center hide_logo" id="my_pic2">
							
							</div>
							<div class="form-group hide_logo text-center">

							   <button type="button" class="btn btn-default" id="btn-upload" onclick="$('#file_image').click();"><span class="fa fa-upload"></span> Upload</button>
							   
						    </div>
							<div class="form-group">
						        <input type="file" class="form-control" id="file_image" name="file_image" style="display: none;"/>
						    </div>
						    
						    <div class="text-center" id="preview_profile"></div>
						    
		    				<div id="load_pend"></div>

						<div class="card-footer">
							
						    	<input type="submit" name="submit" id="btn_update_img" class="btn btn-primary btn-block submitBtn" value="Save"/>
						
						</div>

					</form>

				  </div>

				</div>
			</div>
			<div class="modal-footer">
				
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-gray">Cancel</a>
			</div>

		</div>
	</div>
</div>

<!-- MANAGE bank_details -->

<div class="modal fade" role="dialog" id="bank_details">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" >
				<h3 id="header_bank"><span class="fa fa-money-check"></span> Bank Account</h3>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
				    <table class="table table-bordered table-hover">
				      <thead class="table-dark">
				      	<tr>
				      		<td>Bank Name</td>
				      		<td>Account Name</td>
				      		<td>Account Number</td>
				      	</tr>
				      </thead>
				      <tbody>
				      	<?php  

								$q = mysqli_query($con, 'SELECT * from tbl_company_bank_account where is_active = 1');
								$num=0;
								while ($r = mysqli_fetch_assoc($q)) { $num ++; ?>
										
									<tr>
							      		<td><?php echo $num .'. '. $r['bank_name'] ?></td>
							      		<td><?php echo $r['account_name'] ?></td>
							      		<td><?php echo $r['account_number'] ?></td>
							      	</tr>

								<?php }

							?>

				      	
				      </tbody>
				  </table>
				</div>
			</div>
			<div class="modal-footer">
				
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-danger">Close</a>
			</div>

		</div>
	</div>
</div>