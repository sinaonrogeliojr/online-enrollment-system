<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}

include_once('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$default_color = ' w3-'.$row['default_color'].' ';
		$school_name = ''.$row['company_name1'].'';
		$acronym = ''.$row['acronym'].'';
		$with_payment = ''.$row['with_payment'].'';

	}

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $school_name;  ?></title>
	<?php include_once('links.php'); ?>
</head>

<style type="text/css">
	body {

	  background-color: #f2f2f2;
	}

	.display-none{
		display: none;
	}

	.card-prof{
	  font-family: "Candara", sans-serif;
	  overflow: hidden;
	  background: #fff;
	  border-radius: 10px;
	  box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
	  display: flex;
	  flex-direction: column;
	}

	.card-prof-image img{
	  width: 100%;
	  height: 200px;
	  border-top-left-radius: 10px;
	  border-top-right-radius: 10px;
	  object-fit: cover;
	}

	.profile-image img{
	  z-index: 1;
	  width: 180px;
	  height: 180px;
	  position: relative;
	  margin-top: -100px;
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  border-radius: 100px;
	  border: 10px solid #fff;
	  transition-duration: 0.4s;
	  transition-property: transform;
	}

	.profile-image img:hover{
	  transform: scale(1.1);
	}

	.card-prof-content h5{
	  font-size: 17px;
	  text-align: center;
	  margin: 0;
	}

	.card-prof-content p{
	  font-size: 17px;
	  font-weight: bold;
	  text-align: center;
	  padding: 0 20px 5px 20px;
	}

	#navis {
		display: none;
	}

	#con-home{
		align-items: center;
	}

	.navi-buttons{
		height: 40px;
		width: 40px;
	}

</style>

<!-- <script>
	setInterval(function(){get_up_pics();},2000);
</script> -->

<body onload="load_pics(); load_stud_accounts(); load_accounts_payments(); load_student_info_client(); load_pass();">
<?php include_once("nav.php");?>
 <br>
<div class="container-fluid" style="margin-top: 110px;">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6" id="con-home">
			<!--Profile card start-->
			<div class="card-prof" id="home">
			 	<div class="card-prof-image">
					<img src="../img/default_cp.png" alt="">
			 	</div>
			  	<div class="profile-image">
			  	<span id=my_pic data-toggle="modal" data-target="#manage_account" title="Manage Profile"></span>
			   		<!--  <img src="../img/default_prof.png" alt=""> -->
			 	</div>
			  	<div class="card-prof-content">
			    	<h5><b><?php echo strtoupper($_SESSION['fullname']); ?></b></h5><br/> 
				    <p>
				    	<?php echo strtoupper($_SESSION['grade_name']); ?> <br/>
				    </p>
			  	</div>
			  	<div class="dropdown-divider"></div>
			  	<ul class="nav nav-pills nav-justified" style="font-weight: bolder; margin-bottom: 15px;">
				 	<li class="nav-item">
						<a class="nav-link" data-toggle="pill" href="#dashboard" title="Dashboard" onclick="show_hide_navis('SHOW','dashb')"><img class="img-fluid navi-buttons" src="../img/dashboard.png"></span> </a>
				  	</li>

				  	<?php 

				  		if($with_payment == 'YES' AND $_SESSION["category"] == 'Parent'){ ?>

				  			<li class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#accounts" title="Accounts" onclick="show_hide_navis('SHOW','accnts')"><img class="img-fluid navi-buttons" src="../img/accounts.png"> </a>
							</li>
						  	<li class="nav-item">
						    	<a class="nav-link" data-toggle="pill" href="#payments" title="Payments" onclick="show_hide_navis('SHOW','pays')"><img class="img-fluid navi-buttons" src="../img/payment.png"> </a>
						  	</li>

				  		<?php }?>

				  	<li class="nav-item">
				    	<a class="nav-link" href="#logout_me" data-toggle="modal" title="Logout">
				    	<img class="img-fluid navi-buttons" src="../img/logout.png"> </a>
				  	</li>
				</ul>
			</div>
			<!--Profile card end-->
		</div>
		<div class="col-sm-3"></div>

		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="card" id="navis">

			 	<div class="card-body">
			  		<ul class="nav nav-pills nav-justified" style="font-weight: bolder;  margin-bottom: 15px;">
			  			<li class="nav-item">
					    	<a class="nav-link" data-toggle="pill" href="" id="homes" onclick="show_hide_navis('HIDE')">
					    	<img class="img-fluid navi-buttons" src="../img/home.png"> </a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" data-toggle="pill" href="#dashboard" id="dashb">
					    	<img class="img-fluid navi-buttons" src="../img/dashboard.png"> </a>
					  	</li>

					  		<?php 

				  		if($with_payment == 'YES' AND $_SESSION["category"] == 'Parent'){ ?>

				  			<li class="nav-item">
					    	<a class="nav-link" data-toggle="pill" href="#accounts" id="accnts">
					    	<img class="img-fluid navi-buttons" src="../img/accounts.png"> </a>
						  	</li>
						  	<li class="nav-item">
						    	<a class="nav-link" data-toggle="pill" href="#payments" id="pays">
						    	<img class="img-fluid navi-buttons" src="../img/payment.png"> </a>
						  	</li>

				  		<?php }?>
					</ul>
					<br/>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane fade" id="dashboard">
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label><h3><b>Dashboard</b></h3></label>
									<br/>
									<div id="load_student_info" class="border border-secondary rounded"  style="padding: 8px;">
										
									</div>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

						<div class="tab-pane fade" id="accounts">
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label><h3><b>List of Accounts</b></h3></label>
									<br/>
									<div class="table-responsive">   
										<table class="table table-bordered table-hover" id="tbl_stud_accnts"></table>
									</div>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

						<div class="tab-pane fade" id="payments">
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label><h3><b>List of Payments</b></h3></label>
									<button class="btn btn-info float-right" data-toggle="modal" data-target="#bank_details">
										<span class="fa fa-money-check"></span> <?php echo $acronym; ?> BANK DETAILS
									</button>
									<br/>
									<div class="table-responsive">   
										<table class="table table-bordered table-hover" id="tbl_accnts_payments"></table>
									</div>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<br>
</div>

</body>

<?php include_once("scripts.php");?>
<?php include_once("modals.php");?>

<script type="text/javascript">
	$(document).ready(function(e){
 
	    $("#upload_receipt").on('submit', function(e){
	        e.preventDefault();
	        var a = $("#el").val();

	        var pid = $("#pid_pic").val();

	        var data = new FormData(this)
	        console.log(pid + " " + data);

	        	$.ajax({
	            type: 'POST',
	            url: 'upload_file.php',
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
	            beforeSend: function(){
	               $('.submitBtn').attr("disabled","disabled");
	               if (pid.value == "")
						{
							$("#load_pend").prepend('<div class="well w3-blue">Loading...</div>');
						}
						else
						{
							$("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
						}
	            },
	            success: function(msg){
	            	//alert(msg);

	               if(msg == 1){
	                  
	                  $("#pid_pic").val('');
							$('#doc_type').val('');
							$('#f_name').val('');
							$("#load_pend").html('');
							$("#preview_file").html('');
	                  
	                  $("#btn_attach").addClass('disabled');
	                  $("#btn_pays").removeAttr("disabled");
		               $(".submitBtn").removeAttr("disabled");
		               //$('#fupForm').css("opacity","");

                    	setTimeout(function(){   
				        	$('.statusMsg').html('');
				        		showToast.show('Receipt Succefully Uploaded!',3000);
					    	},2000);
	               }
	               else{

	                  setTimeout(function(){   
					        	$('.statusMsg').html('');
					        	showToast.show('Error in Uploading Receipt !',3000);
					    	},2000);
	               }
	            }
	        });

	    });
	    
	   //file type validation
	   $("#receipt1").change(function() {
	      var file = this.files[0];
	      var imagefile = file.type;
	      var form_data = new FormData();
	      $('#doc_type').val(imagefile.trim(""));
	      var match= ["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","image/jpeg","image/png","image/jpg", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.ms-powerpoint", "text/plain"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6]) || (imagefile==match[7]) || (imagefile==match[8]) || (imagefile==match[9]) || (imagefile==match[10]))){
	            alert('INVALID FILE!');
	            $("#receipt1").val('');
	            return false;
	        }
	        else{

				form_data.append("file", document.getElementById('receipt1').files[0]);

	        	$.ajax({
			      url:"upload_file_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			      	$('#preview_file').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data){
			      	//alert(data);

			       	if(data == 404){

						swal("Warning", "Song Already Exists.", "warning");
						$('#preview_file').html("<label class='text-danger col-sm-12'>File Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");
				    }
				    else
				    {
				       	// $('#btn_remove_file').addClass('display-none');
				       	$('#file_update').html('');
				        $('#preview_file').html(data);
				        var file_name = $('#file_name').val();
				        $('#f_name').val(file_name);
				        $("#btn_attach").removeClass('disabled')
				    }	
			      }
			  });
	      }
	   });
	});
</script>
</html>