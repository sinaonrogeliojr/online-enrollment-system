<?php 
	
	include('../config.php');

	$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$company_name = '<b class="img-responsive" style="font-size:25px;"> '.$row['company_name1'].' </b>';
		$address = '<small style="font-size:30px;"> '.$row['address'].' </small>';
		$default_color = ' w3-'.$row['default_color'].' ';

		$header = '<img src="data:image/jpeg;base64,'.base64_encode($row['header_logo']).'" class="rounded img-fluid"/>';

		$header_bg = ''.$row['header_bg'].'';

	}

?>

<nav class="navbar navbar-default fixed-top w3-card-2" style="background-color: <?php echo $header_bg; ?>;">
	<a class="navbar-brand" href="index.php">
		<?php echo $header; ?>
		<!-- <img src="img/kiddie toes header.png" class="rounded img-fluid"> -->
	</a>
</nav>
