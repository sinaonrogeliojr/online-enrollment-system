<?php 
include_once("../config.php");
include_once("classes.php");
$action = mysqli_real_escape_string($con,$_POST['action']);

switch ($action) {

	case 'load_pass':
		load_pass($con);
	break;

	case 'load_pic':
		$pic = $_SESSION['mypic'];
		load_pic($con,$pic);
	break;

	case 'check_old_pass':
		$old_pass = mysqli_real_escape_string($con,$_POST['old_pass']);
		check_old_pass($con,$old_pass);
	break;

	case 'update_password':
		$old_pass = mysqli_real_escape_string($con,$_POST['old_pass']);
		$retype_pass = mysqli_real_escape_string($con,$_POST['retype_pass']);
		update_password($con,$old_pass,$retype_pass);
	break;
	
	case 'remove_file':
		$file_name = mysqli_real_escape_string($con,$_POST['file_name']);
		remove_file($con,$file_name);
	break;

	case 'load_stud_accnts':
		load_student_accounts($con);
	break;

	case 'load_stud_info_client':
		load_student_info($con);
	break;
	
	//>>> Payments <<<//
	case 'load_accnts_payments':
		load_accnts_payments($con);
	break;

	case 'load_payment_details':
		$pid = $amt_due = mysqli_real_escape_string($con,$_POST['payment_id']);
		load_payment_details($con, $pid);
	break;

	case 'add_payments':

		$amt_due = mysqli_real_escape_string($con,$_POST['amt_due']);
		$amt = mysqli_real_escape_string($con,$_POST['amount']);
		$mop = mysqli_real_escape_string($con,$_POST['mop']);
		$bank = mysqli_real_escape_string($con,$_POST['bank']);
		$acct_name = mysqli_real_escape_string($con,$_POST['accnt_name']);
		$acct_no = mysqli_real_escape_string($con,$_POST['accnt_no']);
		$rec_bank = mysqli_real_escape_string($con,$_POST['receipt_bank']);
		$bank = mysqli_real_escape_string($con,$_POST['bank']);
		$sn = mysqli_real_escape_string($con,$_POST['sender_name']);
		$cno = mysqli_real_escape_string($con,$_POST['cno']);
		$add = mysqli_real_escape_string($con,$_POST['address']);
		$ref_no = mysqli_real_escape_string($con,$_POST['ref_no']);
		$rec_rmt = mysqli_real_escape_string($con,$_POST['recpt_rmt']);
		$pid = mysqli_real_escape_string($con,$_POST['pid']);

		add_sf_payment($con, $pid, $amt_due, $amt, $mop, $bank, $acct_name, $acct_no, $rec_bank, $sn, $cno, $add, $ref_no, $rec_rmt);
	break;
}
?>