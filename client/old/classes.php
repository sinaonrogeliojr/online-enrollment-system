<?php 

session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

function remove_file($con,$file){
	unlink($file);
}

function load_pass($con){

	$id = $_SESSION['login_id'];
	$q = mysqli_query($con, "SELECT * from tbl_user_account where transid = '$id'");

	if (mysqli_num_rows($q)>0) {

			$r = mysqli_fetch_assoc($q);
			$pass = $r['user_pwd'];
		}
	?>

	<input type="password" class="form-control" id="old_pass" required="" value="<?php echo $pass;?>">
	  <div class="input-group-append">
	    <button class="btn btn-outline-secondary" onclick="showpass();">
	    	<span class="fa fa-eye" id="eye"></span><span class="fa fa-eye-slash" id="eye_slash"></span>
	    </button>
	  </div>

	  <script type="text/javascript">
	  		
	  		$(document).ready(function(e) {

	  			$("#eye_slash").css('display','none');

	  			$("#old_pass").keyup(function(e){

		        var old_pass = $("#old_pass").val();

		        if(old_pass != '')
		        {
		           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
		            $.ajax({
				    type:'POST',
				    url:url,
				    data:mydata,
				    cache:false,
				    success:function(data){
			
			    		if(data == 1){

				    		$('#new_pass').focus();
			                $('#old_pass_validator').text(' Your old password is correct.');
			                $('#old_pass_validator').removeClass('text-danger');
				    		$('#old_pass_validator').addClass('text-success');
				    		document.getElementById('btn_up_account').disabled = false;
			    		
				    	}else{

				    		$('#old_pass_validator').text(' Your old password is incorrect.');
				    		$('#old_pass_validator').removeClass('text-success');
				    		$('#old_pass_validator').addClass('text-danger');
				    		document.getElementById('btn_up_account').disabled = true;

				    	}

				    }
				    
				  });

		        } else {
		        	$("#oldpass").focus();      
		        }

		    });

	  		});

	  </script>

<?php }

function check_old_pass($con,$old_pass){

	$userid = $_SESSION['user_name'];
	$sql = mysqli_query($con, "SELECT * from tbl_user_account where user_id = '$userid' and user_pwd = '$old_pass'");

	if(mysqli_num_rows($sql) > 0){
		echo 1;
	}else{
		echo 404;
	}

}

function update_password($con,$old_pass,$retype_pass){

	$userid = $_SESSION['user_name'];
	$sql = mysqli_query($con, "UPDATE tbl_user_account set user_pwd = '$retype_pass' where user_id = '$userid' and user_pwd = '$old_pass'");

	if($sql){
		echo 1;
	}else{
		echo 404;
	}

}

function load_pic($con,$pic){

	if($pic == Null || $pic == ''){

		if($_SESSION['gender'] == 'MALE'){
			echo '<img data-toggle="modal" data-target="#show_mypic" src="../img/male_stud.jpg" class="img-fluid img-thumbnail" width="100"/>';
		}else{
			echo '<img data-toggle="modal" data-target="#show_mypic" src="../img/female_stud.jpg" class="img-fluid img-thumbnail" width="100"/>';
		}
		
	}else{
		// 
		echo '<img data-toggle="modal" data-target="#show_mypic" src="data:image/jpeg;base64,'.base64_encode($pic).'" class="img-fluid img-thumbnail" width="50%"/>';
		
	}

	

}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        return $row['id'];
    }
}

function load_student_accounts($con) {

	$sy = get_school_year_active($con); 
	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con, "SELECT b.`school_year`, a.`balance`, c.`terms`, a.`status`, a.`date_trans` FROM tbl_accounts a LEFT JOIN tbl_school_year b ON a.`school_year` = b.`id` LEFT JOIN tbl_terms c on a.`terms` = c.`id` WHERE a.`student_id` = '$stud_id' AND a.`school_year` = '$sy'");

	if (mysqli_num_rows($sql)) {

?>

	<thead>
		<tr class="table-default">
	        <th>School Year</th>
	        <th>Balance</th>
	        <th>Terms</th>
	        <!-- <th>Status</th> -->
	        <th>Last Transaction</th>
	    </tr>
	</thead>

	<tbody>

<?php while ($row = mysqli_fetch_assoc($sql)) { ?>
		<tr>
			<td><?php echo $row["school_year"]?></td>
			<td><?php echo $row["balance"]?></td>
			<td><?php echo $row["terms"]?></td>
			<!-- <td><?php //echo $row["status"]?></td> -->
			<td><?php echo $row["date_trans"]?></td>
		</tr>
<?php } ?>
	</tbody>

<?php
	}	
}

function load_student_info($con){

	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con,"SELECT t1.*,t2.grade as grade_name FROM tbl_students t1 LEFT JOIN tbl_grade t2 ON t1.`grade` = t2.`ID` where t1.`student_id` = '$stud_id'");

    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);

        $name = $row['lastname'].', '.$row['firstname'].' '.$row['mi'];

    } 
?>

	<ul class="nav nav-pills nav-justified" style="font-weight: bolder;">
	  <li class="nav-item">
	    <a class="nav-link active" data-toggle="pill" href="#info"><span class="fa fa-registered"></span> Personal Information</a>
	  </li>
	  <li class="nav-item	">
	    <a class="nav-link" data-toggle="pill" href="#fam"><span class="fa fa-home"></span> Family Information</a>
	  </li>
	</ul>
	<br>
	<div class="tab-content">

	  <div class="tab-pane container active" id="info">

	  	<ul class="list-group">
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Name:
				<b><?php echo strtoupper($name); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Grade & Section:
				<b><?php echo strtoupper($row['grade_name']) .' - '. strtoupper($row['section']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Gender:
				<b><?php echo strtoupper($row['gender']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Place of birth:
				<b><?php echo strtoupper($row['place_of_birth']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Birth date:
				<b><?php echo strtoupper($row['birthday']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Age:
				<b><?php echo strtoupper($row['age']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Address:
				<b><?php echo strtoupper($row['address']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Contact:
				<b><?php echo strtoupper($row['contact']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Religion:
				<b><?php echo strtoupper($row['religion']); ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Citizenship:
				<b><?php echo strtoupper($row['citizenship']); ?></b>
			</li>


		</ul>

	  </div>
	  <div class="tab-pane container" id="fam">

	  	<?php  

	  		$father= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Father' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($father)>0) {
		        $f = mysqli_fetch_assoc($father);

		        $fname = $f['lastname'].', '.$f['firstname'].' '.$f['mi'];
		        ?>

		        <ul class="list-group">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>FATHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($fname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($f['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($f['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($f['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{

		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-info'>
						<b>FATHER'S INFORMATION</b>
					</li> <br> <span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>

	  	<?php  

	  		$mother= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Mother' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($mother)>0) {
		        $m = mysqli_fetch_assoc($mother);

		        $mname = $m['lastname'].', '.$m['firstname'].' '.$m['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>MOTHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($mname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($m['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($m['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($m['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-info'>
						<b>MOTHER'S INFORMATION</b>
					</li><br><span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>
	  	<?php  

	  		$guardian= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Guardian' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($guardian)>0) {
		        $g = mysqli_fetch_assoc($guardian);

		        $gname = $g['lastname'].', '.$g['firstname'].' '.$g['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-info">
						<b>GUARDIAN'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($gname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($g['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($g['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($g['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-info'>
						<b>GUARDIAN'S INFORMATION</b>
					</li><br><span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>
	  	
		  	<li class="list-group-item d-flex justify-content-between align-items-center">
				Siblings:
				<b><?php echo strtoupper($row['siblings']); ?></b>
			</li>

	  </div>

	</div>

<?php }


//>>> Payments <<<//

function load_accnts_payments($con) {

	$sy = get_school_year_active($con); 
	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con, "SELECT a.`Id`, b.`school_year`, a.`date_due`, a.`amount_due`, a.`due_payment`, a.`due_balance`, a.`balance`, a.`date_trans`, a.`confirmation_status`, a.`payment_status`, a.`file`, a.`file_type` FROM tbl_payment_transactions a LEFT JOIN tbl_school_year b ON a.`school_year` = b.`id` WHERE a.`student_id` = '$stud_id' AND a.`school_year` = '$sy'");

	if (mysqli_num_rows($sql)) {

?>
		<thead>
			<tr class="table-default">
				
				<th>Date Transaction</th>
				<th>School Year</th>
				<th>Payment Details</th>
		        <th>Payment Status</th>
		        <th>Confirmation Status</th>
		        <th>Action</th>
		    </tr>
		</thead>

		<tbody>

	<?php while ($row = mysqli_fetch_assoc($sql)) { ?>
		
		<tr>
			<td><?php echo $row["date_trans"]?></td>
			<td><?php echo $row["school_year"]?></td>
			<td>
				<center>
					<button class="btn btn-info btn-sm" title="VIEW PAYMENT DETAILS" onclick="view_payment_details('<?php echo $row['Id']; ?>')">
						<span class="fa fa-eye"></span> View Details
					</button>
				</center>
			</td>
			<?php  
				if ($row["payment_status"] == 0) {
					echo '<td><center><span class="badge badge-pill badge-danger">NOT PAID</span></center></td>';
				}
				else if ($row["payment_status"] == 1) {
					echo '<td><center><span class="badge badge-pill badge-warning">PAID W/ DUE BALANCE</span></center></td>';
				}
				else if ($row["payment_status"] == 2) {
					echo '<td><center><span class="badge badge-pill badge-primary">PAID W/ CREDIT</span></center></td>';
				}
				else if ($row["payment_status"] == 3) {
					echo '<td><center><span class="badge badge-pill badge-success">PAID</span></center></td>';
				}
			  
				if ($row["confirmation_status"] == 0) {
					echo '<td><center><span class="badge badge-pill badge-danger">NOT YET CONFIRMED</span></center></td>';
				}
				else {
					echo '<td><center><span class="badge badge-pill badge-success">CONFIRMED</span></center></td>';
				}
			?>
			<td>
				<button class="btn btn-success btn-sm" title="PAY" onclick="view_payment('<?php echo $row['Id']; ?>','<?php echo $row['amount_due']; ?>','<?php echo $row['balance']; ?>')"><span class="fa fa-cash-register"></span> PAY</button>
				<!-- <button class="btn btn-warning btn-sm" title="EDIT" onclick=""><span class="fa fa-edit"></span> EDIT</button>
				<button class="btn btn-danger btn-sm" title="DELETE" onclick=""><span class="fa fa-trash-alt"></span> DELETE</button> -->
			</td>
		</tr>

	<?php } ?>
		</tbody>


<?php		
	}
}

function load_payment_details($con, $pid) {

	$stud_id = $_SESSION["id"];
	$sy = get_school_year_active($con); 

	$sql = mysqli_query($con, "SELECT * FROM tbl_payment_transactions WHERE id='$pid' AND student_id='$stud_id' AND school_year='$sy'");

	if (mysqli_num_rows($sql) > 0) {
		
		while ($rows=mysqli_fetch_assoc($sql)) {

?>
	<ul class="list-group">
		<li class="list-group-item d-flex justify-content-between list-group-item-success">
			<b>PAYMENT DETAILS</b>
			<b><button type="button" class="close" data-dismiss="modal">&times;</button></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Date Due:
			<b><?php echo $rows['date_due']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Bank:
			<b><?php echo $rows['bank']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Account No:
			<b><?php echo $rows['account_no']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Account Name:
			<b><?php echo $rows['account_name']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Reference No:
			<b><?php echo $rows['ref_no']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Amount Due:
			<b><?php echo $rows['amount_due']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Due Payment:
			<b><?php echo $rows['due_payment']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Due Balance:
			<b><?php echo $rows['due_balance']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Receipt:
			<button class="btn btn-primary btn-sm"><span class="fa fa-eye"></span> View</button>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
			Remaining Balance:
			<b><?php echo $rows['balance']; ?></b>
		</li>
	</ul>

<?php
			
		}
	}
}

function total_payment($con,$term_id,$grade_id,$item){
	
    if ($item == 0) {
		$sql = mysqli_query($con, "SELECT sum(amount) as amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id'");
    }
    else if ($item == 1) {
		$sql =  mysqli_query($con, "SELECT amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id' ORDER BY id ASC LIMIT 1");
    }
    else if ($item == 2) {
		$sql =  mysqli_query($con, "SELECT amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id' ORDER BY id DESC LIMIT 1");
    }
	
    if (mysqli_num_rows($sql)>0) {
        
    	$row = mysqli_fetch_assoc($sql);

    	return $row['amount'];
    }
}

function add_sf_payment($con,$pid,$amt_due,$amt,$mop,$bank,$actno,$atn,$rec_bank,$sn,$cno,$add,$ref_no,$rec_rmt){

	$stud_id = $_SESSION["id"];
	$sy = get_school_year_active($con); 
	$date_trans = date('Y-m-d H:i:s');
	
	$comp_bal = ($amt - $amt_due);

	if ($comp_bal > 0) {
		$status = 2;
	}
	else if ($comp_bal == 0) {
		$status = 3;
	}
	else {
		$status = 1;
	}


	$sql = mysqli_query($con, "UPDATE tbl_payment_transactions SET due_payment='$amt', due_balance='$comp_bal', mop='1', bank='$bank', account_no='$actno', account_name='$atn', ref_no='$ref_no', file='', file_type='', payment_status='$status' WHERE student_id='$stud_id' AND school_year='$sy' AND id='$pid'");

	// if ($mop == 1) {
	// }
	// else{
	// 	$sql = mysqli_query($con, "UPDATE tbl_payment_transactions SET due_payment='$amt', due_balance='$comp_bal', mop='$mop', account_name='$sn', contact_no='$cno', address='$add', ref_no='$ref_no', payment_status='$status' WHERE student_id='$stud_id' AND school_year='$sy' AND id='$pid'");
	// }

	// $get_amt_ammort = total_payment($con, 4, 2)
	
	// $sql1 = mysqli_query($con, "INSERT INTO tbl_payment_transactions VALUES (Id,'$sy','','','0','0','','','','','','','','','','','','0','0')")

	if ($sql) {
		echo 1;
	}
}



?>

