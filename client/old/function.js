var url = 'function.php';

function load_pass(){

	var mydata = 'action=load_pass';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#show_pass").html(data);
		}
	});

}

function showpass() {

    var x = document.getElementById("old_pass");

    if (x.type === "password") {
        x.type = "text";
        $("#eye").css('display','none');
        $("#eye_slash").css('display','block');

    } else {

        x.type = "password";
        $("#eye").css('display','block');
        $("#eye_slash").css('display','none');

    }
}

function show_hide_navis($option, $id) {

	if ($option == "SHOW") {
		$("#home").hide("fade");
		$("#navis").show("fade");

		$("#" + $id + "").addClass("active");
		$("#homes").removeClass("active");
	}
	else {
		window.location='index.php';
	}
}

function get_up_pics(){

	var mydata = 'action=load_pic';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#my_pic").html(data);
		}
	});
}

function load_pics(){
	var mydata = 'action=load_pic';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#my_pic").html('<center><img src="../img/flat.gif" width="50"></center>');
			$("#my_pic2").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#my_pic").html(data);
			$("#my_pic2").html(data);
		}
	});
}

function load_stud_accounts() {	

	var mydata = 'action=load_stud_accnts';
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_stud_accnts").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#tbl_stud_accnts").html(data);
		}
	});
}

function load_student_info_client() {
	var mydata = 'action=load_stud_info_client';
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#load_student_info").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#load_student_info").html(data);
		}
	});
}

function update_password(){

	var old_pass = $('#old_pass').val();
	var retype_pass = $('#retype_pass').val();

	var mydata = 'action=update_password' + '&old_pass=' + old_pass + '&retype_pass=' + retype_pass;

	if(retype_pass == ''){

	$('#retype_pass').focus();

	}else{

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_up_account").html('Saving data....');
				document.getElementById("btn_up_account").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Password Successfully Updated.","success");
					$('#manage_account').modal('hide');
					document.getElementById("btn_up_account").disabled = false;
					$("#btn_up_account").html('Save');
					$('#old_pass').val('');
					$('#new_pass').val('');
					$('#retype_pass').val('');
					load_pass();
				}
				else
				{
					swal("Error","Error on updating password.","error");
				}
			}
			
		});
	
	}

	
}

//>>>> Payment <<<<

// function show_mop() {

// 	var mop = $("#mop").val();

// 	if (mop == 1) {
// 		$("#bt").show("fade");
// 		$("#rmt").hide("fade");
// 	}
// 	else if (mop == 2) {
// 		$("#rmt").show("fade");
// 		$("#bt").hide("fade");
// 	}
// 	else{
// 		$("#bt").hide("fade");
// 		$("#rmt").hide("fade");
// 	}
// }

function view_payment($id, $amt_due,$rem_bal) {
	
	$("#payment_id").val($id);
	$("#pid_pic").val($id);
	$("#p_amt_due").html("₱ " + $amt_due);
	$("#p_amt_due1").html($amt_due);
	$("#p_rem_bal").html("₱ " + $rem_bal);

	$("#payment").modal('show');
}

function view_payment_details($pid) {

	var mydata = 'action=load_payment_details' + '&payment_id=' + $pid;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#view_payment_details").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			//alert(data);
			$("#view_payment_details").html(data);
			$("#payment_details").modal('show');
		}
	});	
}

function load_accounts_payments() {	

	var mydata = 'action=load_accnts_payments';
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_accnts_payments").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#tbl_accnts_payments").html(data);
		}
	});
}

function view_receipt_pic() {
	alert("dasdas");
	$("#view_receipt").modal('show');
}

function add_payment() {

	var pid = $("#payment_id").val();
	var due_amt = $("#p_amt_due1").text();
	var amt = $("#amt").val();
	var mop = $("#mop").val();
	var bank = $("#bank").val();
	var accnt_no= $("#acct_no").val();
	var accnt_name = $("#acct_name").val();
	var recpt_bank = $("#receipt1").val();
	var sn = $("#sender_name").val();
	var cno = $("#cno").val();
	var address = $("#address").val();
	var ref_no = $("#ref_no").val();
	var recpt_rmt = $("#receipt2").val();

	var mydata = 'action=add_payments' + '&amount=' + amt + '&mop=' + mop + '&bank=' + bank + '&accnt_no=' + accnt_no + 
					 '&accnt_name=' + accnt_name +  '&receipt_bank=' + recpt_bank + '&sender_name=' + sn + '&cno=' + cno +
					 '&address=' + address + '&ref_no=' + ref_no + '&recpt_rmt=' + recpt_rmt + '&amt_due=' + due_amt + '&pid=' + pid; 

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_accnts_payments").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			//alert(data);
			if (data == 1) 
			{
				$("#payment").modal('toggle');
				$("#amt").val("");
				$("#mop").val("");
				$("#bank").val("");
				$("#acct_no").val("");
				$("#acct_name").val("");
				$("#receipt1").val("");
				$("#sender_name").val("");
				$("#cno").val("");
				$("#address").val("");
				$("#ref_no").val("");

				load_accounts_payments();
				swal("SUCCESS", "Succesfully pay your due. Please wait to confirm your payment.", "info");
			}
			else
			{
				$("#payment").modal('toggle');
				$("#amt").val("");
				$("#mop").val("");
				$("#bank").val("");
				$("#acct_no").val("");
				$("#acct_name").val("");
				$("#receipt1").val("");
				$("#sender_name").val("");
				$("#cno").val("");
				$("#address").val("");
				$("#ref_no").val("");

				swal("Oopps", "Something Went Wrong", "warning");
			}
			
		}
	});
}

function del_temp_file_receipt(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;
    
   swal({
      title: "Are you sure ?",
      text: "You want to delete this post ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete",
      closeOnConfirm: true
   },
   function(isConfirm){
   	if (isConfirm) 
   	{
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	//alert(data);

               if (data != "") 
               {
               	showToast.show("File has been removed!",1000);
               	$('#preview_file').html("");
               	$("#btn_attach").addClass('disabled')
               }
               else
               { 
              		alert(data);
               }  
            }
        });
   	} 
   	else 
   	{

   	}
   });
}

$(document).ready(function(e) {

	
	//$("#eye_slash").css('display','none');
	$("#old_pass").keyup(function(e){

        var old_pass = $("#old_pass").val();

        if(old_pass != '')
        {
           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
            $.ajax({
		    type:'POST',
		    url:url,
		    data:mydata,
		    cache:false,
		    success:function(data){
	
	    		if(data == 1){

		    		$('#new_pass').focus();
	                $('#old_pass_validator').text(' Your old password is correct.');
	                $('#old_pass_validator').removeClass('text-danger');
		    		$('#old_pass_validator').addClass('text-success');
		    		document.getElementById('btn_up_account').disabled = false;
	    		
		    	}else{

		    		$('#old_pass_validator').text(' Your old password is incorrect.');
		    		$('#old_pass_validator').removeClass('text-success');
		    		$('#old_pass_validator').addClass('text-danger');
		    		document.getElementById('btn_up_account').disabled = true;

		    	}

		    }
		    
		  });

        } else {
        	$("#oldpass").focus();      
        }

    });

	$("#retype_pass").keyup(function(e){

        var new_pass = $("#new_pass").val();
        var retype_pass = $("#retype_pass").val();

        if(new_pass === retype_pass)
        {
        	$('#retype_pass_validator').text('');
        	document.getElementById('btn_up_account').disabled = false;
        }else {

        	document.getElementById('btn_up_account').disabled = true;
        	$('#btn_update_account').attr('disabled','disabled'); 
        	$('#retype_pass_validator').text(' Password is not equal.');
		    $('#retype_pass_validator').addClass('text-danger');
 
        }

    });

});

$(document).ready(function(e){

	    $("#image_form").on('submit', function(e){
	        e.preventDefault();
	        var a = $("#el").val();

	        	$.ajax({
	            type: 'POST',
	            url: 'upload_user_image.php',
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
	            beforeSend: function(){
	                $('.submitBtn').attr("disabled","disabled");
	                $("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
	            },
	            success: function(msg){
	                if(msg == 1){
	                    document.getElementById('btn_update_img').disabled = false;
	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('Logo Succefully Updated!',3000);
					        load_pics();
					    },1000);
					    $('#tbl_logo').html('Updating image...');
					    $('.hide_logo').css('display','block');
	                    //display_documents();
						$("#load_pend").html('');
						$("#preview_profile").html('');
	                }
	                else{
	                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
	                }
	                $('#image_form').css("opacity","");
	                $(".submitBtn").removeAttr("disabled");
	            }
	        });

	    });
	    
	    //file type validation
	    $("#file_image").change(function() {
	        var file = this.files[0];
	        var imagefile = file.type;
	        var form_data = new FormData();
	        //alert(imagefile);
	        var match= ["image/png","image/jpg","image/bmp","image/jpeg","image/gif"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]))){
	           swal("Warning", "Invalid file", "warning");
	            $("#file_image").val('');
	            return false;
	        }else{

				form_data.append("file_image", document.getElementById('file_image').files[0]);

	        	$.ajax({
			      url:"upload_user_image_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			        $('#preview_profile').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data)
			      {
			       if(data == 404){

					swal("Warning", "Image is too big atleast 1mb.", "warning");
					 $('.hide_logo').css('display','block');
					 $('#preview_profile').html('');
			       }else{
			       	  $('.hide_logo').css('display','none');
			       	  // $('#btn_remove_file').addClass('display-none');
			       	  $('#file_update').html('');
			          $('#preview_profile').html(data);
			          var file_name = $('#file_name').val();
			          document.getElementById('btn_update_img').disabled = false;

			       }	
			      
			      }
			      });

	        }
	    });
});

function del_profile_pic(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;

        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	//alert(data);

               if (data != "") 
               {
               	showToast.show("Image has been removed!",1000);
               	$('.hide_logo').css('display','block');
               	$('#preview_profile').html('');
               //load_pics();
               }
               else
               { 
              		alert(data);
               }  
            }
        });

}