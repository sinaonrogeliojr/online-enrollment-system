<?php 
include_once("../config.php");
include_once("classes.php");
$action = mysqli_real_escape_string($con,$_POST['action']);

switch ($action) {

	//edit stduent

	case 'load_edit_button':
		load_edit_button($con);
	break;

	case 'load_fam':
		$student_id = mysqli_real_escape_string($con,$_POST['student_id']);
		load_fam($con,$student_id);
	break;
	
	case 'load_supporting_files':
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		load_supporting_files($con,$student_id);
	break;

	case 'delete_doc':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		$path = mysqli_real_escape_string($con,$_POST['path']);
		delete_doc($con,$id,$path);
	break;

	case 'save_member':
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		$member = mysqli_real_escape_string($con,$_POST['member']);
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		save_member($con,$grade_id,$member,$student_id);
	break;

	case 'remove_member':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		remove_member($con,$id);
	break;

	case 'load_household_members':
		$student_id = mysqli_real_escape_string($con,$_POST['s_id']);
		$grade_id = mysqli_real_escape_string($con,$_POST['grade_id_h']);
		load_household_members($con,$student_id,$grade_id);
	break;

	case 'update_student_info':

		$s_id = mysqli_real_escape_string($con,$_POST['s_id']);
		$category = mysqli_real_escape_string($con,$_POST['category']);
		$lname = mysqli_real_escape_string($con,$_POST['lname']);
		$fname = mysqli_real_escape_string($con,$_POST['fname']);
		$mname = mysqli_real_escape_string($con,$_POST['mname']);
		$suffix = mysqli_real_escape_string($con,$_POST['suffix']);
		$email = mysqli_real_escape_string($con,$_POST['email']);
		$fb_account = mysqli_real_escape_string($con,$_POST['fb_account']);
		$lrn = mysqli_real_escape_string($con,$_POST['lrn']);
		$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
		$age = mysqli_real_escape_string($con,$_POST['age']);
		$place_of_birth = mysqli_real_escape_string($con,$_POST['place_of_birth']);
		$gender = mysqli_real_escape_string($con,$_POST['gender']);
		$religion = mysqli_real_escape_string($con,$_POST['religion']);
		$citizenship = mysqli_real_escape_string($con,$_POST['citizenship']);
		$mother_tongue = mysqli_real_escape_string($con,$_POST['mother_tongue']);
		$ethnicity_val = mysqli_real_escape_string($con,$_POST['ethnicity_val']);
		$contact = mysqli_real_escape_string($con,$_POST['contact']);
		$address = mysqli_real_escape_string($con,$_POST['address']);

		$lname_f = mysqli_real_escape_string($con,$_POST['lname_f']);
		$fname_f = mysqli_real_escape_string($con,$_POST['fname_f']);
		$mname_f = mysqli_real_escape_string($con,$_POST['mname_f']);
		$contact_f = mysqli_real_escape_string($con,$_POST['contact_f']);
		$occupation_f = mysqli_real_escape_string($con,$_POST['occupation_f']);
		$lname_m = mysqli_real_escape_string($con,$_POST['lname_m']);
		$fname_m = mysqli_real_escape_string($con,$_POST['fname_m']);
		$mname_m = mysqli_real_escape_string($con,$_POST['mname_m']);
		$contact_m = mysqli_real_escape_string($con,$_POST['contact_m']);
		$occupation_m = mysqli_real_escape_string($con,$_POST['occupation_m']);
		$lname_g = mysqli_real_escape_string($con,$_POST['lname_g']);
		$fname_g = mysqli_real_escape_string($con,$_POST['fname_g']);
		$mname_g = mysqli_real_escape_string($con,$_POST['mname_g']);
		$contact_g = mysqli_real_escape_string($con,$_POST['contact_g']);
		$occupation_g = mysqli_real_escape_string($con,$_POST['occupation_g']);
		$relation_g = mysqli_real_escape_string($con,$_POST['relation_g']);
		$siblings = mysqli_real_escape_string($con,$_POST['siblings']);

		$with_medical_condition = mysqli_real_escape_string($con, $_POST['with_medical_condition']);
		$medical_condition_affects = mysqli_real_escape_string($con, $_POST['medical_condition_affects']);
		$medical_condition = mysqli_real_escape_string($con, $_POST['medical_condition']);

		$other_mother_tongue = mysqli_real_escape_string($con, $_POST['other_mother_tongue']);
		$other_ethnicity = mysqli_real_escape_string($con, $_POST['other_ethnicity']);

		$go_to_school_val = mysqli_real_escape_string($con, $_POST['go_to_school_val']);
		$teacher_home_val = mysqli_real_escape_string($con, $_POST['teacher_home_val']);
		$other_th = mysqli_real_escape_string($con, $_POST['other_th']);
		$gadgets_val = mysqli_real_escape_string($con, $_POST['gadgets_val']);
		$other_gadget = mysqli_real_escape_string($con, $_POST['other_gadget']);
		$connected_to_internet_val = mysqli_real_escape_string($con, $_POST['connected_to_internet_val']);
		$internet_val = mysqli_real_escape_string($con, $_POST['internet_val']);
		$other_i = mysqli_real_escape_string($con, $_POST['other_i']);
		$internet_rate = mysqli_real_escape_string($con, $_POST['internet_rate']);
		$learning_dmode = mysqli_real_escape_string($con, $_POST['learning_dmode']);
		$other_p = mysqli_real_escape_string($con, $_POST['other_p']);
		$affects_DE_val = mysqli_real_escape_string($con, $_POST['affects_DE_val']);
		$other_affects = mysqli_real_escape_string($con, $_POST['other_affects']);

		$blended_pmc_val = mysqli_real_escape_string($con, $_POST['blended_pmc_val']);
		$other_pmc = mysqli_real_escape_string($con, $_POST['other_pmc']);
		$suggestions = mysqli_real_escape_string($con, $_POST['suggestions']);

	update_student_info($con,$s_id,$category,$lname,$fname,$mname,$suffix,$email,$fb_account,$lrn,$bdate,$age,$place_of_birth,$gender,$religion,$citizenship,$mother_tongue,$ethnicity_val,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g ,$contact_g,$occupation_g,$relation_g,$siblings,$with_medical_condition,$medical_condition_affects,$medical_condition,$other_mother_tongue,$other_ethnicity,$go_to_school_val,$teacher_home_val,$other_th,$gadgets_val,$other_gadget,$connected_to_internet_val,$internet_val,$other_i,$internet_rate,$learning_dmode,$other_p,$affects_DE_val,$other_affects,$blended_pmc_val,$other_pmc,$suggestions);

	break;

	//edit student
	
	case 'load_pass':
		load_pass($con);
	break;

	case 'load_pic':
		$pic = $_SESSION['mypic'];
		load_pic($con,$pic);
	break;

	case 'check_old_pass':
		$old_pass = mysqli_real_escape_string($con,$_POST['old_pass']);
		check_old_pass($con,$old_pass);
	break;

	case 'update_password':
		$old_pass = mysqli_real_escape_string($con,$_POST['old_pass']);
		$retype_pass = mysqli_real_escape_string($con,$_POST['retype_pass']);
		update_password($con,$old_pass,$retype_pass);
	break;
	
	case 'remove_file':
		$file_name = mysqli_real_escape_string($con,$_POST['file_name']);
		remove_file($con,$file_name);
	break;

	case 'load_stud_accnts':
		load_student_accounts($con);
	break;

	case 'load_stud_info_client':
		$school_year_info = mysqli_real_escape_string($con,$_POST['school_year_info']);
		load_student_info($con,$school_year_info);
	break;
	
	//>>> Payments <<<//
	case 'load_accnts_payments':
		load_accnts_payments($con);
	break;

	case 'load_payment_details':
		$pid = $amt_due = mysqli_real_escape_string($con,$_POST['payment_id']);
		load_payment_details($con, $pid);
	break;

	case 'add_payments':

		$amt_due = mysqli_real_escape_string($con,$_POST['amt_due']);
		$amt = mysqli_real_escape_string($con,$_POST['amount']);
		$mop = mysqli_real_escape_string($con,$_POST['mop']);
		$bank = mysqli_real_escape_string($con,$_POST['bank']);
		$acct_name = mysqli_real_escape_string($con,$_POST['accnt_name']);
		$acct_no = mysqli_real_escape_string($con,$_POST['accnt_no']);
		$rec_bank = mysqli_real_escape_string($con,$_POST['receipt_bank']);
		$bank = mysqli_real_escape_string($con,$_POST['bank']);
		$sn = mysqli_real_escape_string($con,$_POST['sender_name']);
		$cno = mysqli_real_escape_string($con,$_POST['cno']);
		$add = mysqli_real_escape_string($con,$_POST['address']);
		$ref_no = mysqli_real_escape_string($con,$_POST['ref_no']);
		$rec_rmt = mysqli_real_escape_string($con,$_POST['recpt_rmt']);
		$pid = mysqli_real_escape_string($con,$_POST['pid']);

		add_sf_payment($con, $pid, $amt_due, $amt, $mop, $bank, $acct_name, $acct_no, $rec_bank, $sn, $cno, $add, $ref_no, $rec_rmt);
	break;
}
?>