<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}

include_once('../config.php');

$sql = mysqli_query($con, "SELECT * from tbl_company where is_active = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$default_color = ' w3-'.$row['default_color'].' ';
		$school_name = ''.$row['company_name1'].'';
		$acronym = ''.$row['acronym'].'';
		$with_payment = ''.$row['with_payment'].'';

	}

function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade order by orderno asc");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['ID'].'">'.$row['grade'].'</option>';
        }
    }
}

function get_school_year($con){
	
	$id = $_SESSION['id'];

	$sql = mysqli_query($con,"SELECT t1.* FROM tbl_school_year t1
		LEFT JOIN tbl_enrolled_students t2 ON t1.`id` = t2.`school_year`
		WHERE t2.`student_id` = '$id'");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }

}

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $school_name;  ?></title>
	<?php include_once('links.php'); ?>
</head>

<style type="text/css">
	body {

	  background-color: #f2f2f2;
	}

	.display-none{
		display: none;
	}

	.card-prof{
	  font-family: "Candara", sans-serif;
	  overflow: hidden;
	  background: #fff;
	  border-radius: 10px;
	  box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
	  display: flex;
	  flex-direction: column;
	}

	.card-prof-image img{
	  width: 100%;
	  height: 200px;
	  border-top-left-radius: 10px;
	  border-top-right-radius: 10px;
	  object-fit: cover;
	}

	.profile-image img{
	  z-index: 1;
	  width: 180px;
	  height: 180px;
	  position: relative;
	  margin-top: -100px;
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  border-radius: 100px;
	  border: 10px solid #fff;
	  transition-duration: 0.4s;
	  transition-property: transform;
	}

	.profile-image img:hover{
	  transform: scale(1.1);
	}

	.card-prof-content h5{
	  font-size: 17px;
	  text-align: center;
	  margin: 0;
	}

	.card-prof-content p{
	  font-size: 17px;
	  font-weight: bold;
	  text-align: center;
	  padding: 0 20px 5px 20px;
	}

	#navis {
		display: none;
	}

	#con-home{
		align-items: center;
	}

	.navi-buttons{
		height: 40px;
		width: 40px;
	}

</style>

<!-- <script>
	setInterval(function(){get_up_pics();},2000);
</script> -->

<body onload="load_pics(); load_stud_accounts(); load_accounts_payments(); load_student_info_client(); load_pass(); load_edit_button();">
<?php include_once("nav.php");?>
 <br>
<div class="container-fluid" style="margin-top: 110px;">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6" id="con-home">
			<!--Profile card start-->
			<div class="card-prof" id="home">
			 	<div class="card-prof-image">
					<img src="../img/default_cp.png" alt="">
			 	</div>
			  	<div class="profile-image">
			  	<span id=my_pic data-toggle="modal" data-target="#manage_account" title="Manage Profile"></span>
			   		<!--  <img src="../img/default_prof.png" alt=""> -->
			 	</div>
			  	<div class="card-prof-content">
			    	<h5><b><?php echo strtoupper($_SESSION['fullname']); ?></b></h5><br/> 
				    <p>
				    	<?php echo strtoupper($_SESSION['grade_name']); ?> <br/>
				    </p>
			  	</div>
			  	<div class="dropdown-divider"></div>
			  	<ul class="nav nav-pills nav-justified" style="font-weight: bolder; margin-bottom: 15px;">
				 	<li class="nav-item">
						<a class="nav-link" data-toggle="pill" href="#dashboard" title="Dashboard" onclick="show_hide_navis('SHOW','dashb')"><img class="img-fluid navi-buttons" src="../img/dashboard.png"></span> </a>
				  	</li>

				  	<li class="nav-item">
				    	<a class="nav-link" href="#logout_me" data-toggle="modal" title="Logout">
				    	<img class="img-fluid navi-buttons" src="../img/logout.png"> </a>
				  	</li>
				</ul>
			</div>
			<!--Profile card end-->
		</div>
		<div class="col-sm-3"></div>

		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="card" id="navis">

			 	<div class="card-body">
			  		<ul class="nav nav-pills nav-justified" style="font-weight: bolder;  margin-bottom: 15px;">
			  			<li class="nav-item">
					    	<a class="nav-link" data-toggle="pill" href="" id="homes" onclick="show_hide_navis('HIDE')">
					    	<img class="img-fluid navi-buttons" src="../img/home.png"> </a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" data-toggle="pill" href="#dashboard" id="dashb">
					    	<img class="img-fluid navi-buttons" src="../img/dashboard.png"> </a>
					  	</li>
					</ul>
					<br/>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane fade" id="dashboard">
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label><h3><b>Dashboard</b></h3></label>
									<span id="load_edit_button">
										
									</span>
									 <select class="form-control" id="school_year_info" oninput="load_student_info_client();" onchange="load_student_info_client();">
									    <?php echo get_school_year($con); ?>
									  </select>
									<br/>

									<div id="load_student_info" class="border border-secondary rounded"  style="padding: 8px;">
										
									</div>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

						<div class="tab-pane fade" id="accounts">
							<div class="row">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label><h3><b>List of Accounts</b></h3></label>
									<br/>
									<div class="table-responsive">   
										<table class="table table-bordered table-hover" id="tbl_stud_accnts"></table>
									</div>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

					</div>
				</div>
				<br>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<br>
</div>

</body>

<?php include_once("scripts.php");?>
<?php include_once("modals.php");?>
</html>