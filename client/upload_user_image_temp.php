<?php
session_start();    // Increase 'timeout' time<br />
  
  if(!empty($_FILES["file_image"]["type"])){
        $fileName = time().'_'.$_FILES['file_image']['name'];
        $valid_extensions = array("jpeg", "jpg", "png");
        $temporary = explode(".", $_FILES["file_image"]["name"]);
        $file_extension = end($temporary);
        if(($_FILES["file_image"]["size"] < 1000000000) && in_array($file_extension, $valid_extensions)){

            $sourcePath = $_FILES['file_image']['tmp_name'];

            $targetPath = "../files/".$fileName;

            if(move_uploaded_file($sourcePath,$targetPath)){

                $uploadedFile = "../files/".$fileName; ?>

                <?php 
                 if($_FILES["file_image"]["type"] == "image/jpeg" || $_FILES["file_image"]["type"] == "image/jpg" || $_FILES["file_image"]["type"] == "image/png"){ ?>

                  <a href="<?php echo $uploadedFile; ?>" target="_blank"><img src="<?php echo $uploadedFile; ?>" class="img img-fluid img_size img-thumbnail" width='200'></a> 
                  <br>
                  <br>
                  <span class="btn btn-danger text-center" onclick="del_profile_pic('<?php echo $uploadedFile; ?>')">Cancel</span>
                  <input type="hidden" id="file_name" name="file_name" value="<?php echo $uploadedFile; ?>">
                  <hr>
                <?php
                 }
            }
        }else{
          echo '404';
        }
    }
?>