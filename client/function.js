var url = 'function.php';

function load_edit_button(){

	var mydata = 'action=load_edit_button';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_edit_button").html(data);
		}
	});

}

function edit_student_profile(student_id,category,grade_id,lname,fname,mname,suffix,email,fb_account,esc_qvr_number,lrn,bdate,age,place_of_birth,gender,religion,citizenship,mother_tongue,ethnicity,contact,address,siblings,with_medical_condition,medical_condition_affects,medical_condition,go_to_school_val,teacher_partner_home,available_gadgets,connected_to_internet,type_of_internet,internet_rate,learning_delivery_mode,affects_DE,mother_tongue_other,ethnicity_other,teacher_partner_home_other,available_gadgets_other,type_of_internet_other,learning_delivery_mode_other,affects_DE_other,is_blended_pmc,is_blended_pmc_other,suggestions){
	
	//alert(medical_condition);
	$('#header_stud').html(lname +', '+ fname + ' '+ mname);
	//alert(grade_id);
	$('#s_id').val(student_id);
	$('#s_id_doc').val(student_id);
	$('#category').val(category);
	$('#grade_id').val(grade_id);
	$('#lname').val(lname);
	$('#fname').val(fname);
	$('#mname').val(mname);
	$('#suffix').val(suffix);
	$('#email').val(email);

	$('#fb_account').val(fb_account);
	$('#esc_qvr_number').val(esc_qvr_number);
	$('#lrn').val(lrn);
	
	$('#bdate').val(bdate);
	$('#age').val(age);
	$('#place_of_birth').val(place_of_birth);
	$('#gender').val(gender);
	$('#religion').val(religion);
	$('#citizenship').val(citizenship);

	//mother tongue
	$('#mother_tongue').val(mother_tongue);

	var tag = mother_tongue.includes("Tagalog");
	var eng = mother_tongue.includes("English");
	var ilo = mother_tongue.includes("Ilocano");
	var yba = mother_tongue.includes("Ybanag");
	var yo = mother_tongue.includes("Yogad");

	if(tag == true){
		$('#Tagalog').prop('checked', true);
	}else{
		$('#Tagalog').prop('checked', false);
	}

	if(eng == true){
		$('#English').prop('checked', true);
	}else{
		$('#English').prop('checked', false);
	}

	if(ilo == true){
		$('#Ilocano').prop('checked', true);
	}else{
		$('#Ilocano').prop('checked', false);
	}

	if(yba == true){
		$('#Ybanag').prop('checked', true);
	}else{
		$('#Ybanag').prop('checked', false);
	}

	if(yo == true){
		$('#Yogad').prop('checked', true);
	}else{
		$('#Yogad').prop('checked', false);
	}

	if(mother_tongue_other == ''){
		$('#iba_pa').prop('checked', false);
		$('#other_mother_tongue').val('');
	}else{
		$('#iba_pa').prop('checked', true);
		$('#other_mother_tongue').val(mother_tongue_other);
	}

	//mother tongue

	$('#ethnicity_val').val(ethnicity);

	var gad = ethnicity.includes("Gaddang");
	var ifu = ethnicity.includes("Ifugao");
	var iloc = ethnicity.includes("Ilocano");
	var yban = ethnicity.includes("Ybanag");
	var yog = ethnicity.includes("Yogad");

	if(gad == true){
		$('#Gaddang').prop('checked', true);
	}else{
		$('#Gaddang').prop('checked', false);
	}

	if(ifu == true){
		$('#Ifugao').prop('checked', true);
	}else{
		$('#Ifugao').prop('checked', false);
	}

	if(iloc == true){
		$('#Ilocano_e').prop('checked', true);
	}else{
		$('#Ilocano_e').prop('checked', false);
	}

	if(yban == true){
		$('#Ybanag_e').prop('checked', true);
	}else{
		$('#Ybanag_e').prop('checked', false);
	}

	if(yog == true){
		$('#Yogad_e').prop('checked', true);
	}else{
		$('#Yogad_e').prop('checked', false);
	}

	if(ethnicity_other == ''){
		$('#iba_pa_e').prop('checked', false);
		$('#other_ethnicity').val('');
	}else{
		$('#iba_pa_e').prop('checked', true);
		$('#other_ethnicity').val(mother_tongue_other);
	}

	$('#contact').val(contact);
	$('#address').val(address);
	$('#siblings').val(siblings);
	
	$('#with_medical_condition').val(with_medical_condition);
	$('#medical_condition_affects').val(medical_condition_affects);
	$('#medical_condition').val(medical_condition);

	//SURVEY
	$('#go_to_school_val').val(go_to_school_val);

	var g1 = go_to_school_val.includes("Walking");
	var g2 = go_to_school_val.includes("public commute (land/water)");
	var g3 = go_to_school_val.includes("family-owned vehicle");
	var g4 = go_to_school_val.includes("school service");

	if(g1 == true){
		$('#g1').prop('checked', true);
	}else{
		$('#g1').prop('checked', false);
	}

	if(g2 == true){
		$('#g2').prop('checked', true);
	}else{
		$('#g2').prop('checked', false);
	}

	if(g3 == true){
		$('#g3').prop('checked', true);
	}else{
		$('#g3').prop('checked', false);
	}

	if(g4 == true){
		$('#g4').prop('checked', true);
	}else{
		$('#g4').prop('checked', false);
	}

	$('#teacher_home_val').val(teacher_partner_home);

	var t1 = teacher_partner_home.includes("Parents/Guardians");
	var t2 = teacher_partner_home.includes("Elder siblings");
	var t3 = teacher_partner_home.includes("Grandparents");
	var t4 = teacher_partner_home.includes("Extended members of the family");

	if(teacher_partner_home_other == ''){
		$('#iba_pa_th').prop('checked', false);
		$('#other_th').val('');
	}else{
		$('#iba_pa_th').prop('checked', true);
		$('#other_th').val(teacher_partner_home_other);
	}	

	if(t1 == true){
		$('#t1').prop('checked', true);
	}else{
		$('#t1').prop('checked', false);
	}

	if(t2 == true){
		$('#t2').prop('checked', true);
	}else{
		$('#t2').prop('checked', false);
	}

	if(t3 == true){
		$('#t3').prop('checked', true);
	}else{
		$('#t3').prop('checked', false);
	}

	if(t4 == true){
		$('#t4').prop('checked', true);
	}else{
		$('#t4').prop('checked', false);
	}

	$('#gadgets_val').val(available_gadgets);

	var a1 = available_gadgets.includes("Cable Tv");
	var a2 = available_gadgets.includes("Non cable TV");
	var a3 = available_gadgets.includes("Radio");
	var a4 = available_gadgets.includes("Laptop");
	var a5 = available_gadgets.includes("Cellphone");
	var a6 = available_gadgets.includes("Tablet");
	var a7 = available_gadgets.includes("Desktop");

	if(available_gadgets_other == ''){
		$('#iba_pa_g').prop('checked', false);
		$('#other_gadget').val('');
	}else{
		$('#other_gadget').val(available_gadgets_other);
		$('#iba_pa_g').prop('checked', true);
	}

	if(a1 == true){
		$('#a1').prop('checked', true);
	}else{
		$('#a1').prop('checked', false);
	}

	if(a2 == true){
		$('#a2').prop('checked', true);
	}else{
		$('#a2').prop('checked', false);
	}

	if(a3 == true){
		$('#a3').prop('checked', true);
	}else{
		$('#a3').prop('checked', false);
	}

	if(a4 == true){
		$('#a4').prop('checked', true);
	}else{
		$('#a4').prop('checked', false);
	}
	if(a5 == true){
		$('#a5').prop('checked', true);
	}else{
		$('#a5').prop('checked', false);
	}

	if(a6 == true){
		$('#a6').prop('checked', true);
	}else{
		$('#a6').prop('checked', false);
	}

	if(a7 == true){
		$('#a7').prop('checked', true);
	}else{
		$('#a7').prop('checked', false);
	}

	$('#connected_to_internet_val').val(connected_to_internet);

	var c1 = connected_to_internet.includes("YES");
	var c2 = connected_to_internet.includes("NO");

	if(c1 == true){
		$('#c1').prop('checked', true);
	}else{
		$('#c1').prop('checked', false);
	}

	if(c2 == true){
		$('#c2').prop('checked', true);
	}else{
		$('#c2').prop('checked', false);
	}

	$('#internet_val').val(type_of_internet);

	var ia = type_of_internet.includes("No internet connection");
	var ib = type_of_internet.includes("Mobile data (prepaid)");
	var ic = type_of_internet.includes("Mobile data (post paid or plan)");
	var i_d = type_of_internet.includes("Mobile broadband");
	var ie = type_of_internet.includes("Cable internet");
	var i_f = type_of_internet.includes("Fiber internet");

	if(ia == true){
		$('#ia').prop('checked', true);
	}else{
		$('#ia').prop('checked', false);
	}

	if(ib == true){
		$('#ib').prop('checked', true);
	}else{
		$('#ib').prop('checked', false);
	}

	if(ic == true){
		$('#ic').prop('checked', true);
	}else{
		$('#ic').prop('checked', false);
	}

	if(i_d == true){
		$('#i_d').prop('checked', true);
	}else{
		$('#i_d').prop('checked', false);
	}

	if(i_d == true){
		$('#ie').prop('checked', true);
	}else{
		$('#ie').prop('checked', false);
	}

	if(i_d == true){
		$('#i_f').prop('checked', true);
	}else{
		$('#i_f').prop('checked', false);
	}

	if(type_of_internet_other == ''){
		$('#iba_pa_i').prop('checked', false);
		$('#other_i').val('');
	}else{
		$('#other_i').val(type_of_internet_other);
		$('#iba_pa_i').prop('checked', true);
	}

	$('#internet_rate').val(internet_rate);

	$('#learning_dmode').val(learning_delivery_mode);

	var l1 = learning_delivery_mode.includes("Purely modular classes (with modules/lessons for learners, no online classes, parents to guide in lessons)");
	var l2 = learning_delivery_mode.includes("Purely online classes (all classes will be done online)");
	var l3 = learning_delivery_mode.includes("Blended learning (online sessions with modules - learners to attend short online classes and work on their own after)");
	
	if(learning_delivery_mode_other == ''){
		$('#iba_pa_p').prop('checked', false);
		$('#other_p').val('');
	}else{
		$('#other_p').val(learning_delivery_mode_other);
		$('#iba_pa_p').prop('checked', true);
	}

	if(l1 == true){
		$('#l1').prop('checked', true);
	}else{
		$('#l1').prop('checked', false);
	}

	if(l2 == true){
		$('#l2').prop('checked', true);
	}else{
		$('#l2').prop('checked', false);
	}

	if(l3 == true){
		$('#l3').prop('checked', true);
	}else{
		$('#l3').prop('checked', false);
	}

	$('#blended_pmc_val').val(is_blended_pmc);

	var pmc1 = is_blended_pmc.includes("Yes");
	var pmc2 = is_blended_pmc.includes("No");
	var pmc3 = is_blended_pmc.includes("Maybe");

	if(pmc1 == true){
		$('#pmc1').prop('checked', true);
	}else{
		$('#pmc1').prop('checked', false);
	}

	if(pmc2 == true){
		$('#pmc2').prop('checked', true);
	}else{
		$('#pmc2').prop('checked', false);
	}

	if(pmc3 == true){
		$('#pmc3').prop('checked', true);
	}else{
		$('#pmc3').prop('checked', false);
	}

	if(is_blended_pmc_other == '' || is_blended_pmc_other == 'undefined'){
		$('#iba_pa_pmc').prop('checked', false);
		$('#other_pmc').val('');
	}else{
		$('#other_pmc').val(is_blended_pmc_other);
		$('#iba_pa_pmc').prop('checked', true);
	}

	$('#affects_DE_val').val(affects_DE);

	var w1 = affects_DE.includes("Lack of available gadgets/ equipment");
	var w2 = affects_DE.includes("Insufficient load/data allowance");
	var w3 = affects_DE.includes("Unstable mobile/internet connection");
	var w4 = affects_DE.includes("Existing health condition's");
	var w5 = affects_DE.includes("Difficulty in independent learning");
	var w6 = affects_DE.includes("Conflict with other activities(e.g house chores)");
	var w7 = affects_DE.includes("High electrical consumptions");
	var w8 = affects_DE.includes("Distractions(i.e.| social media | noise from community/neighbor)");

	if(w1 == true){
		$('#w1').prop('checked', true);
	}else{
		$('#w1').prop('checked', false);
	}

	if(w2 == true){
		$('#w2').prop('checked', true);
	}else{
		$('#w2').prop('checked', false);
	}

	if(w3 == true){
		$('#w3').prop('checked', true);
	}else{
		$('#w3').prop('checked', false);
	}

	if(w4 == true){
		$('#w4').prop('checked', true);
	}else{
		$('#w4').prop('checked', false);
	}

	if(w5 == true){
		$('#w5').prop('checked', true);
	}else{
		$('#w5').prop('checked', false);
	}

	if(w6 == true){
		$('#w6').prop('checked', true);
	}else{
		$('#w6').prop('checked', false);
	}

	if(w7 == true){
		$('#w7').prop('checked', true);
	}else{
		$('#w7').prop('checked', false);
	}

	if(w8 == true){
		$('#w8').prop('checked', true);
	}else{
		$('#w8').prop('checked', false);
	}

	if(affects_DE_other == ''){
		$('#iba_pa_affects').prop('checked', false);
		$('#other_affects').val('');
	}else{
		$('#other_affects').val(affects_DE_other);
		$('#iba_pa_affects').prop('checked', true);
	}

	$('#suggestions').val(suggestions);

	//SURVEY
	load_fam(student_id);
	load_supporting_files();
	load_household_members();
	$('#manage_account_edit').modal('show');

}

function update_student_info(){

	var s_id = $('#s_id').val();
	var category = $('#category').val();
	var lname = $('#lname').val();      
	var fname = $('#fname').val();      
	var mname = $('#mname').val();      
	var suffix = $('#suffix').val();      
	var email = $('#email').val();      
	var fb_account = $('#fb_account').val();         
	var lrn= $('#lrn').val();      
	var bdate= $('#bdate').val();      
	var age= $('#age').val();      
	var place_of_birth = $('#place_of_birth').val();      
	var gender = $('#gender').val();      
	var religion = $('#religion').val();      
	var citizenship = $('#citizenship').val();      
	var mother_tongue = $('#mother_tongue').val(); 
	var other_mother_tongue = $('#other_mother_tongue').val();
	var other_ethnicity = $('#other_ethnicity').val();     
	var ethnicity_val = $('#ethnicity_val').val();      
	var contact = $('#contact').val();      
	var address = $('#address').val();      

	var lname_f = $('#lname_f').val();  
	var fname_f = $('#fname_f').val();  
	var mname_f = $('#mname_f').val();  
	var contact_f = $('#contact_f').val();  
	var occupation_f = $('#occupation_f').val();  
	var lname_m = $('#lname_m').val();  
	var fname_m = $('#fname_m').val();  
	var mname_m = $('#mname_m').val();  
	var contact_m = $('#contact_m').val();  
	var occupation_m = $('#occupation_m').val();  
	var lname_g = $('#lname_g').val();  
	var fname_g = $('#fname_g').val();  
	var mname_g = $('#mname_g').val();  
	var contact_g = $('#contact_g').val();  
	var occupation_g = $('#occupation_g').val();  
	var relation_g = $('#relation_g').val();  
	var siblings = $('#siblings').val(); 

	var with_medical_condition = $('#with_medical_condition').val();
	var medical_condition_affects = $('#medical_condition_affects').val();
	var medical_condition = $('#medical_condition').val();

	var go_to_school_val = $('#go_to_school_val').val();
	var teacher_home_val = $('#teacher_home_val').val();
	var other_th = $('#other_th').val();
	var gadgets_val = $('#gadgets_val').val();
	var other_gadget = $('#other_gadget').val();
	var connected_to_internet_val = $('#connected_to_internet_val').val();
	var internet_val = $('#internet_val').val();
	var other_i = $('#other_i').val();
	var internet_rate = $('#internet_rate').val();
	var learning_dmode = $('#learning_dmode').val();
	var other_p = $('#other_p').val();
	var affects_DE_val = $('#affects_DE_val').val();
	var other_affects = $('#other_affects').val();

	var blended_pmc_val = $('#blended_pmc_val').val();
	var other_pmc = $('#other_pmc').val();

	var suggestions = $('#suggestions').val();

	var mydata = 'action=update_student_info' + '&s_id=' + s_id + '&category=' + 	category + '&lname=' + 	lname + '&fname=' + fname + '&mname=' + mname + '&suffix=' + suffix + '&email=' + email + '&fb_account=' + fb_account + '&lrn=' + lrn + '&bdate=' + 	bdate + '&age=' + age + '&place_of_birth=' + place_of_birth + '&gender=' + 	gender + '&religion=' + religion + '&citizenship=' + citizenship + '&mother_tongue=' + 	mother_tongue + '&ethnicity_val=' + ethnicity_val + '&contact=' + contact + '&address='+ address + '&lname_f=' + lname_f + '&fname_f=' + fname_f + '&mname_f=' + mname_f + '&contact_f=' + contact_f + '&occupation_f=' + occupation_f + '&lname_m=' + lname_m + '&fname_m=' + fname_m + '&mname_m=' + mname_m + '&contact_m=' + contact_m + '&occupation_m=' + occupation_m + '&lname_g=' + lname_g + '&fname_g=' + fname_g + '&mname_g=' + mname_g + '&contact_g=' + contact_g + '&occupation_g=' + occupation_g + '&relation_g=' + relation_g + '&siblings=' + siblings + '&with_medical_condition=' + with_medical_condition + '&medical_condition_affects=' + medical_condition_affects + '&medical_condition=' + medical_condition + '&other_mother_tongue=' + other_mother_tongue + '&other_ethnicity=' + other_ethnicity + '&go_to_school_val=' + go_to_school_val + '&teacher_home_val=' + teacher_home_val + '&other_th=' + other_th + '&gadgets_val=' + gadgets_val + '&other_gadget=' + other_gadget + '&connected_to_internet_val=' + connected_to_internet_val + '&internet_val=' + internet_val + '&other_i=' + other_i + '&internet_rate=' + internet_rate + '&learning_dmode=' + learning_dmode + '&other_p=' + other_p + '&affects_DE_val=' + affects_DE_val + '&other_affects=' + other_affects + '&blended_pmc_val=' + blended_pmc_val + '&other_pmc=' + other_pmc + '&suggestions=' + suggestions;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 123456) 
	        {
	         	$('#manage_account_edit').modal('hide');
	         	$("#grade_id").val('');
	         	swal("Success","Student Successfully Updated","success");
	         	load_student_info_client();

	        }
	        else{
	          swal("Error","Error on updating data!","error");
	        }
	      }
	});

}

function load_fam(student_id){
	//alert(student_id);
	var mydata = 'action=load_fam' + '&student_id=' + student_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_fam").html(data);
		}
	});	

}

function load_supporting_files(){
	var student_id = $('#s_id').val();
	var mydata = 'action=load_supporting_files' + '&s_id=' + student_id;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#load_files").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#load_files").html(data);
		}
	});
}

function delete_doc(id,path){

	var mydata = 'action=delete_doc' + '&id=' + id + '&path=' + path;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this file?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("file has been removed!",3000);
              	 load_supporting_files();
                }
                else
                {
                    swal("Error","Error on deleting file.","error");
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
}

function save_household(){

	var mydata,grade_id,member,student_id;

	grade_id = $('#grade_id_h').val();
	member = $('#member_value').val();
	student_id = $('#s_id').val();
	mydata = 'action=save_member' + '&grade_id_h=' + grade_id + '&member=' + member + '&s_id=' + student_id;

	$.ajax({
	      type:"POST",
	      url:url,
	      data:mydata,
	      cache:false,
	      success:function(data){
	        console.log(data);
	        if (data == 1) 
	        {
	         	load_household_members();
	         	$('#member_value').val('');

	        }else if(data == 2){
	        	swal("Info","Grade already exists. Please select another grade level.","info");
	        	$('#member_value').val('');
	        }
	        else{
	          swal("Error","Error on adding data!","error");
	        }
	      }
	    });

}

function load_household_members(){

	var student_id = $('#s_id').val();
	var grade_id = $('#grade_id_h').val();
	var mydata = 'action=load_household_members' + '&s_id=' + student_id + '&grade_id_h=' + grade_id;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#load_household_members").html(data);
		}
	});
}

function remove_member(id){

	var mydata = 'action=remove_member' + '&id=' + id;

	$.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 	showToast.show("Data has been removed!",1000);
	              	load_household_members();
                }
                else
                {
                    //alert(data);
                }
            }
        });

}

function get_affects(){

  var chkArray = [];
  
  $("#affects_DE input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#affects_DE_val").val(selected);

}

function iba_pa_affects() {
	  var checkBox = document.getElementById("iba_pa_affects");
  if (checkBox.checked == true){
  	document.getElementById('other_affects').disabled = false;
  } else {
    document.getElementById('other_affects').disabled = true;
    $('#other_affects').val('');
  }
}

function get_gotoschool(){

  var chkArray = [];
  
  $("#go_to_school input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#go_to_school_val").val(selected);

  }

function iba_pa() {
	  var checkBox = document.getElementById("iba_pa");
  if (checkBox.checked == true){
  	//alert(1);
  	document.getElementById('other_mother_tongue').disabled = false
  } else {
  	$('#other_mother_tongue').val('');
    document.getElementById('other_mother_tongue').disabled = true;
    //alert(2);
    
  }
}

function get_style_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#m_tongue input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#mother_tongue").val(selected);
}

function get_ethnicity(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#ethnicity input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;
  //alert(selected)
  $("#ethnicity_val").val(selected);
}

function iba_pa_e() {
	  var checkBox = document.getElementById("iba_pa_e");
  if (checkBox.checked == true){
  	document.getElementById('other_ethnicity').disabled = false;
  } else {
    document.getElementById('other_ethnicity').disabled = true;
    $('#other_ethnicity').val('');
  }
}

function get_is_connected(){

  var chkArray = [];
  
  $("#connected_to_internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#connected_to_internet_val").val(selected);

}

function get_gadgets(){

  var chkArray = [];
  
  $("#gadgets input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#gadgets_val").val(selected);

}

function iba_pa_g() {
	  var checkBox = document.getElementById("iba_pa_g");
  if (checkBox.checked == true){
  	document.getElementById('other_gadget').disabled = false;
  } else {
    document.getElementById('other_gadget').disabled = true;
    $('#other_gadget').val('');
  }
}

function get_ic(){

  var chkArray = [];
  
  $("#internet input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#internet_val").val(selected);

}

function get_delivery_mode(){

  var chkArray = [];
  
  $("#delivery_mode input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#learning_dmode").val(selected);

}

function get_pmc(){

  var chkArray = [];
  
  $("#blended_pmc input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#blended_pmc_val").val(selected);

}

function iba_pa_pmc() {
	  var checkBox = document.getElementById("iba_pa_pmc");
  if (checkBox.checked == true){
  	document.getElementById('other_pmc').disabled = false
  } else {
    document.getElementById('other_pmc').disabled = true
  }
}

function get_preferred_days(){

  var chkArray = [];
  
  $("#preferred_days input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#preferred_days_val").val(selected);
  $('#other_gp').val('');

}

function iba_pa_gp() {
	  var checkBox = document.getElementById("iba_pa_gp");
  if (checkBox.checked == true){
  	$("#preferred_days_val").val('');
  	document.getElementById('other_gp').disabled = false
  } else {
    document.getElementById('other_gp').disabled = true
  }
}

function iba_pa_i() {
	  var checkBox = document.getElementById("iba_pa_i");
  if (checkBox.checked == true){
  	document.getElementById('other_i').disabled = false;
  } else {
    document.getElementById('other_i').disabled = true;
     $('#other_i').val('');
  }
}

function iba_pa_p() {
	  var checkBox = document.getElementById("iba_pa_p");
  if (checkBox.checked == true){
  	document.getElementById('other_p').disabled = false;
  } else {
    document.getElementById('other_p').disabled = true;
     $('#other_p').val('');
  }
}

function get_financial_capability(){

  var chkArray = [];
  
  $("#financial_capability input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#financial_capability_val").val(selected);
  $('#other_fc').val('');

}

function iba_pa_fc() {
	  var checkBox = document.getElementById("iba_pa_fc");
  if (checkBox.checked == true){
  	$("#financial_capability_val").val('');
  	document.getElementById('other_fc').disabled = false
  } else {
    document.getElementById('other_fc').disabled = true
  }
}

function get_teacher_home(){

  var chkArray = [];
  
  $("#teacher_home input:checked").each(function() {
    chkArray.push($(this).val());
  });

  var selected;
  selected = chkArray.join(','+' ') ;
  $("#teacher_home_val").val(selected);

}

function iba_pa_th() {
	  var checkBox = document.getElementById("iba_pa_th");
  if (checkBox.checked == true){
  	document.getElementById('other_th').disabled = false;
  } else {
    document.getElementById('other_th').disabled = true;
    $('#other_th').val('');
  }
}

//edit student

function load_pass(){

	var mydata = 'action=load_pass';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#show_pass").html(data);
		}
	});

}

function showpass() {

    var x = document.getElementById("old_pass");

    if (x.type === "password") {
        x.type = "text";
        $("#eye").css('display','none');
        $("#eye_slash").css('display','block');

    } else {

        x.type = "password";
        $("#eye").css('display','block');
        $("#eye_slash").css('display','none');

    }
}

function show_hide_navis($option, $id) {

	if ($option == "SHOW") {
		$("#home").hide("fade");
		$("#navis").show("fade");

		$("#" + $id + "").addClass("active");
		$("#homes").removeClass("active");
	}
	else {
		window.location='index.php';
	}
}

function get_up_pics(){

	var mydata = 'action=load_pic';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			$("#my_pic").html(data);
		}
	});
}

function load_pics(){
	var mydata = 'action=load_pic';

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#my_pic").html('<center><img src="../img/flat.gif" width="50"></center>');
			$("#my_pic2").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#my_pic").html(data);
			$("#my_pic2").html(data);
		}
	});
}

function load_stud_accounts() {	

	var mydata = 'action=load_stud_accnts';
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_stud_accnts").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#tbl_stud_accnts").html(data);
		}
	});
}

function load_student_info_client() {
	var school_year_info = $('#school_year_info').val();
	var mydata = 'action=load_stud_info_client' + '&school_year_info=' + school_year_info;
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#load_student_info").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#load_student_info").html(data);
		}
	});
}

function update_password(){

	var old_pass = $('#old_pass').val();
	var retype_pass = $('#retype_pass').val();

	var mydata = 'action=update_password' + '&old_pass=' + old_pass + '&retype_pass=' + retype_pass;

	if(retype_pass == ''){

	$('#retype_pass').focus();

	}else{

		$.ajax({
				
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#btn_up_account").html('Saving data....');
				document.getElementById("btn_up_account").disabled = true;
			},
			success:function(data){

				if (data == 1) 
				{
					swal("Success","Password Successfully Updated.","success");
					$('#manage_account').modal('hide');
					document.getElementById("btn_up_account").disabled = false;
					$("#btn_up_account").html('Save');
					$('#old_pass').val('');
					$('#new_pass').val('');
					$('#retype_pass').val('');
					load_pass();
				}
				else
				{
					swal("Error","Error on updating password.","error");
				}
			}
			
		});
	
	}

	
}

//>>>> Payment <<<<

// function show_mop() {

// 	var mop = $("#mop").val();

// 	if (mop == 1) {
// 		$("#bt").show("fade");
// 		$("#rmt").hide("fade");
// 	}
// 	else if (mop == 2) {
// 		$("#rmt").show("fade");
// 		$("#bt").hide("fade");
// 	}
// 	else{
// 		$("#bt").hide("fade");
// 		$("#rmt").hide("fade");
// 	}
// }

function view_payment($id, $amt_due,$rem_bal) {
	
	$("#payment_id").val($id);
	$("#pid_pic").val($id);
	$("#p_amt_due").html("₱ " + $amt_due);
	$("#p_amt_due1").html($amt_due);
	$("#p_rem_bal").html("₱ " + $rem_bal);

	$("#payment").modal('show');
}

function view_payment_details($pid) {

	var mydata = 'action=load_payment_details' + '&payment_id=' + $pid;

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#view_payment_details").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			//alert(data);
			$("#view_payment_details").html(data);
			$("#payment_details").modal('show');
		}
	});	
}

function load_accounts_payments() {	

	var mydata = 'action=load_accnts_payments';
	
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_accnts_payments").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			$("#tbl_accnts_payments").html(data);
		}
	});
}

function view_receipt_pic() {
	alert("dasdas");
	$("#view_receipt").modal('show');
}

function add_payment() {

	var pid = $("#payment_id").val();
	var due_amt = $("#p_amt_due1").text();
	var amt = $("#amt").val();
	var mop = $("#mop").val();
	var bank = $("#bank").val();
	var accnt_no= $("#acct_no").val();
	var accnt_name = $("#acct_name").val();
	var recpt_bank = $("#receipt1").val();
	var sn = $("#sender_name").val();
	var cno = $("#cno").val();
	var address = $("#address").val();
	var ref_no = $("#ref_no").val();
	var recpt_rmt = $("#receipt2").val();

	var mydata = 'action=add_payments' + '&amount=' + amt + '&mop=' + mop + '&bank=' + bank + '&accnt_no=' + accnt_no + 
					 '&accnt_name=' + accnt_name +  '&receipt_bank=' + recpt_bank + '&sender_name=' + sn + '&cno=' + cno +
					 '&address=' + address + '&ref_no=' + ref_no + '&recpt_rmt=' + recpt_rmt + '&amt_due=' + due_amt + '&pid=' + pid; 

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#tbl_accnts_payments").html('<center><img src="../img/flat.gif" width="50"></center>');
		},
		success:function(data){
			//alert(data);
			if (data == 1) 
			{
				$("#payment").modal('toggle');
				$("#amt").val("");
				$("#mop").val("");
				$("#bank").val("");
				$("#acct_no").val("");
				$("#acct_name").val("");
				$("#receipt1").val("");
				$("#sender_name").val("");
				$("#cno").val("");
				$("#address").val("");
				$("#ref_no").val("");

				load_accounts_payments();
				swal("SUCCESS", "Succesfully pay your due. Please wait to confirm your payment.", "info");
			}
			else
			{
				$("#payment").modal('toggle');
				$("#amt").val("");
				$("#mop").val("");
				$("#bank").val("");
				$("#acct_no").val("");
				$("#acct_name").val("");
				$("#receipt1").val("");
				$("#sender_name").val("");
				$("#cno").val("");
				$("#address").val("");
				$("#ref_no").val("");

				swal("Oopps", "Something Went Wrong", "warning");
			}
			
		}
	});
}

function del_temp_file_receipt(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;
    
   swal({
      title: "Are you sure ?",
      text: "You want to delete this post ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete",
      closeOnConfirm: true
   },
   function(isConfirm){
   	if (isConfirm) 
   	{
        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	//alert(data);

               if (data != "") 
               {
               	showToast.show("File has been removed!",1000);
               	$('#preview_file').html("");
               	$("#btn_attach").addClass('disabled')
               }
               else
               { 
              		alert(data);
               }  
            }
        });
   	} 
   	else 
   	{

   	}
   });
}

$(document).ready(function(e) {

	
	//$("#eye_slash").css('display','none');
	$("#old_pass").keyup(function(e){

        var old_pass = $("#old_pass").val();

        if(old_pass != '')
        {
           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
            $.ajax({
		    type:'POST',
		    url:url,
		    data:mydata,
		    cache:false,
		    success:function(data){
	
	    		if(data == 1){

		    		$('#new_pass').focus();
	                $('#old_pass_validator').text(' Your old password is correct.');
	                $('#old_pass_validator').removeClass('text-danger');
		    		$('#old_pass_validator').addClass('text-success');
		    		document.getElementById('btn_up_account').disabled = false;
	    		
		    	}else{

		    		$('#old_pass_validator').text(' Your old password is incorrect.');
		    		$('#old_pass_validator').removeClass('text-success');
		    		$('#old_pass_validator').addClass('text-danger');
		    		document.getElementById('btn_up_account').disabled = true;

		    	}

		    }
		    
		  });

        } else {
        	$("#oldpass").focus();      
        }

    });

	$("#retype_pass").keyup(function(e){

        var new_pass = $("#new_pass").val();
        var retype_pass = $("#retype_pass").val();

        if(new_pass === retype_pass)
        {
        	$('#retype_pass_validator').text('');
        	document.getElementById('btn_up_account').disabled = false;
        }else {

        	document.getElementById('btn_up_account').disabled = true;
        	$('#btn_update_account').attr('disabled','disabled'); 
        	$('#retype_pass_validator').text(' Password is not equal.');
		    $('#retype_pass_validator').addClass('text-danger');
 
        }

    });

});

$(document).ready(function(e){

	    $("#image_form").on('submit', function(e){
	        e.preventDefault();
	        var a = $("#el").val();

	        	$.ajax({
	            type: 'POST',
	            url: 'upload_user_image.php',
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
	            beforeSend: function(){
	                $('.submitBtn').attr("disabled","disabled");
	                $("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
	            },
	            success: function(msg){
	                if(msg == 1){
	                    document.getElementById('btn_update_img').disabled = false;
	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('Logo Succefully Updated!',3000);
					        load_pics();
					    },1000);
					    $('#tbl_logo').html('Updating image...');
					    $('.hide_logo').css('display','block');
	                    //display_documents();
						$("#load_pend").html('');
						$("#preview_profile").html('');
	                }
	                else{
	                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
	                }
	                $('#image_form').css("opacity","");
	                $(".submitBtn").removeAttr("disabled");
	            }
	        });

	    });
	    
	    //file type validation
	    $("#file_image").change(function() {
	        var file = this.files[0];
	        var imagefile = file.type;
	        var form_data = new FormData();
	        //alert(imagefile);
	        var match= ["image/png","image/jpg","image/bmp","image/jpeg","image/gif"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]))){
	           swal("Warning", "Invalid file", "warning");
	            $("#file_image").val('');
	            return false;
	        }else{

				form_data.append("file_image", document.getElementById('file_image').files[0]);

	        	$.ajax({
			      url:"upload_user_image_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			        $('#preview_profile').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data)
			      {
			       if(data == 404){

					swal("Warning", "Image is too big atleast 1mb.", "warning");
					 $('.hide_logo').css('display','block');
					 $('#preview_profile').html('');
			       }else{
			       	  $('.hide_logo').css('display','none');
			       	  // $('#btn_remove_file').addClass('display-none');
			       	  $('#file_update').html('');
			          $('#preview_profile').html(data);
			          var file_name = $('#file_name').val();
			          document.getElementById('btn_update_img').disabled = false;

			       }	
			      
			      }
			      });

	        }
	    });
});

function del_profile_pic(file_name){

	var mydata = 'action=remove_file' + '&file_name=' + file_name;

        $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
            	//alert(data);

               if (data != "") 
               {
               	showToast.show("Image has been removed!",1000);
               	$('.hide_logo').css('display','block');
               	$('#preview_profile').html('');
               //load_pics();
               }
               else
               { 
              		alert(data);
               }  
            }
        });

}