<?php 

session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

//edit student

function delete_doc($con,$id,$path){

	$sql = mysqli_query($con, "DELETE FROM tbl_documents where id = '$id'");
	if ($sql) {
		unlink($path);
		echo 1;
	}

}

function load_edit_button($con){

	$student_id = $_SESSION['id'];

	$sql = mysqli_query($con, "SELECT t1.*,t2.*,t3.grade,t2.is_active AS stat,t4.`section_name` FROM tbl_students t1
			LEFT JOIN tbl_enrolled_students t2 ON t1.`student_id` = t2.`student_id`
			LEFT JOIN tbl_grade t3 ON t1.grade = t3.ID
			LEFT JOIN tbl_sections t4 ON t2.`section_id` = t4.`id`
			WHERE t1.student_id = '$student_id'");

	if(mysqli_num_rows($sql) > 0){

		$row = mysqli_fetch_assoc($sql); ?>

			<button class="btn btn-sm btn-info" onclick="edit_student_profile('<?php echo $row['student_id'] ?>','<?php echo $row['category'] ?>', '<?php echo $row['grade'] ?>','<?php echo $row['lastname'] ?>','<?php echo $row['firstname'] ?>','<?php echo $row['mi'] ?>','<?php echo $row['suffix'] ?>','<?php echo $row['email'] ?>','<?php echo $row['fb_account'] ?>','<?php echo $row['esc_qvr_number'] ?>','<?php echo $row['lrn'] ?>','<?php echo $row['birthday'] ?>','<?php echo $row['age'] ?>','<?php echo $row['place_of_birth'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['religion'] ?>','<?php echo $row['citizenship'] ?>','<?php echo $row['mother_tongue'] ?>','<?php echo $row['ethnicity'] ?>','<?php echo $row['contact'] ?>','<?php echo $row['address'] ?>','<?php echo $row['siblings'] ?>','<?php echo $row['with_medical_condition'] ?>','<?php echo $row['medical_condition_affects'] ?>','<?php echo $row['medical_condition'] ?>', '<?php echo $row['go_to_school']; ?>', '<?php echo $row['teacher_partner_home']; ?>', '<?php echo $row['available_gadgets']; ?>', '<?php echo $row['connected_to_internet']; ?>', '<?php echo $row['type_of_internet']; ?>', '<?php echo $row['internet_rate']; ?>', '<?php echo $row['learning_delivery_mode']; ?>','<?php echo addslashes($row['affects_DE']); ?>','<?php echo $row['mother_tongue_other']; ?>','<?php echo $row['ethnicity_other']; ?>','<?php echo $row['teacher_partner_home_other']; ?>','<?php echo $row['available_gadgets_other']; ?>','<?php echo $row['type_of_internet_other']; ?>','<?php echo $row['learning_delivery_mode_other']; ?>','<?php echo $row['affects_DE_other']; ?>','<?php echo $row['is_blended_pmc']; ?>','<?php echo $row['is_blended_pmc_other']; ?>','<?php echo addslashes($row['suggestions']); ?>');"> <span class="fa fa-edit"> </span> Edit Information
			</button>

	<?php }else{
		
	}


}

function load_fam($con, $student_id){

	$father = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Father' ");

	$row_f = mysqli_fetch_assoc($father); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>FATHER INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_f" class="form-control text-capitalize" value="<?php echo $row_f['lastname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_f" class="form-control text-capitalize" value="<?php echo $row_f['firstname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
						<div class="form-group">
						  <label for="mname"><b>Middlename:</b></label>
						  <input type="text" id="mname_f" class="form-control text-capitalize" value="<?php echo $row_f['mi']; ?>">
						</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_f" class="form-control" value="<?php echo $row_f['contact']; ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_f" class="form-control text-capitalize" value="<?php echo $row_f['occupation']; ?>">
					</div>
				</div>
			</div>
		
		</div>

	</div>

    <?php 

    $mother = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Mother' ");

	$row_m = mysqli_fetch_assoc($mother); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>MOTHER'S INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">

				<div class="col-md-4">
					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_m" class="form-control text-capitalize" value="<?php echo $row_m['lastname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_m" class="form-control text-capitalize" value="<?php echo $row_m['firstname']; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Middlename:</b></label>
					  <input type="text" id="mname_m" class="form-control text-capitalize" value="<?php echo $row_m['mi']; ?>">
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_m" class="form-control" value="<?php echo $row_m['contact']; ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_m" class="form-control text-capitalize" value="<?php echo $row_m['occupation']; ?>">
					</div>
				</div>

			</div>

		</div>

	</div>

    <?php 

    $guardian = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$student_id' and parent_type = 'Guardian' ");

	$row_g = mysqli_fetch_assoc($guardian); ?>
    
    	<div class="card mt-3">

		<div class="card-header text-white w3-blue">
			<b>GUARDIAN'S INFORMATION </b>
		</div>

		<div class="card-body">

			<div class="row">

				<div class="col-md-4">

					<div class="form-group">
					  <label for="lname"><b>Lastname:</b></label>
					  <input type="text" id="lname_g" class="form-control text-capitalize" value="<?php echo $row_g['lastname']; ?>">
					</div>

				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="fname"><b>Firstname:</b></label>
					  <input type="text" id="fname_g" class="form-control text-capitalize" value="<?php echo $row_g['firstname']; ?>">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Middlename:</b></label>
					  <input type="text" id="mname_g" class="form-control text-capitalize" value="<?php echo $row_g['mi']; ?>">
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-4">

					<div class="form-group">
					  <label for="mname"><b>Contact:</b></label>
					  <input type="text" id="contact_g" class="form-control" value="<?php echo $row_g['contact']; ?>">
					</div>

				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="mname"><b>Occupation:</b></label>
					  <input type="text" id="occupation_g" class="form-control text-capitalize" value="<?php echo $row_g['occupation']; ?>">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					  <label for="relation_g"><b>Relation to the Learner:</b></label>
					  <input type="text" id="relation_g" class="form-control text-capitalize" value="<?php echo $row_g['relationship']; ?>">
					</div>
				</div>

			</div>
			
		</div>

	</div>

    <?php 

}

function save_member($con,$grade_id,$member,$student_id){

	$chk = mysqli_query($con, "SELECT * from tbl_households_members where grade_id = '$grade_id' and student_id = '$student_id'");

	if(mysqli_num_rows($chk) > 0){

		echo 2;

	}else{

		$sql = mysqli_query($con, "INSERT INTO tbl_households_members (student_id,grade_id,members) VALUES ('$student_id','$grade_id','$member')");

		if($sql){
			echo 1;
		}

	}

}

function load_household_members($con,$student_id,$grade_id){

	$sql = mysqli_query($con, "SELECT t1.*,t2.grade FROM tbl_households_members t1
				LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.ID where t1.student_id = '$student_id'");

	if (mysqli_num_rows($sql)>0) {
	?>

		<?php while ($row = mysqli_fetch_assoc($sql))  {  ?>

		  <li class="list-group-item d-flex justify-content-between align-items-left">

		    <?php echo $row['grade'] . ' = ' . $row['members']; ?>
		    <span class="fa fa-trash fa-2x w3-hover" onclick="remove_member('<?php echo $row['id'] ?>')"></span>
		  </li>		 
		
		<?php }
	 }

}

function remove_member($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_households_members where id = '$id'");
	if ($sql) {
		echo 1;
	}

}

function update_student_info($con,$s_id,$category,$lname,$fname,$mname,$suffix,$email,$fb_account,$lrn,$bdate,$age,$place_of_birth,$gender,$religion,$citizenship,$mother_tongue,$ethnicity_val,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g ,$contact_g,$occupation_g,$relation_g,$siblings,$with_medical_condition,$medical_condition_affects,$medical_condition,$other_mother_tongue,$other_ethnicity,$go_to_school_val,$teacher_home_val,$other_th,$gadgets_val,$other_gadget,$connected_to_internet_val,$internet_val,$other_i,$internet_rate,$learning_dmode,$other_p,$affects_DE_val,$other_affects,$blended_pmc_val,$other_pmc,$suggestions){

	$school_year = get_school_year_active($con);

	if(strlen($learning_dmode) == 0){
		$learning_delivery_mode = $learning_dmode;
	}else{
		$learning_delivery_mode = $learning_dmode .', '. $other_p;
	}

	if($lrn == 'undefined'){
		$lrn_val = '';
	}else{
		$lrn_val = $lrn;
	}

	$father_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$mother_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$guardian_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

	$sql = mysqli_query($con,"UPDATE tbl_students set lastname = '$lname',firstname = '$fname',mi = '$mname',gender = '$gender',age = '$age',address = '$address',contact = '$contact',birthday = '$bdate',place_of_birth = '$place_of_birth',religion = '$religion', citizenship = '$citizenship', siblings = '$siblings', with_medical_condition = '$with_medical_condition',medical_condition_affects = '$medical_condition_affects', medical_condition = '$medical_condition',suffix = '$suffix',email='$email',fb_account = '$fb_account',mother_tongue = '$mother_tongue',mother_tongue_other = '$other_mother_tongue',ethnicity = '$ethnicity_val',ethnicity_other = '$other_ethnicity',lrn  = '$lrn' WHERE student_id = '$s_id'");

	//Update father

	$chk_f = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Father'");

	if(mysqli_num_rows($chk_f) > 0){
		//update
		$sql2 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_f',firstname = '$fname_f',mi = '$mname_f',address = '$address',contact = '$contact_f',occupation = '$occupation_f' where student_id = '$s_id' and parent_type = 'Father'");
	}else{
		//insert
		$sql2 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$father_id','$lname_f','$fname_f','$mname_f','$address','$contact_f','$s_id','MALE','$occupation_f','Father')");
		$f = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$father_id')");
	}

	//Update mother

	$chk_m = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Mother'");

	if(mysqli_num_rows($chk_m) > 0){

		$sql3 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_m',firstname = '$fname_m',mi = '$mname_m',address = '$address',contact = '$contact_m',occupation = '$occupation_m' where student_id = '$s_id' and parent_type = 'Mother'");

	}else{
		$sql3 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$mother_id','$lname_m','$fname_m','$mname_m','$address','$contact_m','$s_id','FEMALE','$occupation_m','Mother')");
		$m = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$mother_id')");
	}

	//update guardian

	$chk_g = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Guardian'");

	if(mysqli_num_rows($chk_g) > 0){

		$sql4 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_g',firstname = '$fname_g',mi = '$mname_g',address = '$address',contact = '$contact_g',occupation = '$occupation_g',relationship = '$relation_g' where student_id = '$s_id' and parent_type = 'Guardian'");

	}else{

		$sql4 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,occupation,parent_type,relationship) values('$guardian_id','$lname_g','$fname_g','$mname_g','$address','$contact_g','$s_id','$occupation_g','Guardian','$relation_g')");
		$g = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$guardian_id')");

	}	


	$sql5 = mysqli_query($con, "UPDATE tbl_enrolled_students set category = '$category',available_gadgets = '$gadgets_val',available_gadgets_other = '$other_gadget',type_of_internet = '$internet_val',type_of_internet_other = '$other_i', internet_rate = '$internet_rate', learning_delivery_mode = '$learning_dmode',learning_delivery_mode_other = '$other_p', teacher_partner_home = '$teacher_home_val',teacher_partner_home_other = '$other_th', go_to_school = '$go_to_school_val', connected_to_internet = '$connected_to_internet_val', affects_DE = '$affects_DE_val', affects_DE_other = '$other_affects', is_blended_pmc = '$blended_pmc_val', is_blended_pmc_other = '$other_pmc',suggestions = '$suggestions' where student_id = '$s_id'");

	$sql6 =mysqli_query($con,"UPDATE tbl_households_members set is_active = 1, school_year = '$school_year' where student_id = '$s_id'");

	if($sql){
		echo 1;
	}

	if($sql2){
		echo 2;
	}
	if($sql3){
		echo 3;
	}
	if($sql4){
		echo 4;
	}
	if($sql5){
		echo 5;
	}
	if($sql6){
		echo 6;
	}

}

function load_supporting_files($con,$student_id){

	$docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$student_id'");

	if (mysqli_num_rows($docs)>0) {
		$num = 0; ?>
		<ul class="list-group" style="margin-top: 10px;">
				<li class="list-group-item d-flex justify-content-between list-group-item-info">
					<b> SUPPORTING DOCUMENTS AND CREDENTIALS</b>
				</li>
		<?php while ($d = mysqli_fetch_assoc($docs))  { $num++; ?>
			
			
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<b><a href="<?php echo $d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
					</b>
					<b>
						<a download="" href="<?php echo '../'.$d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> 
						</a>
						<a href="#" onclick="delete_doc('<?php echo $d['id'] ?>','<?php echo $d['url'] ?>')" title="Remove file"><span class="fa fa-trash"></span> </a>
					</b>
				</li>

		<?php } ?>
		</ul>
	<?php }

}

///edit student

function remove_file($con,$file){
	unlink($file);
}

function load_pass($con){

	$id = $_SESSION['login_id'];
	$q = mysqli_query($con, "SELECT * from tbl_user_account where transid = '$id'");

	if (mysqli_num_rows($q)>0) {

			$r = mysqli_fetch_assoc($q);
			$pass = $r['user_pwd'];
		}
	?>

	<input type="password" class="form-control" id="old_pass" required="" value="<?php echo $pass;?>">
	  <div class="input-group-append">
	    <button class="btn btn-outline-secondary" onclick="showpass();">
	    	<span class="fa fa-eye" id="eye"></span><span class="fa fa-eye-slash" id="eye_slash"></span>
	    </button>
	  </div>

	  <script type="text/javascript">
	  		
	  		$(document).ready(function(e) {

	  			$("#eye_slash").css('display','none');

	  			$("#old_pass").keyup(function(e){

		        var old_pass = $("#old_pass").val();

		        if(old_pass != '')
		        {
		           	var mydata = 'action=check_old_pass' + '&old_pass=' + old_pass;
		            $.ajax({
				    type:'POST',
				    url:url,
				    data:mydata,
				    cache:false,
				    success:function(data){
			
			    		if(data == 1){

				    		$('#new_pass').focus();
			                $('#old_pass_validator').text(' Your old password is correct.');
			                $('#old_pass_validator').removeClass('text-danger');
				    		$('#old_pass_validator').addClass('text-success');
				    		document.getElementById('btn_up_account').disabled = false;
			    		
				    	}else{

				    		$('#old_pass_validator').text(' Your old password is incorrect.');
				    		$('#old_pass_validator').removeClass('text-success');
				    		$('#old_pass_validator').addClass('text-danger');
				    		document.getElementById('btn_up_account').disabled = true;

				    	}

				    }
				    
				  });

		        } else {
		        	$("#oldpass").focus();      
		        }

		    });

	  		});

	  </script>

<?php }

function check_old_pass($con,$old_pass){

	$userid = $_SESSION['user_name'];
	$sql = mysqli_query($con, "SELECT * from tbl_user_account where user_id = '$userid' and user_pwd = '$old_pass'");

	if(mysqli_num_rows($sql) > 0){
		echo 1;
	}else{
		echo 404;
	}

}

function update_password($con,$old_pass,$retype_pass){

	$userid = $_SESSION['user_name'];
	$sql = mysqli_query($con, "UPDATE tbl_user_account set user_pwd = '$retype_pass' where user_id = '$userid' and user_pwd = '$old_pass'");

	if($sql){
		echo 1;
	}else{
		echo 404;
	}

}

function load_pic($con,$pic){

	if($pic == Null || $pic == ''){

		if($_SESSION['gender'] == 'MALE'){
			echo '<img data-toggle="modal" data-target="#show_mypic" src="../img/male_stud.jpg" class="img-fluid img-thumbnail" width="100"/>';
		}else{
			echo '<img data-toggle="modal" data-target="#show_mypic" src="../img/female_stud.jpg" class="img-fluid img-thumbnail" width="100"/>';
		}
		
	}else{
		// 
		echo '<img data-toggle="modal" data-target="#show_mypic" src="data:image/jpeg;base64,'.base64_encode($pic).'" class="img-fluid img-thumbnail" width="50%"/>';
		
	}

	

}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        return $row['id'];
    }
}

function load_student_accounts($con) {

	$sy = get_school_year_active($con); 
	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con, "SELECT b.`school_year`, a.`balance`, c.`terms`, a.`status`, a.`date_trans` FROM tbl_accounts a LEFT JOIN tbl_school_year b ON a.`school_year` = b.`id` LEFT JOIN tbl_terms c on a.`terms` = c.`id` WHERE a.`student_id` = '$stud_id' AND a.`school_year` = '$sy'");

	if (mysqli_num_rows($sql)) {

?>

	<thead>
		<tr class="table-default">
	        <th>School Year</th>
	        <th>Balance</th>
	        <th>Terms</th>
	        <!-- <th>Status</th> -->
	        <th>Last Transaction</th>
	    </tr>
	</thead>

	<tbody>

<?php while ($row = mysqli_fetch_assoc($sql)) { ?>
		<tr>
			<td><?php echo $row["school_year"]?></td>
			<td><?php echo $row["balance"]?></td>
			<td><?php echo $row["terms"]?></td>
			<!-- <td><?php //echo $row["status"]?></td> -->
			<td><?php echo $row["date_trans"]?></td>
		</tr>
<?php } ?>
	</tbody>

<?php
	}	
}

function load_student_info($con,$school_year_info){

	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con,"SELECT t1.*,t2.grade as grade_name,t3.* FROM tbl_students t1 
		LEFT JOIN tbl_grade t2 ON t1.`grade` = t2.`ID` 
		LEFT JOIN tbl_enrolled_students t3 on t1.student_id = t3.student_id
		where t1.`student_id` = '$stud_id' and t3.school_year = '$school_year_info'");

    if (mysqli_num_rows($sql)>0) {

        $row = mysqli_fetch_assoc($sql);

        $name = $row['lastname'].', '.$row['firstname'].' '.$row['mi'] . ' ' . $row['suffix'];

    } 
    
?>

	<ul class="nav nav-pills nav-justified" style="font-weight: bolder;">
	  <li class="nav-item">
	    <a class="nav-link active" data-toggle="pill" href="#info"><span class="fa fa-registered"></span> Personal Information</a>
	  </li>
	  <li class="nav-item	">
	    <a class="nav-link" data-toggle="pill" href="#fam"><span class="fa fa-home"></span> Family Information</a>
	  </li>
	  <li class="nav-item	">
	    <a class="nav-link" data-toggle="pill" href="#other"><span class="fa fa-info"></span> Other Information</a>
	  </li>
	  <li class="nav-item	">
	    <a class="nav-link" data-toggle="pill" href="#survey"><span class="fa fa-poll"></span> Survey Information</a>
	  </li>
	</ul>
	<br>
	<div class="tab-content">

	  <div class="tab-pane container active" id="info">

	  	<ul class="list-group">
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Name:
				<b><?php echo $name; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Category
				<b><?php echo $row['category']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Lrn
				<b><?php echo $row['lrn']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Grade & Section:
				<b><?php echo $row['grade_name'] .' - '. $row['section']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Gender:
				<b><?php echo $row['gender']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Place of birth:
				<b><?php echo $row['place_of_birth']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Birth date:
				<b><?php echo $row['birthday']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Age:
				<b><?php echo $row['age']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Address:
				<b><?php echo $row['address']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Contact:
				<b><?php echo $row['contact']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Religion:
				<b><?php echo $row['religion']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Citizenship:
				<b><?php echo $row['citizenship']; ?></b>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				Email:
				<b><?php echo $row['email']; ?></b>
			</li>

			<li class="list-group-item d-flex justify-content-between align-items-center">
				Facebook Account:
				<b><?php echo $row['fb_account']; ?></b>
			</li>

			<li class="list-group-item d-flex justify-content-between align-items-center">
				Messenger Account:
				<b><?php echo $row['messenger_account']; ?></b>
			</li>

			<li class="list-group-item d-flex justify-content-between align-items-center">
				Mother Tongue:
				<b>
				<?php 

				if($row['mother_tongue_other'] == ''){

					echo $row['mother_tongue'];

				}else{

					if($row['mother_tongue'] == ''){

						echo $row['mother_tongue_other'];

					}else{

						echo $row['mother_tongue'] . ', ' .$row['mother_tongue_other'];

					}

				}

				?>
			</b>
			</li>

			<li class="list-group-item d-flex justify-content-between align-items-center">
				Ethnicity:
				<b>
				<?php 

				if($row['ethnicity_other'] == ''){

					echo $row['ethnicity'];

				}else{

					if($row['ethnicity'] == ''){

						echo $row['ethnicity_other'];

					}else{

						echo $row['ethnicity'] . ', ' .$row['ethnicity_other'];

					}

				}

				?>
			</b>
			</li>


		</ul>

	  </div>

	  <div class="tab-pane container" id="fam">

	  	<?php  

	  		$father= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Father' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($father)>0) {
		        $f = mysqli_fetch_assoc($father);

		        $fname = $f['lastname'].', '.$f['firstname'].' '.$f['mi'];
		        ?>

		        <ul class="list-group">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-default">
						<b>FATHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($fname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($f['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($f['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($f['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{

		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-default'>
						<b>FATHER'S INFORMATION</b>
					</li> <br> <span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>

	  	<?php  

	  		$mother= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Mother' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($mother)>0) {
		        $m = mysqli_fetch_assoc($mother);

		        $mname = $m['lastname'].', '.$m['firstname'].' '.$m['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-default">
						<b>MOTHER'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($mname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($m['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($m['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($m['occupation']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-default'>
						<b>MOTHER'S INFORMATION</b>
					</li><br><span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>
	  	<?php  

	  		$guardian= mysqli_query($con, "SELECT * from tbl_parents where parent_type = 'Guardian' and student_id = '$stud_id'");

	  		if (mysqli_num_rows($guardian)>0) {
		        $g = mysqli_fetch_assoc($guardian);

		        $gname = $g['lastname'].', '.$g['firstname'].' '.$g['mi'];
		        ?>

		        <ul class="list-group" style="margin-top: 10px;">
		        	<li class="list-group-item d-flex justify-content-between list-group-item-default">
						<b>GUARDIAN'S INFORMATION</b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Name:
						<b><?php echo strtoupper($gname); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Address:
						<b><?php echo strtoupper($g['address']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Contact:
						<b><?php echo strtoupper($g['contact']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Occupation:
						<b><?php echo strtoupper($g['occupation']); ?></b>
					</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Relation:
						<b><?php echo strtoupper($g['relationship']); ?></b>
					</li>
				</ul>

		    <?php }else{
		    	echo "<li class='list-group-item d-flex justify-content-between list-group-item-default'>
						<b>GUARDIAN'S INFORMATION</b>
					</li><br><span class='badge badge-warning'>No data.</span>";
		    } 

	  	?>
	  	
		  	<li class="list-group-item d-flex justify-content-between align-items-center">
				Siblings:
				<b><?php echo strtoupper($row['siblings']); ?></b>
			</li>

	  </div>

	  <div class="tab-pane container" id="other">

	  	<div class="card">

			<div class="card-header w3-blue text-white">
				<b> HEALTH RECORDS </b>
			</div>

			<div class="card-body">

				<div class="form-group">
			  <label for="with_medical_condition">Do you have impairment, disability, or long term medical condition?</label><br>
			 <label class="text-dark"><b><?php echo $row['with_medical_condition']; ?></b></label>
			</div>

			<div class="form-group">
			  <label for="medical_condition_affects">If Yes, does your impairment, disability, or long-term medical condition affect your study?</label><br>
			  <label class="text-dark"><b><?php echo $row['medical_condition_affects']; ?></b></label>
			</div>

			<div class="form-group">
			  <label for="medical_condition">What is your impairment, disability, or long-term medical condition?
			Please leave this part if you opt not to declare your impairment, disability, or long-term medical condition.</label><br>
			 <label class="text-dark"><b><?php echo $row['medical_condition']; ?></b></label>
			</div>

			</div>

	  	</div>

	  	<div class="card mt-3">

	  		<div class="card-header w3-blue text-white" id="support_doc">
	  			<b>SUPPORTING DOCUMENTS AND CREDENTIALS</b>
				</div>

	  		<div class="card-body">

	  			<?php $docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$stud_id'");

				if (mysqli_num_rows($docs)>0) {
					$num = 0; ?>
					<ul class="list-group">
					<?php while ($d = mysqli_fetch_assoc($docs))  { $num++; ?>
						
						
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<b><a href="<?php echo $d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
								</b>
								<b>
									<a download="" href="<?php echo $d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> 
									</a>
								</b>
							</li>

					<?php } ?>
					</ul>
				<?php } ?>

	  		</div>

	  	</div>

	  </div>

	  <div class="tab-pane container" id="survey">

	  	<div class="card">

			<div class="card-header w3-blue text-white">
				<b> HOUSEHOLD CAPACITY AND ACCESS TO DISTANCE LEARNING </b>
			</div>

			<div class="card-body">

				<div class="form-group">
				  <label for="with_medical_condition">How does your child go to school?</label><br>
				 <label class="text-dark">
				 	<b><?php echo str_replace(',', '<br/>', $row['go_to_school']); ?></b>
				 </label>
				</div>

				<?php $sql2 = mysqli_query($con, "SELECT t1.*,t2.grade FROM tbl_households_members t1
				LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.ID where t1.student_id = '$stud_id'");

				if (mysqli_num_rows($sql2)>0) {
				?>
				<div class="form-group">
				  <label for="with_medical_condition">How many of your household members (including the enrollee) are studying in School Year 2020-2021?</label><br>
					<?php while ($r2 = mysqli_fetch_assoc($sql2))  {  ?>

					 <label class="text-dark">
					 	<b>
					    <?php echo $r2['grade'] . ' = ' . $r2['members']; ?>
					    </b>
					</label>	 
					
					<?php } ?>
				</div>
				 <?php } ?>

				 <div class="form-group">
				  	<label for="with_medical_condition">Who among the household members can provide instructional support to the child's distance learning?</label><br>
					 <label class="text-dark"><b>
						<?php 
						if($row['teacher_partner_home_other'] == ''){
							$teacher_partner_home = $row['teacher_partner_home'];
						}else{
							if($row['teacher_partner_home'] == ''){
								$teacher_partner_home = $row['teacher_partner_home_other'];
							}else{
								$teacher_partner_home = $row['learning_delivery_mode'] . ', ' .$row['teacher_partner_home_other'];
							}
						} 
						echo str_replace(',', '<br/>', $teacher_partner_home);
						?>
					 </b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">What devices are available at home that the learner can use for learning?</label><br>
					 <label class="text-dark">
					 	<b>
							<?php 
							if($row['available_gadgets_other'] == ''){
								$available_gadgets = $row['available_gadgets'];
							}else{
								if($row['available_gadgets'] == ''){
									$available_gadgets = $row['available_gadgets_other'];
								}else{
									$available_gadgets = $row['learning_delivery_mode'] . ', ' .$row['available_gadgets_other'];
								}
							} 
							echo str_replace(',', '<br/>', $available_gadgets);
							?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">Do you have a way to connect to the internet?</label><br>
					 <label class="text-dark">
					 	<b>
					 		<?php echo $row['connected_to_internet']; ?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">What type of internet connection do you have at home?</label><br>
					 <label class="text-dark">
					 	<b>
							<?php 
							if($row['type_of_internet_other'] == ''){
								$type_of_internet = $row['type_of_internet'];
							}else{
								if($row['type_of_internet'] == ''){
									$type_of_internet = $row['type_of_internet_other'];
								}else{
									$type_of_internet = $row['learning_delivery_mode'] . ', ' .$row['type_of_internet_other'];
								}
							} 
							echo str_replace(',', '<br/>', $type_of_internet);
							?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">Rate your internet connectivity from 1 to 5 (5 as the strongest 1 as the weakest)</label><br>
					 <label class="text-dark">
					 	<b>
					 		<?php echo $row['internet_rate']; ?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">What setup do you prefer for your children for this coming school 2020-2021?</label><br>
					 <label class="text-dark">
					 	<b>
							<?php 
							if($row['learning_delivery_mode_other'] == ''){
								$learning_delivery_mode = $row['learning_delivery_mode'];
							}else{
								if($row['learning_delivery_mode'] == ''){
									$learning_delivery_mode = $row['learning_delivery_mode_other'];
								}else{
									$learning_delivery_mode = $row['learning_delivery_mode'] . ', ' .$row['learning_delivery_mode_other'];
								}
							} 
							echo str_replace(',', '<br/>', $learning_delivery_mode);
							?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">Are you interested to enroll in blended PMC classes?</label><br>
					 <label class="text-dark">
					 	<b>
							<?php 
							if($row['is_blended_pmc_other'] == ''){
								$is_blended_pmc = $row['is_blended_pmc'];
							}else{
								if($row['is_blended_pmc'] == ''){
									$is_blended_pmc = $row['is_blended_pmc_other'];
								}else{
									$is_blended_pmc = $row['is_blended_pmc'] . ', ' .$row['is_blended_pmc_other'];
								}
							} 

							echo str_replace(',', '<br/>', $is_blended_pmc);
							?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">What are the challenges that may affect your child's learning process through distance education?</label><br>
					 <label class="text-dark">
					 	<b>
						 	<?php 
							if($row['affects_DE_other'] == ''){
								$affects_DE = $row['affects_DE'];
							}else{
								if($row['affects_DE'] == ''){
									$affects_DE = $row['affects_DE_other'];
								}else{
									$affects_DE = $row['affects_DE'] . ', ' .$row['affects_DE_other'];
								}
							} 

							echo str_replace(',', '<br/>', $affects_DE);
							?>
					 	</b>
					</label>
				</div>

				<div class="form-group">
				  	<label for="with_medical_condition">Suggestions</label><br>
					 <label class="text-dark">
					 	<b>
					 		<?php echo $row['suggestions']; ?>
					 	</b>
					</label>
				</div>

			</div>

	  	</div>

	  </div>

	</div>

<?php }


//>>> Payments <<<//

function load_accnts_payments($con) {

	$sy = get_school_year_active($con); 
	$stud_id = $_SESSION["id"];

	$sql = mysqli_query($con, "SELECT a.`Id`, b.`school_year`, a.`date_due`, a.`amount_due`, a.`due_payment`, a.`due_balance`, a.`balance`, a.`date_trans`, a.`confirmation_status`, a.`payment_status`, a.`file`, a.`file_type` FROM tbl_payment_transactions a LEFT JOIN tbl_school_year b ON a.`school_year` = b.`id` WHERE a.`student_id` = '$stud_id' AND a.`school_year` = '$sy'");

	if (mysqli_num_rows($sql)) {

?>
		<thead>
			<tr class="table-default">
				
				<th>Date Transaction</th>
				<th>School Year</th>
				<th>Payment Details</th>
		        <th>Payment Status</th>
		        <th>Confirmation Status</th>
		        <th>Action</th>
		    </tr>
		</thead>

		<tbody>

	<?php while ($row = mysqli_fetch_assoc($sql)) { ?>
		
		<tr>
			<td><?php echo $row["date_trans"]?></td>
			<td><?php echo $row["school_year"]?></td>
			<td>
				<center>
					<button class="btn btn-info btn-sm" title="VIEW PAYMENT DETAILS" onclick="view_payment_details('<?php echo $row['Id']; ?>')">
						<span class="fa fa-eye"></span> View Details
					</button>
				</center>
			</td>
			<?php  
				if ($row["payment_status"] == 0) {
					echo '<td><center><span class="badge badge-pill badge-danger">NOT PAID</span></center></td>';
				}
				else if ($row["payment_status"] == 1) {
					echo '<td><center><span class="badge badge-pill badge-warning">PAID W/ DUE BALANCE</span></center></td>';
				}
				else if ($row["payment_status"] == 2) {
					echo '<td><center><span class="badge badge-pill badge-primary">PAID W/ CREDIT</span></center></td>';
				}
				else if ($row["payment_status"] == 3) {
					echo '<td><center><span class="badge badge-pill badge-success">PAID</span></center></td>';
				}
			  
				if ($row["confirmation_status"] == 0) {
					echo '<td><center><span class="badge badge-pill badge-danger">NOT YET CONFIRMED</span></center></td>';
				}
				else {
					echo '<td><center><span class="badge badge-pill badge-success">CONFIRMED</span></center></td>';
				}
			?>
			<td>
				<button class="btn btn-success btn-sm" title="PAY" onclick="view_payment('<?php echo $row['Id']; ?>','<?php echo $row['amount_due']; ?>','<?php echo $row['balance']; ?>')"><span class="fa fa-cash-register"></span> PAY</button>
				<!-- <button class="btn btn-warning btn-sm" title="EDIT" onclick=""><span class="fa fa-edit"></span> EDIT</button>
				<button class="btn btn-danger btn-sm" title="DELETE" onclick=""><span class="fa fa-trash-alt"></span> DELETE</button> -->
			</td>
		</tr>

	<?php } ?>
		</tbody>


<?php		
	}
}

function load_payment_details($con, $pid) {

	$stud_id = $_SESSION["id"];
	$sy = get_school_year_active($con); 

	$sql = mysqli_query($con, "SELECT * FROM tbl_payment_transactions WHERE id='$pid' AND student_id='$stud_id' AND school_year='$sy'");

	if (mysqli_num_rows($sql) > 0) {
		
		while ($rows=mysqli_fetch_assoc($sql)) {

?>
	<ul class="list-group">
		<li class="list-group-item d-flex justify-content-between list-group-item-success">
			<b>PAYMENT DETAILS</b>
			<b><button type="button" class="close" data-dismiss="modal">&times;</button></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Date Due:
			<b><?php echo $rows['date_due']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Bank:
			<b><?php echo $rows['bank']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Account No:
			<b><?php echo $rows['account_no']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Account Name:
			<b><?php echo $rows['account_name']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Reference No:
			<b><?php echo $rows['ref_no']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Amount Due:
			<b><?php echo $rows['amount_due']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Due Payment:
			<b><?php echo $rows['due_payment']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Due Balance:
			<b><?php echo $rows['due_balance']; ?></b>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center">
			Receipt:
			<button class="btn btn-primary btn-sm"><span class="fa fa-eye"></span> View</button>
		</li>
		<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
			Remaining Balance:
			<b><?php echo $rows['balance']; ?></b>
		</li>
	</ul>

<?php
			
		}
	}
}

function total_payment($con,$term_id,$grade_id,$item){
	
    if ($item == 0) {
		$sql = mysqli_query($con, "SELECT sum(amount) as amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id'");
    }
    else if ($item == 1) {
		$sql =  mysqli_query($con, "SELECT amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id' ORDER BY id ASC LIMIT 1");
    }
    else if ($item == 2) {
		$sql =  mysqli_query($con, "SELECT amount FROM tbl_terms_of_payment WHERE term_id='$term_id' AND grade_id='$grade_id' ORDER BY id DESC LIMIT 1");
    }
	
    if (mysqli_num_rows($sql)>0) {
        
    	$row = mysqli_fetch_assoc($sql);

    	return $row['amount'];
    }
}

function add_sf_payment($con,$pid,$amt_due,$amt,$mop,$bank,$actno,$atn,$rec_bank,$sn,$cno,$add,$ref_no,$rec_rmt){

	$stud_id = $_SESSION["id"];
	$sy = get_school_year_active($con); 
	$date_trans = date('Y-m-d H:i:s');
	
	$comp_bal = ($amt - $amt_due);

	if ($comp_bal > 0) {
		$status = 2;
	}
	else if ($comp_bal == 0) {
		$status = 3;
	}
	else {
		$status = 1;
	}


	$sql = mysqli_query($con, "UPDATE tbl_payment_transactions SET due_payment='$amt', due_balance='$comp_bal', mop='1', bank='$bank', account_no='$actno', account_name='$atn', ref_no='$ref_no', file='', file_type='', payment_status='$status' WHERE student_id='$stud_id' AND school_year='$sy' AND id='$pid'");

	// if ($mop == 1) {
	// }
	// else{
	// 	$sql = mysqli_query($con, "UPDATE tbl_payment_transactions SET due_payment='$amt', due_balance='$comp_bal', mop='$mop', account_name='$sn', contact_no='$cno', address='$add', ref_no='$ref_no', payment_status='$status' WHERE student_id='$stud_id' AND school_year='$sy' AND id='$pid'");
	// }

	// $get_amt_ammort = total_payment($con, 4, 2)
	
	// $sql1 = mysqli_query($con, "INSERT INTO tbl_payment_transactions VALUES (Id,'$sy','','','0','0','','','','','','','','','','','','0','0')")

	if ($sql) {
		echo 1;
	}
}



?>

