<?php 

session_start();
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'PHPMailer/PHPMailerAutoload.php';

function save_logs($con,$userid,$subject,$description){

	$date = date('Y-m-d H:i:s');

	$sql = mysqli_query($con, "INSERT INTO tbl_user_logs (userid,subject,description,date_trans) Values ('$userid','$subject','$description','$date')");

	if($sql){
		echo 'save';
	}
}

function save_last_log($con,$transid){

	$date = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "UPDATE tbl_user_account set last_log = '$date' where transid = '$transid'");

	$save_logs = mysqli_query($con, "INSERT INTO tbl_user_logs (userid,subject,description,date_trans) Values ('$transid','Login Account','Login Account','$date')");
	
	if($sql){
		echo '';
	}

}

function check_email($con,$email){

	$chk = mysqli_query($con,"SELECT * from tbl_user_account where user_id = '$email'");
	//echo mysqli_num_rows($chk);
	if(mysqli_num_rows($chk) > 0){
		echo 1;
	}else{
		echo 2;
	}

}

function get_age($con,$bdate){
	$date = new DateTime($bdate);
	 $now = new DateTime();
	 $interval = $now->diff($date);
	 return $interval->y;
}

function get_school_year_active($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where is_active = 1");
    if (mysqli_num_rows($sql)>0) {
        $row = mysqli_fetch_assoc($sql);
        return $row['id'];
    }
}


function get_acronym($con){
    $s = mysqli_query($con,"SELECT * from tbl_company where is_active = 1");
    if (mysqli_num_rows($s)>0) {
        $row1 = mysqli_fetch_assoc($s);
        return $row1['acronym'];
    }
}

function is_with_payment($con){

	$s2 = mysqli_query($con,"SELECT * from tbl_company where is_active = 1");
    if (mysqli_num_rows($s2)>0) {
        $row2 = mysqli_fetch_assoc($s2);
        return $row2['with_payment'];
    }

}

function gen_id($con){
    $student_id =  rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
    echo $student_id;
}

function save_registration($con,$grade_id,$lname,$fname,$mname,$esc_qvr_number,$bdate,$place_of_birth,$gender,$religion,$citizenship,$contact,$address,$lname_f,$fname_f,$mname_f,$contact_f,$occupation_f,$lname_m,$fname_m,$mname_m,$contact_m,$occupation_m,$lname_g,$fname_g,$mname_g,$contact_g,$occupation_g,$siblings,$si_kindergarten,$si_gradeschool,$si_junior_hs,$si_senior_hs,$with_medical_condition,$medical_condition_affects,$medical_condition,$student_id,$term_id,$suffix,$category,$email,$fb_account,$messenger_account,$mother_tongue,$other_mother_tongue,$ethnicity_val,$other_ethnicity,$gadgets_val,$other_gadget,$internet_val,$other_i,$internet_rate,$lrn,$relation_g,$learning_dmode,$other_p,$preferred_days_val,$other_gp,$financial_capability_val,$other_fc,$teacher_home_val,$other_th,$p_userid,$p_pass,$go_to_school_val,$connected_to_internet_val,$affects_DE_val,$other_affects,$acronym,$blended_pmc_val,$other_pmc,$suggestions,$school_year){

	$date = date('Y-m-d H:i:s');
	$father_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$mother_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$guardian_id = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
	$age = get_age($con,$bdate);

	$sy = mysqli_query($con, "SELECT * from tbl_school_year where id = '$school_year'");
	$r = mysqli_fetch_assoc($sy);
	$sy_name = $r['school_year'];

	if($acronym === 'SSJA'){
		$lbl = 'Family';
    }else if($acronym === 'CFS'){
    	$lbl = 'Community';
    }else{
    	$lbl = 'Community';
    }

	$chk = mysqli_query($con,"SELECT * from tbl_students where lastname = '$lname' and firstname = '$fname'");
	
	if(mysqli_num_rows($chk) > 0){

		 // Account already exists

		$row = mysqli_fetch_assoc($chk);
		$s_id = $row['student_id'];

		$chk_sy = mysqli_query($con, "SELECT * from tbl_enrolled_students where student_id = '$s_id' and school_year = '$school_year'");

		if(mysqli_num_rows($chk_sy) > 0){

			echo 'duplicate';

		}else{
			echo 2;
			$sql_1 = mysqli_query($con, "INSERT INTO tbl_enrolled_students (student_id,grade_id,school_year,date_enrolled,date_trans,category,available_gadgets,available_gadgets_other,type_of_internet,type_of_internet_other,internet_rate,learning_delivery_mode,learning_delivery_mode_other,teacher_partner_home,teacher_partner_home_other,go_to_school,connected_to_internet,affects_DE,affects_DE_other,is_blended_pmc,is_blended_pmc_other,suggestions) values ('$s_id','$grade_id','$school_year','$date','$date','$category','$gadgets_val','other_gadget','$internet_val','$other_i','$internet_rate','$learning_dmode','$other_p','$teacher_home_val','$other_th','$go_to_school_val','$connected_to_internet_val','$affects_DE_val','$other_affects','$blended_pmc_val','$other_pmc','$suggestions')");

		$sql_2 = mysqli_query($con, "UPDATE tbl_students set lastname = '$lname',firstname = '$fname',mi = '$mname',gender = '$gender',age = '$age',address = '$address',contact = '$contact',birthday = '$bdate',place_of_birth = '$place_of_birth',religion = '$religion', citizenship = '$citizenship', siblings = '$siblings', with_medical_condition = '$with_medical_condition',medical_condition_affects = '$medical_condition_affects', medical_condition = '$medical_condition',suffix = '$suffix',email='$email',fb_account = '$fb_account',mother_tongue = '$mother_tongue',mother_tongue_other = '$other_mother_tongue',ethnicity = '$ethnicity_val',ethnicity_other = '$other_ethnicity',lrn  = '$lrn' WHERE student_id = '$s_id'");

		//Update father

		$chk_f = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Father'");

		if(mysqli_num_rows($chk_f) > 0){
			//update
			$sql_3 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_f',firstname = '$fname_f',mi = '$mname_f',address = '$address',contact = '$contact_f',occupation = '$occupation_f' where student_id = '$s_id' and parent_type = 'Father'");
		}else{

			if($lname_f <> ''){

				$sql_3 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$father_id','$lname_f','$fname_f','$mname_f','$address','$contact_f','$s_id','MALE','$occupation_f','Father')");
				$f = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$father_id')");
			}
			
		}

		//Update mother

		$chk_m = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Mother'");

		if(mysqli_num_rows($chk_m) > 0){

				$sql_4 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_m',firstname = '$fname_m',mi = '$mname_m',address = '$address',contact = '$contact_m',occupation = '$occupation_m' where student_id = '$s_id' and parent_type = 'Mother'");

			}else{
				if($lname_m <> ''){
					$sql_4 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$mother_id','$lname_m','$fname_m','$mname_m','$address','$contact_m','$s_id','FEMALE','$occupation_m','Mother')");
					$m = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$mother_id')");
				}
				
			}

		//update guardian

		$chk_g = mysqli_query($con, "SELECT * from tbl_parents where student_id = '$s_id' and parent_type = 'Guardian'");

		if(mysqli_num_rows($chk_g) > 0){

			$sql_5 = mysqli_query($con, "UPDATE tbl_parents set lastname = '$lname_g',firstname = '$fname_g',mi = '$mname_g',address = '$address',contact = '$contact_g',occupation = '$occupation_g',relationship = '$relation_g' where student_id = '$s_id' and parent_type = 'Guardian'");

		}else{

			if($lname_g <> ''){
			$sql_5 = mysqli_query($con, "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,occupation,parent_type,relationship) values('$guardian_id','$lname_g','$fname_g','$mname_g','$address','$contact_g','$s_id','$occupation_g','Guardian','$relation_g')");
			$g = mysqli_query($con, "INSERT INTO tbl_parent_picture(parent_id) values('$guardian_id')");
			}
			
		}

		$total_payment = total_payment($con,$term_id,0,$grade_id);
		$down_payment = total_payment($con,$term_id,1,$grade_id);

		if(is_with_payment($con) == "YES"){

			$sql6 = mysqli_query($con, "INSERT INTO tbl_accounts(student_id,school_year,terms,balance,status,date_trans) values
			('$s_id','$school_year','$term_id','$total_payment',0,'$date')");

			$sql6 = mysqli_query($con, "INSERT INTO tbl_payment_transactions(student_id,school_year,date_due,amount_due,due_payment,due_balance,balance,date_trans,payment_status,confirmation_status) values
				('$s_id','$school_year','2020-05-01','$down_payment','0.0','0.0','$total_payment','$date','0','0')");
		}

		//Old student
    	$subject = $acronym .' ONLINE REGISTRATION FOR THE S.Y. '. strtoupper($sy_name);
    	$body_val = '<h2>Hello '. ucfirst($fname) .',</h2>
    	<p>Thank you very much for entrusting to us the education of your children and hoping to be of service to you through our quality education. <br><br> Welcome to '. $acronym. ' '. $lbl. '!'.' 
    	</p>';

		email_sender($con, $lname,$fname,"","","",$email,$acronym,$subject,$body_val);	

		$description = $lname. ', '.$fname . ' Registered for the S.Y. ' .$sy_name;
		$save_logs = mysqli_query($con, "INSERT INTO tbl_user_logs (userid,subject,description,date_trans) Values ('$s_id','New Register','$description','$date')");

		}

	}else{

		//insert
		
		//Student Account Auto generated
		$s_userid = "S-".rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);
		$s_pass = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

		if($lrn == 'undefined'){
			$lrn_val = '';
		}else{
			$lrn_val = $lrn;
		}

		$sql = "INSERT INTO tbl_students(student_id,lastname,firstname,mi,gender,age,grade,address,contact,birthday,place_of_birth,religion,citizenship,siblings,is_active,with_medical_condition,medical_condition_affects,medical_condition,suffix,email,fb_account,messenger_account,mother_tongue,mother_tongue_other,ethnicity,ethnicity_other,lrn) VALUES('$student_id','$lname','$fname','$mname','$gender','$age','$grade_id','$address','$contact','$bdate','$place_of_birth','$religion','$citizenship','$siblings',1,'$with_medical_condition','$medical_condition_affects','$medical_condition','$suffix','$email','$fb_account','$messenger_account','$mother_tongue','$other_mother_tongue','$ethnicity_val','$other_ethnicity','$lrn_val');";

		$sql .= "INSERT INTO tbl_user_account(user_id,user_pwd,category,code) values('$email','$s_pass','Student','$student_id');";

		$sql .= "INSERT INTO tbl_user_account(user_id,user_pwd,category,code) values('$email','$p_pass','Parent','$student_id');";

		$sql .= "INSERT INTO tbl_student_picture(student_id) values('$student_id');";

		//insert father
		if($lname_f <> ''){
			$sql .= "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$father_id','$lname_f','$fname_f','$mname_f','$address','$contact_f','$student_id','MALE','$occupation_f','Father');";
			$sql .= "INSERT INTO tbl_parent_picture(parent_id) values('$father_id');";
		}else{
			$sql .= "";
		}
		
		//insert mother
		if($lname_m <> ''){

			$sql .= "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,gender,occupation,parent_type) values('$mother_id','$lname_m','$fname_m','$mname_m','$address','$contact_m','$student_id','FEMALE','$occupation_m','Mother');";
			$sql .= "INSERT INTO tbl_parent_picture(parent_id) values('$mother_id');";
		}else{
			$sql .= "";
		}

		//insert guardian
		if($lname_g <> ''){

			$sql .= "INSERT INTO tbl_parents(parent_id,lastname,firstname,mi,address,contact,student_id,occupation,parent_type,relationship) values('$guardian_id','$lname_g','$fname_g','$mname_g','$address','$contact_g','$student_id','$occupation_g','Guardian','$relation_g');";
			$sql .= "INSERT INTO tbl_parent_picture(parent_id) values('$guardian_id');";
		}else{
			$sql .= "";
		}

		$sql .= "INSERT INTO tbl_enrolled_students (student_id,grade_id,school_year,date_enrolled,date_trans,category,available_gadgets,available_gadgets_other,type_of_internet,type_of_internet_other,internet_rate,learning_delivery_mode,learning_delivery_mode_other,teacher_partner_home,teacher_partner_home_other,go_to_school,connected_to_internet,affects_DE,affects_DE_other,is_blended_pmc,is_blended_pmc_other,suggestions) values ('$student_id','$grade_id','$school_year','$date','$date','$category','$gadgets_val','other_gadget','$internet_val','$other_i','$internet_rate','$learning_dmode','$other_p','$teacher_home_val','$other_th','$go_to_school_val','$connected_to_internet_val','$affects_DE_val','$other_affects','$blended_pmc_val','$other_pmc','$suggestions');";

		$sql.="UPDATE tbl_households_members set is_active = 1, school_year = '$school_year';";
			//Payments
		
		$total_payment = total_payment($con,$term_id,0,$grade_id);
		$down_payment = total_payment($con,$term_id,1,$grade_id);

		if(is_with_payment($con) == "YES"){

			$sql2 = mysqli_query($con, "INSERT INTO tbl_accounts(student_id,school_year,terms,balance,status,date_trans) values
			('$student_id','$school_year','$term_id','$total_payment',0,'$date')");

			$sql3 = mysqli_query($con, "INSERT INTO tbl_payment_transactions(student_id,school_year,date_due,amount_due,due_payment,due_balance,balance,date_trans,payment_status,confirmation_status) values
				('$student_id','$school_year','2020-05-01','$down_payment','0.0','0.0','$total_payment','$date','0','0')");
		}else{

			$sql .= "";
			$sql .= "";
		}

		if (mysqli_multi_query($con, $sql)) {

			echo 1;

			$description = $lname. ', '.$fname . ' Registered for the S.Y. ' .$sy_name;

			$save_logs = mysqli_query($con, "INSERT INTO tbl_user_logs (userid,subject,description,date_trans) Values ('$student_id','New Register','$description','$date')");
	    	$subject = $acronym .' ONLINE REGISTRATION FOR THE S.Y. '. strtoupper($sy_name);

	    	$body_val = '<h2>Hello '. ucfirst($fname) .',</h2>
	    	<p>Thank you very much for entrusting to us the education of your children and hoping to be of service to you through our quality education. <br><br> Welcome to '. $acronym. ' '. $lbl. '!'.' 
	    	</p>
	    	<b>Please Secure your account: <br><br> 
	    	Your Registered user name : '. $email . ' <br><br>

	    		Here is your initial passwords: <br><br>
	    		For Parent Account : '. $p_pass .' <br><br>
	    		For Student Account : '. $s_pass .' <br><br> 

	    		Best Regards,<br><br>'. $acronym .' ' .strtoupper($lbl);

			email_sender($con, $lname,$fname,$p_pass,$s_userid,$s_pass,$email,$acronym,$subject,$body_val);	
			
			
		
		}else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($con);
		}

}

}

function email_sender($con, $lname,$fname,$p_pass,$s_userid,$s_pass,$email,$acronym,$subject,$body){

	//$email = 'sinaonrogeliojr@gmail.com';

    	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;  
	    $mail = new PHPMailer();                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'ghitservice2017@gmail.com';                 // SMTP username
	    $mail->Password = 'testing6!';                           // SMTP password
	    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 465;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );

	    //Recipients
	    $mail->setFrom('ghitservice2017@gmail.com', $acronym .' ONLINE REGISTRATION.' );
	    $mail->addAddress($email);     // Add a recipient
	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = $subject;
	    $mail->Body = $body;

	    $mail->AltBody = '';

	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}

}

function remove_file($con,$file_name){
	unlink($file_name);
}

function return_age($con,$bdate){

	$date = new DateTime($bdate);
	$now = new DateTime();
	$interval = $now->diff($date);
	echo $interval->y;
}

function auto_login($con,$email,$p_pass){

	$stmt = mysqli_query($con,"SELECT a.*,b.*,c.user_id,c.transid as login_id,c.user_pwd,c.category,d.grade as gradename FROM tbl_students a 
	LEFT JOIN tbl_student_picture b ON a.student_id = b.student_id
	LEFT JOIN tbl_user_account c ON a.`student_id` = c.`code`
	LEFT JOIN tbl_grade d on a.grade = d.ID where c.user_id = '$email' and c.user_pwd='$p_pass' ");

	if (mysqli_num_rows($stmt)>0) {
	
	$injec = mysqli_fetch_assoc($stmt);

	$transid = $injec['login_id'];

	save_last_log($con,$transid);

	if ($injec['user_id'] === $email && $injec['user_pwd'] === $p_pass) {
	echo 1;
	$_SESSION['id'] = $injec['student_id'];
	$_SESSION['transid'] = $injec['trans_id'];
	$_SESSION['mypic'] = $injec['image'];
	$_SESSION['lname'] = $injec['lastname'];
	$_SESSION['fname'] = $injec['firstname'];
	$_SESSION['mname'] = $injec['mi'];
	$_SESSION['fullname'] = $injec['lastname'].', '.$injec['firstname'].' '.$injec['mi'];
	$_SESSION['gender'] = $injec['gender'];
	$_SESSION['age'] = $injec['age'];
	$_SESSION['grade'] = $injec['grade'];
	$_SESSION['section'] = $injec['section'];
	$_SESSION['address'] = $injec['address'];
	$_SESSION['contact'] = $injec['contact'];
	$_SESSION['category'] = $injec['category'];
	$_SESSION['grade_name'] = $injec['gradename'];
	$_SESSION['user_name'] = $injec['user_id'];
	$_SESSION['login_id'] = $injec['login_id'];

	}
	else 
	{
		echo 0;
	}
	
	}

}

function login($con,$uname,$pwd){

	$stmt = mysqli_query($con,"SELECT a.*,b.*,c.user_id,c.transid as login_id,c.user_pwd,c.category,d.grade as gradename FROM tbl_students a 
	LEFT JOIN tbl_student_picture b ON a.student_id = b.student_id
	LEFT JOIN tbl_user_account c ON a.`student_id` = c.`code`
	left join tbl_grade d on a.grade = d.ID where c.user_id='$uname' and c.user_pwd='$pwd' ");

	$admin = mysqli_query($con,"SELECT * from tbl_user_account where user_id='$uname' and user_pwd='$pwd'");

	if (mysqli_num_rows($stmt)>0) {
	
	$injec = mysqli_fetch_assoc($stmt);
	
	$transid = $injec['login_id'];
	save_last_log($con,$transid);

	if ($injec['user_id'] === $uname && $injec['user_pwd'] === $pwd) {
	echo 1;
	$_SESSION['id'] = $injec['student_id'];
	$_SESSION['transid'] = $injec['trans_id'];
	$_SESSION['mypic'] = $injec['image'];
	$_SESSION['lname'] = $injec['lastname'];
	$_SESSION['fname'] = $injec['firstname'];
	$_SESSION['mname'] = $injec['mi'];
	$_SESSION['fullname'] = $injec['lastname'].', '.$injec['firstname'].' '.$injec['mi'];
	$_SESSION['gender'] = $injec['gender'];
	$_SESSION['age'] = $injec['age'];
	$_SESSION['grade'] = $injec['grade'];
	$_SESSION['section'] = $injec['section'];
	$_SESSION['address'] = $injec['address'];
	$_SESSION['contact'] = $injec['contact'];
	$_SESSION['category'] = $injec['category'];
	$_SESSION['grade_name'] = $injec['gradename'];
	$_SESSION['user_name'] = $injec['user_id'];
	$_SESSION['login_id'] = $injec['login_id'];
	
	}
	else 
	{
		echo 0;
	}
	}
	else if (mysqli_num_rows($admin)>0) {

		$admin_r = mysqli_fetch_assoc($admin);
		
		$transid = $admin_r['transid'];
		save_last_log($con,$transid);
		
		if ($admin_r['user_id'] === $uname && $admin_r['user_pwd'] === $pwd && $admin_r['access_form'] === 'ALL') {

			$_SESSION['admin_pane'] = $admin_r['transid'];
			$_SESSION['user_id'] = $admin_r['user_id'];
			$_SESSION['user_pwd'] = $admin_r['user_pwd'];
			echo 2;

		}

	}else{

		echo 404;
	}	
}
	
function total_fees($con,$grade_id){

	$grade_id = mysqli_real_escape_string($con, $_POST['grade_id']);

    $sql = mysqli_query($con,"SELECT sum(fee) from tbl_school_fees Where grade_id = '$grade_id' or grade_id = 0");

    if (mysqli_num_rows($sql)>0) {
        
    	$row = mysqli_fetch_assoc($sql);

    	return $row['sum(fee)'];

    }
}

function load_school_fees($con,$grade_id){

	$sql = mysqli_query($con, "SELECT * from tbl_school_fees Where grade_id = '$grade_id' or grade_id = 0 ");
	if(mysqli_num_rows($sql)){ ?>

		<thead>
			<tr class="table-default">
		        <th>Fee</th>
		        <th>Amount</th>
		    </tr>
		</thead>

		<tbody>

		<?php while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['name']; ?></td>
				<td><?php echo number_format($row['fee'],2);?></td>
			</tr>

		<?php } ?>

		</tbody>

		<tfoot>
			<tr class="table-success" style="font-weight: bolder;">
				<td>Total Fees</td>
				<td><?php echo number_format(total_fees($con,$grade_id),2); ?></td>
			</tr>
		</tfoot>

	<?php }else{
		echo '<td colspan="2">No Record Found!</td>';
   }

}

function total_payment($con,$term_id,$item,$grade_id){
	
    if ($item == 0) {
	$sql = mysqli_query($con, "SELECT sum(amount) as amount FROM tbl_terms_of_payment WHERE term_id = '$term_id' and grade_id = '$grade_id'");
    
    if (mysqli_num_rows($sql)>0) {
        
    	$row = mysqli_fetch_assoc($sql);

    	return $row['amount'];
    }

    }else if ($item == 1) {
	$sql2 =  mysqli_query($con, "SELECT amount as amount FROM tbl_terms_of_payment WHERE term_id = '$term_id' and grade_id = '$grade_id' ORDER BY id ASC LIMIT 1");
    
    if (mysqli_num_rows($sql2)>0) {
        
    	$row2 = mysqli_fetch_assoc($sql2);

    	return $row2['amount'];
    }

    }else if ($item == 2) {
	$sql =  mysqli_query($con, "SELECT amount as amount FROM tbl_terms_of_payment WHERE term_id = '$term_id' and grade_id = '$grade_id' ORDER BY id DESC LIMIT 1");
    
    if (mysqli_num_rows($sql3)>0) {
        
    	$row3 = mysqli_fetch_assoc($sql3);

    	return $row3['amount'];
    }

    }
	
    
}

function load_terms_of_payment($con,$term_id,$grade_id){

	$sql = mysqli_query($con, "SELECT * from tbl_terms_of_payment Where term_id = '$term_id' and grade_id = '$grade_id'");

	if(mysqli_num_rows($sql)){ ?>

		<thead>
			<tr class="table-default">
		        <th>Payment</th>
		        <th>Amount</th>
		    </tr>
		</thead>

		<tbody>

		<?php while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['type']; ?></td>
				<td><?php echo number_format($row['amount'],2);?></td>
			</tr>

		<?php } ?>

		</tbody>

		<tfoot>
			<tr class="table-success" style="font-weight: bolder;">
				<td>Total Fees</td>
				<td><?php echo number_format(total_payment($con,$term_id,0,$grade_id),2); ?></td>
			</tr>
		</tfoot>

	<?php }else{
		echo '<td colspan="2">No Record Found!</td>';
   }

}

function load_supporting_files($con,$student_id){

	$docs =mysqli_query($con, "SELECT * from tbl_documents where student_id = '$student_id'");

	if (mysqli_num_rows($docs)>0) {
		$num = 0; ?>
		<ul class="list-group" style="margin-top: 10px;">
				<li class="list-group-item d-flex justify-content-between list-group-item-info">
					<b> SUPPORTING DOCUMENTS AND CREDENTIALS</b>
				</li>
		<?php while ($d = mysqli_fetch_assoc($docs))  { $num++; ?>
			
			
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<b><a href="<?php echo $d['url'] ?>" target="_blank" title="View file"><?php echo strtoupper($d['name']); ?> </a>
					</b>
					<b>
						<a download="" href="<?php echo '../'.$d['url'] ?>" target="_blank" title="Download file"><span class="fa fa-download"></span> 
						</a>
						<a href="#" onclick="delete_doc('<?php echo $d['id'] ?>','<?php echo $d['url'] ?>')" title="Remove file"><span class="fa fa-trash"></span> </a>
					</b>
				</li>

		<?php } ?>
		</ul>
	<?php }

}


function delete_doc($con,$id,$path){

	$sql = mysqli_query($con, "DELETE FROM tbl_documents where id = '$id'");
	if ($sql) {
		unlink($path);
		echo 1;
	}

}

function save_member($con,$grade_id,$member,$student_id){

	$chk = mysqli_query($con, "SELECT * from tbl_households_members where grade_id = '$grade_id' and student_id = '$student_id'");

	if(mysqli_num_rows($chk) > 0){

		echo 2;

	}else{

		$sql = mysqli_query($con, "INSERT INTO tbl_households_members (student_id,grade_id,members) VALUES ('$student_id','$grade_id','$member')");

		if($sql){
			echo 1;
		}

	}

}

function load_household_members($con,$student_id,$grade_id){

	$sql = mysqli_query($con, "SELECT t1.*,t2.grade FROM tbl_households_members t1
				LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.ID where t1.student_id = '$student_id'");

	if (mysqli_num_rows($sql)>0) {
	?>

		<?php while ($row = mysqli_fetch_assoc($sql))  {  ?>

		  <li class="list-group-item d-flex justify-content-between align-items-left">

		    <?php echo $row['grade'] . ' = ' . $row['members']; ?>
		    <span class="fa fa-trash fa-2x w3-hover" onclick="remove_member('<?php echo $row['id'] ?>')"></span>
		  </li>		 
		
		<?php }
	 }

}

function remove_member($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_households_members where id = '$id'");
	if ($sql) {
		echo 1;
	}

}

//Manage Scholastic
function save_scholastic($con,$grade_id,$scholastic_value,$student_id,$school_year_val){

	$sql = mysqli_query($con, "INSERT INTO tbl_scholastic_information (student_id,grade_id,school_name,school_year) VALUES ('$student_id','$grade_id','$scholastic_value','$school_year_val')");

	if($sql){
		echo 1;
	}

}

function load_scholastics($con,$student_id,$grade_id){

	$sql = mysqli_query($con, "SELECT t1.*,t2.grade,t3.`school_year` as sy FROM tbl_scholastic_information t1
	LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.ID
	LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` where t1.student_id = '$student_id'");

	if (mysqli_num_rows($sql)>0) {
	?>

		<?php while ($row = mysqli_fetch_assoc($sql))  {  ?>

		  <li class="list-group-item d-flex justify-content-between align-items-left">

		    <?php echo $row['grade'] . ' = ' . $row['school_name']. ' ' .'('.$row['sy'].')'; ?>
		    <span class="fa fa-trash fa-2x w3-hover" onclick="remove_scholastic('<?php echo $row['id'] ?>')"></span>
		  </li>		 
		
		<?php }
	 }

}

function remove_scholastic($con,$id){

	$sql = mysqli_query($con, "DELETE FROM tbl_scholastic_information where id = '$id'");
	if ($sql) {
		echo 1;
	}

}


?>